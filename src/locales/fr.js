export const fr = {
  'FORM': {
    'TOP_POSTS': {
      'FORM_HEADER': 'Forme :',
      'ETYMOLOGY_HEADER': 'Étymologie :',
      'DESKTOP': {
        'FORM_HEADER': 'Discussion sur la forme',
        'ETYMOLOGY_HEADER': "Discussion sur l'étymologie",
      }
    },
    'SEE_ALSO': {
      'TITLE': 'Voir aussi',
      'RELATED': 'Vocabulaire apparenté',
      'FORM_VARIATION': 'Variante graphique',
      'DERIVED': 'Mots dérivés'
    },
    'NO_RESULTS': 'Pas de résultats pour cette entrée',
    'CLOSE_FORMS_SUGGESTIONS': 'SUGGESTIONS DE RECHERCHE :',
    'DEFINITION': 'Définition',
    'DESKTOP': {
      'RESEARCHED_FORM': 'Terme recherché : ',
    }
  },
  'LEXICAL_SENSE_DETAIL': {
    'EXAMPLE_LABEL': 'Exemple',
    'ABOUT_SENSE_TITLE': 'Sens et usages',
    'BACK_TO_FORM_LINK': 'Retour à la liste des définitions',
    'BACK_TO_SENSE_LINK': 'Retour à la définition',
    'EDIT_THIS_DEFINITION': 'Modifier cette définition',
    'NO_RESULTS': "Cette définition n'existe pas",
    'ACTIONS': {
      'LIKE': 'Je valide !',
      'UNLIKE': "J'annule ma validation...",
      'ASK_REMOVAL': "J'invalide !",
      'UNASK_REMOVAL': "J'annule mon invalidation...",
      'REPORT': 'Je signale !',
      'UNREPORT': 'Je retire mon signalement',
      'PROCESS': 'Marquer comme traité',
      'UNPROCESS': 'Marquer comme "non traité"',
      'REMOVE': 'Supprimer',
      'REMOVE_ARE_YOU_SURE': 'Vous êtes sur le point de supprimer une définition, êtes-vous sûr ?',
      'LEXICAL_SENSE_REMOVE_SUCCESS': 'La définition a été supprimée.'
    },
    'DESKTOP': {
      'LIKE_COUNT': '{{count}} personne a validé cette définition',
      'LIKE_COUNT_plural': '{{count}} personnes\nont validé cette définition',
      'LIKE_COUNT_zero': "Personne n'a validé cette définition",
      'SOURCE': 'Source : {{source}}',
      'TOP_POSTS': {
        'SENSE_HEADER': "Discussion sur l'usage"
      },
      'DEFINITION': 'Définition',
      'SOURCES': 'Sources',
      'GEOGRAPHICAL_DETAILS': 'Précisions géographiques',
    }
  },
  'INDEX': {
    'SEARCH_PLACEHOLDER': 'Écrivez le terme recherché ici',
    'GEOLOC_PLACEHOLDER': 'Indiquer un lieu',
    'AUTO_GEOLOC': 'Utiliser ma position actuelle',
    'FOOTER': "Comprendre et partager \n les mots avec plusieurs dictionnaires \n du français parlé partout dans le monde. \n Participer à l'enrichissement collectif",
    'SEARCH_BUTTON': "Rechercher"
  }, 
  'GEOLOC': {
    'ERRORS': {
      'PERMISSION_DENIED': "Nous n'avons pas la permission d'accéder à votre localisation",
      'UNKNOWN': "Un problème nous empêche de vous géolocaliser"

    }
  },
  'FOOTER': {
    'IMPROVE_THE_DDF': 'Enrichir le'
  },
  'MOBILE_MENU': {
    'PRESENTATION': 'Présentation du projet',
    'GCU': 'Mentions légales',
    'PARTNERS': 'Partenaires',
    'CREDITS': 'Crédits',
    'HELP': 'Aide et documentation',
    'MY_ACCOUNT': 'Mon compte',
    'MY_ACCOUNT_CONTRIBUTIONS': 'Les dernières contributions',
    'LOGIN': 'Se connecter',
    'LOGOUT': 'Se déconnecter',
    'SUBSCRIBE': "S'inscrire",
    'ADMIN': {
      'USERS': "Gestion des utilisateurs"
    },
    'OPERATORS': {
      'CONTRIBUTIONS': "Gestion des contributions"
    }
  },
  'GEOGRAPHICAL_DETAILS': {
    'NO_COUNTRY_LABEL': 'Monde',
    'TITLE': 'Précisions géographiques',
    'PAGE_CAPTION': "D'après les données collectées, ce sens est un usage notamment dans les régions suivantes : ",
    'NO_RESULTS': "Pas d'information géographique pour cette définition"
  },
  'SOURCES_PAGE': {
    'TITLE': 'Sources',
    'WIKTIONARY_LINK': 'Historique des versions de «\u202F{{formQuery}}\u202F»'
  },
  'HELP':  {
    'HEADER': "Aide et documentation",
    'HOW_TO_SEARCH': "Effectuer une recherche simple",
    'SECTION_LINKS': {
      'SEARCH_AND_USER_ACCOUNT': "Recherche\net compte utilisateur",
      'UNDERSTAND_ARTICLE': "Comprendre un article\n de dictionnaire",
      'HOW_TO_CONTRIBUTE': "Contribution \n et enrichissement",
      'OFFLINE': "Faire vivre le DDF\nhors des écrans"
    },
    'DESKTOP': {
      'BACK_LINK': 'Retour au menu'
    }
  },
  'DESKTOP_HEADER': {
    'TITLE': 'Dictionnaire\ndes francophones',
    'SEARCH_BUTTON': "Rechercher",
    'SEARCH_PLACEHOLDER': 'Écrivez le terme recherché ici',
    'TOOLTIPS': {
      'HELP': 'Aide',
      'MY_ACCOUNT': 'Mon compte',
      'LOGIN': 'Se connecter',
      'ADMIN': "Panneau d'administration"
    }
  },
  'DESKTOP_FOOTER': {
    'PRESENTATION': 'Présentation du projet',
    'ABOUT': 'À propos',
    'GCU': 'Mentions légales', 
    'PARTNERS': 'Partenaires', 
    'CREDITS': 'Crédits', 
    'VERSION_HISTORY': 'Historique des versions',
    'OUR_PARTNERS': 'En partenariat avec : '
  },
  'GCU_PAGE_TITLE': 'Mentions légales',
  'PARTNERS_PAGE_TITLE': 'Partenaires',
  'CREDITS_PAGE_TITLE': 'Crédits',
  'PROJECT_PRESENTATION_PAGE_TITLE': 'Présentation du projet',
  'LOGIN_PAGE': {
    'TITLE': "S'identifier",
    'IF_YOU_ALREADY_HAVE_AN_ACCOUNT': 'Si vous avez déjà un compte, identifiez vous :',
    'EMAIL': "Adresse de courriel",
    'PASSWORD': 'Mot de passe',
    'CREATE_ACCOUNT_INFOTEXT': "Sinon, vous pouvez <1>créer un compte</1>.",
    'LOST_CREDENTIALS_INFOTEXT': 'Vous ne retrouvez plus votre mot de passe ?',
    'LOST_CREDENTIALS_LINK_TEXT': "Procédure de récupération de compte à partir d'une adresse courriel",
    'SUBMIT': 'Valider'
  },
  'PASSWORD_FORGOTTEN_PAGE': {
    'TITLE': "Mot de passe oublié",
    'PROCEDURE': "Pour réinitialiser le mot de passe, merci de renseigner l'adresse mail utilisée pour vous connecter.",
    'EMAIL': "Adresse de courriel",
    'NEXT_STEP': "Une fois ce formulaire validé, vous recevrez un mail à l'adresse indiquée contenant un lien de réinitialisation ede votre mot de passe.",
    'SUBMIT': 'Valider',
    'MAIL_SENT_SUCCESS': 'Votre demande a bien été prise en compte, vérifiez votre messagerie pour réinitialiser votre mot de passe.'
  },
  'SUBSCRIBE_PAGE': {
    'TITLE': "S'inscrire",
    'SECOND_LEVEL_TITLE': "Formulaire d'inscription",
    'TOP_INFOBOX_CONTENT': "Créer un compte permet d'affiner votre navigation dans le Dictionnaire des francophones, et de retrouver les apports que vous y faites." +
      "\nNous invitons les utilisateurs à ne pas communiquer de données personnelles dans les champs suivants s'ils souhaitent garder l'anonymat en utilisant le " +
      "Dictionnaire des francophones, notamment en préférant l'usage d'un pseudonyme ne mentionnant pas leur nom et/ou prénom.",
    'GEOLOC_DISCLAIMER': "Remplir le champ \"Lieu de vie\" vous permettra de pré remplir cette information lors de vos prochaines visites. " + 
      "La géolocalisation proposée sur ce site est imprécise, pour des raisons de confidentialité. Vous ne pouvez sélectionner dans la liste des choix " + 
      "proposés que des villes, arrondissements, communes, régions ou pays. Il est notamment impossible d'indiquer dans ce champ une adresse complète. Vous " +
      "pourrez modifier ces informations ultérieurement à partir de l'onglet \"mon profil\". Cette information ne sera pas accessible au public mais " + 
      "seulement à l'équipe en charge de l'animation éditoriale du Dictionnaire des francophones.",
    'GCU_CHECKBOX_LABEL': "J'accepte les <1>conditions générales d'utilisation</1>.",
    'MANDATORY_FIELDS': '* Champs obligatoires',
    'SUBMIT': 'Valider',
    'CANCEL': 'Annuler'
  },
  'MY_ACCOUNT': {
    'TITLE': 'Mon compte',
    'MY_PROFILE_LINK': 'Mon profil',
    'MY_CONTRIBUTIONS_LINK': 'Mes contributions',
    'DESKTOP_MENU_TITLE': 'CATÉGORIES',
    'PROFILE': {
      'INDEX': {
        'THIS_IS_YOUR_PROFILE': 'Voici les informations de votre profil :',
        'EDIT_PROFILE': 'Compléter mon profil',
        'EDIT_SETTINGS': 'Modifier mes préférences',
        'LOGOUT': 'Se déconnecter',
      },
      'EDIT': {
        'TITLE': 'Modifier mon profil',
        'YOU_CAN_EDIT_THESE_FIELDS': "Si vous le souhaitez, vous pouvez ajouter les informations suivantes sur votre profil. Ces informations ne sont pas " + 
          "accessibles par le public mais permettront d'améliorer les travaux scientifiques ou de recherche. Elles permettront par exemple de mener des analyses " + 
          "ou des études sociolinguistiques sur les contributions du Dictionnaire des francophones. Ces données pourront être communiquées à des partenaires tiers " + 
          "à des fins exclusivement scientifiques et/ou de recherche ce qui nécessite le recueil de votre consentement au préalable.\nAvertissement : nous invitons " + 
          "les utilisateurs à ne pas communiquer de données personnelles ni d'opinions politiques ou religieuses dans les champs proposés ci-dessous.",
        'SUBMIT': 'Valider',
        'PLACEHOLDERS': {
          'PLACE': 'Lieu de vie',
          'GENDER': 'Genre (masculin, féminin, autre)',
          'BIRTH_YEAR': 'Année de naissance',
          'LINGUISTIC_PROFILE': 'Profil linguistique',
          'SPOKEN_LANGUAGES' : 'Autres langues parlées',
          'OTHER_INFORMATION' : "Champ d'expertise"
        },
        'SUCCESS': "Profil mis à jour avec succès"
      },
      'SETTINGS': {
        'TITLE': 'Modifier mes préférences',
        'CURRENT': 'Actuellement : ',
        'MODIFY': 'Modifier',
        'SUBMIT': 'Valider',
        'CANCEL': 'Annuler',
        'PLACE': {
          'TITLE': 'Lieu',
          'HINT': "Vous pouvez modifier ici la zone d'usage à privilégier dans les recherches",
          'EMPTY': "(aucun lieu)"
        },
        'PREFERRED_DOMAIN': {
          'TITLE': "Domaine du vocabulaire",
          'HINT': "Vous pouvez spécifier un domaine de préférence pour vos recherches, tel que biologie,  linguistique, jeux vidéos…",
          'EMPTY': "(aucune préférence)"
        },
        'PASSWORD_EDIT': {
          'TITLE': "Modifier mon mot de passe",
          'TOP_CAPTION': "Vous souhaitez modifier le mot de passe de votre compte :",
          'OLD_PASSWORD_CAPTION': "Saisissez l'ancien mot de passe",
          'OLD_PASSWORD_PLACEHOLDER': "Mot de passe",
          'NEW_PASSWORD_CAPTION': "Nouveau mot de passe",
          'NEW_PASSWORD_PLACEHOLDER': "Mot de passe",
          'PASSWORD_CONFIRMATION_CAPTION': 'Confirmation',
          'PASSWORD_CONFIRMATION_PLACEHOLDER': "Mot de passe",
          'CANCEL': 'Annuler',
          'SUBMIT': 'Valider',
          'SUCCESS': "Mot de passe mis à jour"
        },
        'ACCOUNT_DELETION': {
          'TITLE': "Supprimer mon compte",
          'TOP_CAPTION': "Attention cette action est définitive. La suppression de votre compte n'effacera pas les apports et discussions passées.",
          'CANCEL': 'Annuler',
          'SUBMIT': 'Valider',
          'SUCCESS': "Le compte a été supprimé",
          'ARE_YOU_SURE': 'Vous êtes sur le point de supprimer votre compte, êtes vous sûr ?'
        }
      }
    },
    'CONTRIBUTIONS':{
      'TITLE': "Mes contributions"
    }
  },
  'FORM_ERRORS': {
    'FIELD_ERRORS': {
      'REQUIRED': 'Champ obligatoire',
      'INVALID_EMAIL': "Format de l'adresse de courriel invalide",
      'PASSWORD_TOO_SHORT': 'Mot de passe trop court',
      'EMAIL_ALREADY_REGISTERED': 'Cette adresse de courriel est déjà utilisée',
      'PASSWORDS_DO_NOT_MATCH': 'La confirmation ne correspond pas au nouveau mot de passe',
      'WRONG_OLD_PASSWORD': "L'ancien mot de passe est incorrect",
      'GCU_MUST_BE_ACCEPTED': "Vous devez accepter les conditions générales"
    },
    'GENERAL_ERRORS': {
      'FORM_VALIDATION_ERROR': 'Certains champs sont invalides',
      'UNEXPECTED_ERROR': "Un problème non identifié nous empêche d'effectuer cette action. Réessayez plus tard",
      'INVALID_CREDENTIALS': 'Ces identifiants ne sont pas valides',
      'USER_MUST_BE_AUTHENTICATED': 'Vous devez être connecté avec un compte utilisateur pour effectuer cette action',
      'USER_NOT_ALLOWED': "Vous n'avez pas la permission d'effectuer cette action"
    }
  },
  'CONFIRMATION_MODAL': {
    'YES': 'Oui',
    'NO': 'Non'
  },
  'ADMIN': {
    'USERS': {
      'ACTIONS': {
        'UNREGISTER_ACCOUNT': 'Supprimer le compte',
        'DISABLE_ACCOUNT': 'Bloquer le compte',
        'ENABLE_ACCOUNT': 'Débloquer le compte'
      },
      'VALIDATION': {
        'UNREGISTER_ACCOUNT': 'Vous êtes sur le point de supprimer un compte, êtes vous sûr ?'
      },
      'IS_UNREGISTERED': "Ce compte a été supprimé"
    },
    'INDEX': {
      'TITLE': 'Administration'
    }
  },
  'CONTRIBUTION': {
    'CREATE_LEXICAL_SENSE': {
      'TITLE': 'Ajouter une définition et un mot',
      'FORM_PLACEHOLDER': 'Mot ou expression',
      'POS_PLACEHOLDER': 'Catégorie grammaticale',
      'DEFINITION_PLACEHOLDER': 'Définition',
      'CANCEL': 'Annuler',
      'NEXT': 'Continuer',
      'SUBMIT': 'Publier'
    },
    'EDIT_LEXICAL_SENSE': {
      'TITLE': 'Modifier une définition'
    },
    'DESKTOP_TITLE': 'Enrichir le Dictionnaire des Francophones'
  },
  'CONTRIBUTIONS_LIST': {
    'FILTERS': {
      'ADD_FILTERS_BUTTON': 'Ajouter des filtres',
      'EDIT_FILTERS_BUTTON': 'Modifier les filtres',
      'SAVE': 'Enregistrer',
      'CANCEL': 'Annuler',
      'RADIO_DISABLED_OPTION_LABEL': 'Ne pas filtrer'
    },
    'TABLE': {
      'HEADERS': {
        'DATE': 'Date',
        'FORM': 'Forme',
        'DEFINITION': 'Définition',
        'PERSON': 'Personne',
        'STATUS': 'État',
        'REPORTINGS': 'Sign.',
        'REPORTINGS_TOOLTIP': 'Signalements',
        'ACTIONS': 'Actions'
      },
      'NO_RESULTS': 'Aucun résultat',
      'ERROR_RESULTS': "Un problème inattendu nous empêche d'afficher les résultats",
      'SEE_NEXT': "Voir les contributions suivantes"
    }
  },
  'DOCUMENT_TITLES': {
    'DEFAULT': 'Dictionnaire des francophones',
    'GENERIC': '{{title}} - Dictionnaire des francophones',
    'FORM_SEARCH': 'Recherche «\u202F{{formQuery}}\u202F» - Dictionnaire des francophones',
    'LEXICAL_SENSE': 'Définition «\u202F{{formQuery}}\u202F» - Dictionnaire des francophones',
    'CREATE_LEXICAL_SENSE': 'Ajouter une définition - Dictionnaire des francophones',
    'EDIT_LEXICAL_SENSE': 'Modifier la définition «\u202F{{formWrittenRep}}\u202F» - Dictionnaire des francophones',
    'HELP': 'Aide - Dictionnaire des francophones',
    'SUBSCRIBE': 'Inscription - Dictionnaire des francophones',
    'LOGIN': 'Connexion - Dictionnaire des francophones',
    'MY_ACCOUNT': 'Mon compte - Dictonnaire des francophones',
    'ADMIN_INDEX': 'Admin - Dictonnaire des francophones',
    'ADMIN_CONTRIBUTIONS': 'Contributions - Admin - Dictonnaire des francophones',
    'ADMIN_USERS': 'Utilisateurs - Admin - Dictonnaire des francophones'
  },
  'AUTOCOMPLETE': {
    'NO_RESULTS': 'Pas de résultats'
  }
};
