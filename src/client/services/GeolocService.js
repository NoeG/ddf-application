import {
  Observable, 
  ReplaySubject, 
  Subject, 
  from as observableFrom, 
  EMPTY, 
  of as observableOf,
  merge,
  race
} from 'rxjs';
import {
  shareReplay,
  refCount, 
  switchMap, 
  skip, 
  take, 
  tap, 
  map, 
  catchError, 
  concat, 
  debounceTime,
  delay
} from 'rxjs/operators';
import gql from 'graphql-tag';

import {getDDFApolloClient} from './DDFApolloClient';
import {isValidPlace} from '../utilities/helpers/countries';
import {gqlPlaceFragment} from '../utilities/helpers/places';
import {GenericFuzzySearcher} from './GenericFuzzySearcher';
import {getUserAuthenticationService} from './UserAuthenticationService';


/**
 *
 * A service providing logic around login functionality.
 *
 * # Methods : 
 *
 * ## setManualPlace(place)
 *
 * Sets manually a place, that is immediatly emitted into the stream of current places (geolocService.currentPlace).
 * @param {Place} place - The place object. No check is performed on the given object, so you must insure it is a valid Place object.
 *
 *
 *
 * ## setAutoPlace()
 *
 * Sets the place detected by the browser geolocation API into the stream of selected places. Because the detection
 * of the geolocated place is asynchronous, it returns an observable to know the status of the request.
 *
 * If another place is set in the stream of current places before the request for the auto place finishes, the request
 * is aborted and the automatically detected place is not set in the stream.
 *
 *
 * @return {Observable<Place>} In case of success, it emits the Place object that is set to the current places stream, then completes. 
 *                             In case of error, it can return one of the following values : 
 *
 *                             - { status: 'aborted' } : this error object is returned if another place was set to the current places stream before
 *                                                       the request for getting the auto place was finished
 *                             - { status: 'not_found'} : this error object is returned if the geonames database could not find a place related to 
 *                                                        the coordinates given by the browser
 *                             - { status: 'browser_geolocation_error', error: <PositionError> } :
 *                             - if the browser geolocation API returned an error, the error is forwarded as is. See the navigator.geolocation
 *                               spec for details. Most common case is that the user denied the autorisation to geolocate on its device,
 *                               in which case the error object is `{code: 1}`
 *                             - {
 *                                  graphQLErrors: <any>, 
 *                                  networkErrors: <any>
 *                               }  : in case of an error during the apollo graphQL request, it is returned as is.
 *                             - <any>          : any unexpected error occuring during the pipeline of observables will be forwarded
 *        
 *
 *
 * ## getPlaceSearcher(opts)
 *
 * @param {Object} opts
 * @param {Number} opts.defaultFirst - The default value of the number of places to return at each request. Defaults to 10 if not provided.
 * @return {PlaceSearcher}
 *
 * Gives a new instance of PlaceSearcher. An object used to search a list of places, and debouncing several requests in short time frame,
 * and make sure the results of several network requests come back in order. This is intended to be used by an autocompletion widget for
 * looking for places. See the description of the PlaceSearcher object.
 *
 *
 *
 *
 * ## initialPlace()
 *
 * @return {Observable<Place>}
 * Gives the initial place. Either the last known place, that was stored on local storage. Otherwise detect place from browser location, or
 * use a default value as last fallback. The observable emits one place then completes.
 *
 *
 *
 *
 * ## placeFromCoordinates(opts)
 *
 * @param {Object} opts      - The coordinates to get a place for
 * @param {number} opts.lat
 * @param {number} opts.lng
 * @return {Observable<Place>}
 *
 * Makes a graphQL request to get a Place object for the given coordinates. In case of success, it emits the place then completes.
 * In case of error : 
 * - If no place was found (an empty result was returned), the observable errors with the value {status: 'not_found'}
 * - In case of an unexpected error (probably an apolloError, it is forwarded)
 *
 *
 *
 *
 * ## searchPlaces(qs, opts)
 *
 * @param {String} qs - The query string to search places for
 * @param {Object} opts
 * @param {Number} opts.first - The maximum number of places to return in the request. If not specified, default to 10,
 * 
 * @return {Observable<Array<Place>>} - Returns an observable that will emit an array of places, result of the query, and complete. The 
 *                                      array might be empty. In case of error, it will throw the error given by the apollo client.
 *
 *
 * # Properties : 
 *
 * ##currentPlace 
 *
 * @type {Observable<Place>} An observable that provides the current place the user is localised at. On subscription it immediatly emits the 
 *                           current place (except if the initial place has not been resolved yet). Then it emits each time the place is changed
 *                           via the GeolocService methods. The current place stream begins with the place returned by GeolocService.initialPlace().
 *                           If the UserAuthenticationService is provided, the current place is also updated on login of a user which has a place
 *                           defined in its user settings.
 *
 *
 *
 *
 *
 * # Other
 *
 * ## PlaceSearcher
 *
 * ## constructor(opts)
 *
 * @param {Object} opts
 * @param {Number} opts.defaultFirst - The default value of the number of places to return at each request. Defaults to 10 if not provided.
 *
 *
 *
 * ### Methods
 *
 * #### search(qs, opts)
 *
 * @param {String} qs - The query string to search places
 * @param {Object} opts
 * @param {Number} opts.first - The maximum number of places to return in the request. If not specified, default to the number given on construction
 *                              of the PlaceSearcher object.
 *
 * ### clear()
 * 
 * Update results with an empty array and cancel any pending request.
 *
 * 
 *
 * ### Properties
 *
 * #### results 
 * @type {Observable<Array<Place>>}
 *  
 *
 *
 * # Internal
 *
 * ## browserLocationObservable 
 *
 * @type {Observable<Position>}   
 * An observable that provides the browser's detected position and then completes. Wrapper around the navigator.geolocation.getCurrentPosition() API.
 * The observable throws the PositionError object returned by the API if need be.
 *
 *
 *
 * # Types
 *
 * ## Place
 * @typedef {Object} Place
 * @property {String} id
 * @property {'City'|'State'|'Country'} __typename 
 * @property {String} countryCode
 * @property {String} countryId - Applies only for places of type City or State
 * @property {String} countryName - Applies only for places of type City or State
 * @property {String} stateName - Applies only for places of type City 
 */

const LOCAL_STORAGE_CURRENT_PLACE_KEY = 'GeolocServiceCurrentPlace';

export const PlaceFromCoordinatesQuery = gql`
  query PlaceFormCoordinates_Query($lat: Float, $lng: Float) {
    placeByGeoCoords(lat: $lat, lng: $lng) {
      ...PlaceFragment
    }
  }

  ${gqlPlaceFragment}
`;

export const SearchPlacesQuery = gql`
  query SearchPlaces($qs: String, $first: Int) {
    places(qs: $qs, first: $first) {
      edges {
        node {
          id
          ...PlaceFragment
        }
      }
    }
  }
  ${gqlPlaceFragment}
`;

export class GeolocService {

  /**
   * @param {Object} options.apolloClient
   * @param {Object} options.userAuthenticationService [optional] If provided, this service is used in order to dected the login
   *                                                   of a user. When the user logs in, the current place is updated with its predefined place
   *                                                   from the user's settings.
   */
  constructor({apolloClient, userAuthenticationService} = {}) {
    /** Setup dependent services */
    this.apolloClient = apolloClient || getDDFApolloClient();
    this.userAuthenticationService = userAuthenticationService;

    /** _browserLocationObservable **/
    this._browserLocationObservable = new Observable((observer) => {
      navigator.geolocation.getCurrentPosition((position) => {
        observer.next(position);
        observer.complete();
      }, (positionError) => {
        observer.error({
          status: 'browser_geolocation_error', 
          error: positionError
        });
      }, {
        timeout: 5000
      });
    }).pipe(
      shareReplay(1),
    );


    /** _placeFromBrowserLocation **/
    this._placeFromBrowserLocation = this._browserLocationObservable.pipe(
      switchMap(position => this.placeFromCoordinates({
        lat: position.coords.latitude,
        lng: position.coords.longitude
      }))
    )

    this._currentPlaceSubject = new ReplaySubject(1);
    this.initialPlace().subscribe(initialPlace => this._currentPlaceSubject.next(initialPlace));

    /**
     * Store current place in local storage
     */
    this._currentPlaceSubject.subscribe({
      next: currentPlace => localStorage.setItem(LOCAL_STORAGE_CURRENT_PLACE_KEY, JSON.stringify(currentPlace))
    });


    /**
     * Subscribe to current user observable, in order to update current place if necessary 
     */
    if (this.userAuthenticationService) {
      this.userAuthenticationService.currentUser.subscribe(currentUser => {
        if (currentUser.isLogged && currentUser?.user?.place) {
          this.setManualPlace(currentUser.user.place); 
        }
      });
    }
  }

  /* 
   * Use localStorage to store the last place known. Fallback strategy, localize at France
   */
  initialPlace() {
    let place = JSON.parse(localStorage.getItem(LOCAL_STORAGE_CURRENT_PLACE_KEY));
    if (!isValidPlace(place)) {
      /* Default place is France */
      place = {
        id: "geonames:3017382",
        __typename: "Country",
        name: "France",
        countryCode: "FR"
      };
    }
    return observableOf(place);
  }

  get currentPlace() {
    return this._currentPlaceSubject;
  }

  /**
   * Makes a graphQL request to get the place object that matches the most the given coordinates.
   *
   * Returns an observable that emits the place object and completes. In case of error from the apollo client,
   * the observable will throw the error as emitted by the apollo client query() method
   */
  placeFromCoordinates({lat, lng}) {
    return observableFrom(this.apolloClient.query({
      query: PlaceFromCoordinatesQuery,
      variables: {lat, lng}
    })).pipe(
      map((queryResult) => {
        if (queryResult.data.placeByGeoCoords) {
          return queryResult.data.placeByGeoCoords
        } else {
          throw {status: 'not_found', inputOptions: {lat, lng}, gqlQueryResult: queryResult};
        }
      })
    );
  }

  setAutoPlace() {
    let returnedObservable = new ReplaySubject(1);

    /* 
     * we setup a race between the observable currentPlace and the request for an automatic geolocation. If a new currentPlace is 
     * set (most likely from a call to setManualPlace()) before the geolocation requests finishes, we abort the latter 
     */
    race(
      this._placeFromBrowserLocation,
      this.currentPlace.pipe(
        skip(1), /* first element is current location replayed on any subscription */
        take(1), /* if second element occurs, it means a new place was set before the auto geoloc requests finished */
        map(() => {
          /* we transform this element into the error to be thrown for this case */
          throw {status: 'aborted'};
        })
      )
    ).pipe(
      take(1), /* just to be safe, but the observable is expected to emit only one value anyway */
      tap((place) => {
        /* notify the observable returned by setAutoPlace() */
        returnedObservable.next(place);
        returnedObservable.complete();
      }),
      catchError((err) => {
        /* follow the error to the observable returned by setAutoPlace(), and silence it in this observable chain */
        returnedObservable.error(err);
        console.warn('GeolocService.setAutoPlace() : Could not set the place automatically', err);
        return EMPTY;
      })
    ).subscribe((place) => {
      this._currentPlaceSubject.next(place); 
    }); 

    return returnedObservable;
  }


  setManualPlace(place) {
    this._currentPlaceSubject.next(place);
  }

  searchPlaces(qs, {first: _first} = {}) {
    let first = _first || 10;
    return observableFrom(this.apolloClient.query({
      query: SearchPlacesQuery,
      variables: {qs, first}
    })).pipe(
      map(queryResult => queryResult.data.places.edges.map(edge => edge.node)),
    );
  }

  getPlaceSearcher({defaultFirst}) {
    return new PlaceSearcher({geolocService: this, defaultFirst});
  }
}

let geolocService;
export function getGeolocService() { 
  return geolocService || (geolocService = new GeolocService({
    userAuthenticationService: getUserAuthenticationService()
  }));
};



class PlaceSearcher extends GenericFuzzySearcher {
  constructor({
    geolocService,
    defaultFirst  
  } = {}) {
    super({defaultFirst});

    /* Setup geoloc service */
    this.geolocService = geolocService || getGeolocService();
  }


  performSearch(qs, {first}) {
    return this.geolocService.searchPlaces(qs, {first});
  }
}


