import {NEVER, EMPTY} from 'rxjs';


export class GeolocServiceMock  {
  constructor() {
    this.initialPlace = jest.fn(() => EMPTY);
    this.currentPlace = NEVER;
    this.placeFromCoordinates = jest.fn(() => EMPTY);
    this.setAutoPlace = jest.fn(() => EMPTY);
    this.setManualPlace = jest.fn();
    this.searchPlaces = jest.fn(() => EMPTY);
    this.getPlaceSearcher = jest.fn(() => new PlaceSearcherMock());
  }
}

export class PlaceSearcherMock {
  constructor() {
    this.search = jest.fn();
    this.clear = jest.fn();
    this.results = NEVER;
  }
}
