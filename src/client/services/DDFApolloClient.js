import ApolloClient from "apollo-boost";
import {InMemoryCache, IntrospectionFragmentMatcher} from 'apollo-cache-inmemory';
import {FragmentTypes} from '../../generated/FragmentTypes';

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: FragmentTypes
});

const cache = new InMemoryCache({fragmentMatcher});

const DDFApolloClient = new ApolloClient({
  uri: "/graphql",
  cache
});

export function getDDFApolloClient() {
  if (process.env.NODE_ENV === 'test') {
    let miniStackTrace = (new Error()).stack.split('\n').splice(1,5).join('\n');
    console.warn("You are using the default apollo client DDFApolloClient in a test environment. This probably won't work " +
      "because the default apollo client hasn't access to the graphQL mocks (as provided by apollo MockedProvider). You should either mock " +
      "the service that use the apollo client in your test suite, or inject this service with a mocked apollo client such as the one " +
      "defined at /jest/utilities/MockedApolloClient.js\n\n" +
      miniStackTrace
    );
  }
  return DDFApolloClient;
}
