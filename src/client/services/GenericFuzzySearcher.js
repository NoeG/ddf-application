import {
  Subject, 
  of as observableOf,
  merge,
} from 'rxjs';
import {
  switchMap, 
  tap, 
  map, 
  debounceTime,
  delay
} from 'rxjs/operators';

/**
 * This is an abstract class, do not use directly.
 *
 * This class is a foundation for a FuzzySearcher service, initially intended to be used in order to control an autocomplete widget.
 *
 * The service interface is the following : 
 *
 * - FuzzySearcher.search(queryString, {first: 10})   : launch a search with the given query string, returning max 'first' results (optionnal)
 * - FuzzySearcher.clear()  : clear search results
 * - FuzzySeacher.results : an observable returning the results
 *
 *
 * In order to implement your FuzzySearcher, you must extend this class, and implement the method FuzzySearcher.performSearch(queryString, {first}) 
 * which must return an Observable, which data will be directly forwarded to the results observable (ideally you want to return a list of object, 
 * that is the result of your fuzzy search).
 *
 * For an example of implementation, see the class PlaceSearcher implemented in 'GeolocService.js'
 *
 */
export class GenericFuzzySearcher {
  constructor({
    defaultFirst  
  } = {}) {
    /* Setup this.defaultFirst */
    this.defaultFirst = defaultFirst || 10;

    /* Stream of object, representing the current for places search. 
     * The objects are in the following format : 
     *
     * {
     *    qs: <string>,
     *    first: <number>
     * }
     *
     * qs : the string query
     * first : the maximum number of results to return
     */
    this._queryStringSubject = new Subject();

    /* Stream of events (value is not important) that request a clearing of the result array */
    this._clearEventSubject = new Subject();
  }

  search(qs, {first} = {}) {
    this._queryStringSubject.next({qs, first});
  }

  clear() {
    this._clearEventSubject.next(1);
  }

  performSearch() {
    throw new Error('Must be implemented');
  }

  get results() {
    let qsObservable = this._queryStringSubject.pipe(
      debounceTime(200),
      map((obj) => {return {event: 'search', value: obj}})
    );
    let clearEvtObservable = this._clearEventSubject.pipe(
      map(() => {return {event: 'clear'}})
    );

    let resultsObservable = merge(qsObservable, clearEvtObservable).pipe(
      switchMap((val) => {
        if (val.event == 'clear') {
          return observableOf([]);
        } else if (val.event == 'search') {
          let {qs, first: _first} = val.value;
          let first = _first || this.defaultFirst;
          return this.performSearch(qs, {first});
        }
      })
    );

    return resultsObservable;
  }
}
