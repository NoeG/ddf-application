import {GeolocService, PlaceFromCoordinatesQuery, SearchPlacesQuery, getPlaceCountryId} from '../GeolocService';
import {skip, take, takeUntil, tap, map} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs';
import {timer} from 'rxjs';

const mockGeolocation = {
  getCurrentPosition: jest.fn()
};
let savedGeolocationValue;
const apolloClientMock = {
  query: jest.fn()
}

let geolocService;

beforeAll(() => {
  /* setup navigator.geolocation mock */
  savedGeolocationValue = global.navigator.geolocation;
  global.navigator.geolocation = mockGeolocation;
});

beforeEach(() => {
  mockGeolocation.getCurrentPosition.mockReset();
  apolloClientMock.query.mockReset();
  geolocService = new GeolocService({apolloClient: apolloClientMock});
});

/* Restore navigator.geolocation */
afterAll(() => {
  global.navigator.geolocation = savedGeolocationValue;
});


describe('geolocService._browserLocationObservable', () => {
  describe('calls the navigator geoloc API only once', () => {
    test('with two subscribers', (done) => {
      mockGeolocation.getCurrentPosition.mockImplementation((success) => {
        success('your position');
      });

      geolocService._browserLocationObservable.subscribe((val) => {
        expect(val).toEqual('your position');
      });

      geolocService._browserLocationObservable.subscribe({
        complete: () => {
          expect(mockGeolocation.getCurrentPosition.mock.calls.length).toEqual(1);
          done();
        }
      });
    });

    test('with two subscriber before the first event is emitted', (done) => {
      let geolocSuccessCallback;
      mockGeolocation.getCurrentPosition.mockImplementation((success) => {
        geolocSuccessCallback = success;
      });

      geolocService._browserLocationObservable.subscribe((val) => {
        expect(val).toEqual('your position');
      });

      geolocService._browserLocationObservable.subscribe({
        complete: () => {
          expect(mockGeolocation.getCurrentPosition.mock.calls.length).toEqual(1);
          done();
        }
      });

      geolocSuccessCallback('your position');
    });
  });

  describe('in case of error', () => {
    test('calls the API again when there is a new subscription after an error', (done) => {
      mockGeolocation.getCurrentPosition.mockImplementation((success, error) => {
        error({code: 1, message: "User denied Geolocation"});
      });

      geolocService._browserLocationObservable.subscribe({
        error: (err) => {
          expect(err).toEqual({
            status: 'browser_geolocation_error',
            error: {
              code: 1,
              message: "User denied Geolocation"
            }
          })
        }
      });

      mockGeolocation.getCurrentPosition.mockImplementation((success) => {
        success('your position');
      });

      let count = 0;
      geolocService._browserLocationObservable.subscribe({
        next: val => {
          count++;
          expect(val).toEqual('your position');
        },
        complete: () => {
          expect(mockGeolocation.getCurrentPosition.mock.calls.length).toEqual(2);
          expect(count).toEqual(1);
          done();
        }
      });
    });
  });
});


/*
 * setAutoPlace(): 
 *
 * # Normal workflow
 *
 *  - The detected place is emitted in the current places stream
 *  - The returned observable emits the detected place and completes
 *
 * # When the geoloc API returns an error
 *
 *  - the returned observable throws the error
 *
 * # When the graphQL request returns an error
 *
 *  - the returned observable throws the error
 *
 * # When the graphQL request returns no place
 *
 *  - the returned observable errors with the object {type: 'not_found'}
 *
 * # When a manual place is set before the auto place request finishes
 *  
 *  - the returned observable errors with {type: 'aborted'}
 *  - the automatically detected place is not added to the current place stream
 *
 */
describe("geolocService.setAutoPlace()", () => {
  let myPlace = {
    id: "geonames:6454071",
    __typename: "City",
    name: "Grenoble",
    countryCode: "FR",
    countryId: "geonames:geonames:3017382",
    countryName: "France",
    stateCode: "84",
    stateName: "Auvergne-Rhône-Alpes"
  }

  beforeEach(() => {

    let position = {
      coords: {
        accuracy: 1853,
        altitude: null,
        altitudeAccuracy: null,
        heading: null,
        latitude: 45.18,
        longitude: 5.72,
        speed: null
      },
      timestamp: 1561365913706
    };


    let myPlaceGQLResponse = {
      "data": {
        "placeByGeoCoords": Object.assign(myPlace)
      },
      "loading": false,
      "networkStatus": 7,
      "stale": false
    }

    mockGeolocation.getCurrentPosition.mockImplementation((success) => {
      setTimeout(() => success(position), 100);
    });

    apolloClientMock.query.mockImplementation(() => {
      return new Promise((resolve) => resolve(myPlaceGQLResponse));
    });
  });


  describe('success call', () => {
    it('emits the detected place in the stream of current places', (done) => {
      geolocService.currentPlace.pipe(
        skip(1), /* Skip initial place */
        take(1),
      ).subscribe({
        next: place => expect(place).toEqual(myPlace),
        complete: () => {
          /* Expect apollo client to have been called with correct graphQL request */
          expect(apolloClientMock.query.mock.calls.length).toEqual(1);
          expect(apolloClientMock.query.mock.calls[0][0]).toEqual({
            query: PlaceFromCoordinatesQuery,
            variables: {lat: 45.18, lng: 5.72}
          });
          done();
        },
        error: err => done.fail(err)
      })

      geolocService.setAutoPlace();
    });

    it('emits the detected place in the returned observable, and this observable completes', (done) => {
      let placeObservable = geolocService.setAutoPlace();
      let place;
      placeObservable.subscribe({
        next: val => place = val,
        complete: () => {
          expect(place).toEqual(myPlace);
          done();
        },
        error: err => done.fail(err)
      });
    })

    it('works after a manual place has been set', (done) => {
      let myManualPlace = {
        id: "geonames:3017382",
        __typename: "Country",
        name: "ManualCountry",
        countryCode: "MC"
      };
      geolocService.setManualPlace(myManualPlace);
      geolocService.setAutoPlace().subscribe(place => {
        expect(place).toEqual(myPlace);
        done();
      });
    });
  });

  describe("error cases", () => {
    /* Disable console for those tests, to avoid polluting test output (console logging is to be expected) */
    let consolewarn;
    beforeAll(() => {
      consolewarn = console.warn;
      console.warn = jest.fn(); 
    });
    afterAll(() => {
      console.warn = consolewarn;
    });

    it("throws any error from the navigator geolocation API", (done) => {
      let positionError = {code: 1, message: "User denied Geolocation"};
      let expectedError = {status: 'browser_geolocation_error', error: positionError};

      mockGeolocation.getCurrentPosition.mockImplementation((success, error) => {
        setTimeout(() => error(positionError), 100);
      });

      let placeObservable = geolocService.setAutoPlace();
      placeObservable.subscribe({
        error: (err) => {
          expect(err).toEqual(expectedError);
          done();
        }
      });
    });

    it("throws any error from the apollo client", (done) => {
      let apolloClientError = {
        "graphQLErrors": [],
        "networkError": {
          "name": "ServerError",
          "response": {},
          "statusCode": 400,
          "result": {
            "errors": [
              {
                "message": "Cannot query field on type \"PlaceInterface\".",
                "locations": [
                  {
                    "line": 6,
                    "column": 5
                  }
                ],
                "stack": "GraphQLError: Cannot query field on type \"PlaceInterface\".\n    at Object.Field..."
              }
            ]
          }
        },
        "message": "Network error: Response not successful: Received status code 400"
      }

      apolloClientMock.query.mockImplementation(() => {
        return new Promise((resolve, reject) => {
          reject(apolloClientError)
        })
      });

      let placeObservable = geolocService.setAutoPlace();
      placeObservable.subscribe({
        error: (err) => {
          expect(err).toEqual(apolloClientError);
          done();
        }
      });
    });

    describe("another place is set before request finishes", () => {
      let myManualPlace = {
        id: "geonames:6618608",
        __typename: "City",
        name: "Paris 02",
        countryCode: "FR",
        countryId: "geonames:geonames:3017382",
        countryName: "France",
        stateName: "Île-de-France"
      }

      it("cancels the request, and doesn't set the automatic place in the current place stream", (done) => {
        let r = geolocService.setAutoPlace();
        geolocService.setManualPlace(myManualPlace);

        let count = 0;
        geolocService.currentPlace.pipe(
          takeUntil(timer(300))
        ).subscribe({
          next: (place) => {
            expect(place).toEqual(myManualPlace);
            count++;
          },
          complete: () => {
            expect(count).toEqual(1); /* only one place must be emitted */
            done();
          }
        });
      });

      it("returns an error in the returned observable", (done) => {
        let placeObservable = geolocService.setAutoPlace();
        geolocService.setManualPlace(myManualPlace);

        placeObservable.subscribe({
          error: (err) => {
            expect(err).toEqual({status: 'aborted'});
            done();
          }
        })
      });
    });

    it("throws an error if no place is returned by the graphQL request", (done) => {
      let emptyGqlResponse =  {
        "data": {
          "placeByGeoCoords": null
        },
        "loading": false,
        "networkStatus": 7,
        "stale": false
      };
      apolloClientMock.query.mockImplementation((done) => {
        return new Promise((resolve) => resolve(emptyGqlResponse));
      });
      let expectedError = {
        status: 'not_found',
        gqlQueryResult: {
          data: {
            placeByGeoCoords: null,
          },
          loading: false,
          networkStatus: 7,
          stale: false,
        },
        inputOptions: {
          lat: 45.18,
          lng: 5.72,
        }
      };

      let placeObservable = geolocService.setAutoPlace();
      placeObservable.subscribe({
        error: (err) => {
          expect(err).toEqual(expectedError)
          done();
        }
      });
    });
  });
});

describe("currentPlace", () => {
  it("emits the last place on subscribe", (done) => {
    let myManualPlace = {
      id: "geonames:6618608",
      __typename: "City",
      name: "Paris 02#custom",
      countryCode: "FR",
      countryId: "geonames:geonames:3017382",
      countryName: "France",
      stateName: "Île-de-France"
    }
    geolocService.setManualPlace(myManualPlace);
    geolocService.currentPlace
    .pipe(take(1))
    .subscribe(place => {
      expect(place).toEqual(myManualPlace);
      done();
    });
  });
});

/**
 * PlaceSearcher
 *
 * - It makes the correct apollo client queries
 * - If the second query resolves faster than the first, only the second result is emitted
 *
 *  # Error
 *
 *  - It throws the apollo client error
 */
describe("PlaceSearcher", () => {
  let placeSearcher;
  beforeEach(() => {
    placeSearcher = geolocService.getPlaceSearcher({defaultFirst: 4});
  });

  it("calls the apollo client with correct query", (done) => {
    let placesApolloResult = {
      "data": {
        "places": {
          "edges": [{
            "node": {
              "id": "geonames:3017382",
              "__typename": "Country",
              "name": "Germany",
              "countryCode": "GE"
            }
          }]
        }
      }
    };

    apolloClientMock.query.mockReturnValue(new Promise((resolve) => resolve(placesApolloResult)));


    placeSearcher.results.pipe(
      take(1)
    ).subscribe({
      complete: () => {
        expect(apolloClientMock.query.mock.calls.length).toEqual(1);
        expect(apolloClientMock.query.mock.calls[0][0]).toEqual({
          query: SearchPlacesQuery,
          variables: {qs: 'my search', first: 4}
        });
        done();
      }
    });

    placeSearcher.search('my search');
  });

  it("bypass the first query when the second resolves before", (done) => {
    let places = [{
      "id": "geonames:3017382",
      "__typename": "Country",
      "name": "France",
      "countryCode": "FR"
    },
    {
      "id": "geonames:11832782",
      "__typename": "City",
      "name": "Frances",
      "countryCode": "PH",
      "countryId": "geonames:geonames:1694008",
      "countryName": "Philippines",
      "stateName": "Luçon centrale"
    }];

    let placesApolloResult1 = {
      "data": {
        "places": {
          "edges": [{
            "node": {
              "id": "geonames:3017382",
              "__typename": "Country",
              "name": "Germany",
              "countryCode": "GE"
            }
          }]
        }
      }
    };

    let placesApolloResult2 = {
      "data": {
        "places": {
          "edges": [{
            "node": {
              "id": "geonames:3017382",
              "__typename": "Country",
              "name": "France",
              "countryCode": "FR"
            }
          },
          {
            "node": {
              "id": "geonames:11832782",
              "__typename": "City",
              "name": "Frances",
              "countryCode": "PH",
              "countryId": "geonames:geonames:1694008",
              "countryName": "Philippines",
              "stateName": "Luçon centrale"
            }
          }]
        }
      }
    };

    apolloClientMock.query.mockReturnValueOnce(new Promise(resolve => setTimeout(() => resolve(placesApolloResult1), 1200)));
    apolloClientMock.query.mockReturnValueOnce(new Promise(resolve => setTimeout(() => resolve(placesApolloResult2), 10)));

    let count = 0;
    placeSearcher.results.pipe(
      takeUntil(timer(1500))
    ).subscribe({
      next: (result) => {
        count++;
        expect(result).toEqual(places);
      },
      complete: () => {
        expect(count).toEqual(1);
        done();
      }
    });

    placeSearcher.search('search1');
    setTimeout(() => placeSearcher.search('search2'), 550);
  });

  describe("clear()", () => {
    it("pushes an empty array in results and cancel a pending request", (done) => {
      let places = [{
        "id": "geonames:3017382",
        "__typename": "Country",
        "name": "Germany",
        "countryCode": "GE"
      }];

      let placesApolloResult1 = {
        "data": {
          "places": {
            "edges": [{
              "node": {
                "id": "geonames:3017382",
                "__typename": "Country",
                "name": "Germany",
                "countryCode": "GE"
              }
            }]
          }
        }
      };

      apolloClientMock.query.mockReturnValueOnce(new Promise(resolve => setTimeout(() => resolve(placesApolloResult1), 400)));

      let count = 0;
      placeSearcher.results.pipe(
        takeUntil(timer(800))
      ).subscribe({
        next: (result) => {
          count++;
          expect(result).toEqual([]);
        },
        complete: () => {
          expect(count).toEqual(1);
          done();
        }
      });

      placeSearcher.search('search1');
      /* clear after debounce time. TODO find a way to have the clear command take precedence over the search even if it occurs
       * before debounce time */
      setTimeout(() => placeSearcher.clear(), 300);
    });
  });
});


describe("usage of current user location", () => {
  it("updates the location on user login", (done) => {
    let loggedUser = {
      "id": "person/OperatorPerson",
      "avatar": null,
      "nickName": "Derek Test",
      "userAccount": {
        "username": "operator@ddf.fr",
        "isAdmin": false,
        "isOperator": true,
        "isContributor": false
      },
      "place": {
        "id": "geonames:6441798",
        "__typename": "City",
        "name": "Tassin-la-Demi-Lune",
        "countryCode": "FR",
        "countryId": "geonames:3017382",
        "countryName": "France",
        "stateName": "Auvergne-Rhône-Alpes"
      }
    };

    let myManualPlace = {
      id: "geonames:6618608",
      __typename: "City",
      name: "Paris 02",
      countryCode: "FR",
      countryId: "geonames:geonames:3017382",
      countryName: "France",
      stateName: "Île-de-France"
    }
    let userAuthenticationServiceMock = {
      currentUser: new BehaviorSubject({isLogged: false, user: null})
    }
    geolocService = new GeolocService({
      apolloClient: apolloClientMock,
      userAuthenticationService: userAuthenticationServiceMock
    });

    expect.assertions(2);
    geolocService.setManualPlace(myManualPlace);
    geolocService.currentPlace
      .pipe(
        take(2),
        map((place, index) => {
          if (index === 0) {
            expect(place.name).toEqual('Paris 02');
          } else if (index === 1) {
            expect(place.name).toEqual('Tassin-la-Demi-Lune');
          }
        }))
      .subscribe({
        complete: () => done()
      });

    userAuthenticationServiceMock.currentUser.next({
      isLogged: true,
      user: loggedUser
    });
  });

  it("does nothing if the logged in user has not place in its settings", (done) => {
    let loggedUser = {
      "id": "person/OperatorPerson",
      "avatar": null,
      "nickName": "Derek Test",
      "userAccount": {
        "username": "operator@ddf.fr",
        "isAdmin": false,
        "isOperator": true,
        "isContributor": false
      },
      "place": null
    };

    let myManualPlace = {
      id: "geonames:6618608",
      __typename: "City",
      name: "Paris 02",
      countryCode: "FR",
      countryId: "geonames:geonames:3017382",
      countryName: "France",
      stateName: "Île-de-France"
    }
    let userAuthenticationServiceMock = {
      currentUser: new BehaviorSubject({isLogged: false, user: null})
    }
    geolocService = new GeolocService({
      apolloClient: apolloClientMock,
      userAuthenticationService: userAuthenticationServiceMock
    });

    expect.assertions(1);
    geolocService.setManualPlace(myManualPlace);
    geolocService.currentPlace
      .pipe(
        take(2),
        map((place, index) => {
          return {place, index};
        })
      )
      .subscribe(({place, index}) => {
        if (index === 0) {
          expect(place.name).toEqual('Paris 02');
          done();
        } else {
          /* We don't expect any more place to be emitted */
          done.fail(new Error(`The observable currentPlace emitted more than one place. The emitted place was : \n ${JSON.stringify(place, null, 2)}`));
        } 
      });

    userAuthenticationServiceMock.currentUser.next({
      isLogged: true,
      user: loggedUser
    });
  });


});
