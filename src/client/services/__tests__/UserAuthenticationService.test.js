import React from 'react';
import {
  UserAuthenticationService, 
  gqlMeQuery, 
  gqlRegisterUserAccountMutation, 
  gqlLoginMutation, 
  gqlLogoutMutation,
  gqlResetUserAccountPasswordMutation,
  gqlResetUserAccountPasswordByMailMutation,
  gqlUnregisterUserAccountMutation
} from '../UserAuthenticationService';
import {renderHook, act} from '@testing-library/react-hooks'
import {MockedProvider} from '@apollo/react-testing';
import {generateMockedApolloClient} from '../../../../jest/utilities/servicesMocks/MockedApolloClient.js';
import {generateApolloCache} from '../../../../jest/utilities/apolloCache';
import Cookies from 'js-cookie';



describe('UserAuthenticationService', () => {

  describe('current user', () => {
    it('returns the current user', (done) => {
      let sampleUser = {
        id: 'user-1',
        avatar: null,
        nickName: 'John  Doe',
        __typename: 'Person',
        userAccount: {
          username: 'john.doe@ddf.fr',
          isAdmin: false,
          isOperator: false,
          isContributor: true,
          __typename: 'UserAccount'
        },
        place: {
          id: 'place-1',
          name: 'France',
          countryCode: 'FR',
          __typename: 'Country'
        }
      };
      let apolloClient = generateMockedApolloClient({
        mocks: [{
          request: {
            query: gqlMeQuery
          },
          result: {
            "data": {
              "me": sampleUser
            }
          }
        }]
      });
      let userAuthenticationService = new UserAuthenticationService({apolloClient});
      userAuthenticationService.currentUser.subscribe((user) => {
        expect(user).toEqual({
          isLogged: true,
          user: sampleUser
        });
        done();
      });
    });

    it('returns the object with isLogged: false, when no user is logged', (done) => {
      let apolloClient = generateMockedApolloClient({
        mocks: [{
          request: {
            query: gqlMeQuery
          },
          result: {
            "errors": [
              {
                "message": "USER_MUST_BE_AUTHENTICATED",
                "statusCode": 401,
                "extraInfos": "User must be authenticated ! (Blocked by `isAuthenticatedRule` 🛡 rule)",
                "stack": [
                  "I18nError: User must be authenticated ! (Blocked by `isAuthenticatedRule` 🛡 rule)",
                ]
              }
            ]
          }
        }]
      });
      let userAuthenticationService = new UserAuthenticationService({apolloClient});
      expect.assertions(1);
      userAuthenticationService.currentUser.subscribe((user) => {
        expect(user).toMatchObject({
          isLogged: false
        });
        done();
      });
    });
  });

  describe('useSubscribe', () => {
    it('registers users, calls login, and updates current user', async () => {
      let loginWasCalled = false;
      let getMeMock = () => {
        if (loginWasCalled) {
          return {
            data: {
              me: {
                id: 'user-1',
                avatar: null,
                nickName: 'John  Doe',
                __typename: 'Person',
                userAccount: {
                  username: 'john.doe@ddf.fr',
                  isAdmin: false,
                  isOperator: false,
                  isContributor: true,
                  __typename: 'UserAccount'
                },
                place: {
                  id: 'place-1',
                  name: 'France',
                  countryCode: 'FR',
                  __typename: 'Country'
                }
              }

            }
          };
        } else {
          return {
            "errors": [
              {
                "message": "USER_MUST_BE_AUTHENTICATED",
                "statusCode": 401,
                "extraInfos": "User must be authenticated ! (Blocked by `isAuthenticatedRule` 🛡 rule)",
                "stack": [
                  "I18nError: User must be authenticated ! (Blocked by `isAuthenticatedRule` 🛡 rule)",
                ]
              }
            ]
          }
        }
      };

      let mocks = [{
        request: {
          query: gqlRegisterUserAccountMutation,
          variables: {
            username: 'john.doe@ddf.fr',
            email: 'john.doe@ddf.fr',
            password: 'mypassword',
            nickName: 'John Doe',
            placeId: 'city-1'
          },
        },
        result: {
          "data": {
            "registerUserAccount": {
              "success": true,
              "__typename": "RegisterUserAccountPayload"
            }
          }
        }
      }, {
        request: {
          query: gqlLoginMutation,
          variables: {
            username: "john.doe@ddf.fr", 
            password: "mypassword"
          }
        },
        result: () => {
          loginWasCalled = true;
          return {
            "data": {
              "login": {
                "success": true,
                "__typename": "LoginPayload"
              }
            }
          }
        }
      }, {
        request: {
        query: gqlMeQuery
        },
        result: getMeMock
      }, {
        request: {
        query: gqlMeQuery
        },
        result: getMeMock
      }];
      let apolloClient = generateMockedApolloClient({mocks});
      let userAuthenticationService = new UserAuthenticationService({apolloClient, t: e => e});
      const {result} = renderHook(() => userAuthenticationService.useSubscribe(), {
        wrapper: ({children}) => <MockedProvider mocks={mocks} cache={generateApolloCache()}>{children}</MockedProvider>
      });

      let currentUserAssertionsPromise = new Promise((resolve) => {
        let counter = 0;
        userAuthenticationService.currentUser.subscribe((currentUser) => {
          if (counter === 0) {
            expect(currentUser.isLogged).toEqual(false);
          } else {
            expect(currentUser.isLogged).toEqual(true);
            expect(currentUser.user.id).toEqual('user-1');
            resolve();
          }
          counter++;
        });
      });

      let submitResult;
      await act(async () => {
        submitResult = await result.current.subscribe({
          email: 'john.doe@ddf.fr',
          password: 'mypassword',
          nickName: 'John Doe',
          placeId: 'city-1'
        });
      });

      expect(submitResult.success).toEqual(true);
      await currentUserAssertionsPromise;
    });
  });


  describe('useLogin', () => {
    it('calls login, and updates current user', async () => {
      let loginWasCalled = false;
      let getMeMock = () => {
        if (loginWasCalled) {
          return {
            data: {
              me: {
                id: 'user-1',
                avatar: null,
                nickName: 'John  Doe',
                __typename: 'Person',
                userAccount: {
                  username: 'john.doe@ddf.fr',
                  isAdmin: false,
                  isOperator: false,
                  isContributor: true,
                  __typename: 'UserAccount'
                },
                place: {
                  id: 'place-1',
                  name: 'France',
                  countryCode: 'FR',
                  __typename: 'Country'
                }
              }

            }
          };
        } else {
          return {
            "errors": [
              {
                "message": "USER_MUST_BE_AUTHENTICATED",
                "statusCode": 401,
                "extraInfos": "User must be authenticated ! (Blocked by `isAuthenticatedRule` 🛡 rule)",
                "stack": [
                  "I18nError: User must be authenticated ! (Blocked by `isAuthenticatedRule` 🛡 rule)",
                ]
              }
            ]
          }
        }
      };

      let mocks = [{
        request: {
          query: gqlLoginMutation,
          variables: {
            username: "john.doe@ddf.fr", 
            password: "mypassword"
          }
        },
        result: () => {
          loginWasCalled = true;
          return {
            "data": {
              "login": {
                "success": true,
                "__typename": "LoginPayload"
              }
            }
          }
        }
      }, {
        request: {
        query: gqlMeQuery
        },
        result: getMeMock
      }, {
        request: {
        query: gqlMeQuery
        },
        result: getMeMock
      }];
      let apolloClient = generateMockedApolloClient({mocks});
      let userAuthenticationService = new UserAuthenticationService({apolloClient, t: e => e});
      const {result} = renderHook(() => userAuthenticationService.useLogin(), {
        wrapper: ({children}) => <MockedProvider mocks={mocks} cache={generateApolloCache()}>{children}</MockedProvider>
      });

      let currentUserAssertionsPromise = new Promise((resolve) => {
        let counter = 0;
        userAuthenticationService.currentUser.subscribe((currentUser) => {
          if (counter === 0) {
            expect(currentUser.isLogged).toEqual(false);
          } else {
            expect(currentUser.isLogged).toEqual(true);
            expect(currentUser.user.id).toEqual('user-1');
            resolve();
          }
          counter++;
        });
      });

      let submitResult;
      await act(async () => {
        submitResult = await result.current.login({
          email: 'john.doe@ddf.fr',
          password: 'mypassword'
        });
      });

      expect(submitResult.success).toEqual(true);
      await currentUserAssertionsPromise;
    });
  });

  describe('useLogout', () => {
    it('calls logout, and updates current user', async () => {
      let logoutWasCalled = false;
      let getMeMock = () => {
        if (!logoutWasCalled) {
          return {
            data: {
              me: {
                id: 'user-1',
                avatar: null,
                nickName: 'John  Doe',
                __typename: 'Person',
                userAccount: {
                  username: 'john.doe@ddf.fr',
                  isAdmin: false,
                  isOperator: false,
                  isContributor: true,
                  __typename: 'UserAccount'
                },
                place: {
                  id: 'place-1',
                  name: 'France',
                  countryCode: 'FR',
                  __typename: 'Country'
                }
              }

            }
          };
        } else {
          return {
            "errors": [
              {
                "message": "USER_MUST_BE_AUTHENTICATED",
                "statusCode": 401,
                "extraInfos": "User must be authenticated ! (Blocked by `isAuthenticatedRule` 🛡 rule)",
                "stack": [
                  "I18nError: User must be authenticated ! (Blocked by `isAuthenticatedRule` 🛡 rule)",
                ]
              }
            ]
          }
        }
      };

      let mocks = [{
        request: {
          query: gqlLogoutMutation
        },
        result: () => {
          logoutWasCalled = true;
          return {
            "data": {
              "logout": {
                "success": true,
                "__typename": "LogoutPayload"
              }
            }
          }
        }
      }, {
        request: {
        query: gqlMeQuery
        },
        result: getMeMock
      }, {
        request: {
        query: gqlMeQuery
        },
        result: getMeMock
      }];
      let apolloClient = generateMockedApolloClient({mocks});
      let userAuthenticationService = new UserAuthenticationService({apolloClient, t: e => e});
      const {result} = renderHook(() => userAuthenticationService.useLogout(), {
        wrapper: ({children}) => <MockedProvider mocks={mocks} cache={generateApolloCache()}>{children}</MockedProvider>
      });

      let currentUserAssertionsPromise = new Promise((resolve) => {
        let counter = 0;
        userAuthenticationService.currentUser.subscribe((currentUser) => {
          if (counter === 0) {
            expect(currentUser.isLogged).toEqual(true);
            expect(currentUser.user.id).toEqual('user-1');
          } else {
            expect(currentUser.isLogged).toEqual(false);
            resolve();
          }
          counter++;
        });
      });

      let submitResult;
      await act(async () => {
        submitResult = await result.current.logout();
      });

      expect(submitResult.success).toEqual(true);
      await currentUserAssertionsPromise;
    });
  });

  describe('useResetPassword', () => {
    it('calls the reset password mutation', async () => {
      let mocks = [{
        request: {
          query: gqlResetUserAccountPasswordMutation,
          variables: {
            oldPassword: 'myoldpassword',
            newPassword: 'mynewpassword',
            newPasswordConfirm: 'mynewpassword',
          }
        },
        result: {
          "data": {
            "resetUserAccountPassword": {
              "success": true,
              "__typename": "ResetUserAccountPasswordPayload"
            }
          }
        }
      }];

      let apolloClient = generateMockedApolloClient({mocks});
      let userAuthenticationService = new UserAuthenticationService({apolloClient, t: e => e});
      const {result} = renderHook(() => userAuthenticationService.useResetPassword(), {
        wrapper: ({children}) => <MockedProvider mocks={mocks} cache={generateApolloCache()}>{children}</MockedProvider>
      });

      let submitResult;
      await act(async () => {
        submitResult = await result.current.resetPassword({
          oldPassword: 'myoldpassword',
          newPassword: 'mynewpassword',
          newPasswordConfirm: 'mynewpassword'
        });
      });

      expect(submitResult.success).toEqual(true);
    });
  });


  describe('useResetPasswordByMail', () => {
    it('calls the reset password by email mutation', async () => {
      let mocks = [{
        request: {
          query: gqlResetUserAccountPasswordByMailMutation,
          variables: {
            email: 'john.doe@ddf.fr',
            redirectUri: '/myUri',
          }
        },
        result: {
          "data": {
            "resetUserAccountPasswordByMail": {
              "success": true,
              "__typename": "ResetUserAccountPasswordByMailPayload"

            }
          }
        }
      }];

      let apolloClient = generateMockedApolloClient({mocks});
      let userAuthenticationService = new UserAuthenticationService({apolloClient, t: e => e});
      const {result} = renderHook(() => userAuthenticationService.useResetPasswordByMail(), {
        wrapper: ({children}) => <MockedProvider mocks={mocks} cache={generateApolloCache()}>{children}</MockedProvider>
      });

      let submitResult;
      await act(async () => {
        submitResult = await result.current.resetPassword({
          email: 'john.doe@ddf.fr',
          redirectUri: '/myUri'
        });
      });

      expect(submitResult.success).toEqual(true);
    });
  });

  describe('useUnregisterUserAccount', () => {
    it('calls the unregister account mutation, and updates current user', async () => {
      let unregisterWasCalled = false;
      let getMeMock = () => {
        if (!unregisterWasCalled) {
          return {
            data: {
              me: {
                id: 'user-1',
                avatar: null,
                nickName: 'John  Doe',
                __typename: 'Person',
                userAccount: {
                  username: 'john.doe@ddf.fr',
                  isAdmin: false,
                  isOperator: false,
                  isContributor: true,
                  __typename: 'UserAccount'
                },
                place: {
                  id: 'place-1',
                  name: 'France',
                  countryCode: 'FR',
                  __typename: 'Country'
                }
              }

            }
          };
        } else {
          return {
            "errors": [
              {
                "message": "USER_MUST_BE_AUTHENTICATED",
                "statusCode": 401,
                "extraInfos": "User must be authenticated ! (Blocked by `isAuthenticatedRule` 🛡 rule)",
                "stack": [
                  "I18nError: User must be authenticated ! (Blocked by `isAuthenticatedRule` 🛡 rule)",
                ]
              }
            ]
          }
        }
      };

      let mocks = [{
        request: {
          query: gqlUnregisterUserAccountMutation
        },
        result: () => {
          unregisterWasCalled = true;
          return {
            "data": {
              "unregisterUserAccount": {
                "success": true,
                "__typename": "UnregisterUserAccountPayload"
              }
            }
          }
        }
      }, {
        request: {
        query: gqlMeQuery
        },
        result: getMeMock
      }, {
        request: {
        query: gqlMeQuery
        },
        result: getMeMock
      }];
      let apolloClient = generateMockedApolloClient({mocks});
      let userAuthenticationService = new UserAuthenticationService({apolloClient, t: e => e});
      const {result} = renderHook(() => userAuthenticationService.useUnregisterUserAccount(), {
        wrapper: ({children}) => <MockedProvider mocks={mocks} cache={generateApolloCache()}>{children}</MockedProvider>
      });

      let currentUserAssertionsPromise = new Promise((resolve) => {
        let counter = 0;
        userAuthenticationService.currentUser.subscribe((currentUser) => {
          if (counter === 0) {
            expect(currentUser.isLogged).toEqual(true);
            expect(currentUser.user.id).toEqual('user-1');
          } else {
            expect(currentUser.isLogged).toEqual(false);
            resolve();
          }
          counter++;
        });
      });

      let submitResult;
      await act(async () => {
        submitResult = await result.current.unregisterUserAccount();
      });

      expect(submitResult.success).toEqual(true);
      await currentUserAssertionsPromise;
    });
  });


  describe('session errors', () => {
    let envSave = process.env.SYNAPTIX_USER_SESSION_COOKIE_NAME;
    let cookiesGetSave, cookiesRemoveSave;
    beforeAll(() => {
      process.env.SYNAPTIX_USER_SESSION_COOKIE_NAME = 'SNXID'; 
      cookiesGetSave = Cookies.get;
      cookiesRemoveSave = Cookies.remove;
    });
    afterAll(() => {
      process.env.SYNAPTIX_USER_SESSION_COOKIE_NAME = envSave;
      Cookies.get = cookiesGetSave;
      Cookies.remove = cookiesRemoveSave;
    });

    it('does nothing when /me returns an error but there is no session', async () => {
      /* simulate SERVER_ERROR */
      Cookies.get = jest.fn((cookieName) => {
        if (cookieName === 'SNXID') {
          return undefined;
        } else {
          throw new Error(`Cookies.get mock : Unexpected cookie name ${cookieName}`);
        }
      });
      let apolloClient = generateMockedApolloClient({
        mocks: [{
          request: {
            query: gqlMeQuery
          },
          result: {
            "data": {
              "me": null,
            },
            "errors": [new Error('SERVER_ERROR')]
          }
        }]
      });
      let notificationServiceMock = {
        error: jest.fn()
      };
      let userAuthenticationService = new UserAuthenticationService({apolloClient, notificationService: notificationServiceMock});
      let promise = new Promise((resolve) => {
        userAuthenticationService.currentUser.subscribe((user) => {
          expect(user).toMatchObject({
            isLogged: false
          });
          expect(user.apiError.graphQLErrors[0].message).toEqual('SERVER_ERROR');
          resolve();
        });
      });
      await promise;
      expect(Cookies.get.mock.calls.length).toEqual(1);
      expect(notificationServiceMock.error.mock.calls.length).toEqual(0);
    });

    it('deletes cookie and notify user in case of unexpected error on /me with an existing session', async () => {
      /* simulate SERVER_ERROR */
      Cookies.get = jest.fn((cookieName) => {
        if (cookieName === 'SNXID') {
          return 'cookie-value';
        } else {
          throw new Error(`Cookies.get mock : Unexpected cookie name ${cookieName}`);
        }
      });
      Cookies.remove = jest.fn();
      let apolloClient = generateMockedApolloClient({
        mocks: [{
          request: {
            query: gqlMeQuery
          },
          result: {
            "data": {
              "me": null,
            },
            "errors": [new Error('SERVER_ERROR')]
          }
        }]
      });
      let notificationServiceMock = {
        error: jest.fn()
      };
      let userAuthenticationService = new UserAuthenticationService({apolloClient, notificationService: notificationServiceMock});
      let promise = new Promise((resolve) => {
        userAuthenticationService.currentUser.subscribe((user) => {
          expect(user).toMatchObject({
            isLogged: false
          });
          expect(user.apiError.graphQLErrors[0].message).toEqual('SERVER_ERROR');
          resolve();
        });
      });
      await promise;
      expect(Cookies.get.mock.calls.length).toEqual(1);
      expect(notificationServiceMock.error.mock.calls[0][0]).toEqual('Votre session a expiré');
      expect(Cookies.remove.mock.calls[0][0]).toEqual('SNXID');
    });
  });
});

