import {UserAuthenticationService as CortexUserAuthenticationService} from '@mnemotix/synaptix-client-toolkit';
import i18next from 'i18next';
import gql from 'graphql-tag';
import * as Yup from 'yup';
import {tap} from 'rxjs/operators';
import Cookies from 'js-cookie'

import {ROUTES} from '../routes';
import {getDDFApolloClient} from './DDFApolloClient';
import {gqlPlaceFragment} from '../utilities/helpers/places';
import {gqlUserAccountGroupsFragment} from '../utilities/helpers/userAccountGroups';
import {notificationService as appNotificationSevice} from './NotificationService';

export {
  gqlLoginMutation, 
  gqlLogoutMutation,
  gqlResetUserAccountPasswordMutation,
  gqlResetUserAccountPasswordByMailMutation,
  gqlUnregisterUserAccountMutation
} from '@mnemotix/synaptix-client-toolkit';

export const gqlRegisterUserAccountMutation = gql`
  mutation RegisterUserAccount($username: String!, $password: String!, $email: String!, $nickName: String!, $placeId: ID) {
    registerUserAccount(input: {
      username: $username, 
      password: $password, 
      email: $email, 
      nickName: $nickName
      personInput: {placeInput: {id: $placeId}}
    }) {
      success
    }
  }
`;

export const gqlMeQuery = gql`
  query Me {
    me {
      id
      avatar
      nickName
      userAccount {
        username
        ...UserAccountGroupsFragment
      }
      place {
        id
        ...PlaceFragment
      }
    }
  }

  ${gqlPlaceFragment}
  ${gqlUserAccountGroupsFragment}
`;

export class UserAuthenticationService extends CortexUserAuthenticationService {
  constructor({t, apolloClient, notificationService} = {}) {
    let _apolloClient = apolloClient || getDDFApolloClient();
    super({
      t, 
      apolloClient: _apolloClient, 
      meQuery: gqlMeQuery,

      i18nKeyPrefix: 'FORM_ERRORS'
    });

    this.notificationService = notificationService || appNotificationSevice;

    /**
     * Detect when the API returned an error. The default behavior is to consider the logged as logged out. However, if the error code
     * is different than USER_MUST_BE_AUTHENTICATED, and a current user cookie is present, we delete the cookie and notify the user
     * that an unknown problem caused him to be logged out
     *
     * TODO : 
     * - configure cookie name from env variables
     * - write unit tests for differents scenarios
     * - think about a backend solution (without need to read cookies on the frontend)
     */
    this.currentUser$.subscribe(({apiError}) => {
      if (apiError) {
        let errorIsUnexpected = !apiError.graphQLErrors?.some(graphQLError => graphQLError.message === 'USER_MUST_BE_AUTHENTICATED');
        let hasSessionCookie = !!Cookies.get(process.env.SYNAPTIX_USER_SESSION_COOKIE_NAME);
        if (errorIsUnexpected && hasSessionCookie) {
          Cookies.remove(process.env.SYNAPTIX_USER_SESSION_COOKIE_NAME);
          this.notificationService.error('Votre session a expiré');
        }
      }
    });
  }

  getSubscribeValidationSchema() {
    return Yup.object().shape({
      nickName: Yup.string()
        .required(this.t('FORM_ERRORS.FIELD_ERRORS.REQUIRED')),
      email: Yup.string()
        .email(this.t('FORM_ERRORS.FIELD_ERRORS.INVALID_EMAIL'))
        .required(this.t('FORM_ERRORS.FIELD_ERRORS.REQUIRED')), 
      password: Yup.string()
        .required(this.t('FORM_ERRORS.FIELD_ERRORS.REQUIRED')),
      gcu: Yup.boolean()
        .required(this.t('FORM_ERRORS.FIELD_ERRORS.GCU_MUST_BE_ACCEPTED'))
        .oneOf([true], this.t('FORM_ERRORS.FIELD_ERRORS.GCU_MUST_BE_ACCEPTED'))
    });
  }

  /**
   * React hook that implement the subscribe logic. Internally it uses the hook useMutation().
   * The hook gives a function to call when the subscribe form is submitted. The function is designed
   * to work out of the box with a formik form (you can pass it to formik form 'onSubmit' handler).
   * You can also use it without formik, in which case just call the handler with you form values, 
   * without second parameter (the formikOptions object).
   * If formikOptions is provided, the field errors are automatically set if the mutation return form validation
   * errors.
   *
   * After the mutation has successfully run, the handler automatically logins the new created user. It then 
   * updates the service's `currentUser` observable.
   *
   * The hook returns the property `success` set  to `true` if all operation succeded.
   * The hook returns the property `globalErrorMessage` set to a i18nized string in case of error.
   *
   * @return {Object} result
   * @return {function} result.subscribe
   * @return {string} result.globalErrorMessage   This property is set if the login mutation returned an error that could be interpreted, 
   *                                              it gives a human readable (already internationalized) error message to display to the user.
   * @return {boolean} result.success Set to true if all operations were successfull
   */
  useSubscribe() {
    let {subscribe: superSubscribe, globalErrorMessage, success} = super.useSubscribe({
      mutation: gqlRegisterUserAccountMutation,
      mutationName: 'registerUserAccount'
    });

    function subscribe(values, formikOptions) {
      return superSubscribe(
        {
          username: values.email,
          ...values
        },
        formikOptions
      );
    }

    return {
      subscribe,
      globalErrorMessage,
      success
    }
  }

  useLogin() {
    let {login: superLogin, globalErrorMessage, success} = super.useLogin();

    function login({email, password}, formikOptions) {
      return superLogin({username: email, password}, formikOptions);
    }

    return {login, globalErrorMessage, success};
  }


  /**
   * React hook. Gives the current logged user. If there is not logged in user it redirects to another URL
   * (presumably a page that isn't restricted to logged in users). 
   *
   *
   * By defaults, it redirects to the route LOGIN (as defined in `routes.js`, presumably '/login').
   * If you pass the option {redirectToIndex: true}, it redirects to '/'.
   * If you pass an URL to the option {redirectTo: <string}>, it redirects to that URL.
   *
   * @param {Object} options
   * @param {Object} options.userAuthenticationService Dependency injection for UserAuthenticationService. This can be omitted and it will
   *                                                   use the application default service
   * @param {boolean} options.redirectToIndex If true, it redirects to '/' when the user is not logged
   * @param {String} options.redirectTo The URL to use as redirection when the user is not logged
   * @return {Object} result
   * @return {Object} result.user The current user object
   * @return {boolean} result.isLogged Flag that tells if there is a logged in user
   */
  useLoggedUser(options = {}) {
    let redirectTo = options.redirectTo || (options.redirectToIndex ? '/' : ROUTES.LOGIN);
    return super.useLoggedUser({
      redirectTo,
      defaultUserValue: {place: {}}
    });
  }
}

let userAuthenticationService;
export function getUserAuthenticationService() { 
  return userAuthenticationService  || (userAuthenticationService = new UserAuthenticationService({t: i18next.t.bind(i18next)}));
};

