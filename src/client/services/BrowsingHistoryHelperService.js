import React, {useContext} from 'react';
import invariant from 'invariant';
import {useHistory, matchPath} from 'react-router-dom';
import {RoutesMetadata} from '../routes';


/**
 *
 * This sevices provides helpes to help navigating back in the application. The different scenarios covered : 
 *
 * - When the user logins, he wants to go back to the page he was on before accessing the login screen
 * - When the user is sent to the login screen and cancels the login action (click on back arrow), he wants to go back to the previous screen
 *   that didn't need authentication
 * - When the user logouts, if he was on a screen that needs authentication (e.g "my account" page) he wants to go back to the index. If he 
 *   was on a page that doesn't need authentication, he wants to be back to that page
 * - When the user closes a page from the "secondary navigation space" ("my account" pages, "credits" pages, etc.), we wants to go back to 
 *   the previous page of the main space (forms and lexical senses pages).
 *
 *
 * Usage : on top of the react tree, you must place the BrowsingHistoryHelperProvider, above your app root, but below react-router's
 * BrowserRouter which this service depends on.
 *
 *    <BrowserRouter>
 *      <BrowsingHistoryHelperProvider>
 *        <MyApp />
 *      </BrowsingHistoryHelperProvider>
 *    </BrowserRouter>
 *
 *
 * In a component in which you need the BrowsingHistoryHelperService, call the hook useBrowsingHistoryHelperService
 *
 * function myComponent(props) {
 *   const {browsingHistoryHelperService} = useBrowsingHistoryHelperService();
 * }
 *
 *
 * Service's methods :
 *
 * - getAfterLogoutLocation() :  Returns the location to redirect the user after a logout action
 * - getAfterLoginLocation() : Returns the location to redirect the user after a login action
 * - getBackFromSecondaryScreenLocation() : Returns the location to redirect the user after a click on "back" on a secondary screen
 * - getPreviousPageFormWrittenRep() : If the previous page was associated with a form (ontology's form, not HTML form), that is it was a page
 *                                     like the list of lexical senses, or detail of a lexical sense, this returns the form in question.
 *
 * 
 *
 */

export class BrowsingHistory {
  constructor({history}) {
    this.history = history;

    /** 
     * This array stores the visited locations. The objects are in the following format : 
     *
     * {
     *   location: <Object>  // location object as provided by react-router
     *   routeDef: <Object>  // the route definition object that matches the location, as defined in the array RoutesMetadata in routes.js 
     * }
     *
     * The locations are ordered from the most recent to the oldest. That is, on navigation new locations are added to the beginning of the array.
     */
    this.visitedLocations = [];
    this.historyListener = this.historyListener.bind(this);
    this.history.listen(this.historyListener)
    
    /* Initialize with current location */
    this.historyListener(this.history.location);
  }

  historyListener(location) {
    /*
     * On each location that is visited, we need to determine what route is accessed (from the array of route definitions defined in routes.js).
     * We store the object containing the route definition and the exact location in our array storing the history of visited locations
     */
    let routeMatch;
    let routeDef = RoutesMetadata.find(routeDef => {
      routeMatch = matchPath(location.pathname, {path: routeDef.path});
      return routeMatch;
    });
    if (routeDef) {
      this.visitedLocations.unshift({
        location, 
        routeDef,
        routeMatch
      });
    }
  }

  /**
   * This helper has two mode, depending on the provided option `lookAtPreviousPage`.
   *
   * By default, it just tells if the user can stay on the current page after being logged out. If yes, this will returns the route object for
   * the current page (but there is no need to navigate). Otherwise it will return null, and the consumer service will need to redirect to its fallback,
   * (presumably the index). This is the intended behavior for mobile navigation.
   *
   * If the options `lookAtPreviousPage` is set at true, it assumes the user is on a dedicated page for the logout action. The function tells if the page
   * previously visited by the user (before he was on the screen for logout) needed authentication. If the previous page didn't need authentication,
   * the route object to that previous page is returned, and the consumer can redirect to it. Otherwise, null is returned and the consumer service redirect
   * to its fallback (presumably the index). This is the intended behavior for desktop navigation.
   *
   * @param {Object} options
   * @param {Boolean} [lookAtPreviousPage=false] - If true
   *
   * @return {Object || null} result
   * @return {Object} result.location
   * @return {Object} result.routeDef
   */
  getAfterLogoutLocation({lookAtPreviousPage = false} = {}) {
    let visitedLocation = lookAtPreviousPage ? this.visitedLocations[1] : this.visitedLocations[0];
    if (visitedLocation && visitedLocation.routeDef.needsAuthentication) {
      return null;
    } else {
      return visitedLocation;
    }
  }

  /**
   * This helper returns the page that the user must be redirected to after a login action. It is the previous visited page, that is not
   * part of the authentication process (routes that are tagged with partOfAuthenticationProcess in RoutesMetadata in routes.js)
   *
   * Otherwise it returns null.
   *
   * @return {Object || null} result
   * @return {Object} result.location
   * @return {Object} result.routeDef
   */
  getAfterLoginLocation() {
    let previousVisitedLocation = this.visitedLocations.find(locObject => !locObject.routeDef.partOfAuthenticationProcess)
    return previousVisitedLocation || null;
  }

  /**
   * This helper returns the page that the user needs to be redirected to, after closing a secondary screen. It returns the first previously visited page
   * that is flagged as `isMainNavigationSpace` in RoutesMetadata in routes.js
   *
   * Otherwise it returns null.
   *
   * @return {Object || null} result
   * @return {Object} result.location
   * @return {Object} result.routeDef
   */
  getBackFromSecondaryScreenLocation() {
    let previousVisitedLocation = this.visitedLocations.find(locObject => locObject.routeDef.isMainNavigationSpace)
    return previousVisitedLocation || null; 
  }

  /**
   *
   * @return {Object} result
   * @return {string} result.formWrittenRep
   */
  getPreviousPageFormWrittenRep() {
    let lastPage = this.visitedLocations[1];
    return {
      formWrittenRep: lastPage?.routeMatch?.params?.formQuery
    };
  }
}

let BrowsingHistoryContext = React.createContext();
let browsingHistorySeviceSingleton;


export function BrowsingHistoryHelperProvider(props) {
  let history = useHistory();
  if (!browsingHistorySeviceSingleton) {
    browsingHistorySeviceSingleton = new BrowsingHistory({history});
  }

  return <BrowsingHistoryContext.Provider value={browsingHistorySeviceSingleton}>{props.children}</BrowsingHistoryContext.Provider>;
}


export function useBrowsingHistoryHelperService() {
  return {
    browsingHistoryHelperService: useContext(BrowsingHistoryContext)
  };
}

