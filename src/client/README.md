# Client

Ce README décrit différents aspect du code client (application React)

[[_TOC_]]

# I18n



# Stylesheets

## Stylus

On utilise le language Stylus, qui compile en CSS. Les librairies utilisées sont :

- [rupture](https://github.com/jescalan/rupture) : fournit des fonctions pour déclarer les media queries de façon concise.

La philosophie utilisée pour le projet est de créer des fichiers stylus partagés qui n'exportent que des fonctions ou des variables, pas des sélecteurs globaux.
C'est le cas pour les deux librairies utilisées ci dessus. Au lieu d'un fichier stylus qui déclarerait un sélecteur `.grid`, puis d'utiliser le `.grid` à chaque
balise HTML qui devrait servir de container de grille, le fichier stylus déclare une fonction `grid()`, à appliquer à chaque selecteur qui doit servir de container de grille.

Example de ce qu'on veut éviter :

`myComponent.html`

```html
<div class="my-component grid">  <!-- non semantic class name polluting HTML lisibility, confusing for maintanability in the long run -->
  ...
</div>

```

`myComponent.styl`

```
@require '../assets/stylesheets/grid.styl';  // will import CSS rule for selector .grid, polluting global CSS namespace and rules
```

Example de ce qu'on cherche à faire :

`myComponent.html`
```html
<div class="myComponent-CSSMODULEHASH">
  ...
</div>
```

`myComponent.styl`

```
@require '../assets/stylesheets/grid.styl';  // only imports variables and functions, has no impact on the CSS output 

.myComponent {
  grid()  // will generate the CSS rules for grid, inside .myComponent selector
}
```


Celà permet :

- d'être cohérent avec l'utilisation de CSS modules, et l'absence de sélecteur globaux
- éviter les effets de bords 
- prioriser la lisibilité et la maintenance du code, sur l'optimisation du CSS de sortie


## CSS modules

On utilise le CSS scopé avec css modules pour toutes les feuilles de style. Chaque component déclare un fichier de styl du même nom,
et fait un import en javascript.  Le bundle webpack transforme alors ces feuilles de style avec CSS modules pour génerer des selecteurs 
hashés unique et éviter les collisions dans le namespace CSS global.

`myComponent.js`
```js
import style from 'myComponent.stylus'

function MyComponent() {
  return <div className={style.container}></div>
}
```

`myComponent.styl`
```
.container {
  ...
}
```

Dans l'ordre webpack fait `code stylus` -> `code CSS` -> `code CSS scopé`. À la fin de la transformation le sélecteur `.container` devient
`.container-HASH`.

## Architecture des fichiers de style

- `src/client/assets/stylesheets/main.styl` : fichiers de style globaux qui ne doivent être importés qu'une fois
  (dans `main.js`), avec des règles globales (`html`, déclarations des polices, ...)
- `src/client/assets/stylesheets/global.styl` déclare l'ensemble des variables globales pour toute l'application. Est destiné à être importé par toutes les
  feuilles de styles
- Tous les autres fichiers de `src/client/assets/stylesheets` sont destiné à être importé dans les fichiers de styl des component qui ont besoin des fonctionnalités
  du fichier. Ces fichiers ne doivent déclarer que des variables et des fonctions, pas de sélecteurs CSS, qui se retrouveraient dupliqués dans la sortie CSS de chaque 
  feuille de style qui fait un import.
- En particulier `src/client/assets/stylesheets/helpers` sont pour les fichiers qui déclarent des fonction qui génere des règles CSS pour des cas d'usage précis.
  Par exemple le helper `loadingPlaceholder.styl` qui génère les règles nécessaire à la création d'une animation de loading sur n'importe quel élément.

# Animations de loading

Les différents points de l'application qui impliquent des temps de chargement et le besoin de gérer des animations de chargement adéquates : 

- À la racine de l'appli, un composant `React.Suspense`. À vérifier l'ensemble des cas qui poussent le composant à renderer son loader. L'idée 
  est qu'il serve pour toute la phase d'initialisation de l'appli au premier chargement, tant qu'il reste des données à charger qui empêche une navigation 
  fluide (éviter l'effet rendering petit à petit qui font sauter l'écran au premier chargement). Pour l'instant il est utilisé le temps du chargement des traductions
  des textes de l'interface (i18n).
- À chaque component qui fait une requête graphQL. Lorsque le component est monté, il y a un temps de chargement pour la requête apollo.

  Les différents cas de loading possible : 
    
    - montage du component, sans données en cache
    - montage du component, avec données en cache (donc données à afficher, mais également temps de chargement pour le retour de la requête de vérification)
    - update des données apollo sur component déjà monté 

  Les différentes stratégie d'affichage pendant un chargement : 

    - masquer le component tant qu'on n'a pas les données (utilisé pour les component secondaires de l'interface comme FormSeeAlso)
    - afficher un placeholder animé du contenu (façon facebook, comme le component FormLexicalSenseList)
    - afficher le contenu en cache mais grisé pour indiquer qu'une requête de rafraichissement est en cours (utilisé pour le tableau des conrtibutions par exemple)

# Gestions des erreurs

Les erreurs à prendre en compte sont liées aux requêtes graphQL. Stratégie actuelle : 

- Pour un composant secondaire (comme FormSeeAlso ou FormTopPosts) on masque simplement le composant en cas d'erreur
- Pour un composant critique, on affiche un message d'erreur générique dans l'interface

Il faut penser à prévoir un cas de gestion d'erreur pour tous les composants qui utilisent `@withApollo` car sinon le comportement
par défaut fait afficher le message d'erreur brut dans l'interface utilisateur, ce qu'on veut éviter.

Dans tous les cas, il faut penser à produire une trace dans la console pour pouvoir débugger les erreurs.

# Services

Les services sont situés dans `./src/client/services`.

Mise en place de services RxJS. Chaque service exporte une classe (à utiliser quand on veut faire sa propre instance, ou créer
une classe child pour étendre les méthodes du service), et une instance singleton (l'instance partagée par défaut par les components de l'application).

Pour voir comment ça fonctionne, regarder l'implémentation de ces différents services : 

- `notificationService` : étends le service fourni par cortex-core. On peut voir dans le component `Login` (issue de cortex-core aussi) qu'on remplace
l'utilisation par défaut de l'instance de cortex-core par notre propre instance (dependency injection). Ce service est consommé par le component
`MobileErrorBanner` (qui est instancié par `MobileMainLayout`).
- `geolocService` : toute la logique autour de la géolocalisation de l'utilisateur. La logique RxJS est bien mise en valeur avec la transformation/fusion
de plusieurs flux d'évènements pour obtenir des Observables facile  à consommer par les components consommateurs.
- `menuService` : service minimaliste mais qui illustre la façon de partager un état global entre components sans passer par une machinerie de React Context
ou de redux.

Chaque component qui utilise un service implémente un pattern de dependency injection dans son constructeur. Voir par exemple `GeolocPicker`.
Dans le code application, on utilise le service `geolocService` par défaut, donc il n'y a pas besoin d'injecter spécifiquement le service à `GeolocPicker`
qui peut être déclaré comme ceci `<GeolocPicker />`. En revanche, dans les [tests unitaires](./components/widgets/__tests__/GeolocPicker.test.js), 
on souhaite utiliser une instance spécifique de `GeolocService` (avec des méthodes mockées), on crée donc notre propre instance 
`geolocServiceMock` et on l'injecte comme ceci `<GeolocPicker geolocService={geolocServiceMock} />`.


# Pages de contenu statique (page d'aide, credits, etc.)

Comme le contenu de ces pages contient beaucoup de texte fixe, avec du formattage léger (paragraphes, gras, peut être des titres), on a des components
qui gère le layout de la page : `InformationPage` pour les pages de types "Conditions générale d'utilisation"  et `HelpPage` pour les pages d'aide.

On génère ensuite le contenu à partir d'un fichier markdown situé dans `./assets/markdown` et une configuration webpack spécifique aux assets `.md` pour 
transformer les fichiers markdown en source HTML . Voir l'implémentation de `Gcu` (`./components/routes/Gcu.js`) et de `HowToSearch` (`./components/routes/help/HowToSearch.js`).

# Routing

L'appli utilise react-router, mais par dessus on a rajouté une petite couche d'ulititaire de routing maison, à défaut d'avoir trouvé une librairie satisfaisante.

## Définition des routes

Toutes les routes de l'appli doivent être déclarées dans `src/client/route.js`.
Dans ce fichier, on déclare le tableau `RoutesMetadata` qui a la forme suivante : 

```js
export const RoutesMetadata = [{
  name: 'MY_COLLECTION_ITEM',
  path: '/mycollection/:id',
  needsAuthentication: true
}, {
  ...
}]
```

Chaque object du tableau défini une route, avec les propriété obligatoires `name` et `path`. En plus, on peut rajouter des propriétés qui donnent des infos
supplémentaire sur la route (dans l'exemple `needsAuthentication`). Voir la doc dans le fichier `route.js` pour les détails. Ces propriétés supplémentaires sont
utilisées par le service `BrowsingHistoryHelperService`.

Le fichier `routes.js` exporte la variable `ROUTES`, générée à partir de `RoutesMetadata`, cet objet a le format suivant : 

```js
const ROUTES = {
  MY_COLLECTION_ITEM: '/mycollection/:id'
}
```

Il permet un accès plus simple aux routes, grace à son format de dictionnaire, comme on le voit dans la section suivante.

**Attention :** L'ordre des routes déclarées dans `RoutesMetadata` est important. La première route dont le path match la location actuelle sera
considéré comme la route actuelle. Il faut donc mettre les routes ordonnées de la plus spécifique à la moins spécifique. Exemple :

```js
// Bad ordering. The route "/items/5" matches both ITEMS and ITEM, and because ITEMS is first it will be considered to be the active route
const RoutesMetadata = [{
  name: 'ITEMS',
  path: '/items',
}, {
  name: 'ITEM',
  path: '/items/:id'
}]


// Good ordering. The route "/items/5" matches both ITEMS and ITEM, but ITEM is first so it is correctly considired as the current route.
// The route "/items/" matches only ITEMS, so it is not a problem if the algorithm checks it against ITEM first
const RoutesMetadata = [{
  name: 'ITEM',
  path: '/items/:id'
}, {
  name: 'ITEMS',
  path: '/items',
}]
```

## Routage courant avec react-router

L'utilisation courante des routes suit l'utilisation habituelle de react-router, en se basant sur l'objet `ROUTES` afin d'avoir une *single source of truth*.

1. Rendering conditionnée à une route, via react-router

```js
import {Route}  from 'react-router-dom';
import {ROUTES} from './routes.js';

// partie rendering 
<div class="myApp">
  <Route path={ROUTES.MY_COLLECTION_ITEM} component={CollectionItem}/>
</div>
```

2. Faire un lien dynamique vers une route

```js
import {formatRoute} from 'react-router-named-routes';
import {Link} from 'react-router-dom';
import {ROUTES} from './routes.js';

// partie rendering 
<Link to={formatRoute(ROUTES.MY_COLLECTION_ITEM, {id: itemId})}>
  Lien vers l'item {itemName}
</Link>
```


## Le service BrowsingHistoryHelperService

Le but de ce service et de garder un historique des routes visiter, afin de pouvoir facilement faire des retour arrière à l'utilisateur en fonction
des scénarios de navigation :

- De nombreux écrans sont designés de façon modale, d'un point de vue UX on peut les fermer et s'attendre à revenir à l'écran de l'espace de navigation
  principal sur lequel on se trouvait avant de se rendre sur un écran modal. (par exemple toutes les pages "mon compte", ou les pages de type "mentions légales").
- Les différents scénarios d'authentication : où rediriger l'utilisateur après un login, après un subscribe, après un logout, etc.

Pour que le service fonctionne, il faut mettre en haut de l'arbre React, un `<BrowsingHistoryHelperProvider>`. Ce provider doit se trouver en dessous du provider
de react-router `<BrowserRouter>` car il utilise l'ojbet `history` fournit par react-router. Ce provider premettra ensuite à chaque composant de l'application
d'accéder au service `BrowsingHistoryHelperService` en utilisant le hook `useBrowsingHistoryHelperService`.

### Exemple d'utilisation

```js
// main.js
<BrowserRouter>
  <BrowsingHistoryHelperProvider>
    <MyApp />
  </BrowsingHistoryHelperProvider>
</BrowserRouter>


// MyComponent.js
function MyComponent(props) {
  let browsingHistoryHelperService = useBrowsingHistoryHelperService();

  return <button onClick={login}>Login</button>;

  async function login() {
    await performLogin();
    history.push(browsingHistoryHelperService.getAfterLoginLocation());
  }
}
```

Le service `BrowsingHistoryHelperService` fournit plusieurs fonction utilitaires pour obtenir le path de location où rediriger l'utilisateur selon le scénario. 
Dans cet example `getAfterLoginLocation()`. Voir la documentation dans `src/client/services/BrowsingHistoryHelperService.js`.

# Problèmes identifiés


- L'animation de fermeture de menu lors de la navigation vers une nouvelle page ne fonctionne pas, car la navigation entraine le montage
  d'un nouveau component `MainMobileLayout`, hors c'est ce component qui contient le DOM du menu, donc le DOM du menu est supprimé (et remplacé par une 
  nouvelle instance) on ne le voit pas se refermer avec une animation (il disparait simplement). À voir si ça vaut le coup de réfléchir le code 
  différemment (avoir des components de menu et quelques autres DOM globaux comme les notifications par exemple qui soient plus haut que le routing dans 
  l'arbre des components). Mais attention à ne pas : 
    - faire ça au détriment de la lisibilité et la maintenabilité du code
    - y passer trop de temps de devoleppement
    
## Absence de code splitting et problème rencontré en voulant l'implémenter

Pour mettre en place le code splitting d'un point de vue programmation c'est assez simple. La config webpack actuelle est déjà prête à splitter le bundle
si des imports dynamiques sont fait dans le code JS.

En pratique, si on veut faire du code splitting au niveau du routage : 
chaque route correspond à un composant React. Quand un utilisateur n'a pas encore navigué vers la route en question, on veut s'éviter de charger
le composant (et tout ses descendants). Quand l'utilisateur veut naviguer vers une route non encore visitée, alors on peut charger dynamiquement
le composant et le rendre à l'écran. Pour celà on va utiliser un import dynamique plutôt que statique.

Le problème : il faut gérer le temps de chargement lié au téléchargement du composant React. D'un point de vue UX, on voudrait pouvoir rester
sur la page actuelle, tout en affichant une animation de chargement, puis basculer vers la nouvelle page une fois que le téléchargement est prêt.

Avec react-router, et la façon dont react permet le chargement dynamique de composants (via React.Lazy), c'est impossible. Le router
ne donne pas d'outil pour délayer la navigation vers une route, donc on va tout de suite avoir à l'écran l'affichage du composant pas encore chargé.
Et la seule solution que React propose pour gérer un composant pas encore chargé, c'est via `React.Suspense`, d'afficher un contenu statique de remplacement.

Ce n'est pas une solution acceptable d'un point de vue UX. 

Une solution serait de remplacer react-router par un router comme Curi.js (https://curi.js.org/) qui est plus centralisateur dans sa logique et procure
beaucoup d'outil pour avoir plus de flexibilité dans les solutions à mettre en place.

