import React from 'react';
import PropTypes from 'prop-types';
import {withTranslation} from 'react-i18next';
import {Link} from 'react-router-dom';
import classNames from 'classnames';
import {Helmet} from 'react-helmet-async';

import {ROUTES} from '../../routes';
import {ResponsiveMainLayout} from './ResponsiveMainLayout';
import {HelpMobileHeader} from '../mobile/MobileMainLayout/HelpMobileHeader';
import {DesktopSubHeader} from '../desktop/DesktopSubHeader';
import style from './HelpPage.styl';

/**
 * Usage : 
 *
 * <HelpPage
 *  title="Aide et documentation"
 *  subtitle="Effectuer une recherche simple"
 *  content={<div>My content</div>}
 * />
 *
 */
@withTranslation()
export class HelpPage extends React.Component {
  static propTypes = {
    /** The title of the page */
    title: PropTypes.string,
    /** The subtitle of the page */
    subtitle: PropTypes.string,
    /** The title of the help section */
    helpSectionTitle: PropTypes.string,
    /** A React node to insert as the main body of the help page */
    content: PropTypes.node,
    /** A color string among the application color palette as defined in forColors.styl */
    color: PropTypes.string
  }

  render() {
    const {color, t} = this.props;
    return (
      <React.Fragment>
        <Helmet>
          <title>{t('DOCUMENT_TITLES.HELP')}</title>
        </Helmet>
        <ResponsiveMainLayout mobileHeader={this.renderMobileHeader()} desktopUseDefaultGrid>
          {this.renderDesktopSubHeader()}
          <div className={classNames([style.content, style[color]])}>
            {this.props.content}
          </div>
        </ResponsiveMainLayout>
      </React.Fragment>
    );
  }


  renderMobileHeader() {
    return <HelpMobileHeader 
      title={this.props.title}
      search={true}
      ddfLogo={true} 
    />;
  }

  renderDesktopSubHeader() {
    const {t, color} = this.props;

    return (
      <DesktopSubHeader 
        primaryTitle={this.props.title}
        secondaryTitle={this.props.helpSectionTitle}
        backLinkText={t('HELP.DESKTOP.BACK_LINK')}
        backLinkTo={ROUTES.HELP_INDEX}
        color={color}
      />
    );
  }
}
