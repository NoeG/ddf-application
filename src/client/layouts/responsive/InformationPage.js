import React from 'react';
import {themr} from 'react-css-themr';
import {Helmet} from 'react-helmet-async';
import {useTranslation} from 'react-i18next';


import {SimpleModalPage} from './SimpleModalPage';
import style from './InformationPage.styl';

/**
 * Usage :
 *
 * <InformationPage 
 *   title="Mon titre" 
 *   content="Mon contenu" 
 *   mobileControls={['close','back]}
 *   onBack={callback}
 *   onClose={callback}
 *   theme={style}
 * />
 *
 *
 */
function InformationPage(props){
  const {t} = useTranslation();
  const {theme, title} = props;
  return (
    <React.Fragment>
      <Helmet>
        <title>{t('DOCUMENT_TITLES.GENERIC', {title})}</title>
      </Helmet>
      <SimpleModalPage {...props} theme={theme}/>
    </React.Fragment>
  );
}

const InformationPageExport = themr('InformationPage', style)(InformationPage);
export {InformationPageExport as InformationPage};
