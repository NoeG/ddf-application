import React from 'react';
import PropTypes from 'prop-types';

import {MobileMainLayout} from '../mobile//MobileMainLayout';
import {DesktopMainLayout} from '../desktop/DesktopMainLayout';
import {Desktop, Mobile} from '../MediaQueries';

export class ResponsiveMainLayout extends React.Component {

  static propTypes = {
    /** The element to use as header for the mobile layout. Passed as it is to MobileMainLayout */
    mobileHeader: PropTypes.node,
    /** The element to use as footer for the mobile layout. Passed as it is to MobileMainLayout */
    mobileFooter: PropTypes.node,
    /** 
     *  A flag that if provided, will put the props.children content into a DesktopGrid, otherwise the content
     *  will be inserted as is in the desktop layout
     */
    desktopUseDefaultGrid: PropTypes.bool,
    /**
     * This property is passed to DesktopHeader
     */
    desktopHeaderHideSearch: PropTypes.bool,
    /** A style object as provided by CSS modules. The classes used are : ".mobileMainLayoutContainer" */
    theme: PropTypes.object,
    /** The current form written representation, used for the "improve DDF" button (desktop layout) */
    currentForm: PropTypes.string
  }


  render() {
    const {currentForm} = this.props;
    return (
      <>
        <Desktop>
          <DesktopMainLayout 
            useDefaultGrid={this.props.desktopUseDefaultGrid}
            headerHideSearch={this.props.desktopHeaderHideSearch}
            currentForm={currentForm}
          >
            {this.props.children}
          </DesktopMainLayout>
        </Desktop>
        <Mobile>
          <MobileMainLayout header={this.props.mobileHeader} footer={this.props.mobileFooter} theme={this.props.theme}>
            {this.props.children}
          </MobileMainLayout>
        </Mobile>
      </>
    );
  }
}
