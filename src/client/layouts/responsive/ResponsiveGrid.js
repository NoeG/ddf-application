import React from 'react';
import PropTypes from 'prop-types';

import {DesktopGrid} from '../desktop/DesktopGrid';
import {Desktop, Mobile} from '../MediaQueries';

/**
 *
 * Use the DesktopGrid if desktop, otherwise just use the provided children content
 *
 */
export class ResponsiveGrid extends React.Component {

  static propTypes = {
    desktopSideColumn: PropTypes.node,
    desktopFirstSection: PropTypes.node
  };

  render() {
    const {desktopSideColumn, desktopFirstSection, children} = this.props;

    return (
      <>
        <Desktop>
          <DesktopGrid sideColumn={desktopSideColumn} firstSection={desktopFirstSection}>
            {children}
          </DesktopGrid>
        </Desktop>
        <Mobile>
          {children}
        </Mobile>
      </>
    );
  }

}
