import React                  from 'react';
import PropTypes              from 'prop-types';
import classNames             from 'classnames';
import {useTranslation}       from 'react-i18next';
import {formatRoute}          from 'react-router-named-routes';
import {Link, useRouteMatch}  from 'react-router-dom';
import StickyFooter           from 'react-sticky-footer';

import {ROUTES}           from '../../../routes';
import {ImproveDdfButton} from '../../../components/widgets/ImproveDdfButton';

import ddfLogo from '../../../assets/images/ddf_logo.png';
import creativeCommonsLogo from '../../../assets/images/creative_commons.svg';
import mcLogo from '!responsive-loader?size=120!../../../assets/images/partners/MIN_Culture_RVB.png';
import iifLogo from '!responsive-loader?size=250!../../../assets/images/partners/Kx300_Logo_2iF_VECTO.jpg';
import oifLogo from '../../../assets/images/partners/OIF.jpg';
import style from './DesktopFooter.styl';
import {footerLinksBannerHeight, footerPartnersLinksHeight} from './DesktopFooter.vars.json';

DesktopFooter.propTypes = {
  /** The current form written representation, used for the "improve DDF" button */
  currentForm: PropTypes.string
};
export function DesktopFooter(props) {
  const {t} = useTranslation();
  const {currentForm} = props;
  const shrinkableFooterHeight = footerLinksBannerHeight + footerPartnersLinksHeight;
  const ddfButtonDisplayed = useDdfButton();

  return (
    <React.Fragment>
      <If condition={ddfButtonDisplayed}>
        <StickyFooter stickyStyles={{left: '0', right: '0', pointerEvents: 'none', zIndex: '5'}} bottomThreshold={shrinkableFooterHeight}>
          <div className={style.improveSection}>
            <ImproveDdfButton currentForm={currentForm}/>
          </div>
        </StickyFooter>
      </If>
      <If condition={!ddfButtonDisplayed}>
        <div className={style.improveSectionFiller}></div>
      </If>

      <div className={style.mainSection}>
        <div className={classNames([style.footerSection, style.logo])}>
          <img className={style.ddfLogo} src={ddfLogo} />
        </div>

        <div className={classNames([style.footerSection, style.links])}>
          <div className={style.widthContainer}>
            <Link to={ROUTES.PRESENTATION}>{t('DESKTOP_FOOTER.PRESENTATION')}</Link>
            <Link to={ROUTES.GCU}>{t('DESKTOP_FOOTER.GCU')}</Link>
            <Link to={ROUTES.CREDITS}>{t('DESKTOP_FOOTER.CREDITS')}</Link>
            <Link to={ROUTES.PARTNERS}>{t('DESKTOP_FOOTER.PARTNERS')}</Link>
          </div>
        </div>

        <div className={classNames([style.footerSection, style.licence])}>
          <img className={style.creativeCommonsLogo} src={creativeCommonsLogo} />
        </div>
      </div>


      <div className={style.partnersSection}>
        <div className={style.caption}>{t('DESKTOP_FOOTER.OUR_PARTNERS')}</div>
        <div className={style.logos}>
          <img className={style.logo} src={mcLogo}/> 
          <img className={style.logo} src={iifLogo}/> 
          <img className={style.logo} src={oifLogo}/> 
        </div>
      </div>
    </React.Fragment>
  );

  /**
   * React hook. Returns boolean, true if the "improve DDF" button must be displayed, false otherwise
   */
  function useDdfButton() {
    /** List of routes for which the improve ddf button should not appear */
    const formLexicalSenseEditRouteMatch = useRouteMatch(ROUTES.FORM_LEXICAL_SENSE_EDIT);
    const createLexicalSenseRouteMatch = useRouteMatch(ROUTES.CREATE_LEXICAL_SENSE);
    return !(formLexicalSenseEditRouteMatch || createLexicalSenseRouteMatch);
  }

}
