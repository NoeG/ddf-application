import React, {useState}  from 'react';
import classNames         from 'classnames';
import PropTypes          from 'prop-types';
import {useTranslation}   from 'react-i18next';
import {formatRoute}      from 'react-router-named-routes';
import {useHistory, Link} from 'react-router-dom';
import {useObservable}    from 'react-use';
import {ParseNewlines} from '@mnemotix/synaptix-client-toolkit';

import {ROUTES} from '../../../routes';
import {FormSearchInput} from '../../../components/widgets/FormSearchInput';
import {BorderedButton} from '../../../components/widgets/BorderedButton';
import {getUserAuthenticationService} from '../../../services/UserAuthenticationService';

import ddfLogo from '../../../assets/images/ddf_logo.png';
import geolocMarkerIcon from '../../../assets/images/geoloc_marker.svg';
import helpIcon from '../../../assets/images/desktop_header/help.svg';
import accountIcon from '../../../assets/images/desktop_header/account.svg';
import style from './DesktopHeader.styl';


DesktopHeader.propTypes = {
  /**
   * Boolean flag to hide the search bar and search button from the header. By default, the search is visible.
   */
  hideSearch: PropTypes.bool
}
export function DesktopHeader(props) {
  const {hideSearch} = props;
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const {t} = useTranslation();
  const history = useHistory();
  const {isLogged, user} = useObservable(userAuthenticationService.currentUser, {isLogged: false});
  const [searchQuery, updateSearchQuery] = useState('');


  return (
    <div className={style.header}>
      <Link className={classNames([style.headerSection, style.title])} to={"/"}>
        <img className={style.ddfLogo} src={ddfLogo} />
        <div className={style.ddfTitle}>
          <ParseNewlines text={t('DESKTOP_HEADER.TITLE')}/>
        </div>
      </Link>

      <div className={classNames([style.headerSection, style.searchBarSection])}>
        <If condition={!hideSearch}>
          <div className={style.searchBarContainer}>
            <FormSearchInput 
              size="medium" 
              placeholder={t('DESKTOP_HEADER.SEARCH_PLACEHOLDER')}
              onChange={(e) => updateSearchQuery(e.target.value)}
              onSubmit={performSearch}
              autofocus={false}
            />
          </div>
          <Link to="/">
            <img className={style.geolocMarkerIcon} src={geolocMarkerIcon}/>
          </Link>
          <BorderedButton 
            className={style.searchButton}
            text={t('DESKTOP_HEADER.SEARCH_BUTTON')} theme={style}
            onClick={performSearch}
          />
        </If>
      </div>

      <div className={classNames([style.headerSection, style.buttons])}>
        <If condition={user?.userAccount?.isAdmin || user?.userAccount?.isOperator}>
          <Link className={style.textualLink} to={ROUTES.ADMIN} title={t('DESKTOP_HEADER.TOOLTIPS.ADMIN')}>
            Admin
          </Link>
        </If>
        <Link className={style.button} to={ROUTES.HELP_INDEX} title={t('DESKTOP_HEADER.TOOLTIPS.HELP')}>
          <img src={helpIcon} />
        </Link>
        <Link 
          className={style.button} 
          to={isLogged ? ROUTES.MY_ACCOUNT_PROFILE : ROUTES.LOGIN}
          title={t(`DESKTOP_HEADER.TOOLTIPS.${isLogged ? 'MY_ACCOUNT' : 'LOGIN'}`)}
        >
          <img src={accountIcon} />
        </Link>
      </div>
    </div>
  );

  function performSearch() {
    if(!searchQuery) {
      return;
    }
    history.push(formatRoute(ROUTES.FORM_SEARCH, {formQuery: searchQuery}));
  }
}
