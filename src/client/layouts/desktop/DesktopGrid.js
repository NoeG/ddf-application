import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import style from './DesktopGrid.styl';

/**
 * This is a grid to insert into the main desktop layout content section. This grid handles the generic layout for the Ddf desktop pages
 * which can be either : 
 *
 *
 *        +-------------------------------------------+
 *        |                                           |
 *        |    First section                          |
 *        |                                           |
 *        +-------------------------------------------+
 *        |                                           |
 *        |    Content                                |
 *        |                                           |
 *        |                                           |
 *        |                                           |
 *        |                                           |
 *        |                                           |
 *        |                                           |
 *        +-------------------------------------------+
 *
 *
 * Or : 
 *
 *  +-----------------------------------------+
 *  |                                         |
 *  |     First section                       |
 *  |                                         |
 *  +-----------------------------------------+--------------+
 *  |                                         |              |
 *  |     Content                             | Side         |
 *  |                                         | column       |
 *  |                                         |              |
 *  |                                         |              |
 *  |                                         |              |
 *  |                                         |              |
 *  |                                         |              |
 *  |                                         |              |
 *  |                                         |              |
 *  +-----------------------------------------+--------------+
 *
 */


/**
 *
 * Usage :
 *
 * <DesktopGrid
 *    sideColumn={SideColumnContent} 
 *    firstSection={FirstSectionContent}
 * >
 *   main content
 *
 * </DesktopGrid>
 *
 * If a side column is given, the layout will be 3/4 for main content and 1/4 for side column content,
 * otherwise the layout is a single centered column.
 *
 * If a first section is given, it will be on top of the main content with the same width, but the side column
 * will be aligned with the main content
 */
export class DesktopGrid extends React.Component {

  static propTypes = {
    sideColumn: PropTypes.node,
    firstSection: PropTypes.node
  };

  render() {
    const {firstSection, sideColumn} = this.props;

    return (
      <div className={classNames([style.grid, {[style.withSideColumn]: sideColumn}])}>
        <If condition={firstSection}>
          <div className={style.firstSection}>
            {firstSection}
          </div>
        </If>

        <div className={style.row}>
          <div className={style.mainContent}>
            {this.props.children}
          </div>
          <If condition={sideColumn}>
            <div className={style.sideColumn}>
              {sideColumn} 
            </div>
          </If>
        </div>
      </div>
    );
  }

}
