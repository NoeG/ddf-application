import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import {DesktopHeader}  from './DesktopMainLayout/DesktopHeader';
import {DesktopFooter}  from './DesktopMainLayout/DesktopFooter';
import {DesktopGrid}    from './DesktopGrid';
import {CookieDisclaimer} from '../../components/general/CookieDisclaimer';

import style from './DesktopMainLayout.styl';


/**
 *
 * Usage :
 *
 * <DesktopMainLayout>
 *   Content
 * </DesktopMainLayout>
 *
 */
export class DesktopMainLayout extends React.Component {

  static propTypes = {
    /**
     * If true, the content will be inserted into a DesktopGrid component, with only one column centered. Otherwise,
     * the content will be inserted as it is without being wrapped in a grid
     */
    useDefaultGrid: PropTypes.bool,
    /**
     * This property is passed to DesktopHeader
     */
    headerHideSearch: PropTypes.bool,
    /** The current form written representation, used for the "improve DDF" button */
    currentForm: PropTypes.string,
    hideContributionFooter: PropTypes.bool,
  };

  render() {
    const {headerHideSearch, useDefaultGrid, currentForm, hideContributionFooter = false} = this.props;
    const GridComponent = useDefaultGrid ? DesktopGrid : React.Fragment;

    return (
      <React.Fragment>
        <CookieDisclaimer/>
        <DesktopHeader hideSearch={headerHideSearch}/>
        <div className={style.content}>
          <GridComponent>
            {this.props.children}
          </GridComponent>
        </div>
      
        <If condition={!hideContributionFooter}>
          <DesktopFooter currentForm={currentForm}/>
        </If>
      </React.Fragment>
    );
  }
}

