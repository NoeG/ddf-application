import React from 'react';
import ReactResponsive, {useMediaQuery} from 'react-responsive';
import mediaQueries from '../assets/stylesheets/mediaQueries.json';

export const Desktop = props => <ReactResponsive {...props} minWidth={mediaQueries.desktopMinWidth} />;
export const Mobile = props => <ReactResponsive {...props} maxWidth={mediaQueries.mobileMaxWidth} />;

/**
 * Hook returning the object 
 *
 * {
 *   isDesktop: <boolean>,
 *   isMobile: <boolean>
 * }
 */
export function useMediaQueries() {
  let isDesktop = useMediaQuery({minWidth: mediaQueries.desktopMinWidth});
  return {
    isDesktop,
    isMobile: !isDesktop
  };
}
