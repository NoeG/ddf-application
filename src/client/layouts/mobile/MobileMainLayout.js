import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import {MobileHeader} from './MobileMainLayout/MobileHeader';
import {menuService} from '../../services/MenuService';
import {MobileMenu} from './MobileMainLayout/MobileMenu';
import {CookieDisclaimer} from '../../components/general/CookieDisclaimer';

import style from './MobileMainLayout.styl';

/**
 * Usage : 
 *
 * <MobileMainLayout
 *   header={<MobileHedear />}
 *   footer={<MobileFooter />}
 *   theme={style}
 * />
 *
 */
export class MobileMainLayout extends React.Component {
  static defaultProps = {
    theme: {}
  }

  static propTypes = {
    /** The element to use as header for the mobile layout. If not provided, it uses MobileHeader by default */
    header: PropTypes.node,
    /** The element to use as footer for the mobile layout. If not provided, no footer is used */
    footer: PropTypes.node,
    /** A style object as provided by CSS modules. The classes used are : ".mobileMainLayoutContainer", ".mobileMainLayoutContentContainer" */
    theme: PropTypes.object
  };

  state = {
    leftPanelOpened: false,
  }

  componentDidMount() {
    this._msSub = menuService.displayStatus.subscribe(displayStatus => {
      this.setState({leftPanelOpened: displayStatus === 'open'})
    });    
  }

  componentWillUnmount() {
    this._msSub.unsubscribe();
  }

  render() {
    const {theme, header, children, footer} = this.props;
    return (
      <>
        <div className={classNames([
          style.mainContainer, 
          theme.mobileMainLayoutContainer,
          {
            [style.freeze]: this.state.leftPanelOpened
          }
        ])}>

          <CookieDisclaimer/>

          <div className={style.mobileHeader}>
            {header || <MobileHeader/>}
          </div>

          <div className={classNames([style.contentContainer, theme.mobileMainLayoutContentContainer])}>
            {children}
          </div>

          <If condition={footer}>
            <div className={style.mobileFooter}>
              {footer}
            </div>
          </If>

          <div className={classNames([style.foldingLeftPanelContainer, {[style.closed]: !this.state.leftPanelOpened}])}>
            <div className={style.leftPanel}>
              <MobileMenu />
            </div>
          </div>
        </div>

      </>
    );
  }
}

