import React from 'react';

import style from './FullScreenOverlay.styl';

export class FullScreenOverlay extends React.Component {

  render () {
    return (
      <div className={style.container}>
        {this.props.children}
      </div>
    );
  }
}
