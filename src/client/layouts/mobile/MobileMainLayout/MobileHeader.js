import React from 'react';
import classNames from 'classnames';
import PropTypes from "prop-types";
import {withRouter, Link} from 'react-router-dom';
import {formatRoute}      from 'react-router-named-routes';

import {ROUTES} from '../../../routes';
import {FormSearchInput} from '../../../components/widgets/FormSearchInput';
import {getGeolocService} from '../../../services/GeolocService';
import {getPlaceRegionInfo} from '../../../utilities/helpers/places';
import {menuService} from '../../../services/MenuService';
import style from './MobileHeader.styl';
import burgerMenuIcon from '../../../assets/images/burger_menu.svg';
import searchIcon from '../../../assets/images/search.svg';
import geolocMarkerIcon from '../../../assets/images/geoloc_marker.svg';
import geolocMarkerWhiteIcon from '../../../assets/images/geoloc_marker_white.svg';
import ddfLogo from '../../../assets/images/ddf_logo.svg';


/**
 *
 * Usage :
 *
 * - With content
 *   
 *   <MobileHeader content={<div>my content</div>} />
 *
 * - With search field
 *
 *   <MobileHeader search={true} />
 *
 * - With geolocation marker
 *
 *   <MobileHeader geolocMarker={true} />
 *
 * - With ddf logo
 *
 *   <MobileHeader ddfLogo={true} />
 *
 */
@withRouter
export class MobileHeader extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    search: PropTypes.bool,
    geolocMarker: PropTypes.bool,
    currentForm: PropTypes.string,
    content: PropTypes.node,
    ddfLogo: PropTypes.bool,
    geolocService: PropTypes.object
  }
  
  constructor(props) {
    super(props);
    this.state = {
      searchOpened: false,
      searchQuery: '',
      currentPlace: {name: ''},
      geolocOpened: false
    }
    this.searchFieldRef = React.createRef();
    this.geolocService = props.geolocService || getGeolocService();
  }

  componentDidMount() {
    this._cpSub = this.geolocService.currentPlace.subscribe(currentPlace => this.setState({currentPlace}));
  }

  componentWillUnmount() {
    this._cpSub.unsubscribe();
  }

  render() {
    return (
      <>
        {this.renderGeolocInformationHeader()}

        <div className={classNames([style.container, style.withSearchClosed])}>
          {this.renderPreContentActions()}
          {this.renderContent()}
          {this.renderPostContentActions()}
          {this.renderSearchOpenedOverlay()}

        </div>
      </>
    );
  }

  renderContent() {
    const {currentForm, content, search} = this.props;
    let self = this;

    /* 
     * Setup a click handler on the content, to open the search field pre populated with the current form. Behavior only for a mobile header
     * with searchbar enabled and a current form specified
     */
    if (search && currentForm) {
      return (
        <div onClick={contentClickHandler}>
          {this.props.content}
        </div>
      );
      function contentClickHandler() {
        if (search && currentForm) {
          self.openSearchForm({inputValue: currentForm});
        }
      }
    } else {
      return this.props.content;
    }
  }

  renderGeolocInformationHeader() {
    if (this.props.geolocMarker)  {
      return (
        <div className={classNames([style.geolocInfoHiderContainer, {[style.hidden]: !this.state.geolocOpened}])}>
          <div className={style.geolocInfo} onClick={() => this.props.history.push("/")}>
            <div className={style.geolocIcon}>
              <img src={geolocMarkerWhiteIcon}/>
            </div>
            <span className={style.locationText}>
              <span className={style.placeName}>{this.state.currentPlace.name}</span>
              <span className={style.regionName}>&nbsp;{getPlaceRegionInfo(this.state.currentPlace)}</span>
            </span>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }


  renderPostContentActions({searchOpenMode} = {}) {
    return (
      <div className={classNames([style.actions, style.postContent])}>
        {this.renderSearchField(searchOpenMode)}
        {this.renderGeolocMarker({searchOpenMode})}
        <button className={style.menuButton} onClick={() => menuService.toggleMenu()}>
          <img src={burgerMenuIcon}/>
        </button>
      </div>
    );
  }

  renderPreContentActions() {
    if (this.props.ddfLogo) {
      return (
        <div className={classNames([style.actions, style.preContent])}>
          <Link to="/" className={style.ddfLogo}>
            <img src={ddfLogo}/>
          </Link>
        </div>
      );
    } else {
      return null;
    }
  }

  renderSearchOpenedOverlay() {
    if (this.props.search) {
      return (
        <div className={classNames([
          style.container, 
          style.withSearchOpened,
          {
            [style.hidden]: !this.state.searchOpened
          }
        ])}>
          {this.renderPostContentActions({searchOpenMode: true})}
          <div className={style.backgroundOverlay}/>
        </div>
      );
    } else {
      return null;
    }
  }


  renderSearchField(searchOpenMode) {
    if (this.props.search) {
      return searchOpenMode ? this.renderOpenedSearchField() : this.renderSearchButton();
    } else {
      return null;
    }
  }

  renderSearchButton() {
    return (
      <div className={style.searchButton} onClick={() => this.openSearchForm()}>
        <img src={searchIcon}/>
      </div>
    );
  }

  renderOpenedSearchField() {
    return (
      <>
        <div className={style.searchBarContainer}>
          <FormSearchInput 
            theme="dark" 
            size="medium" 
            closeButton={true}
            onClose={() => this.closeSearchForm()}
            onChange={(e) => this.setState({searchQuery: e.target.value})}
            onSubmit={() => this.performSearch()}
            ref={this.searchFieldRef}
          />
        </div>
      </>
    );
  }

  openSearchForm({inputValue} = {}) {
    this.setState({
      searchOpened: true
    });
    this.searchFieldRef.current.resetInput({inputValue});
  }

  closeSearchForm() {
    this.setState({
      searchOpened: false
    });
  }

  performSearch() {
    if(!this.state.searchQuery) {
      return;
    }
    this.props.history.push(formatRoute(ROUTES.FORM_SEARCH, {formQuery: this.state.searchQuery}));
    this.closeSearchForm();
  }

  renderGeolocMarker({searchOpenMode}) {
    if (!this.props.geolocMarker) {
      return null;
    }
    let icon = searchOpenMode ? geolocMarkerWhiteIcon : geolocMarkerIcon;

    return (
      <div className={style.geolocButton} onClick={() => this.toggleGeolocInfo()}>
        <img src={icon}/>
      </div>
    );
  }

  toggleGeolocInfo() {
    this.setState({
      geolocOpened: !this.state.geolocOpened
    });
  }
}
