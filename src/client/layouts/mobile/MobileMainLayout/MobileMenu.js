import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {useTranslation} from 'react-i18next';
import {useHistory, withRouter, Link} from 'react-router-dom';
import {useObservable} from 'react-use';

import {ROUTES} from '../../../routes';
import {menuService} from '../../../services/MenuService';
import {getUserAuthenticationService} from '../../../services/UserAuthenticationService';
import {notificationService as appNotificationService} from '../../../services/NotificationService';
import {useBrowsingHistoryHelperService} from '../../../services/BrowsingHistoryHelperService';

import style from './MobileMenu.styl';

import gcuIcon from '../../../assets/images/gcu_icon.svg';
import helpIcon from '../../../assets/images/help_icon.svg';
import ddfLogo from '../../../assets/images/ddf_logo.svg';
import contributionsIcon from '../../../assets/images/contributions.svg';
import settingsIcon from '../../../assets/images/settings.svg';
import creditsIcon from '../../../assets/images/credits_icon.svg';
import partnersIcon from '../../../assets/images/partners_icon.svg';

@withRouter
class MenuLink extends React.Component {
  static propTypes = {
    to: PropTypes.string.isRequired
  }

  constructor(props) {
    super(props);
    this.navigate = this.navigate.bind(this);
  }

  render() {
    return (
      <Link to={this.props.to} onClick={this.navigate} className={style.menuEntry}>
        {this.props.children}
      </Link>
    );
  }

  navigate(evt) {
    menuService.closeMenu();
  }
}

export function MobileMenu(props) {
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const notificationService = props.notificationService || appNotificationService;
  const history = useHistory();
  const {t} = useTranslation();
  const {isLogged, user} = useObservable(userAuthenticationService.currentUser, {});
  const {logout} = userAuthenticationService.useLogout();
  const {browsingHistoryHelperService} = useBrowsingHistoryHelperService();

  return (
    <div className={classNames([style.container, {[style.noLoggedUser]: !isLogged}])}>
      {renderCurrentUserSection()}
      <MenuLink to={ROUTES.PRESENTATION}>
        <div className={classNames([style.icon, style.ddfIcon])}>
          <img src={ddfLogo}/>
        </div>
        <div className={style.label}>
          {t('MOBILE_MENU.PRESENTATION')}
        </div>
      </MenuLink>
      <MenuLink to={ROUTES.GCU}>
        <div className={style.icon}>
          <img src={gcuIcon}/>
        </div>
        <div className={style.label}>
          {t('MOBILE_MENU.GCU')}
        </div>
      </MenuLink>
      <MenuLink to={ROUTES.CREDITS}>
        <div className={style.icon}>
          <img src={creditsIcon}/>
        </div>
        <div className={style.label}>
          {t('MOBILE_MENU.CREDITS')}
        </div>
      </MenuLink>
      <MenuLink to={ROUTES.PARTNERS}>
        <div className={style.icon}>
          <img src={partnersIcon}/>
        </div>
        <div className={style.label}>
          {t('MOBILE_MENU.PARTNERS')}
        </div>
      </MenuLink>
      <MenuLink to={ROUTES.HELP_INDEX}>
        <div className={style.icon}>
          <img src={helpIcon}/>
        </div>
        <div className={style.label}>
          {t('MOBILE_MENU.HELP')}
        </div>
      </MenuLink>
    </div>
  );

  function renderCurrentUserSection() {
    if (isLogged) {
      return (
        <React.Fragment>
          <div className={style.userSection}>
            <div className={style.username}>{user.nickName}</div>
            <div className={style.email}>{user.userAccount.username}</div>
          </div>
          <MenuLink to={ROUTES.MY_ACCOUNT_PROFILE}>
            <div className={style.icon}>
              <img src={settingsIcon}/>
            </div>
            <div className={style.label}>
              {t('MOBILE_MENU.MY_ACCOUNT')}
            </div>
          </MenuLink>
          <MenuLink to={ROUTES.MY_ACCOUNT_CONTRIBUTIONS}>
            <div className={style.icon}>
              <img src={contributionsIcon}/>
            </div>
            <div className={style.label}>
              {t('MOBILE_MENU.MY_ACCOUNT_CONTRIBUTIONS')}
            </div>
          </MenuLink>
          <If condition={user.userAccount.isOperator || user.userAccount.isAdmin}>
            <hr/>
            <MenuLink to={ROUTES.CONTRIBUTIONS_REVIEW}>
              <div className={style.icon}>
              </div>
              <div className={style.label}>
                {t('MOBILE_MENU.OPERATORS.CONTRIBUTIONS')}
              </div>
            </MenuLink>
            <If condition={user.userAccount.isAdmin}>
              <MenuLink to={ROUTES.ADMIN_USERS}>
                <div className={style.icon}>
                </div>
                <div className={style.label}>
                  {t('MOBILE_MENU.ADMIN.USERS')}
                </div>
              </MenuLink>
            </If>
            <hr/>
          </If>

          <a href="#" onClick={handleLogout} className={style.menuEntry}>
            <div className={style.icon}>
            </div>
            <div className={style.label}>
              {t('MOBILE_MENU.LOGOUT')}
            </div>
          </a>
          <hr/>
        </React.Fragment>
      )
    } else {
      return (
        <React.Fragment>
          <MenuLink to={ROUTES.LOGIN}>
            <div className={style.icon}>
              <img src={settingsIcon}/>
            </div>
            <div className={style.label}>
              {t('MOBILE_MENU.LOGIN')}
            </div>
          </MenuLink>
          <MenuLink to={ROUTES.SUBSCRIBE}>
            <div className={style.icon}>
              <img src={contributionsIcon}/>
            </div>
            <div className={style.label}>
              {t('MOBILE_MENU.SUBSCRIBE')}
            </div>
          </MenuLink>
          <hr/>
        </React.Fragment>
      );
    }
  }

  async function handleLogout(evt) {
    evt.preventDefault();
    menuService.closeMenu();
    let res = await logout();
    if (res.success) {
      /* 
       * Redirect strategy : if the current page is not suited for logged out user, redirect to index, otherwise
       * don't redirect
       */
      let loc = browsingHistoryHelperService.getAfterLogoutLocation();
      if (!loc) {
        history.push('/');
      }
    }
    if (res.globalErrorMessage) {
      notificationService.error(res.globalErrorMessage);
    }
  }
}
