import React from 'react';

import {MobileHeader} from './MobileHeader';
import style from './HelpMobileHeader.styl';

export class HelpMobileHeader extends React.Component {
  render() {
    return (
      <MobileHeader
        search={this.props.search}
        geolocMarker={this.props.geolocMarker}
        ddfLogo={this.props.ddfLogo}
        backButton={this.props.backButton}
        onBack={this.props.onBack}
        content={this.renderTitle()}
      />
    );
  }

  renderTitle() {
    return (
      <If condition={this.props.title}>
        <div className={style.title}>
          {this.props.title} 
        </div> 
      </If>
    );
  }

}
