import React from 'react';
import PropTypes from 'prop-types';
import {withTranslation} from 'react-i18next';

import {ImproveDdfButton} from '../../../components/widgets/ImproveDdfButton';

import style from './MobileFooter.styl';

@withTranslation()
export class MobileFooter extends React.Component {
  static propTypes = {
    currentForm: PropTypes.string,
  }

  render() {
    const {currentForm} = this.props;
    return (
      <div className={style.placeholder}>
        <div className={style.fixedContainer}>
          <ImproveDdfButton currentForm={currentForm}/>
        </div>
      </div>
    );
  }
}
