import React from 'react';
import PropTypes from 'prop-types';

import {MobileHeader} from './MobileHeader';
import style from './FormMobileHeader.styl';

export class FormMobileHeader extends React.Component {
  static propTypes = {
    geolocMarker: PropTypes.bool,
    search: PropTypes.bool,
    currentForm: PropTypes.string
  }

  render() {
    const {search, geolocMarker, currentForm} = this.props;
    return (
      <MobileHeader
        search={search}
        geolocMarker={geolocMarker}
        content={this.renderTitle()}
        currentForm={currentForm}
      />
    );
  }

  renderTitle() {
    const {currentForm} = this.props;
    if (currentForm) {
      return (
        <div className={style.title}>
          {currentForm} 
        </div> 
      )
    } else {
      return null;
    }
  }
}
