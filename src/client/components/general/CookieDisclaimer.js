import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {useCookies} from 'react-cookie';

import {Button} from '../widgets/forms/Button';

import style from './CookieDisclaimer.styl';

export function CookieDisclaimer(props) {
  const COOKIE_NAME = 'ddf-cookies-disclaimer';
  const COOKIE_DURATION = 365;

  const [cookies, setCookie] = useCookies([COOKIE_NAME]);

  /**
   * If cookie exists, the user has already validated the disclaimer, we display nothing
   */
  if (cookies[COOKIE_NAME]) return null;
  
  return (
    <div className={style.container}>
      <div className={style.text}>
        Ce site utilise des cookies afin d'analyser le trafic des visites. Aucune information personnelle permettant de vous identifier 
        n'est partagée
      </div>

      <div className={style.buttonArea}>
        <Button 
          className={style.button}
          onClick={validateDisclaimer}
        >
          Je valide
        </Button>
      </div>
    </div>
  );

  function validateDisclaimer() {
    setCookie(COOKIE_NAME, 'accepted', {
      expires: new Date(Date.now() + 1000*60*60*24*COOKIE_DURATION),
    });
  }
}
