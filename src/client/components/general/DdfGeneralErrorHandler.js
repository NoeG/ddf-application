import React from 'react';
import {withRouter} from 'react-router-dom';

import {ErrorHandler} from './ErrorHandler';
import {DdfLoadingSplashScreen} from './DdfLoadingSplashScreen';
import style from './DdfGeneralErrorHandler.styl';

@withRouter
export class DdfGeneralErrorHandler extends ErrorHandler {
  render() {
    if (this.state.hasError) {
      return (
        <DdfLoadingSplashScreen>
          <div className={style.error}>
            Un problème technique nous empêche d'afficher ce contenu.
          </div>
        </DdfLoadingSplashScreen>
      );
    }
    return this.props.children;
  }
}

