import React from 'react';

import DdfLogo from '../../assets/images/ddf_logo.png';
import style from './DdfLoadingSplashScreen.styl';

export class DdfLoadingSplashScreen extends React.Component {
  render() {
    return (
      <div className={style.screen}>
        <div className={style.logo}>
          <img src={DdfLogo}/> 
        </div>
        {this.props.children}
      </div>
    );
  }
}
