import React, {useState, useEffect, useRef} from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import {notificationService as appNotificationService} from '../../services/NotificationService';

import style from './NotificationsDisplay.styl';
import crossWhite from '../../assets/images/cross_white.svg';

let messageCounter = 0;

export function NotificationsDisplay(props) {
  const notificationService = props.notificationService || appNotificationService;
  const [messages, updateMessages] = useState([]);
  const closeButtonRef = useRef();

  useEffect(() => {
    const subscription = notificationService.messages$.subscribe((newMessage) => {
      newMessage.id = messageCounter++; 
      updateMessages(currentMessages => currentMessages.concat([newMessage]));
    });
    return () => subscription.unsubscribe();
  }, [notificationService.messages$]);

  useEffect(() => {
    if (closeButtonRef.current) {
      closeButtonRef.current.focus();
    }
  });


  return messages.map(renderMessage);

  function renderMessage(message, index, messages) {
    let isLast = (index === (messages.length - 1));

    return (
      <div key={message.id} className={style.container} onClick={() => closeMessage(message)}>
        <div className={classNames([style.messageContainer, style[message.type]])} onClick={e => messageBoxClicked(e, message)}>
          <button className={style.closeButton} onClick={() => closeMessage(message)} ref={closeButtonRef}>
            <img src={crossWhite} />
          </button>
          <div className={style.text}>{message.message}</div>
          <If condition={message.confirm}>
            <div className={style.confirmControls}>
              <div className={style.control} onClick={() => closeMessage(message, {choice: 'yes'})}>{message.yesText}</div>
              <div className={style.control} onClick={() => closeMessage(message, {choice: 'no'})}>{message.noText}</div>
            </div>
          </If>
        </div>
      </div>
    )
  }

  /**
   * Different strategies depending on the type of message : 
   *
   * - If this is a message without confirmation controls, any click in the message box closes the message (the same way cliking outside does)
   * - If this is a message with confirmation, a click in the message box does nothing
   */
  function messageBoxClicked(evt, message) {
    evt.stopPropagation();
    if (!message.confirm) {
      closeMessage(message);
    }
  }

  function closeMessage(message, {choice} = {}) {
    if (!choice) {
      /* 
       * In case of "close" action (without clicking the confirmation controls, but by clicking on the cross or outside the box,
       * if this is a confirmation message, consider it to be a "no" confirmation, if this is a message without confirmation controls,
       * consider it to be a "yes" confirmation.
       */
      choice = message.confirm ? 'no' : 'yes';
    }
    choice === 'no' ?  message.confirmWithNo() : message.confirmWithYes();
    updateMessages((currentMessages) => {
      return currentMessages.filter(a => a.id != message.id);
    });
  }
}
