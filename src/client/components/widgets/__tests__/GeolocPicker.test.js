import React                          from 'react';
import {render, cleanup, fireEvent}   from '@testing-library/react';
import waait                          from 'waait';
import {Subject, throwError}          from 'rxjs';

import {renderWithMocks}  from '../../../../../jest/utilities/renderWithMocks';
import {GeolocPicker}     from '../GeolocPicker'; 
import {GeolocServiceMock} from '../../../services/__mocks__/GeolocService.mock';
import {CortexNotificationServiceMock} from '@mnemotix/cortex-core/testing';

let geolocServiceMock, notificationServiceMock;
let container, findByTestId;

beforeEach(() => {
  geolocServiceMock = new GeolocServiceMock(); 
  notificationServiceMock = new CortexNotificationServiceMock();
  ({container, findByTestId} = renderWithMocks({
    element: <GeolocPicker geolocService={geolocServiceMock} notificationService={notificationServiceMock}/>
  }));
});
afterEach(cleanup);

/* Disable console.warn for these tests, to avoid polluting the test output (console logging is to be expected) */
let consolewarn;
beforeAll(() => {
  consolewarn = console.warn;
  console.warn = jest.fn(); 
});
afterAll(() => {
  console.warn = consolewarn;
});

describe("auto geoloc button", () => {
  let autoGeolocButton, resultSubject;
  beforeEach(async () => {
    resultSubject = new Subject();
    geolocServiceMock.setAutoPlace.mockImplementation(() => resultSubject);
    autoGeolocButton = await findByTestId("autoGeolocButton");
  });

  describe("loading animation", () => {
    it("has loading animation while the request is pending", async () => {
      fireEvent.click(autoGeolocButton);
      expect(autoGeolocButton).toHaveClass('loading');
      resultSubject.complete();
      expect(autoGeolocButton).not.toHaveClass('loading');
    });

    it("removes the loading animation if the request errors", async () => {
      fireEvent.click(autoGeolocButton);
      expect(autoGeolocButton).toHaveClass('loading');
      resultSubject.error('my error');
      expect(autoGeolocButton).not.toHaveClass('loading');
    })
  });

  describe("error messages", () => {
    it("displays specific message when user denied permission", () => {
      geolocServiceMock.setAutoPlace.mockImplementation(() => throwError({
        status: 'browser_geolocation_error',
        error: {
          code: 1,
          message: 'User denied Geolocation'
        }
      }));
      fireEvent.click(autoGeolocButton);
      expect(notificationServiceMock.alert.mock.calls[0][0]).toEqual("Nous n'avons pas la permission d'accéder à votre localisation");
    });

    it("displays generic message when error is unknown", () => {
      geolocServiceMock.setAutoPlace.mockImplementation(() => throwError({
        any: 'error'
      }));
      fireEvent.click(autoGeolocButton);
      expect(notificationServiceMock.alert.mock.calls[0][0]).toEqual('Un problème nous empêche de vous géolocaliser');
    });

    it("doesn't display message when geoloc is aborted", () => {
      geolocServiceMock.setAutoPlace.mockImplementation(() => throwError({
        status: 'aborted'
      }));
      fireEvent.click(autoGeolocButton);
      expect(notificationServiceMock.alert.mock.calls.length).toEqual(0);
    });
  });
});

