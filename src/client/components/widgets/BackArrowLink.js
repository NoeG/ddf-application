import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import backArrowPurple from '../../assets/images/back_arrow_purple.svg';
import style from './BackArrowLink.styl';

export class BackArrowLink extends React.Component {

  static propTypes = {
    /** The link text */
    text: PropTypes.string,
    /** The link target */
    to:  PropTypes.string,
  }


  render() {
    return (
      <Link className={style.backButton} to={this.props.to}>
        <img src={backArrowPurple} />
        <span>{this.props.text}</span>
      </Link>
    );
  }
}
