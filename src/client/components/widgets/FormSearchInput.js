import React from 'react';
import PropTypes from "prop-types";
import classNames from 'classnames';


import style from './FormSearchInput.styl';
import searchIcon from '../../assets/images/search.svg';

/**
 *
 * Usage :
 *
 * <FormSearchInput
 *  theme='dark'
 *  size='large'
 *  autofocus={false}
 * />
 *
 *
 * Properties : 
 *
 * - theme : 'dark'|'light' (default)
 * - size  : 'large'(default)|'medium' 
 * - autofocus : true (default)|false
 *
 */
export class FormSearchInput extends React.Component {

  static propTypes = {
    placeholder: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    onClose: PropTypes.func,
    theme: PropTypes.oneOf(['dark', 'light']),
    size: PropTypes.oneOf(['large', 'medium']),
    closeButton: PropTypes.bool,
    autofocus: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleOnClose = this.handleOnClose.bind(this);
    this.inputRef = React.createRef();
    this.state = {
      value: this.props.value || '',
      theme: props.theme || 'light',
      size: props.size || 'large',
      textSize: 'normal',
      autofocus: props.autofocus === false ? false : true
    }
  }

  componentDidMount() {
    if (this.state.autofocus) {
      this.inputRef.current.focus();
    }
  }
  
  componentDidUpdate(prevProps) {
    if (this.props.value !== prevProps.value) {
      this.setState({value: this.props.value});
    }
  }

  render() {
    const {t} = this.props;
    let styles = [
      style.container,
      style[this.state.theme],
      style[this.state.size],
      style[`text-size-${this.state.textSize}`]
    ];

    return (
      <div className={classNames(styles)}>
        <div className={style.searchIcon} onClick={this.handleSubmit}>
          <img src={searchIcon}/>
        </div>

        <form onSubmit={this.handleSubmit}>
          <input 
            type="text" 
            autoCapitalize="none"
            autoCorrect="off"
            placeholder={this.props.placeholder}
            onChange={this.handleChange}
            value={this.state.value}
            ref={this.inputRef}
          />
        </form>

        <Choose>
          <When condition={this.props.closeButton}>
          <div className={style.closeIcon} onClick={this.handleOnClose}>
            &#x2715;
          </div>
        </When>
        <Otherwise>
          <div className={style.rightHandPlaceholder}/>
        </Otherwise>
        </Choose>

      </div>
    );
  }

  handleChange(evt) {
    let inputValue = evt.target.value;
    let textSize = inputValue.length > 20 ? 'small' : 'normal';
    this.setState({
      value: inputValue,
      textSize: textSize
    });

    if(this.props.onChange) {
      this.props.onChange(evt);
    }
  }

  handleSubmit(evt) {
    evt.preventDefault();
    if(!this.state.value) {
      return;
    }
    if(this.props.onSubmit) {
      this.props.onSubmit(evt);
    }
  }

  handleOnClose() {
    if(this.props.onClose) {
      this.props.onClose();
    }
  }

  resetInput({inputValue} = {}) {
    this.setState({
      value: inputValue || ''
    });
    if (this.state.autofocus) {
      setTimeout(() => this.inputRef.current.focus(), 100);
    }
  }
}


