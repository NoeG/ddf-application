import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import formsStyle from './Forms.styl';


let uidCounter = 0;
function generateUid() {
  return `radiobutton-${uidCounter++}`;
}

RadioButton.propTypes = {
  label: PropTypes.node
};
export function RadioButton(props) {
  const {label, ...otherProps} = props;
  let uid = generateUid();
  
  return (
    <div className={formsStyle.checkboxRow}>
      <input 
        id={uid}
        className={formsStyle.customRadioButton} 
        type="radio" 
        {...otherProps} 
      />
      <label htmlFor={uid}>{label}</label>
    </div>
  );
}
