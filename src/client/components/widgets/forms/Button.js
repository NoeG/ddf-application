import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import style from './Forms.styl';

export class Button extends React.Component {
  static propTypes = {
    secondary: PropTypes.bool,
    /**
     * One of the defined color keywords, see `forColors.styl`.
     * Default color is purple
     */
    colorTheme: PropTypes.string,
    /** 
     * By default the HTML element generated is <button>, but you can use another element or React Component, 
     * which will be applied the same styling
     */
    as: PropTypes.elementType,
    className: PropTypes.string,
  }

  render() {
    const {secondary, loading, colorTheme, as, className, ...otherProps} = this.props;
    const ComponentToUse = as || "button";

    return (

      <ComponentToUse
        className={classNames([
          style.button, 
          style[colorTheme], 
          {
            [style.secondary]: secondary,
            [style.loading]: loading, 
          },
          className
        ])} 
        {...otherProps}
      />
    );
  }
}
