import React, {useState} from 'react';

/**
 * Instances of this class must be passed throught useAutocompleteController() which will assign
 * them this.updateSuggestions
 */
export class AutocompleteController {
  constructor() {
    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
    this.getSuggestionValue = this.getSuggestionValue.bind(this);
    this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
    this.onSuggestionSelected = this.onSuggestionSelected.bind(this);
  }

  onSuggestionsFetchRequested() {}
  getSuggestionValue (suggestion) {return suggestion}
  onSuggestionsClearRequested() {}
  onSuggestionSelected() {}
  get displayNoResults() {
    return true;
  }
}

export function useAutocompleteController(controller) {
  let [suggestions, stateUpdateSuggestions] = useState([]);

  /* 
   * This flag indicates if the "no results" box is to be displayed. It is true on the following conditions : 
   * - the results box must be visible (that is, input is focused and/or with content)
   * - the result array returned by onSuggestionsFetchRequested() is empty
   *
   * This flag should be set to false onSuggestionsClearRequested
   */
  let [noResults, updateNoResults] = useState(false);
  controller.updateSuggestions = (newSuggestions) =>  {
    stateUpdateSuggestions(newSuggestions);
    updateNoResults(newSuggestions.length === 0);
  };
  let prevOnSuggestionsClearRequested = controller.onSuggestionsClearRequested;
  controller.onSuggestionsClearRequested = () => {
    prevOnSuggestionsClearRequested();
    updateNoResults(false);
  };
  return {suggestions, noResults};
}
