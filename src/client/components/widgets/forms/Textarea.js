import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import style from './Forms.styl';

export class Textarea extends React.Component {

  static propTypes = {
    /**
     * Flag to indicate if the input is required 
     */
    isRequired: PropTypes.bool,
    /**
     * One of the defined color keywords, see `forColors.styl`.
     * Default color is purple
     */
    colorTheme: PropTypes.string
  }
  
  render() {
    let {placeholder, isRequired, colorTheme, ...otherProps} = this.props;
    if (placeholder && isRequired) {
      placeholder += " *";
    }

    return (
      <textarea
        className={classNames([style.textarea, style[colorTheme]])}
        required={isRequired} 
        placeholder={placeholder} 
        {...otherProps}
      />
    );
  }
}

