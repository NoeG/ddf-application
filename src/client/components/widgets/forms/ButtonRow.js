import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import style from './ButtonRow.styl';

export class ButtonRow extends React.Component {
  
  render() {
    return (
      <div className={style.container}>
        {this.props.children}
      </div>
    );
  }
}
