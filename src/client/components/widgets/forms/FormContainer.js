import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import style from './Forms.styl';


/**
 *
 * A simple <form> component without extra functionnality (unlike "./formik/Form"),
 * but with the same styling
 */
export class FormContainer extends React.Component {
  static propTypes = {
    /**
     * A color theming for the elements of the form. One of the defined color keywords, see `forColors.styl`.
     * Default color is purple
     */
    colorTheme: PropTypes.string
  }

  render() {
    const {colorTheme, ...otherProps} = this.props;
    return <form className={classNames(style.form, style[colorTheme])} {...otherProps} />
  }
}
