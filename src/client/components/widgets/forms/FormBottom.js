import React from 'react';

import style from './FormBottom.styl';

/**
 *
 * This is a layout helper component. The children passed to this component will be displayed on bottom of the Form 
 * (must be used inside the Form component), i.e on bottom of the mobile page if the form content is shorter than the
 * height of the page, or directly on bottom of the content if scrolling is needed
 */
export class FormBottom extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div className={style.spacing} />
        {this.props.children}
      </React.Fragment>
    );
  }
}
