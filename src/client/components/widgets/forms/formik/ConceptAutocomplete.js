import React, {useState, useEffect} from 'react';
import invariant from 'invariant';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import {from as observableFrom} from 'rxjs';
import {map} from 'rxjs/operators';
import {useApolloClient} from '@apollo/react-hooks';

import {getDDFApolloClient} from '../../../../services/DDFApolloClient';
import {Autocomplete} from './Autocomplete';
import {AutocompleteController} from '../AutocompleteController';
import {GenericFuzzySearcher} from '../../../../services/GenericFuzzySearcher';

export const gqlSearchConcepts = gql`
  query SearchConcepts($qs: String!, $first: Int, $filters: [String!]){
    concepts(qs: $qs, filters: $filters, first: $first, sortings: [{sortBy: "prefLabel"}]) {
      edges {
        node {
          id
          prefLabel
        }
      }
    }
  }
`

class ConceptSearcher extends GenericFuzzySearcher{
  constructor({apolloClient, defaultFirst, schemeId} = {}) {
    super({defaultFirst});
    invariant(schemeId, "schemeId must be provided");
    this.apolloClient = apolloClient || getDDFApolloClient();
    this.schemeId = schemeId;
  }

  performSearch(qs, {first}) {
    let filters = [
      `inScheme:${this.schemeId}`
    ];

    if(!qs || qs === ""){
      filters.push("isTopInScheme:true");
    }

    return observableFrom(this.apolloClient.query({
      query: gqlSearchConcepts,
      variables: {
        qs, 
        first,
        filters
      }
    })).pipe(
      map(queryResult => queryResult.data.concepts.edges.map(edge => edge.node)),
    );
  }
}

export class ConceptAutocompleteController extends AutocompleteController {
  constructor({maxSuggestionsLength, apolloClient, schemeId} = {}) {
    super();
    invariant(schemeId, "schemeId must be provided");
    this.conceptSearcher = new ConceptSearcher({defaultFirst: maxSuggestionsLength, apolloClient, schemeId});

    this._subscription = this.conceptSearcher.results.subscribe((concepts) => {
      /** 
       * Update suggestions list only if the autocompleteController was already "mounted" by the Autocomplete component, i.e
       * it has the updateSuggestions() method set up
       */
      if (this.updateSuggestions) {
        this.updateSuggestions(concepts);
      }
    });
  }

  updateSchemeId(schemeId) {
    this.conceptSearcher.schemeId = schemeId
  }

  onSuggestionsFetchRequested(suggestionInput) {
    let qs = suggestionInput.value;
    this.conceptSearcher.search(qs);
  }

  getSuggestionValue(suggestedConcept) {
    return suggestedConcept?.prefLabel;
  } 

  getFormikSuggestionValue(suggestedConcept) {
    return suggestedConcept?.id || null;
  }

  renderSuggestionText(suggestedConcept) {
    return suggestedConcept?.prefLabel;
  }

  onSuggestionsClearRequested() {
    this.conceptSearcher.clear(); 
  }

  shouldRenderSuggestions() {
    return true;
  }


  clean() {
    this._subscription.unsubscribe();
  }
}


ConceptAutocomplete.propTypes = {
  /**
   * The URI of the filter to apply for the concept. This must be a URI recognizable by graphDB, either a shortcut like 
   * "ddfa:domain" or a full URI "http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type".
   *
   * Beware, do not inculed the "inScheme:" prefix, this will be automatically added for the graphQL query
   */
  schemeId: PropTypes.string.isRequired,
  maxSuggestionsLength: PropTypes.number,
};
export function ConceptAutocomplete(props) {
  const {maxSuggestionsLength} = props;
  const apolloClient = useApolloClient();
  let [controller, setController] = useState(() => new ConceptAutocompleteController({
    apolloClient, 
    maxSuggestionsLength,
    schemeId: props.schemeId
  }));

  useEffect(() => {
    /* 
     * equivalent to componentWillUnmount, this effect be only run once, and its cleanup fonction should be 
     * called when the component unmounts 
     */
    return () => controller.clean();
  }, [controller]);

  useEffect(() => {
    /* Effect to update the schemeId when the prop schemeId changes */
    controller.updateSchemeId(props.schemeId);
  }, [controller, props.schemeId]);

  return (
    <Autocomplete
      controller={controller}
      autoCapitalize="none"
      autoCorrect="off"
      {...props}
    />
  );
}

