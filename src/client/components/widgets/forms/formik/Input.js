import React from 'react';
import {connect, getIn} from 'formik';
import {Input as RawInput} from '../Input';
import {isRequiredField} from '../../../../utilities/FormikIsRequiredField';
import formsStyle from '../Forms.styl';

@connect
export class Input extends React.Component {

  render() {
    const {formik, ...otherProps} = this.props;
    const {handleChange, handleBlur} = formik;
    const {name} = otherProps;
    const isRequired = typeof otherProps.isRequired !== 'undefined' ? otherProps.isRequired : isRequiredField(formik, name);

    const error = getIn(formik.errors, name);
    /** input value if no value prop is provided to this component */
    let inputValue = (getIn(formik.values, name) !== undefined) ? getIn(formik.values, name) : getIn(formik.initialValues, name);

    return (
      <React.Fragment>
        <If condition={error}>
          <p className={formsStyle.inputErrorMessage} data-testid={`${name}-error-msg`}>{error}</p>
        </If>
        <RawInput 
          onChange={handleChange}
          onBlur={handleBlur}
          {...otherProps} 
          isRequired={isRequired}
          value={otherProps.value || inputValue}
        />
      </React.Fragment>
    );
  }
}

