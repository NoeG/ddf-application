import React from 'react';

import {Autocomplete} from './Autocomplete';
import {AutocompleteController} from '../AutocompleteController';
import {getGeolocService} from '../../../../services/GeolocService';
import {getPlaceRegionInfo} from '../../../../utilities/helpers/places';
import style from './PlaceAutocomplete.styl';


export class PlaceAutocompleteController extends AutocompleteController {
  constructor({geolocService}) {
    super();
    this.geolocService = geolocService;
    this.placeSearcher = this.geolocService.getPlaceSearcher({defaultFirst: 5});

    this._psSubscription = this.placeSearcher.results.subscribe((places) => {
      /** 
       * Update suggestions list only if the autocompleteController was already "mounted" by the Autocomplete component, i.e
       * it has the updateSuggestions() method set up
       */
      if (this.updateSuggestions) {
        this.updateSuggestions(places);
      }
    });
  }

  onSuggestionsFetchRequested(suggestionInput) {
    let qs = suggestionInput.value;
    this.placeSearcher.search(qs);
  }

  getSuggestionValue(suggestedPlace) {
    return suggestedPlace?.name;
  } 

  getFormikSuggestionValue(suggestedPlace) {
    return suggestedPlace?.id || null;
  }

  onSuggestionsClearRequested() {
    this.placeSearcher.clear(); 
  }

  renderSuggestion(suggestedPlace) {
    let placeSecondaryText = getPlaceRegionInfo(suggestedPlace);
    return (
      <>
        <div className={style.suggestedPlace}>
          <span className={style.placeName}>{suggestedPlace.name}</span>
          <If condition={placeSecondaryText}>
            <span>&nbsp;{placeSecondaryText}</span>
          </If>
        </div>
      </>
    );
  }

  clean() {
    this._psSubscription.unsubscribe();
  }
}

export class PlaceAutocomplete extends React.Component {
  constructor(props) {
    super(props);
    this.geolocService = props.geolocService || getGeolocService();
    this.controller = new PlaceAutocompleteController({geolocService: this.geolocService});
  }

  componentWillUnmount() {
    this.controller.clean();
  }

  render() {
    return (
      <Autocomplete
        controller={this.controller}
        {...this.props}
      />
    );
  }
}

