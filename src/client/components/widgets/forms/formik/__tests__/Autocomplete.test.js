import React, {useState} from 'react';
import {Formik} from 'formik';
import {act} from 'react-dom/test-utils';
import {render, cleanup, fireEvent} from '@testing-library/react';
import waait from 'waait';

import {Autocomplete} from '../Autocomplete';
import {AutocompleteController} from '../../AutocompleteController';
import {Form} from '../Form';


afterEach(cleanup);
/* Disable console.warn for the tests */
let consolewarn;
beforeAll(() => {
  consolewarn = console.warn;
  console.warn = jest.fn(); 
});
afterAll(() => {
  console.warn = consolewarn;
});


/**
 * The test controller returns the suggestions synchronously, if the input is empty it returns no suggestions,
 * otherwise it returns 3 auto generated suggestions objects : 
 *  - {id: "${input}1", value: "${input} suggestion one"}
 *  - {id: "${input}2", value: "${input} suggestion two"}
 *  - {id: "${input}3", value: "${input} suggestion three"}
 *
 * The suggested value is the `value` property of the suggestion object.
 * The formik value is the `id` property of the suggestion object.
 */
export class TestAutocompleteController extends AutocompleteController {
  constructor() {
    super();
    let self = this;
    this.onSuggestionsFetchRequested = jest.fn((suggestionInput) => {
      let value = suggestionInput.value;
      self.updateSuggestions([
        {id: `${value}1`, value: `${value} suggestion one`},
        {id: `${value}2`, value: `${value} suggestion two`},
        {id: `${value}3`, value: `${value} suggestion three`},
      ]);
    });
    this.getSuggestionValue = jest.fn(suggestion => {
      return suggestion?.value;
    });
    this.getFormikSuggestionValue = jest.fn(suggestion => {
      return suggestion?.id || null;
    });
    this.onSuggestionsClearRequested = jest.fn(() => {
      return self.updateSuggestions([]);
    });
    this.renderSuggestionText = jest.fn(suggestion => {
      return suggestion?.value;
    });
  }
}


/**
 * tests : 
 *
 * - # The binding with formik value works correctly
 *   - the formik value is correctly updated when a suggestion is selected
 *   - without selectNullOnEmpty (default), when the input is cleared, the formik value doesn't change
 *   - with selectNullOnEmpty, the formik value is set to null when the input is cleared
 */
describe('binding with formik', () => {
  it('update formik value when a suggestion is selected', async () => {
    function TestComponent(props) {
      const [controller] = useState(() => new TestAutocompleteController());
      return (
        <Formik
          initialValues={{myField: null}}
          render={({values}) => (
            <Form>
              <Autocomplete
                controller={new TestAutocompleteController()}
                placeholder="My Field"
                name="myField" 
              />
              <span data-testid="values">{JSON.stringify(values)}</span>
            </Form>
          )}
        />
      );
    }
    const {container, findByText, findByTestId, findByPlaceholderText} = render(<TestComponent />);
    let input = await findByPlaceholderText("My Field");
    fireEvent.change(input, {target : {value : 'search'}})
    input.focus();
    fireEvent.click(await findByText("search suggestion one"));
    let valuesSpan = await findByTestId('values');
    expect(JSON.parse(valuesSpan.textContent)).toEqual({myField: 'search1'});
  });

  it("doesn't update formik value when the input is cleared", async () => {
    function TestComponent(props) {
      const [controller] = useState(() => new TestAutocompleteController());
      return (
        <Formik
          initialValues={{myField: 'initialValue'}}
          render={({values}) => (
            <Form>
              <Autocomplete
                controller={new TestAutocompleteController()}
                placeholder="My Field"
                name="myField" 
              />
              <span data-testid="values">{JSON.stringify(values)}</span>
            </Form>
          )}
        />
      );
    }
    const {container, findByText, findByTestId, findByPlaceholderText} = render(<TestComponent />);
    let input = await findByPlaceholderText("My Field");
    fireEvent.change(input, {target : {value : ''}})
    let valuesSpan = await findByTestId('values');
    expect(JSON.parse(valuesSpan.textContent)).toEqual({myField: 'initialValue'});
  });

  it('with selectNullOnEmpty, it updates formik value with null when the input is cleared', async () => {
    function TestComponent(props) {
      const [controller] = useState(() => new TestAutocompleteController());
      return (
        <Formik
          initialValues={{myField: 'initialValue'}}
          render={({values}) => (
            <Form>
              <Autocomplete
                controller={new TestAutocompleteController()}
                placeholder="My Field"
                name="myField" 
                selectNullOnEmpty
              />
              <span data-testid="values">{JSON.stringify(values)}</span>
            </Form>
          )}
        />
      );
    }
    const {container, findByText, findByTestId, findByPlaceholderText} = render(<TestComponent />);
    let input = await findByPlaceholderText("My Field");
    fireEvent.change(input, {target : {value : 'search'}})
    input.focus();
    fireEvent.click(await findByText("search suggestion one"));
    fireEvent.change(input, {target : {value : ''}})
    let valuesSpan = await findByTestId('values');
    expect(JSON.parse(valuesSpan.textContent)).toEqual({myField: null});
  });
});

/**
 *
 * tests : 
 *
 * - # Good use of formik status for storing the textual value
 *   - Input field is initialized with initialStatus
 *   - Input field is updated if formik status  is externally updated
 *   - Selecting a suggestion updates correctly the formik status (while preserving the exiting status object)
 *   - Selecting a suggestion updates correctly the formik status if no status object exists
 *   - selectNullOnEmpty, clearing the input updates formik status with an empty value
 *   - The autocomplete doesn't crash if someone corrupts the formik status object
 *
 */
describe("use of formik status", () => {
  it('initializes input field with value from initialStatus', async () => {
    function TestComponent(props) {
      const [controller] = useState(() => new TestAutocompleteController());
      return (
        <Formik
          initialStatus={{autocompleteInputValues: {myField: 'testValue'}}}
          render={() => (
            <Form>
              <Autocomplete
                controller={new TestAutocompleteController()}
                name="myField" 
              />
            </Form>
          )}
        />
      );
    }
    const {container} = render(<TestComponent />);
    await act(waait);
    expect(container.querySelector('input').value).toEqual('testValue');
  });

  it('updates input field according to formik status, when that status is updated externally', async () => {
    function TestComponent(props) {
      const [controller] = useState(() => new TestAutocompleteController());
      return (
        <Formik
          initialStatus={{autocompleteInputValues: {myField: 'testValue'}}}
          render={({setStatus}) => (
            <Form>
              <Autocomplete
                controller={new TestAutocompleteController()}
                name="myField" 
              />
              <button type="button" onClick={() => setStatus({autocompleteInputValues: {myField: 'new value'}})}>Click me</button>
            </Form>
          )}
        />
      );
    }
    const {container, findByText} = render(<TestComponent />);
    fireEvent.click(await findByText("Click me"));
    await act(waait);
    expect(container.querySelector('input').value).toEqual('new value');
  });
  
  it('updates the formik status on suggestion selection', async() => {
    function TestComponent(props) {
      const [controller] = useState(() => new TestAutocompleteController());
      return (
        <Formik
          initialStatus={
            {
              autocompleteInputValues: {myField: 'testValue'},
              otherField: 'foo'
            }
          }
          render={({status}) => (
            <Form>
              <Autocomplete
                controller={new TestAutocompleteController()}
                placeholder="My Field"
                name="myField" 
              />
              <span data-testid="status">{JSON.stringify(status)}</span>
            </Form>
          )}
        />
      );
    }
    const {container, findByText, findByTestId, findByPlaceholderText} = render(<TestComponent />);
    let input = await findByPlaceholderText("My Field");
    fireEvent.change(input, {target : {value : 'search'}})
    input.focus();
    fireEvent.click(await findByText("search suggestion one"));
    let statusSpan = await findByTestId('status');
    expect(JSON.parse(statusSpan.textContent)).toEqual({
      autocompleteInputValues: {myField: 'search suggestion one'},
      otherField: 'foo'
    });
  });

  it('updates the formik status if no status object exists', async() => {
    function TestComponent(props) {
      const [controller] = useState(() => new TestAutocompleteController());
      return (
        <Formik
          render={({status}) => (
            <Form>
              <Autocomplete
                controller={new TestAutocompleteController()}
                placeholder="My Field"
                name="myField" 
              />
              <span data-testid="status">{JSON.stringify(status)}</span>
            </Form>
          )}
        />
      );
    }
    const {container, findByText, findByTestId, findByPlaceholderText} = render(<TestComponent />);
    let input = await findByPlaceholderText("My Field");
    fireEvent.change(input, {target : {value : 'search'}})
    input.focus();
    fireEvent.click(await findByText("search suggestion one"));
    let statusSpan = await findByTestId('status');
    expect(JSON.parse(statusSpan.textContent)).toEqual({
      autocompleteInputValues: {myField: 'search suggestion one'}
    });
  });
    

  it('with selectNullOnEmpty, it clears the value in formik status when input is empty', async() => {
    function TestComponent(props) {
      const [controller] = useState(new TestAutocompleteController());
      return (
        <Formik
          initialStatus={
            {
              autocompleteInputValues: {myField: 'testValue'},
              otherField: 'foo'
            }
          }
          render={({status}) => (
            <Form>
              <Autocomplete
                controller={new TestAutocompleteController()}
                placeholder="My Field"
                name="myField" 
                selectNullOnEmpty
              />
              <span data-testid="status">{JSON.stringify(status)}</span>
            </Form>
          )}
        />
      );
    }
    const {container, findByText, findByTestId, findByPlaceholderText} = render(<TestComponent />);
    let input = await findByPlaceholderText("My Field");
    fireEvent.change(input, {target : {value : ''}})
    input.focus();
    let statusSpan = await findByTestId('status');
    expect(JSON.parse(statusSpan.textContent)).toEqual({
      autocompleteInputValues: {myField: ''},
      otherField: 'foo'
    });
  });

  it("doesn't crash if the status object is corrupted", async() => {
    function TestComponent(props) {
      const [controller] = useState(new TestAutocompleteController());
      return (
        <Formik
          initialStatus={{ autocompleteInputValues: {myField: 'testValue'}}}
          render={({setStatus}) => (
            <Form>
              <Autocomplete
                controller={new TestAutocompleteController()}
                placeholder="My Field"
                name="myField" 
              />
              <button type="button" onClick={() => setStatus({something: 'else'})}>Click me</button>
            </Form>
          )}
        />
      );
    }
    const {container, findByText, findByTestId, findByPlaceholderText} = render(<TestComponent />);
    fireEvent.click(await findByText("Click me"));
    await act(waait);
    expect(container.querySelector('input').value).toEqual('');
  });
});
