import React, {useState} from 'react';
import {Formik} from 'formik';
import {act} from 'react-dom/test-utils';
import {render, cleanup, fireEvent} from '@testing-library/react';
import waait from 'waait';

import {DomainAutocomplete} from '../DomainAutocomplete';
import {gqlSearchConcepts} from '../ConceptAutocomplete';
import {Form} from '../Form';
import {renderWithMocks} from '../../../../../../../jest/utilities/renderWithMocks';


afterEach(cleanup);

/* Disable console.warn for the tests */
let consolewarn;
beforeAll(() => {
  consolewarn = console.warn;
  console.warn = jest.fn(); 
});
afterAll(() => {
  console.warn = consolewarn;
});

const gqlMocks = [{
  request: {
    query: gqlSearchConcepts,
    variables: { 
      qs: 'ethno',
      first: 8,
      filters: ['inScheme:http://data.dictionnairedesfrancophones.org/authority/domain']
    },
  },
  result: {
    "data": {
      "concepts": {
        "__typename": "ConceptConnection",
        "edges": [{
          "__typename": "ConceptEdge",

          "node": {
            "__typename": "Concept",
            "id": "ddfa:domaine/ethnobiologie",
            "prefLabel": "ethnobiologie"
          }
        }, {
          "__typename": "ConceptEdge",
          "node": {
            "__typename": "Concept",
            "id": "ddfa:domaine/ethnologie",
            "prefLabel": "ethnologie"
          }
        }],
      }
    }
  }
}];

describe('DomainAutocomplete component', () => {
  it('searches a concept and update formik value on selection', async () => {
    function TestComponent(props) {
      return (
        <Formik
          initialValues={{myDomain: null}}
          render={({values}) => (
            <Form>
              <DomainAutocomplete
                placeholder="My Domain"
                name="myDomain" 
                maxSuggestionsLength={8}
              />
              <span data-testid="values">{JSON.stringify(values)}</span>
            </Form>
          )}
        />
      );
    }
    const {container, findByText, findByTestId, findByPlaceholderText} = renderWithMocks({
      gqlMocks,
      element: <TestComponent />
    });
    let input = await findByPlaceholderText("My Domain");
    fireEvent.change(input, {target : {value : 'ethno'}})
    input.focus();
    fireEvent.click(await findByText("ethnologie"));
    let valuesSpan = await findByTestId('values');
    expect(JSON.parse(valuesSpan.textContent)).toEqual({myDomain: 'ddfa:domaine/ethnologie'});
  });
});

