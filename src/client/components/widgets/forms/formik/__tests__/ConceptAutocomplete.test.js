import React, {useState} from 'react';
import {Formik} from 'formik';
import {act} from 'react-dom/test-utils';
import {render, cleanup, fireEvent} from '@testing-library/react';
import waait from 'waait';

import {ConceptAutocomplete, gqlSearchConcepts} from '../ConceptAutocomplete';
import {Form} from '../Form';
import {renderWithMocks} from '../../../../../../../jest/utilities/renderWithMocks';


afterEach(cleanup);

/* Disable console.warn for the tests */
let consolewarn;
beforeAll(() => {
  consolewarn = console.warn;
  console.warn = jest.fn(); 
});
afterAll(() => {
  console.warn = consolewarn;
});

const gqlMocks = [{
  request: {
    query: gqlSearchConcepts,
    variables: { 
      qs: 'ethno',
      first: 8,
      filters: ['inScheme:http://data.dictionnairedesfrancophones.org/authority/domain']
    },
  },
  result: {
    "data": {
      "concepts": {
        "__typename": "ConceptConnection",
        "edges": [{
          "__typename": "ConceptEdge",

          "node": {
            "__typename": "Concept",
            "id": "ddfa:domaine/ethnobiologie",
            "prefLabel": "ethnobiologie"
          }
        }, {
          "__typename": "ConceptEdge",
          "node": {
            "__typename": "Concept",
            "id": "ddfa:domaine/ethnologie",
            "prefLabel": "ethnologie"
          }
        }],
      }
    }
  }
}, {
  request: {
    query: gqlSearchConcepts,
    variables: { 
      qs: 'pronom',
      first: 8,
      filters: ['inScheme:http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type']
    },
  },
  result: {
    "data": {
      "concepts": {
        "__typename": "ConceptConnection",
        "edges": [{
          "__typename": "ConceptEdge",
          "node": {
            "__typename": "Concept",
            "id": "lexinfo:demonstrativePronoun",
            "prefLabel": "pronom démonstratif"
          }
        }, {
          "__typename": "ConceptEdge",
          "node": {
            "__typename": "Concept",
            "id": "lexinfo:indefinitePronoun",
            "prefLabel": "pronom indéfini"
          }
        }]
      }
    }
  }
}];

describe('ConceptAutocomplete component', () => {

  it('searches a concept and update formik value on selection', async () => {
    function TestComponent(props) {
      return (
        <Formik
          initialValues={{myConcept: null}}
          render={({values}) => (
            <Form>
              <ConceptAutocomplete
                placeholder="Mon concept"
                name="myConcept" 
                maxSuggestionsLength={8}
                schemeId="http://data.dictionnairedesfrancophones.org/authority/domain"
              />
              <span data-testid="values">{JSON.stringify(values)}</span>
            </Form>
          )}
        />
      );
    }
    const {container, findByText, findByTestId, findByPlaceholderText} = renderWithMocks({
      gqlMocks,
      element: <TestComponent />
    });
    let input = await findByPlaceholderText("Mon concept");
    fireEvent.change(input, {target : {value : 'ethno'}})
    input.focus();
    fireEvent.click(await findByText("ethnologie"));
    let valuesSpan = await findByTestId('values');
    expect(JSON.parse(valuesSpan.textContent)).toEqual({myConcept: 'ddfa:domaine/ethnologie'});
  });

  it('updates the inScheme filter, and show different results when a new search is done', async () => {
    /**
     * - First the autocomplete searches for concept that are domains
     * - we search for "ethno"
     * - we select the suggestion "ethnologie"
     * - we click the button that changes the schemeId to "part-of-speech-type", we search for part of speeches
     * - we search for "pronom"
     * - we select the suggestion "pronom démonstratif"
     */
    function TestComponent(props) {
      const [schemeId, updateSchemeId] = useState("http://data.dictionnairedesfrancophones.org/authority/domain");
      return (
        <Formik
          initialValues={{myConcept: null}}
          render={({values}) => (
            <Form>
              <ConceptAutocomplete
                placeholder="Mon concept"
                name="myConcept" 
                maxSuggestionsLength={8}
                schemeId={schemeId}
              />
              <button type="button" onClick={() => updateSchemeId("http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type")}>
                Click me 
              </button>
              <span data-testid="values">{JSON.stringify(values)}</span>
            </Form>
          )}
        />
      );
    }
    const {container, findByText, findByTestId, findByPlaceholderText} = renderWithMocks({
      gqlMocks,
      element: <TestComponent />
    });
    let input = await findByPlaceholderText("Mon concept");
    fireEvent.change(input, {target : {value : 'ethno'}})
    input.focus();
    fireEvent.click(await findByText("ethnologie"));
    fireEvent.click(await findByText("Click me"));
    fireEvent.change(input, {target : {value : 'pronom'}})
    input.focus();
    fireEvent.click(await findByText("pronom démonstratif"));
    let valuesSpan = await findByTestId('values');
    expect(JSON.parse(valuesSpan.textContent)).toEqual({myConcept: 'lexinfo:demonstrativePronoun'});
  });
});
