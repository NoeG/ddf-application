import React, {useState, useEffect} from 'react';
import invariant from 'invariant';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import {from as observableFrom} from 'rxjs';
import {map} from 'rxjs/operators';
import {useApolloClient} from '@apollo/react-hooks';

import {getDDFApolloClient} from '../../../../services/DDFApolloClient';
import {Autocomplete} from './Autocomplete';
import {AutocompleteController} from '../AutocompleteController';
import {GenericFuzzySearcher} from '../../../../services/GenericFuzzySearcher';

export const gqlSearchPersons = gql`
  query SearchPersons($qs: String!, $first: Int){
    persons(qs: $qs, first: $first) {
      edges {
        node {
          id
          nickName
          userAccount{
            id
          }
        }
      }
    }
  }
`;

class personSearcher extends GenericFuzzySearcher{
  constructor({apolloClient, defaultFirst, schemeId} = {}) {
    super({defaultFirst});
    this.apolloClient = apolloClient || getDDFApolloClient();
  }

  performSearch(qs, {first}) {
    return observableFrom(this.apolloClient.query({
      query: gqlSearchPersons,
      variables: {
        qs, 
        first,
      }
    })).pipe(
      map(queryResult => queryResult.data.persons.edges.map(edge => edge.node)),
    );
  }
}

export class PersonAutocompleteController extends AutocompleteController {
  constructor({maxSuggestionsLength, apolloClient} = {}) {
    super();
    this.personSearcher = new personSearcher({defaultFirst: maxSuggestionsLength, apolloClient});

    this._subscription = this.personSearcher.results.subscribe((persons) => {
      /** 
       * Update suggestions list only if the autocompleteController was already "mounted" by the Autocomplete component, i.e
       * it has the updateSuggestions() method set up
       */
      if (this.updateSuggestions) {
        this.updateSuggestions(persons);
      }
    });
  }

  onSuggestionsFetchRequested(suggestionInput) {
    let qs = suggestionInput.value;
    this.personSearcher.search(`^${qs}.*`);
  }

  getSuggestionValue(suggestedPerson) {
    return suggestedPerson?.nickName;
  } 

  getFormikSuggestionValue(suggestedPerson) {
    return suggestedPerson;
  }

  renderSuggestionText(suggestedPerson) {
    return suggestedPerson?.nickName;
  }

  onSuggestionsClearRequested() {
    this.personSearcher.clear(); 
  }

  clean() {
    this._subscription.unsubscribe();
  }
}


PersonAutocomplete.propTypes = {
  maxSuggestionsLength: PropTypes.number
};
export function PersonAutocomplete(props) {
  const {maxSuggestionsLength} = props;
  const apolloClient = useApolloClient();
  let [controller, setController] = useState(() => new PersonAutocompleteController({
    apolloClient, 
    maxSuggestionsLength,
  }));

  useEffect(() => {
    /* 
     * equivalent to componentWillUnmount, this effect be only run once, and its cleanup fonction should be 
     * called when the component unmounts 
     */
    return () => controller.clean();
  }, [controller]);

  return (
    <Autocomplete
      controller={controller}
      {...props}
    />
  );
}

