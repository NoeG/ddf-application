import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {connect, getIn} from 'formik';

import {Autocomplete as RawAutocomplete} from '../Autocomplete';
import {isRequiredField} from '../../../../utilities/FormikIsRequiredField';

import formsStyle from '../Forms.styl';

/**
 * Binding an autocomplete with formik presents some specific challenges due to the nature of an autocomplete.
 * The autocomplete has to deal with 2 values : the textual value which is the content of the text <input> 
 * and the programmatical value (most of the time it's just an object id, but it can also be a whole object, or anything) associated
 * with the selected entry.
 *
 * To have a good binding we need : 
 * - the input textual value to reflect formik current value
 * - to transmit to formik the programmatical value corresponding to the selection
 *
 * For the first point, we use formik's generic "status" object, in order to store additionnal data, that is 
 * the textual value. We store it in the following way : 
 * status = {
 *   autocompleteInputValues: {
 *     [inputName]: 'value'
 *   }
 * }
 * Warning, if your formik form uses `setStatus` without taking care of merging the status object, thus replacing 
 * it, it will break the functionality of autocomplete.
 * As a user, you need to set formik initialStatus to an object matching this format, in order for the autocomplete
 * input to be already filled with the initial value.
 * The autocomplete will then take care to update this value on user input.
 *
 * For the second point, we extend the autocomplete controller with a metod getFormikSuggestionValue(). As a reminder,
 * the autocomplete controller method getSuggestionValue() returns a textual value, from the user selected suggestion, 
 * in order to fill the input. By default this value is also given to formik as value for the field. However if you 
 * want to use a different value for the input (e.g a label) and formik (e.g an id), you can implement getFormikSuggestionValue().
 *
 * When the user enters some text but doesn't select any suggestion, no update is made on formik. (Neihter the programmatical value 
 * nor the textual value). You can choose that clearing the input counts as resetting the field to null (this is not the default behavior).
 * To do this, set the prop `selectNullOnEmpty`. In this case, formik values are updated when the input becomes empty
 */
_Autocomplete.propTypes = {
  /**
   * Same as Autocomplete (non formik) controller. 
   *
   * Except for the method onSuggestionSelected(), which is implemented internally to link the value to formik.
   */
  controller: PropTypes.shape({
    onSuggestionsFetchRequested: PropTypes.func.isRequired,
    getSuggestionValue: PropTypes.func.isRequired,
    onSuggestionsClearRequested: PropTypes.func.isRequired,
    renderSuggestion: PropTypes.func,
    renderSuggestionText: PropTypes.func,
    getFormikSuggestionValue: PropTypes.func,
    shouldRenderSuggestions: PropTypes.func,
    displayNoResults: PropTypes.bool
  }).isRequired,
  placeholder: PropTypes.string,
  /**
   * A style object (as given by CSS modules), which will be merged with the default style of the component
   */
  theme: PropTypes.object,
  /**
   * The form input name
   */
  name: PropTypes.string.isRequired
}
function _Autocomplete(props) {
  const {formik, controller, name, ...otherProps} = props;
  const {handleBlur} = formik;
  const isRequired = typeof otherProps.isRequired !== 'undefined' ? otherProps.isRequired : isRequiredField(formik, name);
  const [inputValue, setInputValue] = useState();
  let autocompleteInputValues = formik.status?.autocompleteInputValues;

  controller.onSuggestionSelected = (evt, {suggestion}) => {
    const {formik, name, controller} = props;
    formik.setFieldValue(name, controller.getFormikSuggestionValue ? controller.getFormikSuggestionValue(suggestion) : suggestion);
    let newStatus = formik.status || {};
    if (!newStatus.autocompleteInputValues) {
      newStatus.autocompleteInputValues = {};
    }
    newStatus.autocompleteInputValues[name] = controller.getSuggestionValue(suggestion) || '';
    formik.setStatus(newStatus);
  };

  useEffect(() => {
    setInputValue(autocompleteInputValues && autocompleteInputValues[name]);
  }, [autocompleteInputValues && autocompleteInputValues[name]]);

  function onChangeHandler(e, {newValue}) {
    setInputValue(newValue);
  }

  const error = getIn(formik.errors, name);


  return (
    <React.Fragment>
      <If condition={error}>
        <p className={formsStyle.inputErrorMessage} data-testid={`${name}-error-msg`}>{error}</p>
      </If>
      <RawAutocomplete
        controller={controller}
        inputOnBlur={handleBlur}
        inputName={name}
        isRequired={isRequired}
        inputValue={props.inputValue || inputValue}
        inputOnChange={props.inputOnChange || onChangeHandler}
        {...otherProps}
      />
    </React.Fragment>
  );
}


export const Autocomplete = connect(_Autocomplete);
