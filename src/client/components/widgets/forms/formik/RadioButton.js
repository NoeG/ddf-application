import React from 'react';
import {connect, getIn} from 'formik';
import {RadioButton as RawRadioButton} from '../RadioButton';
import formsStyle from '../Forms.styl';

@connect
export class RadioButton extends React.Component {
  render() {
    const {formik, ...otherProps} = this.props;
    const {handleChange, handleBlur} = formik;
    const {name} = otherProps;

    let inputValue = (getIn(formik.values, name) !== undefined) ? getIn(formik.values, name) : getIn(formik.initialValues, name);
    let isChecked = (inputValue === this.props.value);

    return (
      <RawRadioButton
        onChange={handleChange}
        onBlur={handleBlur}
        checked={isChecked}
        {...otherProps} 
      />
    );
  }
}

