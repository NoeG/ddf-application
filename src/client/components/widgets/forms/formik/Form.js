import React from 'react';
import {Form as FormikForm} from 'formik';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import style from '../Forms.styl';

export class Form extends React.Component {
  static propTypes = {
    /**
     * A color theming for the elements of the form. One of the defined color keywords, see `forColors.styl`.
     * Default color is purple
     */
    colorTheme: PropTypes.string
  }

  render() {
    const {colorTheme, ...otherProps} = this.props;
    return <FormikForm className={classNames(style.form, style[colorTheme])} {...otherProps} />
  }
}
