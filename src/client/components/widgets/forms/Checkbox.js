import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import formsStyle from './Forms.styl';

Checkbox.propTypes = {
  label: PropTypes.node,
  /**
   * Flag to indicate if the input is required 
   */
  isRequired: PropTypes.bool,
};
export function Checkbox(props) {
  const {label, isRequired, ...otherProps} = props;
  const id = `checkboxid-${props.name}`;
  
  return (
    <div className={formsStyle.checkboxRow}>
      <input 
        id={id}
        className={formsStyle.customCheckbox} 
        type="checkbox" 
        required={isRequired}
        {...otherProps} 
      />
      <label htmlFor={id}>{label}</label>
    </div>
  );
}
