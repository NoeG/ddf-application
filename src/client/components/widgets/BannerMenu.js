import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';

import style from './BannerMenu.styl';

export class BannerMenu extends React.Component {

  static propTypes = {
    /**
     * It's the same object format that the one used by DesktopSideMenu
     */
    entries: PropTypes.arrayOf(PropTypes.shape({
      text: PropTypes.string.isRequired,
      to: PropTypes.string.isRequired
    }))
  };
  
  render() {
    const {entries} = this.props;
    return (
      <div className={style.container}>
        {entries.map((entry, idx) => (
          <NavLink 
            key={idx} 
            className={style.link}
            activeClassName={style.active}
            to={entry.to}
          >
            {entry.text}
          </NavLink> 
        ))}
      </div>
    );
  }
}
