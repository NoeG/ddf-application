import React              from 'react';
import {withTranslation}  from 'react-i18next';
import classNames         from 'classnames';
import {finalize}          from 'rxjs/operators';


import {getGeolocService}                from '../../services/GeolocService';
import {notificationService}          from '../../services/NotificationService';
import {Autocomplete}                 from './forms/Autocomplete';
import {PlaceAutocompleteController}  from './forms/formik/PlaceAutocomplete';
import style                          from './GeolocPicker.styl';
import geolocIcon                     from '../../assets/images/geoloc.svg';

class GeolocPickerAutocompleteController extends PlaceAutocompleteController {
  onSuggestionSelected(evt, {suggestion: place}) {
    this.geolocService.setManualPlace(place);
  }
}

@withTranslation()
export class GeolocPicker extends React.Component {

  constructor(props) {
    super(props);
    this.geolocService = props.geolocService || getGeolocService();
    this.notificationService = props.notificationService || notificationService;
    this.setAutoPlace = this.setAutoPlace.bind(this);
    this.handleAutoGeolocError = this.handleAutoGeolocError.bind(this);
    this.state = {
      inputValue: '',
      autoGeolocIsLoading: false
    }
    this.autocompleteController = new GeolocPickerAutocompleteController({
      geolocService: this.geolocService
    });
  }

  componentDidMount() {
    this._cpSubscription = this.geolocService.currentPlace.subscribe((place) => {
      this.setState({
        inputValue: place.name
      });
    });
  }

  componentWillUnmount() {
    this.autocompleteController.clean();
    this._cpSubscription.unsubscribe();
  }



  setAutoPlace() {
    if (this.state.autoGeolocIsLoading) return;
    this.setState({
      autoGeolocIsLoading: true
    });
    this.geolocService.setAutoPlace().pipe(
      finalize(() => this.setState({autoGeolocIsLoading: false}))
    )
    .subscribe({
      error: this.handleAutoGeolocError
    });
  }

  handleAutoGeolocError(error) {
    const {t} = this.props;
    console.warn('geolocService.setAutoPlace() emitted the following error', error);
    let alertMsg = '';

    if (error.status == 'browser_geolocation_error' && error.error.code == 1) {
      alertMsg = t('GEOLOC.ERRORS.PERMISSION_DENIED');
    } else if (error.status != 'aborted') {
      alertMsg = t('GEOLOC.ERRORS.UNKNOWN');
    }

    alertMsg && this.notificationService.alert(alertMsg, {type: 'error'});
  }

  render() {
    const {t} = this.props;

    return (
      <React.Fragment>
        <Autocomplete 
          controller={this.autocompleteController} 
          inputValue={this.state.inputValue}
          inputOnChange={(e, {newValue}) => this.setState({inputValue: newValue})}
          placeholder={t('INDEX.GEOLOC_PLACEHOLDER')}
          theme={style}
        />
        <div 
          className={classNames([style.autoGeoloc, {
            [style.loading]: this.state.autoGeolocIsLoading
          }])}
          data-testid="autoGeolocButton"
          onClick={this.setAutoPlace}
        >
          <div className={style.geolocIcon}>
            <img src={geolocIcon}/>
          </div>
          <span>{t('INDEX.AUTO_GEOLOC')}</span>
        </div>
      </React.Fragment>

    );
  }
}
