import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import crossWhite from '../../assets/images/cross_white.svg';
import style from './Tag.styl';

Tag.propTypes = {
  /**
   * Callback called when the close button is clicked
   */
  onClose: PropTypes.func,
  text: PropTypes.string,
  className: PropTypes.string
};
export function Tag(props) {
  let {text, onClose} = props;
  
  return (
    <div className={classNames([style.tag, props.className])}>
      <span className={style.text}>{props.text}</span>
      <button className={style.closeButton} onClick={() => onClose && onClose()}>
        <img src={crossWhite} />
      </button>
    </div>
  );
}
