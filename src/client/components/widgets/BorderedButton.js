import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import style from './BorderedButton.styl';


/**
 *
 * Usage : 
 *
 * <BorderedButton 
 *  text="Rechercher"
 *  onClick={() => this.performSearch()}
 *  className="searchButton"
 *  theme={theme}
 * />
 *
 *
 *
 */
export class BorderedButton extends React.Component {

  static propTypes = {
    /**
     * This class name will be applied to the top level DOM element of this component 
     */
    className: PropTypes.string,
    /** 
     * `theme` is an object as imported by CSS modules. The used class names are : ".text" (nested under top lever element)
     *
     *  @example 
     *
     *  CSS module file exported to variable `style` : 
     *
     *  ```
     *  .searchButton {
     *     ...
     *  }
     *  .searchButton .text {
     *    ...
     *  }
     *  ```
     *
     *  <BorderedButton className={style.searchButton} theme={style} />
     */
    theme: PropTypes.object,
    onClick: PropTypes.func,
    text: PropTypes.string
  }

  static defaultProps = {
    className: 'borderedButton',
    theme: {}
  }

  constructor(props) {
    super(props);
    this.onClickHandler = this.onClickHandler.bind(this);
  }

  render() {
    const {theme} = this.props;
    const customClassName = this.props.className;

    return (
      <button className={classNames([style.button, this.props.className])} onClick={this.onClickHandler}>
        <div className={classNames([style.text, theme.text])}>{this.props.text}</div>
      </button>
    );
  }

  onClickHandler() {
    if (this.props.onClick) {
      this.props.onClick();
    }
  }
}
