import React from 'react';
import PropTypes from 'prop-types';
import {withTranslation} from 'react-i18next';
import queryString from 'query-string';
import {Link} from 'react-router-dom';

import {ROUTES} from '../../routes';

import style from './ImproveDdfButton.styl';
import ddfWhiteLogo from '../../assets/images/ddf_logo_white.svg';
import plusButtonIcon from '../../assets/images/circled_plus_white.svg';

@withTranslation()
export class ImproveDdfButton extends React.Component {
  static propTypes = {
    currentForm: PropTypes.string,
  }

  render() {
    const {t, currentForm} = this.props;
    return (
      <Link className={style.container} to={{
        pathname: ROUTES.CREATE_LEXICAL_SENSE,
        search: currentForm && queryString.stringify({form: currentForm})
      }}>
        <div className={style.textLine}>
          <span>{t('FOOTER.IMPROVE_THE_DDF')}</span>
          <div className={style.ddfLogo}><img src={ddfWhiteLogo}/></div>
          <div className={style.plusButton}><img src={plusButtonIcon}/></div>
        </div>
      </Link>
    );
  }
}

