import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';

import style from './DesktopSideMenu.styl';

DesktopSideMenu.propTypes = {
  /**
   * It's the same object format that the one used by BannerMenu
   */
  entries: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired
  })),
  title: PropTypes.string
};
export function DesktopSideMenu(props) {
  const {entries, title} = props;

  return (
    <div className={style.container}>
      <div className={style.title}>
        {title}
      </div>
      {entries.map((entry, idx) => (
        <NavLink 
          key={idx} 
          className={style.link}
          activeClassName={style.active}
          to={entry.to}
        >
          {entry.text}
        </NavLink> 
      ))}
    </div>
  );
}

