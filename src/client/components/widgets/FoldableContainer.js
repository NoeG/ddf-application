import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import rightArrow from '../../assets/images/right_arrow_pink.svg';
import bottomArrow from '../../assets/images/bottom_arrow_white.svg';
import style from './FoldableContainer.styl';

export class FoldableContainer extends React.Component {

  static propTypes = {
    /**
     * Render prop, render content to display inside the foldable container. The function takes an option objects as argument with the
     * following propertise :
     * - close: function, call this function to close the foldable container
     * - open: function, call this function to open the foldable container
     */
    children: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    /**
     * A theme object. The class used are  :
     *   ".foldableContainerContent"
     */
    theme: PropTypes.object
  }

  static defaultProps = {
    theme: {}
  }

  state = {
    isOpen: false
  }

  constructor(props) {
    super(props);
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
  }
  
  render() {
    const {title, children, theme} = this.props;
    return (
      <div className={classNames([
        style.container, 
        {
          [style.open]: this.state.isOpen, 
          [style.closed]: !this.state.isOpen
        }
      ])}>
        <div className={style.titleBar} onClick={() => this.toggle()}>
          <div className={style.title}>{title}</div>
          <img className={classNames([style.icon, style.rightArrow])} src={rightArrow}/>
          <img className={classNames([style.icon, style.bottomArrow])} src={bottomArrow}/>
        </div>
        <If condition={this.state.isOpen}>
          <div className={classNames([style.content, theme.foldableContainerContent])}>
            {children({close: this.close, open: this.open})}
          </div>
        </If>
      </div>
    );
  }

  toggle() {
    this.setState(prevState => ({isOpen: !prevState.isOpen}));
  }

  close() {
    this.setState({isOpen: false});
  }

  open() {
    this.setState({isOpen: true});
  }
}
