import React, {useState} from 'react';
import PropTypes from 'prop-types';

import {getUserAuthenticationService} from '../../services/UserAuthenticationService';
import {ContributionsListTable} from './ContributionsList/ContributionsListTable';
import {Filters} from './ContributionsList/Filters';
import {PersonFilter, ProcessedFilter, ReviewedStatusFilter, useFilter} from './ContributionsList/filterUtility';

/**
 *
 * @param {int} [pageSize] - Controls how many results are displayed
 * @param {UserAuthenticationService} userAuthenticationService
 * @param {boolean} filterOnLoggedUser
 * @return {*}
 * @constructor
 */
ContributionsList.propTypes = {
  /** See prop hideColumns of ContributionsListTable */
  hideColumns: PropTypes.arrayOf(PropTypes.string),
  /** See prop pageSize of ContributionsListTable */
  pageSize: PropTypes.number,
  /** See prop filterOnLoggedUser of ContributionsListTable */
  filterOnLoggedUser: PropTypes.bool,
  /** Set this flag to enable the panel of filters */
  enableFiltering: PropTypes.bool,
  /** 
   * Filters initial values. Warning, this will enable the filter specified, even if the prop enableFiltering is not true.
   * The format is an object, in which the keys are the filter name, and the value the value to set to the filter (see Filter 
   * class defined in filterUtility.js)
   */
  filtersInitialValues: PropTypes.object
};
export function ContributionsList(props = {}) {
  /* Services DI */
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();

  let {
    pageSize, 
    filterOnLoggedUser, 
    hideColumns,
    enableFiltering,
    filtersInitialValues = {}
  } = props;

  let [filterOnUserAccountId, setFilterOnUserAccountId] = useState();

  let [personFilter, updatePersonFilter] = useFilter({filterClass: PersonFilter});
  let [processedStatusFilter, updateProcessedStatusFilter] = useFilter({filterClass: ProcessedFilter, filtersInitialValues});
  let [reviewedStatusFilter, updateReviewedStatusFilter] = useFilter({filterClass: ReviewedStatusFilter, filtersInitialValues});

  /* Determine API filters values from UI filters selection */
  let filterOnReviewed = null, filterOnExistingSuppressionRating = null, filterOnProcessed = null;
  if (processedStatusFilter.enabled) {
    filterOnProcessed = (processedStatusFilter.value === 'processed');
  }
  if (reviewedStatusFilter.enabled) {
    switch (reviewedStatusFilter.value) {
      case 'notReviewed': 
        filterOnReviewed = false;
        break;
      case 'askedForSuppression': 
        filterOnReviewed = true;
        filterOnExistingSuppressionRating = true;
        break;
      case 'reviewed':
        filterOnReviewed = true;
        filterOnExistingSuppressionRating = false;
        break;
    }
  }

  return (
    <React.Fragment>
      <If condition={enableFiltering}>
        <Filters 
          filters={[personFilter, reviewedStatusFilter, processedStatusFilter]}
          onFiltersUpdate={onFiltersUpdate}
          onFilterDisable={onFilterDisable}
        />
      </If>
      <ContributionsListTable
        pageSize={pageSize}
        filterOnLoggedUser={filterOnLoggedUser}
        filterOnUserAccountId={(personFilter.enabled && personFilter.value) || undefined}
        filterOnReviewed={filterOnReviewed}
        filterOnExistingSuppressionRating={filterOnExistingSuppressionRating}
        filterOnProcessed={filterOnProcessed}
        hideColumns={hideColumns}
        userAuthenticationService={userAuthenticationService}
      />
    </React.Fragment>
  );

  function onFiltersUpdate(newFiltersValues) {
    let newProcessedStatus = newFiltersValues.find(f => f.name === 'processedStatus');
    let newPerson = newFiltersValues.find(f => f.name === 'person');
    let newReviewedStatus = newFiltersValues.find(f => f.name === 'reviewedStatus');
    updateProcessedStatusFilter(newProcessedStatus);
    updatePersonFilter(newPerson);
    updateReviewedStatusFilter(newReviewedStatus);
  }

  function onFilterDisable(filterName) {
    switch(filterName) {
      case 'processedStatus':
        updateProcessedStatusFilter({enabled: false});
        break;
      case 'person':
        updatePersonFilter({enabled: false});
        break;
      case 'reviewedStatus':
        updateReviewedStatusFilter({enabled: false});
        break;
      default:
        return;
    }
  }

}
