import {gqlGetLexicalEntryInfo} from '../../LexicalSenseEditor';
import {gqlSearchConcepts} from '../../../widgets/forms/formik/ConceptAutocomplete';

export const gqlMocks = [{
  request: {
    query: gqlGetLexicalEntryInfo,
    variables: { 
      formWrittenRep: 'pomme',
    },
  },
  result: {
    "data": {
      "lexicalEntryContribInputsFromFormWrittenRep": {
        "__typename": "FormWrittenRepContribInputDefinitions",
        "lexicalEntryTypeName": "Word",
        "grammaticalCategorySchemeId": "http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type"
      }
    }
  }
}, {
  request: {
    query: gqlGetLexicalEntryInfo,
    variables: { 
      formWrittenRep: 'pommier',
    },
  },
  result: {
    "data": {
      "lexicalEntryContribInputsFromFormWrittenRep": {
        "__typename": "FormWrittenRepContribInputDefinitions",
        "lexicalEntryTypeName": "Word",
        "grammaticalCategorySchemeId": "http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type"
      }
    }
  }
}, {
  request: {
    query: gqlGetLexicalEntryInfo,
    variables: { 
      formWrittenRep: 'manger une pomme',
    },
  },
  result: {
    "data": {
      "lexicalEntryContribInputsFromFormWrittenRep": {
        "__typename": "FormWrittenRepContribInputDefinitions",
        "lexicalEntryTypeName": "MultiWordExpression",
        "grammaticalCategorySchemeId": "http://data.dictionnairedesfrancophones.org/authority/multiword-type"
      }
    }
  }
}, {
  request: {
    query: gqlGetLexicalEntryInfo,
    variables: { 
      formWrittenRep: '-eur',
    },
  },
  result: {
    "data": {
      "lexicalEntryContribInputsFromFormWrittenRep": {
        "__typename": "FormWrittenRepContribInputDefinitions",
        "lexicalEntryTypeName": "Affix",
        "grammaticalCategorySchemeId": "http://data.dictionnairedesfrancophones.org/authority/term-element"
      }
    }
  }
}, {
  request: {
    query: gqlSearchConcepts,
    variables: { 
      qs: 'nom',
      first: 20,
      filters: ['inScheme:http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type']
    },
  },
  result: {
    "data": {
      "concepts": {
        "__typename": "ConceptConnection",
        "edges": [{
          "__typename": "ConceptEdge",
          "node": {
            "__typename": "Concept",
            "id": "lexinfo:noun",
            "prefLabel": "nom"
          }
        }, {
          "__typename": "ConceptEdge",
          "node": {
            "__typename": "Concept",
            "id": "lexinfo:indefinitePronoun",
            "prefLabel": "pronom indéfini"
          }
        }]
      }
    }
  }
}, {
  request: {
    query: gqlSearchConcepts,
    variables: { 
      qs: 'loc',
      first: 20,
      filters: ['inScheme:http://data.dictionnairedesfrancophones.org/authority/multiword-type']
    },
  },
  result: {
    "data": {
      "concepts": {
        "__typename": "ConceptConnection",
        "edges": [{
          "__typename": "ConceptEdge",
          "node": {
            "__typename": "Concept",
            "id": "ddfa:multiword-type/interjectivePhrase",
            "prefLabel": "locution interjective"
          }
        }, {
          "__typename": "ConceptEdge",
          "node": {
            "__typename": "Concept",
            "id": "ddfa:multiword-type/interrogativePhrase",
            "prefLabel": "locution interrogative"
          }
        }]
      }
    }
  }
}, {
  request: {
    query: gqlSearchConcepts,
    variables: { 
      qs: 'suf',
      first: 20,
      filters: ['inScheme:http://data.dictionnairedesfrancophones.org/authority/term-element']
    },
  },
  result: {
    "data": {
      "concepts": {
        "__typename": "ConceptConnection",
        "edges": [{
          "__typename": "ConceptEdge",
          "node": {
            "__typename": "Concept",
            "id": "lexinfo:suffix",
            "prefLabel": "suffixe"
          }
        }]
      }
    }
  }
}];
