import React from 'react';
import {cleanup, fireEvent} from '@testing-library/react';
import {act} from 'react-dom/test-utils';
import waait from 'waait';

import {renderWithMocks} from '../../../../../jest/utilities/renderWithMocks';
import {ContributionsList} from '../ContributionsList';
import {gqlContributionsQuery} from '../ContributionsList/ContributionsListTable';

afterEach(cleanup);

let userAuthenticationServiceMock = {
  useLoggedUser: () => ({
    user: {}
  })
};

/**
 * TODO : 
 * - update processed status test, because by default there is no initial value for the filter
 * - make a test that the initial values for filters work
 */

describe('processed filter', () => {
  it('calls API when filter is set to unprocessed', async () => {
    let apiResult = jest.fn(() => ({
      "data": {
        "contributedLexicalSenses": {
          "edges": [],
          "pageInfo": {
            "endCursor": "offset:1",
            "hasNextPage": false,
            "__typename": "PageInfo"
          },
          "__typename": "LexicalSenseConnection"
        }
      }
    }));
    let gqlMocks = [{
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ],
          "filterOnLoggedUser":false,
          "filterOnReviewed": null,
          "filterOnExistingSuppressionRating": null,
          "filterOnProcessed": null
        }
      },
      result: () => {
        return {
          "data": {
            "contributedLexicalSenses": {
              "edges": [],
              "pageInfo": {
                "endCursor": "offset:1",
                "hasNextPage": false,
                "__typename": "PageInfo"
              },
              "__typename": "LexicalSenseConnection"
            }
          }
        };
      }
    }, {
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ],
          "filterOnLoggedUser":false,
          "filterOnReviewed": null,
          "filterOnExistingSuppressionRating": null,
          "filterOnProcessed": false
        }
      },
      result: () => {
        return apiResult();
      }
    }];

    const {container, findByText} = renderWithMocks({
      gqlMocks: gqlMocks,
      element: <ContributionsList userAuthenticationService={userAuthenticationServiceMock} enableFiltering/>,
    });

    fireEvent.click(await findByText('Ajouter des filtres'));
    fireEvent.click(await findByText('Afficher seulement les définitions non traitées'));
    fireEvent.click(await findByText('Enregistrer'));
    await findByText('Modifier les filtres');
    expect(apiResult).toHaveBeenCalled();
  });

  it('calls API when filter is set to processed', async () => {
    let apiResult = jest.fn(() => ({
      "data": {
        "contributedLexicalSenses": {
          "edges": [],
          "pageInfo": {
            "endCursor": "offset:1",
            "hasNextPage": false,
            "__typename": "PageInfo"
          },
          "__typename": "LexicalSenseConnection"
        }
      }
    }));
    let gqlMocks = [{
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ],
          "filterOnLoggedUser":false,
          "filterOnReviewed": null,
          "filterOnExistingSuppressionRating": null,
          "filterOnProcessed": null
        }
      },
      result: () => {
        return {
          "data": {
            "contributedLexicalSenses": {
              "edges": [],
              "pageInfo": {
                "endCursor": "offset:1",
                "hasNextPage": false,
                "__typename": "PageInfo"
              },
              "__typename": "LexicalSenseConnection"
            }
          }
        };
      }
    }, {
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ],
          "filterOnLoggedUser":false,
          "filterOnReviewed": null,
          "filterOnExistingSuppressionRating": null,
          "filterOnProcessed": true
        }
      },
      result: () => {
        return apiResult();
      }
    }];

    const {container, findByText} = renderWithMocks({
      gqlMocks: gqlMocks,
      element: <ContributionsList userAuthenticationService={userAuthenticationServiceMock} enableFiltering/>,
    });

    fireEvent.click(await findByText('Ajouter des filtres'));
    fireEvent.click(await findByText('Afficher seulement les définitions traitées'));
    fireEvent.click(await findByText('Enregistrer'));
    await findByText('Modifier les filtres');
    expect(apiResult).toHaveBeenCalled();
  });
});

describe('reviewed status filter', () => {
  it('calls API when filter is set to notReviewed', async () => {
    let apiResult = jest.fn(() => ({
      "data": {
        "contributedLexicalSenses": {
          "edges": [],
          "pageInfo": {
            "endCursor": "offset:1",
            "hasNextPage": false,
            "__typename": "PageInfo"
          },
          "__typename": "LexicalSenseConnection"
        }
      }
    }));
    let gqlMocks = [{
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ],
          "filterOnLoggedUser":false,
          "filterOnReviewed": null,
          "filterOnExistingSuppressionRating": null,
          "filterOnProcessed": null,
        }
      },
      result: () => {
        return {
          "data": {
            "contributedLexicalSenses": {
              "edges": [],
              "pageInfo": {
                "endCursor": "offset:1",
                "hasNextPage": false,
                "__typename": "PageInfo"
              },
              "__typename": "LexicalSenseConnection"
            }
          }
        };
      }
    }, {
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ],
          "filterOnLoggedUser":false,
          "filterOnLoggedUser":false,
          "filterOnReviewed": false,
          "filterOnExistingSuppressionRating": null,
          "filterOnProcessed": null,
        }
      },
      result: () => {
        return apiResult();
      }
    }];

    const {container, findByText} = renderWithMocks({
      gqlMocks: gqlMocks,
      element: <ContributionsList userAuthenticationService={userAuthenticationServiceMock} enableFiltering/>,
    });

    fireEvent.click(await findByText('Ajouter des filtres'));
    fireEvent.click(await findByText('Définitions non relues'));
    fireEvent.click(await findByText('Enregistrer'));
    await findByText('Modifier les filtres');
    expect(apiResult).toHaveBeenCalled();
  });

  it('calls API when filter is set to askedForSuppression', async () => {
    let apiResult = jest.fn(() => ({
      "data": {
        "contributedLexicalSenses": {
          "edges": [],
          "pageInfo": {
            "endCursor": "offset:1",
            "hasNextPage": false,
            "__typename": "PageInfo"
          },
          "__typename": "LexicalSenseConnection"
        }
      }
    }));
    let gqlMocks = [{
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ],
          "filterOnLoggedUser":false,
          "filterOnReviewed": null,
          "filterOnExistingSuppressionRating": null,
          "filterOnProcessed": null,
        }
      },
      result: () => {
        return {
          "data": {
            "contributedLexicalSenses": {
              "edges": [],
              "pageInfo": {
                "endCursor": "offset:1",
                "hasNextPage": false,
                "__typename": "PageInfo"
              },
              "__typename": "LexicalSenseConnection"
            }
          }
        };
      }
    }, {
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ],
          "filterOnLoggedUser":false,
          "filterOnLoggedUser":false,
          "filterOnReviewed": true,
          "filterOnExistingSuppressionRating": true,
          "filterOnProcessed": null,
        }
      },
      result: () => {
        return apiResult();
      }
    }];

    const {container, findByText} = renderWithMocks({
      gqlMocks: gqlMocks,
      element: <ContributionsList userAuthenticationService={userAuthenticationServiceMock} enableFiltering/>,
    });

    fireEvent.click(await findByText('Ajouter des filtres'));
    fireEvent.click(await findByText('Définitions en demande de suppression'));
    fireEvent.click(await findByText('Enregistrer'));
    await findByText('Modifier les filtres');
    expect(apiResult).toHaveBeenCalled();
  });

  it('calls API when filter is set to reviewed', async () => {
    let apiResult = jest.fn(() => ({
      "data": {
        "contributedLexicalSenses": {
          "edges": [],
          "pageInfo": {
            "endCursor": "offset:1",
            "hasNextPage": false,
            "__typename": "PageInfo"
          },
          "__typename": "LexicalSenseConnection"
        }
      }
    }));
    let gqlMocks = [{
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ],
          "filterOnLoggedUser":false,
          "filterOnReviewed": null,
          "filterOnExistingSuppressionRating": null,
          "filterOnProcessed": null,
        }
      },
      result: () => {
        return {
          "data": {
            "contributedLexicalSenses": {
              "edges": [],
              "pageInfo": {
                "endCursor": "offset:1",
                "hasNextPage": false,
                "__typename": "PageInfo"
              },
              "__typename": "LexicalSenseConnection"
            }
          }
        };
      }
    }, {
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ],
          "filterOnLoggedUser":false,
          "filterOnLoggedUser":false,
          "filterOnReviewed": true,
          "filterOnExistingSuppressionRating": false,
          "filterOnProcessed": null,
        }
      },
      result: () => {
        return apiResult();
      }
    }];

    const {container, findByText} = renderWithMocks({
      gqlMocks: gqlMocks,
      element: <ContributionsList userAuthenticationService={userAuthenticationServiceMock} enableFiltering/>,
    });

    fireEvent.click(await findByText('Ajouter des filtres'));
    fireEvent.click(await findByText('Définitions relues et validées'));
    fireEvent.click(await findByText('Enregistrer'));
    await findByText('Modifier les filtres');
    expect(apiResult).toHaveBeenCalled();
  });
});

describe('filters initial values', () => {
  it('sets the filters initial values based on props', async () => {
    let apiResult = jest.fn(() => ({
      "data": {
        "contributedLexicalSenses": {
          "edges": [],
          "pageInfo": {
            "endCursor": "offset:1",
            "hasNextPage": false,
            "__typename": "PageInfo"
          },
          "__typename": "LexicalSenseConnection"
        }
      }
    }));
    let gqlMocks = [{
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ],
          "filterOnLoggedUser":false,
          "filterOnReviewed": null,
          "filterOnExistingSuppressionRating": null,
          "filterOnProcessed": true,
        }
      },
      result: apiResult,
    }];

    const {container, findByText} = renderWithMocks({
      gqlMocks: gqlMocks,
      element: <ContributionsList 
        userAuthenticationService={userAuthenticationServiceMock} 
        filtersInitialValues={{
          processedStatus: 'processed'
        }}
        enableFiltering
      />,
    });

    await findByText('Modifier les filtres');
    await act(waait);
    expect(apiResult).toHaveBeenCalled();
  });
});
