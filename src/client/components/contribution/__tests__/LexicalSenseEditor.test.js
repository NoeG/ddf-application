import React from 'react';
import {act} from 'react-dom/test-utils';
import {render, cleanup, fireEvent, waitForElementToBeRemoved} from '@testing-library/react';
import waait from 'waait';

import {ROUTES} from '../../../routes';
import {LexicalSenseEditor} from '../LexicalSenseEditor';
import {gqlMocks} from './__gql_mocks__/LexicalSenseEditor.gqlMocks';
import {renderWithMocks} from '../../../../../jest/utilities/renderWithMocks';


afterEach(cleanup);

/* Disable console.warn for the tests */
let consolewarn;
beforeAll(() => {
  consolewarn = console.warn;
  console.warn = jest.fn(); 
});
afterAll(() => {
  console.warn = consolewarn;
});



describe("Creation", () => {
  describe("Changing the lexical entry type", () => {
    it('Allow to choose a grammatical category, then change the entry type, then choose again a different grammatical category', async () => {
      let submitMock = jest.fn();
      const {container, findByText, findByTestId, findByPlaceholderText} = renderWithMocks({
        gqlMocks,
        element: <LexicalSenseEditor
          onSubmit={submitMock}
        />
      });

      let formWrittenRepInput = await findByPlaceholderText('Mot ou expression *');
      fireEvent.change(formWrittenRepInput, {target: {value: 'pomme'}});
      fireEvent.click(await findByText("Continuer"));
      let grammaticalCategoryInput = await findByPlaceholderText('Catégorie grammaticale *');
      fireEvent.change(grammaticalCategoryInput, {target: {value: 'nom'}});
      grammaticalCategoryInput.focus();
      fireEvent.click(await findByText('pronom indéfini'));
      fireEvent.click(await findByTestId('edit-form-written-rep-button'));
      formWrittenRepInput = await findByPlaceholderText('Mot ou expression *');
      fireEvent.change(formWrittenRepInput, {target: {value: 'manger une pomme'}});
      fireEvent.click(await findByText("Continuer"));
      grammaticalCategoryInput = await findByPlaceholderText('Catégorie grammaticale *');
      expect(grammaticalCategoryInput.value).toEqual('');
      fireEvent.change(grammaticalCategoryInput, {target: {value: 'loc'}});
      grammaticalCategoryInput.focus();
      fireEvent.click(await findByText('locution interjective'));
      let definitionInput = await findByPlaceholderText('Définition *');
      fireEvent.change(definitionInput, {target: {value: 'ma définition'}});
      fireEvent.click(await findByText('Publier'));
      await act(waait);
      expect(submitMock.mock.calls.length).toEqual(1);
      expect(submitMock.mock.calls[0][0]).toEqual({
        formWrittenRep: 'manger une pomme',
        grammaticalCategoryId: 'ddfa:multiword-type/interjectivePhrase',
        lexicalEntryTypeName: "MultiWordExpression",
        definition: 'ma définition'
      });
    });

    it("Keeps the selected grammatical category if the user come back to change the written form, but the lexical entry doesn't change", async() => {
      const {container, findByText, findByTestId, findByPlaceholderText} = renderWithMocks({
        gqlMocks,
        element: <LexicalSenseEditor />
      });

      let formWrittenRepInput = await findByPlaceholderText('Mot ou expression *');
      fireEvent.change(formWrittenRepInput, {target: {value: 'pomme'}});
      fireEvent.click(await findByText("Continuer"));
      let grammaticalCategoryInput = await findByPlaceholderText('Catégorie grammaticale *');
      fireEvent.change(grammaticalCategoryInput, {target: {value: 'nom'}});
      grammaticalCategoryInput.focus();
      fireEvent.click(await findByText('pronom indéfini'));
      fireEvent.click(await findByTestId('edit-form-written-rep-button'));
      formWrittenRepInput = await findByPlaceholderText('Mot ou expression *');
      fireEvent.change(formWrittenRepInput, {target: {value: 'pommier'}});
      fireEvent.click(await findByText("Continuer"));
      grammaticalCategoryInput = await findByPlaceholderText('Catégorie grammaticale *');
      expect(grammaticalCategoryInput.value).toEqual('pronom indéfini');
    });
  });

  describe("directly to step 2", () => {
    it("takes the form written rep from the props", async () => {
      const {container, findByText, findByTestId, findByPlaceholderText} = renderWithMocks({
        gqlMocks,
        element: <LexicalSenseEditor
          formWrittenRep="manger une pomme"
        />,
      });

      let formWrittenRepElement = await findByText('manger une pomme');
      expect(formWrittenRepElement.textContent).toEqual('manger une pomme');
      let publishButton = await findByText('Publier');
      expect(publishButton.textContent).toEqual('Publier');
    });
  });
});

describe("Editing", () => {
  it("populates the screen at STEP2 with all values from props", async () => {
    let submitMock = jest.fn();
    const {container, findByText, findByTestId, findByPlaceholderText, queryByTestId} = renderWithMocks({
      gqlMocks,
      element: <LexicalSenseEditor
        onSubmit={submitMock}
        formWrittenRep="pomme"
        grammaticalCategoryId="lexinfo:noun"
        grammaticalCategoryInputValue="nom"
        definition="ma définition"
      />
    });

    let formWrittenRepElement = await findByText('pomme');
    expect(formWrittenRepElement.textContent).toEqual('pomme');

    /* we need to wait for the placeholder input to disappear, otherwise we will input into it instead of 
     * the autocomplete input */
    let autocompleteInputPlaceholder = queryByTestId('autocomplete-loading-placeholder');
    if (autocompleteInputPlaceholder) {
      await waitForElementToBeRemoved(() => queryByTestId('autocomplete-loading-placeholder'));
    }
    let grammaticalCategoryInput = await findByPlaceholderText('Catégorie grammaticale *');
    expect(grammaticalCategoryInput.value).toEqual('nom');

    let definitionInput = await findByPlaceholderText('Définition *');
    expect(definitionInput.value).toEqual('ma définition');

    fireEvent.click(await findByText('Publier'));
    await act(waait);

    expect(submitMock.mock.calls.length).toEqual(1);
    expect(submitMock.mock.calls[0][0]).toEqual({
      formWrittenRep: 'pomme',
      grammaticalCategoryId: 'lexinfo:noun',
      definition: 'ma définition',
      lexicalEntryTypeName: 'Word'
    });
  });
});



