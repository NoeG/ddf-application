import React from 'react';
import {act} from 'react-dom/test-utils';
import {render, cleanup, fireEvent, waitForElementToBeRemoved} from '@testing-library/react';
import waait from 'waait';

import {ROUTES} from '../../../routes';
import {CreateLexicalSense, gqlCreateLexicalSense} from '../CreateLexicalSense';
import {renderWithMocks} from '../../../../../jest/utilities/renderWithMocks';
import {gqlMocks} from './__gql_mocks__/LexicalSenseEditor.gqlMocks';
import {generateUserAuthenticationServiceMock} from '../../../../../jest/utilities/servicesMocks/UserAuthenticationServiceMock';

let userAuthenticationServiceMock = generateUserAuthenticationServiceMock({loggedIn: true});

afterEach(cleanup);

/* Disable console.warn for the tests */
let consolewarn;
beforeAll(() => {
  consolewarn = console.warn;
  console.warn = jest.fn(); 
});
afterAll(() => {
  console.warn = consolewarn;
});

describe("CreateLexicalSense", () => {
  it('Calls the CreatexLexicalSense mutation on submit', async () => {
    let mutationCalled = false;
    let gqlCreationMock = {
      request: {
        query: gqlCreateLexicalSense,
        variables: { 
          formWrittenRep: 'manger une pomme',
          definition: 'ma définition',
          lexicalEntryTypeName: 'MultiWordExpression',
          grammaticalCategoryId: 'ddfa:multiword-type/interjectivePhrase'
        },
      },
      result: () => {
        mutationCalled = true;
        return {
          "data": {
            "createLexicalSense": {
              "createdEdge": {
                "node": {
                  "id": "lexical-sense/fv52jfndd2bke2",
                  "lexicalEntry": {
                    "canonicalForm": {
                      "writtenRep": "Manger une pomme",
                      "__typename": "Form"
                    },
                    "__typename": "MultiWordExpression"
                  },
                  "__typename": "LexicalSense"
                },
                "__typename": "LexicalSenseEdge"
              },
              "__typename": "CreateLexicalSensePayload"
            }
          }
        }
      }
    };

    const {container, findByText, findByTestId, findByPlaceholderText} = renderWithMocks({
      gqlMocks: [...gqlMocks, gqlCreationMock],
      element: <CreateLexicalSense 
        userAuthenticationService={userAuthenticationServiceMock} 
      />
    });

    let formWrittenRepInput = await findByPlaceholderText('Mot ou expression *');
    fireEvent.change(formWrittenRepInput, {target: {value: 'manger une pomme'}});
    fireEvent.click(await findByText("Continuer"));
    let grammaticalCategoryInput = await findByPlaceholderText('Catégorie grammaticale *');
    expect(grammaticalCategoryInput.value).toEqual('');
    fireEvent.change(grammaticalCategoryInput, {target: {value: 'loc'}});
    grammaticalCategoryInput.focus();
    fireEvent.click(await findByText('locution interjective'));
    let definitionInput = await findByPlaceholderText('Définition *');
    fireEvent.change(definitionInput, {target: {value: 'ma définition'}});
    fireEvent.click(await findByText('Publier'));
    await act(waait);
    await act(waait);
    expect(mutationCalled).toEqual(true);
  });

  it("takes the form written rep from querystring parameter 'form'", async () => {
    let mutationCalled = false;
    let gqlCreationMock = {
      request: {
        query: gqlCreateLexicalSense,
        variables: { 
          formWrittenRep: 'manger une pomme',
          definition: 'ma définition',
          lexicalEntryTypeName: 'MultiWordExpression',
          grammaticalCategoryId: 'ddfa:multiword-type/interjectivePhrase'
        },
      },
      result: () => {
        mutationCalled = true;
        return {
          "data": {
            "createLexicalSense": {
              "createdEdge": {
                "node": {
                  "id": "lexical-sense/fv52jfndd2bke2",
                  "lexicalEntry": {
                    "canonicalForm": {
                      "writtenRep": "Manger une pomme",
                      "__typename": "Form"
                    },
                    "__typename": "MultiWordExpression"
                  },
                  "__typename": "LexicalSense"
                },
                "__typename": "LexicalSenseEdge"
              },
              "__typename": "CreateLexicalSensePayload"
            }
          }
        }
      }
    };
    const {container, findByText, findByTestId, findByPlaceholderText, queryByTestId} = renderWithMocks({
      gqlMocks: [...gqlMocks, gqlCreationMock],
      element: <CreateLexicalSense 
        userAuthenticationService={userAuthenticationServiceMock} 
      />,
      locationPath: "/sense/new?form=manger%20une%20pomme",
      routePath: ROUTES.CREATE_LEXICAL_SENSE
    });

    let formWrittenRepElement = await findByText('manger une pomme');
    expect(formWrittenRepElement.textContent).toEqual('manger une pomme');

    /* we need to wait for the placeholder input to disappear, otherwise we will input into it instead of 
     * the autocomplete input */
    let autocompleteInputPlaceholder = queryByTestId('autocomplete-loading-placeholder');
    if (autocompleteInputPlaceholder) {
      await waitForElementToBeRemoved(() => queryByTestId('autocomplete-loading-placeholder'));
    }
    /* Select the grammatical category */
    let grammaticalCategoryInput = await findByPlaceholderText('Catégorie grammaticale *');
    fireEvent.change(grammaticalCategoryInput, {target: {value: 'loc'}});
    grammaticalCategoryInput.focus();
    fireEvent.click(await findByText('locution interjective'));

    /* input the definition */
    let definitionInput = await findByPlaceholderText('Définition *');
    fireEvent.change(definitionInput, {target: {value: 'ma définition'}});

    /* publish */
    fireEvent.click(await findByText('Publier'));
    await act(waait);
    await act(waait);
    expect(mutationCalled).toEqual(true);
  });
});
