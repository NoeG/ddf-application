import React  from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import gql from 'graphql-tag';
import {useMutation} from '@apollo/react-hooks';
import {formatRoute} from 'react-router-named-routes';
import {useHistory, useLocation} from 'react-router-dom';
import queryString from 'query-string';
import {Helmet} from 'react-helmet-async';

import {ROUTES} from '../../routes';
import {SimpleModalPage} from '../../layouts/mobile/SimpleModalPage';
import {DesktopMainLayout} from '../../layouts/desktop/DesktopMainLayout';
import {DesktopSubHeader} from '../../layouts/desktop/DesktopSubHeader';
import {useMediaQueries} from '../../layouts/MediaQueries';
import {notificationService as appNotificationService} from '../../services/NotificationService';
import {useBrowsingHistoryHelperService} from '../../services/BrowsingHistoryHelperService';
import {GraphQLErrorHandler} from '../../services/GraphQLErrorHandler';
import {getUserAuthenticationService} from '../../services/UserAuthenticationService';
import {LexicalSenseEditor} from './LexicalSenseEditor';

import simpleModalPageStyle from '../../layouts/responsive/SimpleModalPage.styl';
import style from './CreateLexicalSense.styl';

export const gqlCreateLexicalSense = gql`
  mutation CreateLexicalSense($definition: String!, $formWrittenRep: String!, $lexicalEntryTypeName: String!, $grammaticalCategoryId: String) {
    createLexicalSense(input: {
      definition: $definition, 
      formWrittenRep: $formWrittenRep,
      lexicalEntryTypeName: $lexicalEntryTypeName,
      grammaticalCategoryId: $grammaticalCategoryId
    }) {
      createdEdge {
        node {
          id
          lexicalEntry {
            canonicalForm {
              id
              writtenRep
            }
          }
        }
      }
    }
  }
`;

/**
 * This component requires a user to be logged. Otherwise it will redirect to the login page.
 *
 * The CreateLexicalSense screen uses the LexicalSenseEditor in the following way : 
 *
 * - By default, the editor is empty
 * - If the URL has the querystring parameter "form", the editor is initialized with this value as formWrittenRep
 * - On submit, the mutation to create a lexical sense is run
 */
export function CreateLexicalSense(props) {
  /* Services DI */
  const notificationService = props.notificationService || appNotificationService;
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();

  /* Hooks */
  const {isLogged} = userAuthenticationService.useLoggedUser();
  const {t} = useTranslation();
  const history = useHistory();
  const location = useLocation();
  const {browsingHistoryHelperService} = useBrowsingHistoryHelperService();
  const [createLexicalSenseMutation] = useMutation(gqlCreateLexicalSense);
  const {isMobile} = useMediaQueries();
  

  /** Initializing state from query string params */
  const parsedQueryString = queryString.parse(location.search);
  let initialFormWrittenRep = parsedQueryString.form;

  if (!isLogged) {
    return null;
  } else {
    return isMobile ? renderMobile() : renderDesktop();
  }

  function renderMobile() {
    return (
      <SimpleModalPage
        title={t('CONTRIBUTION.CREATE_LEXICAL_SENSE.TITLE')}
        content={renderContent()}
        color="yellow"
      />
    );
  }

  function renderDesktop() {
    return (
      <DesktopMainLayout useDefaultGrid>
        <DesktopSubHeader 
          primaryTitle={t('CONTRIBUTION.DESKTOP_TITLE')}
          secondaryTitle={t('CONTRIBUTION.CREATE_LEXICAL_SENSE.TITLE')}
          theme={style}
        />
        <div className={simpleModalPageStyle.content}>
          {renderContent()}
        </div>
      </DesktopMainLayout>
    );
  }

  function renderContent() {
    return (
      <React.Fragment>
        <Helmet>
          <title>{t('DOCUMENT_TITLES.CREATE_LEXICAL_SENSE')}</title>
        </Helmet>
        <LexicalSenseEditor 
          formWrittenRep={initialFormWrittenRep}
          onSubmit={handleSubmit}
          onCancel={navigateBack}
        />
      </React.Fragment>
    );
  }

  async function handleSubmit(formikValues, formikOptions) {
    try {
      let result = await createLexicalSenseMutation({
        variables: formikValues
      });
      let newSense = result.data.createLexicalSense.createdEdge.node;
      /* Build route to the newly created sense from the returned data */
      let senseUrl = formatRoute(ROUTES.FORM_LEXICAL_SENSE, {
        formQuery: newSense.lexicalEntry.canonicalForm.writtenRep,
        lexicalSenseId: newSense.id
      });
      history.push(senseUrl);
    } catch (error) {
      let {globalErrorMessage} = GraphQLErrorHandler(error, {formikOptions, t});
      notificationService.error(globalErrorMessage);
    } finally {
      formikOptions.setSubmitting(false);
    }
  }

  function navigateBack() {
    let locObject = browsingHistoryHelperService.getBackFromSecondaryScreenLocation();
    history.push(locObject ? locObject.location : '/');
  }
}
