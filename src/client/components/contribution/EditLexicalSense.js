import React, {useEffect} from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import gql from 'graphql-tag';
import {formatRoute} from 'react-router-named-routes';
import {useHistory, useLocation, useRouteMatch} from 'react-router-dom';
import {useQuery, useMutation} from '@apollo/react-hooks';
import {Helmet} from 'react-helmet-async';

import {ROUTES} from '../../routes';
import {SimpleModalPage} from '../../layouts/mobile/SimpleModalPage';
import {DesktopMainLayout} from '../../layouts/desktop/DesktopMainLayout';
import {DesktopSubHeader} from '../../layouts/desktop/DesktopSubHeader';
import {useMediaQueries} from '../../layouts/MediaQueries';
import {notificationService as appNotificationService} from '../../services/NotificationService';
import {getUserAuthenticationService} from '../../services/UserAuthenticationService';
import {LexicalSenseEditor} from './LexicalSenseEditor';
import {gqlLexicalEntryPartOfSpeechListFragment, getLexicalEntryGrammaticalCategory} from '../../utilities/helpers/lexicalEntryPartOfSpeechList';
import {GraphQLErrorHandler} from '../../services/GraphQLErrorHandler';

import simpleModalPageStyle from '../../layouts/responsive/SimpleModalPage.styl';
import style from './CreateLexicalSense.styl';

export let gqlLexicalSenseQuery = gql`
  query LexicalSense_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId){
      id
      definition
      lexicalEntry{
        id
        canonicalForm{
          id
          writtenRep
        }
        ...LexicalEntryPartOfSpeechListFragment
      }
    }
  }

  ${gqlLexicalEntryPartOfSpeechListFragment}
`;


export let gqlUpdateLexicalSense = gql`
  mutation UpdateLexicalSense($lexicalSenseId: ID!, $definition: String!, $grammaticalCategoryId: String) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,
      definition: $definition,
      grammaticalCategoryId: $grammaticalCategoryId
    }) {
      updatedObject {
        id
        definition
        lexicalEntry {
          id
          canonicalForm {
            id
            writtenRep
          }
        }
      }
    }
  }
`;

export function EditLexicalSense(props) {
  /* Services DI */
  const notificationService = props.notificationService || appNotificationService;
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();

  /* Hooks */
  const {isLogged} = userAuthenticationService.useLoggedUser();
  const {t} = useTranslation();
  const history = useHistory();
  const location = useLocation();
  const match = useRouteMatch();
  const [updateLexicalSenseMutation] = useMutation(gqlUpdateLexicalSense);
  const {isMobile} = useMediaQueries();

  /** Initializing state from query string params */
  let formWrittenRep = decodeURIComponent(match.params.formQuery);
  let lexicalSenseId = decodeURIComponent(match.params.lexicalSenseId);

  /* GraphQL query */
  const {data, loading, error} = useQuery(gqlLexicalSenseQuery, {
    variables: {lexicalSenseId},
    fetchPolicy: 'cache-and-network'
  });
  const lexicalSense = data?.lexicalSense;

  useEffect(() => {
    if (error) {
      let {globalErrorMessage} = GraphQLErrorHandler(mutationResult.error, {t});
      notificationService.error(globalErrorMessage);
    }
  }, [error]);

  let grammaticalCategory = lexicalSense && getLexicalEntryGrammaticalCategory(lexicalSense.lexicalEntry);

  if (!isLogged) {
    return null;
  } else {
    return isMobile ? renderMobile() : renderDesktop();
  }

  function renderMobile() {
    return (
      <SimpleModalPage
        title={t('CONTRIBUTION.EDIT_LEXICAL_SENSE.TITLE')}
        content={renderContent()}
        color="yellow"
      />
    );
  }

  function renderDesktop() {
    return (
      <DesktopMainLayout useDefaultGrid>
        <DesktopSubHeader 
          primaryTitle={t('CONTRIBUTION.DESKTOP_TITLE')}
          secondaryTitle={t('CONTRIBUTION.EDIT_LEXICAL_SENSE.TITLE')}
          theme={style}
        />
        <div className={simpleModalPageStyle.content}>
          {renderContent()}
        </div>
      </DesktopMainLayout>
    );
  }


  function renderContent() {
    return (
      <React.Fragment>
        <Helmet>
          <title>{t('DOCUMENT_TITLES.EDIT_LEXICAL_SENSE', {formWrittenRep})}</title>
        </Helmet>
        <LexicalSenseEditor 
          formWrittenRep={formWrittenRep}
          grammaticalCategoryId={grammaticalCategory?.id}
          grammaticalCategoryInputValue={grammaticalCategory?.prefLabel}
          definition={lexicalSense?.definition}
          canEditFormWrittenRep={false}
          onSubmit={handleSubmit}
          onCancel={navigateBack}
        />
      </React.Fragment>
    );
  }

  async function handleSubmit(formikValues, formikOptions) {
    try {
      let result = await updateLexicalSenseMutation({
        variables: {
          ...formikValues,
          lexicalSenseId
        }
      });
      navigateBack();
    } catch (error) {
      let {globalErrorMessage} = GraphQLErrorHandler(error, {formikOptions, t});
      notificationService.error(globalErrorMessage);
    } finally {
      formikOptions.setSubmitting(false);
    }
  }

  function navigateBack() {
    let senseUrl = formatRoute(ROUTES.FORM_LEXICAL_SENSE, {
      formQuery: formWrittenRep,
      lexicalSenseId
    });
    history.push(senseUrl);
  }
}
