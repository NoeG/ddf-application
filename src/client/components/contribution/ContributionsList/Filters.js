import React, {useState} from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import invariant from 'invariant';
import {useTranslation} from 'react-i18next';
import {Formik} from 'formik';

import {Filter} from './filterUtility';
import {Button} from '../../widgets/forms/Button';
import {RadioButton as FormikRadioButton} from '../../widgets/forms/formik/RadioButton';
import {PersonAutocomplete} from '../../widgets/forms/formik/PersonAutocomplete';
import {Form} from '../../widgets/forms/formik/Form';
import {ButtonRow} from '../../widgets/forms/ButtonRow';
import {Tag} from '../../widgets/Tag';

import style from './Filters.styl';

Filters.propTypes = {

  /**
   * This is a handler, called when the filters panel is closed with the "save" button. It gives the value of the filters as they were
   * edited by the user.
   *
   * Syntax : 
   *
   * onFiltersUpdate(newFiltersValues)
   *
   * Parameters :
   *
   * `newFiltersValues` is an array of objects giving the new value for one filter. The object filter has the following syntax : 
   *
   * {
   *   name: <string>
   *   value: <string>
   *   enabled: <boolean>
   *   data: <any>
   * }
   *
   * - name: the name that is a unique identifier of the filter
   * - value: the textual value that will be used directly for the filter value on the API request
   * - enabled : the flag indicates if the filter is currently enabled
   * - data : extra data, is used for the person filter, in order to pass the whole Person object, in addition to the filter value (which is the user ID)
   *
   */
  onFiltersUpdate: PropTypes.func.isRequired,
  /**
   * Callback called when a single filter is disabled. 
   *
   * Syntax : 
   *
   * onFilterDisable(filterName)
   *
   */ 
  onFilterDisable: PropTypes.func.isRequired,
  /** 
   * List of filter to use. Note : for text filters, only the specific mechanism for the person filter (must be named 'person') is implemented.
   * There is no generic implementation of a text filter.
   *
   * You may however add any radio filter you want.
   */
  filters: PropTypes.arrayOf(PropTypes.instanceOf(Filter)).isRequired
};
export function Filters(props) {

  let {
    filters,
    onFiltersUpdate,
    onFilterDisable
  } = props;

  const {t} = useTranslation();
  const [filtersPanelOpen, setFiltersPanelOpen] = useState(false);
  const openFiltersPanel = () => setFiltersPanelOpen(true);
  const closeFiltersPanel = () => setFiltersPanelOpen(false);

  invariant(new Set(filters.map(f => f.name)).size === filters.length, "Duplicate filter name. Names are used as the filter identifier, thus should not be duplicated");

  let formikInitialValues = {};
  let formikInitialStatus = {autocompleteInputValues: {}};
  /* Populate formikInitialValues */
  filters.forEach(filter => {
    if (filter.type === 'radio') {
      formikInitialValues[filter.name] = filter.enabled ? filter.value : 'disabled';
    } else if (filter.type === 'text' && filter.name === 'person') {
      /* 
       * Special case for the 'person' filter, we need to store the 'data' field which contains the user object, and 
       * initialize the PersonAutocomplete displayed value 
       */
      if (filter.enabled) {
        formikInitialValues[filter.name] = filter.data;
        formikInitialStatus.autocompleteInputValues[filter.name] = filter.data.nickName;
      }
    }
  });

  return (
    <div className={style.container}>
      <If condition={filtersPanelOpen}>
        {renderFiltersPanel()}
      </If>
      <If condition={!filtersPanelOpen}>
        {renderFiltersOverview()}
      </If>
    </div>
  );

  function renderFiltersOverview() {
    let enabledFilters = filters.filter(f => f.enabled);
    let buttonText = enabledFilters.length > 0 ? t('CONTRIBUTIONS_LIST.FILTERS.EDIT_FILTERS_BUTTON') : t('CONTRIBUTIONS_LIST.FILTERS.ADD_FILTERS_BUTTON');

    return (
      <React.Fragment>
        <Button onClick={openFiltersPanel}>{buttonText}</Button>
        <div className={style.badgesContainer}>
          {
            enabledFilters.map(filter => (
              <Tag key={filter.name} text={filter.tagLabel} onClose={() => onFilterDisable(filter.name)} className={style.badge}/>
            ))
          }
        </div>
      </React.Fragment>
    );
  }

  function renderFiltersPanel() {
    return (
      <div className={style.filtersPanel}>
        <Formik
          initialValues={formikInitialValues}
          initialStatus={formikInitialStatus}
          onSubmit={submitFilters}
          render={({values, isSubmitting}) => (
            <Form>
              {filters.map(renderFilterPanelSection)}
              <ButtonRow>
                <Button type="button" secondary onClick={closeFiltersPanel}>{t('CONTRIBUTIONS_LIST.FILTERS.CANCEL')}</Button>
                <Button type="submit" disabled={isSubmitting}>{t('CONTRIBUTIONS_LIST.FILTERS.SAVE')}</Button>
              </ButtonRow>
            </Form>
          )}
        />
      </div>
    );
  }

  function renderFilterPanelSection(filter) {
    invariant(!(filter.type === 'text' && filter.name !== 'person'), "Generic filters of type text are not supported. Only the filter 'person' is supported");

    return (
      <React.Fragment key={filter.name}>
        <div className={style.panelSectionTitle}>{filter.editionTitleLabel}</div>
        <If condition={filter.type === 'radio'}>
          {renderRadioFilterPanelSection(filter)}
        </If>
        <If condition={filter.type === 'text' && filter.name === 'person'}>
          {renderPersonFilterPanelSection(filter)}
        </If>
      </React.Fragment>
    )
  }

  function renderRadioFilterPanelSection(filter) {
    return (
      <React.Fragment>
        {filter.options.map(option => <FormikRadioButton key={option.value} name={filter.name} value={option.value} label={option.label}/>)}
        <FormikRadioButton name={filter.name} value="disabled" label={t('CONTRIBUTIONS_LIST.FILTERS.RADIO_DISABLED_OPTION_LABEL')}/>
      </React.Fragment>
    )
  }

  function renderPersonFilterPanelSection(filter) {
    return  <PersonAutocomplete
      name="person"
      placeholder="Filtrer sur un utilisateur"
      selectNullOnEmpty
    />
  }

  function submitFilters(values, {setSubmitting}) {
    /**
     * Radio filters : detect when the choice is 'disabled', or when a value is selected 
     */
    let newFiltersValues = filters.filter(f => f.type === 'radio').map(({name, value: previousValue}) => {
      let isDisabled = values[name] === 'disabled';
      return {
        name,
        value: isDisabled ?  previousValue : values[name],
        enabled: !isDisabled
      }
    });

    /**
     * Person filter
     */
    let personFilter = filters.find(f => f.name === 'person');
    if (personFilter) {
      let formikValue = values[personFilter.name];
      let newFilterValue;
      if (formikValue)  {
        newFilterValue = {
          name: personFilter.name,
          value: formikValue.userAccount.id,
          data: formikValue,
          enabled: true
        };
      } else {
        /* Send enabled: false + previous values */
        newFilterValue = {
          name: personFilter.name,
          value: personFilter.value,
          data: personFilter.data,
          enabled: false
        };
      }
      newFiltersValues.push(newFilterValue);
    }

    onFiltersUpdate(newFiltersValues);
    closeFiltersPanel();
  }

}


