import {useState} from 'react';

/** 
 * Utility class to represent a filter 
 *
 * @property {'text'|'radio'} type
 * @property {string} name Acts as an identifier for the filter
 * @property {boolean} enabled 
 * @property {string} value The programmatical value for the filter. Intended to be used as it is in the API query
 * @property {string} tagLabel The text to use to display the filter current value in a tag.
 * @property {any} data Free field to store any data necessary to keep the state of the filter
 * @property {string} editionTitleLabel The label to use a title for the filter section in the filters edition panel
 * @property {Array<option>} options Description of the options in case of type 'radio'
 * @property {string} options[].value
 * @property {string} options[].label
 */
export class Filter {
  constructor(opts) {
    this.enabled = opts.enabled || false;
    this.type = opts.type;
    this.name = opts.name;
    this.value = opts.value;
    this.data = opts.data;
    this.editionTitleLabel = opts.editionTitleLabel;
    this.options = opts.options;
  }
}

/**
 * Specificity of this subclass :
 *
 * The property `data` must contain a user object as returned by the autocompleted component PersonAutocomplete.
 *
 */
export class PersonFilter extends Filter {
  constructor(opts = {}) {
    super(opts);
    this.type = 'text';
    this.name = opts.name || 'person';
    this.editionTitleLabel = opts.editionTitleLabel || 'Personne';
  }

  get tagLabel() { 
    return `Personne: ${this.data.nickName}`;
  }
}


export class ProcessedFilter extends Filter {
  static defaultFilterName = 'processedStatus';

  constructor(opts = {}) {
    super(opts);
    this.type = 'radio';
    this.name = opts.name || ProcessedFilter.defaultFilterName;
    this.editionTitleLabel = opts.editionTitleLabel || 'Définitions traitées';
    this.options = [{
      value: 'processed',
      label: 'Afficher seulement les définitions traitées'
    }, {
      value: 'unprocessed',
      label: 'Afficher seulement les définitions non traitées'
    }];
    this.tagLabels = {
      'processed': 'Définitions traitées',
      'unprocessed': 'Définitions non traitées'
    }
  }

  get tagLabel() {
    return this.tagLabels[this.value];
  }
}

export class ReviewedStatusFilter extends Filter {
  static defaultFilterName = 'reviewedStatus';

  constructor(opts = {}) {
    super(opts);
    this.type = 'radio';
    this.name = opts.name || ReviewedStatusFilter.defaultFilterName;
    this.editionTitleLabel = opts.editionTitleLabel || 'Définitions relues';
    this.options = [{
      value: 'notReviewed',
      label: 'Définitions non relues'
    }, {
      value: 'askedForSuppression',
      label: 'Définitions en demande de suppression'
    }, {
      value: 'reviewed',
      label: 'Définitions relues et validées'
    }];
    this.tagLabels = {
      'notReviewed': 'Définitions non relues',
      'askedForSuppression': 'Définitions en demandes de suppression',
      'reviewed': 'Définitions relues et validées'
    }
  }

  get tagLabel() {
    return this.tagLabels[this.value];
  }
}

/**
 * Utility hook, to simplify the update callback (boilerplate), and allow the initialization of the filter with default value
 */
export function useFilter({
  filterClass, 
  filtersInitialValues = {}, 
  filterConstructorOptions
}) {
  let filterName = filterConstructorOptions?.name || filterClass.defaultFilterName;
  let [filter, internalUpdateFilter] = useState(() => {
    let options = {...filterConstructorOptions};
    if (filtersInitialValues[filterName]) {
      options.value = filtersInitialValues[filterName];
      options.enabled = true;
    }
    return new filterClass(options);
  });

  /**
   * @param {Object} options
   * @param {string} options.value
   * @param {boolean} options.enabled
   * @param {any} options.data
   */
  function updateFilter(opts) {
     internalUpdateFilter(filter => new filterClass({...filter, ...opts}));
  }

  return [filter, updateFilter];
}
