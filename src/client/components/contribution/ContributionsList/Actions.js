import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {useObservable} from 'react-use';

import {getUserAuthenticationService} from '../../../services/UserAuthenticationService';
import {GraphQLErrorHandler} from '../../../services/GraphQLErrorHandler';
import {notificationService as appNotificationService} from '../../../services/NotificationService';
import {ValidationRatingAction} from '../../LexicalSense/LexicalSenseActions/ValidationRatingAction';
import {SuppressionRatingAction} from '../../LexicalSense/LexicalSenseActions/SuppressionRatingAction';
import {RemoveAction} from '../../LexicalSense/LexicalSenseActions/RemoveAction';
import {ProcessAction} from '../../LexicalSense/LexicalSenseActions/ProcessAction';

import style from './Actions.styl';

Actions.propTypes = {};
export function Actions(props) {
  const {lexicalSense, onRemoveSuccess, onRemoveUpdateCache} = props;
  let {t} = useTranslation();
  let notificationService = props.notificationService || appNotificationService;
  let userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const {user} = useObservable(userAuthenticationService.currentUser, {});

  /**
   * Safeguard, actions should be displayed only for the admin panel
   */
  if (!(user?.userAccount?.isOperator || user?.userAccount?.isAdmin)) {
    return null;
  }

  return (
    <div className={style.actions}>
      <If condition={user?.userAccount?.isOperator}>
        <ValidationRatingAction 
          lexicalSense={lexicalSense} 
          theme={style}
          onActionError={onActionError} 
          enableRefetchQuery
        />

        <SuppressionRatingAction 
          lexicalSense={lexicalSense} 
          theme={style} 
          onActionError={onActionError} 
          enableRefetchQuery
        />
      </If>

      <If condition={user?.userAccount?.isAdmin}>
        <ProcessAction
          lexicalSense={lexicalSense}
          theme={style}
          onActionError={onActionError}
        />
        <RemoveAction 
          lexicalSense={lexicalSense} 
          theme={style} 
          onActionError={onActionError} 
          onRemoveSuccess={onRemoveSuccess} 
          onRemoveUpdateCache={onRemoveUpdateCache}
        />
      </If>
    </div>
  );

  async function onActionError(error) {
    let {globalErrorMessage} = GraphQLErrorHandler(error, {t});
    await notificationService.error(globalErrorMessage);
  }
}
