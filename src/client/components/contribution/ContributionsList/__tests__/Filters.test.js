import React from 'react';
import {render, cleanup, fireEvent} from '@testing-library/react';

import {renderWithMocks} from '../../../../../../jest/utilities/renderWithMocks';
import {Filters} from '../Filters';
import {Filter} from '../filterUtility';
import {gqlSearchPersons} from '../../../widgets/forms/formik/PersonAutocomplete';

afterEach(cleanup);

describe("Filters", () => {
  describe("radio filter", () => {
    it('selects the correct radio button when the filter has an initial value', async () => {
      let myFilter = new Filter({
        enabled: true,
        type: 'radio',
        name: 'myCustomFilter',
        value: 'option1',
        editionTitleLabel: 'My Custom Filter',
        options: [{
          value: 'option1',
          label: 'My first option'
        }, {
          value: 'option2',
          label: 'My second option'
        }]
      });

      let {container, findByText, findByLabelText} = render(<Filters
        filters={[myFilter]}
        onFiltersUpdate={() => {}}
        onFilterDisable={() => {}}
      />);
      fireEvent.click(await findByText("Modifier les filtres"));
      let option1Radio = await findByLabelText('My first option');
      let disabledRadio = await findByLabelText('Ne pas filtrer');
      expect(option1Radio.checked).toEqual(true);
      expect(disabledRadio.checked).toEqual(false);
    });

    it('selects the correct radio button when the filter is initially disabled', async () => {
      let myFilter = new Filter({
        enabled: false,
        type: 'radio',
        name: 'myCustomFilter',
        value: 'option1',
        editionTitleLabel: 'My Custom Filter',
        options: [{
          value: 'option1',
          label: 'My first option'
        }, {
          value: 'option2',
          label: 'My second option'
        }]
      });

      let {container, findByText, findByLabelText} = render(<Filters
        filters={[myFilter]}
        onFiltersUpdate={() => {}}
        onFilterDisable={() => {}}
      />);
      fireEvent.click(await findByText("Ajouter des filtres"));
      let option1Radio = await findByLabelText('My first option');
      let disabledRadio = await findByLabelText('Ne pas filtrer');
      expect(option1Radio.checked).toEqual(false);
      expect(disabledRadio.checked).toEqual(true);
    });

    it('calls onFiltersUpdate with the correct values', async () => {
      /* 
       * Test case : 
       * - one radio filter has a selected value
       * - one radio filter is disabled
       */
      let priceFilter = new Filter({
        enabled: false,
        type: 'radio',
        name: 'price',
        editionTitleLabel: 'Price',
        options: [{
          value: 'expansive',
          label: 'Expansive'
        }, {
          value: 'cheap',
          label: 'Cheap'
        }]
      });
      let colorFilter = new Filter({
        enabled: false,
        type: 'radio',
        name: 'color',
        editionTitleLabel: 'Color',
        options: [{
          value: 'red',
          label: 'Red'
        }, {
          value: 'blue',
          label: 'Blue'
        }]
      });

      let onFiltersUpdate = jest.fn();

      let {container, findByText, findByLabelText} = render(<Filters
        filters={[priceFilter, colorFilter]}
        onFiltersUpdate={onFiltersUpdate}
        onFilterDisable={() => {}}
      />);

      fireEvent.click(await findByText("Ajouter des filtres"));
      fireEvent.click(await findByLabelText('Expansive'));
      fireEvent.click(await findByText('Enregistrer'));
      await findByText("Ajouter des filtres");
      expect(onFiltersUpdate.mock.calls[0][0]).toContainEqual({
        name: 'price',
        value: 'expansive',
        enabled: true
      });
      expect(onFiltersUpdate.mock.calls[0][0]).toContainEqual({
        name: 'color',
        value: undefined,
        enabled: false
      });
    });
  });

  describe('person filter', () => {
    it('display empty input when disabled', async () => {
      let personFilter = new Filter({
        enabled: false,
        type: 'text',
        name: 'person',
        value: 'john-id',
        data: {
          nickName: 'John',
          userAccount: {
            id: 'john-id'
          }
        },
        editionTitleLabel: 'Personne'
      });

      const {container, findByText, findByLabelText, findByPlaceholderText} = renderWithMocks({
        gqlMocks: [],
        element: (
          <Filters
            filters={[personFilter]}
            onFiltersUpdate={() => {}}
            onFilterDisable={() => {}}
          />
        )
      });

      fireEvent.click(await findByText("Ajouter des filtres"));
      let input = await findByPlaceholderText('Filtrer sur un utilisateur');
      expect(input.value).toEqual('');
    });


    it('displays the current user name when enabled', async () => {
      let personFilter = new Filter({
        enabled: true,
        type: 'text',
        name: 'person',
        value: 'john-id',
        data: {
          nickName: 'John',
          userAccount: {
            id: 'john-id'
          }
        },
        editionTitleLabel: 'Personne'
      });

      const {container, findByText, findByLabelText, findByPlaceholderText} = renderWithMocks({
        gqlMocks: [],
        element: (
          <Filters
            filters={[personFilter]}
            onFiltersUpdate={() => {}}
            onFilterDisable={() => {}}
          />
        )
      });

      fireEvent.click(await findByText("Modifier les filtres"));
      let input = await findByPlaceholderText('Filtrer sur un utilisateur');
      expect(input.value).toEqual('John');
    });

    it('calls onFiltersUpdate with the selected user', async () => {
      let personFilter = new Filter({
        type: 'text',
        name: 'person',
        editionTitleLabel: 'Personne'
      });

      let gqlMocks = [{
        request: {
          query: gqlSearchPersons,
          variables: {
            qs: '^jo.*',
            first: 10
          }
        },
        result: {
          "data": {
            "persons": {
              "__typename": "PersonConnection",

              "edges": [
                {
                  "__typename": "PersonEdge",

                  "node": {
                    "__typename": "Person",

                    "id": "john-id",
                    "nickName": "John",
                    "userAccount": {
                      "__typename": "UserAccount",

                      "id": "john-account-id"
                    }
                  }
                }
              ]
            }
          }
        }
      }];

      let onFiltersUpdate = jest.fn();

      const {container, findByText, findByLabelText, findByPlaceholderText} = renderWithMocks({
        gqlMocks,
        element: (
          <Filters
            filters={[personFilter]}
            onFiltersUpdate={onFiltersUpdate}
            onFilterDisable={() => {}}
          />
        )
      });

      fireEvent.click(await findByText('Ajouter des filtres'));
      let input = await findByPlaceholderText('Filtrer sur un utilisateur');
      fireEvent.change(input, {target : {value : 'jo'}})
      input.focus();
      fireEvent.click(await findByText('John'));
      fireEvent.click(await findByText('Enregistrer'));
      await findByText('Ajouter des filtres');
      expect(onFiltersUpdate.mock.calls[0][0][0]).toMatchObject({
        name: 'person',
        value: 'john-account-id',
        data: {
          'id': 'john-id',
          'nickName': 'John',
          'userAccount': {
            'id': 'john-account-id'
          }
        },
        enabled: true
      });
    });

    it('calls onFiltersUpdate with the person filter disabled, when the autocomplete is cleared', async () => {
      let personFilter = new Filter({
        enabled: true,
        type: 'text',
        name: 'person',
        value: 'john-account-id',
        data: {
          'id': 'john-id',
          'nickName': 'John',
          'userAccount': {
            'id': 'john-account-id'
          }
        },
        editionTitleLabel: 'Personne'
      });

      let onFiltersUpdate = jest.fn();

      const {container, findByText, findByLabelText, findByPlaceholderText} = renderWithMocks({
        gqlMocks: [],
        element: (
          <Filters
            filters={[personFilter]}
            onFiltersUpdate={onFiltersUpdate}
            onFilterDisable={() => {}}
          />
        )
      });

      fireEvent.click(await findByText('Modifier les filtres'));
      let input = await findByPlaceholderText('Filtrer sur un utilisateur');
      fireEvent.change(input, {target : {value : ''}})
      fireEvent.click(await findByText('Enregistrer'));
      await findByText('Modifier les filtres');
      expect(onFiltersUpdate.mock.calls[0][0]).toContainEqual({
        name: 'person',
        value: 'john-account-id',
        data: {
          'id': 'john-id',
          'nickName': 'John',
          'userAccount': {
            'id': 'john-account-id'
          }
        },
        enabled: false
      });
    });
  });
});
