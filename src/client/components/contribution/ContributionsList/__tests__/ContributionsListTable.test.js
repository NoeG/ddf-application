import React from 'react';
import {act} from 'react-dom/test-utils';
import waait from 'waait';
import {GraphQLError} from 'graphql';

import {renderWithMocks} from '../../../../../../jest/utilities/renderWithMocks';
import {ContributionsListTable, gqlContributionsQuery} from '../ContributionsListTable';

describe('Empty results', () => {
  it('displays a warning message', async () => {
    let gqlMocks = [{
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "filterOnLoggedUser": false,
          "sortings": [{
            "sortBy": "createdAt",
            "isSortDescending": true
          }]
        }
      },
      result: {
        "data": {
          "contributedLexicalSenses": {
            "edges": [],
            "pageInfo": {
              "endCursor": "offset:1",
              "hasNextPage": false,
              "__typename": "PageInfo"
            },
            "__typename": "LexicalSenseConnection"
          }
        }
      }
    }];

    let userAuthenticationServiceMock = {
      useLoggedUser: () => ({user:{}})
    };

    const {container} = renderWithMocks({
      gqlMocks: gqlMocks,
      element: <ContributionsListTable 
        pageSize={20}
        filterOnLoggedUser={false}
        userAuthenticationService={userAuthenticationServiceMock}
      />,
    });

    await act(waait);
    await act(waait);

    expect(container).toMatchSnapshot();
  });
});

describe('Errors from API', () => {
  it('displays the empty table with a warning message in case of error, and logs the error in the console', async () => {
    let consoleerror = console.error;
    console.error = jest.fn();
    let gqlMocks = [{
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "filterOnLoggedUser": false,
          "sortings": [{
            "sortBy": "createdAt",
            "isSortDescending": true
          }]
        }
      },
      result: {
        "errors": [new Error('First test error')],
      }
    }];

    let userAuthenticationServiceMock = {
      useLoggedUser: () => ({user:{}})
    };

    const {container} = renderWithMocks({
      gqlMocks: gqlMocks,
      element: <ContributionsListTable 
        pageSize={20}
        filterOnLoggedUser={false}
        userAuthenticationService={userAuthenticationServiceMock}
      />,
    });

    await act(waait);
    await act(waait);

    expect(container).toMatchSnapshot();
    expect(console.error.mock.calls[0][0]).toEqual('API error received in the component ContributionsListTable');
    expect(console.error.mock.calls[1]).toBeDefined();
    console.error = consoleerror;
  });
});

