import React, {useState} from 'react';
import {useQuery} from '@apollo/react-hooks';
import {gql} from 'apollo-boost';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import {formatRoute} from 'react-router-named-routes';
import {Link} from 'react-router-dom';
import invariant from 'invariant';
import {useTranslation} from 'react-i18next';

import {ROUTES} from '../../../routes';
import {Col, Row, Table} from '../../widgets/Table';
import {getUserAuthenticationService} from '../../../services/UserAuthenticationService';
import {apolloRemoveNodeCacheUpdate} from '../../../utilities/apolloRemoveNodeCacheUpdate';
import {Actions} from './Actions';

import toRemoveIcon from '../../../assets/images/circled_minus_pink.svg';
import toValidateIcon from '../../../assets/images/circled_checkmark_purple.svg';
import toReport from '../../../assets/images/circled_exclamation_orange.svg';
import reportingsIcon from '../../../assets/images/exclamation_orange.svg';
import interrogationIcon from '../../../assets/images/interrogation_gray.svg';
import squaredCheckmarkIcon from '../../../assets/images/squared_checkmark_green.svg';
import checkmarkPurpleIcon from '../../../assets/images/checkmark_purple.svg';
import exclamationPinkIcon from '../../../assets/images/exclamation_pink.svg';
import style from './ContributionsListTable.styl';
import {usePaginatedQuery} from "@mnemotix/synaptix-client-toolkit";

export const gqlContributionsQuery = gql`
  query ContributionsList (
    $first: Int, 
    $after: String, 
    $filterOnLoggedUser: Boolean!, 
    $filterOnUserAccountId: ID, 
    $sortings: [SortingInput],
    $filterOnReviewed: Boolean,
    $filterOnExistingSuppressionRating: Boolean,
    $filterOnProcessed: Boolean
  ){
    contributedLexicalSenses(
      first: $first, 
      after: $after, 
      filterOnLoggedUser: $filterOnLoggedUser, 
      filterOnUserAccountId: $filterOnUserAccountId, 
      sortings: $sortings,
      filterOnReviewed: $filterOnReviewed,
      filterOnExistingSuppressionRating: $filterOnExistingSuppressionRating,
      filterOnProcessed: $filterOnProcessed
    ){
      edges{
        node{
          id
          definition
          createdAt
          creatorPerson {
            nickName
          }
          canonicalForm{
            writtenRep
          }
          reviewed
          processed
          validationRatingsCount
          suppressionRatingsCount
          reportingRatingsCount
          reportingRating {
            id
          }
          validationRating {
            id
          }
          suppressionRating {
            id
          }
        }
      }
      pageInfo{
        endCursor
        hasNextPage
      }
    }
  }
`;


ContributionsListTable.propTypes = {
  /**
   * If you want to mask some columns, put the column names in this array (default, all columns are displayed).
   * Possible values : 'person', 'actions'
   */
  hideColumns: PropTypes.arrayOf(PropTypes.oneOf(['person', 'actions'])),
  /**
   * This flag filters the results, only the contributions for which the creator is the user currently logged in.
   * (this work with the value of the cookies, unlike the other filters)
   */
  filterOnLoggedUser: PropTypes.bool,
  /**
   * Page size of reluts, for pagination. Defaults to 20
   */
  pageSize: PropTypes.number,
  /**
   * Filter the results to show only contributions for which the creator is the account matching the provided ID
   */
  filterOnUserAccountId: PropTypes.string,
  filterOnScore: PropTypes.oneOf(['positive', 'negative'])
};
export function ContributionsListTable(props) {
  /* Services DI */
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const {t} = useTranslation();

  const {
    hideColumns, 
    filterOnUserAccountId, 
    filterOnReviewed, 
    filterOnExistingSuppressionRating, 
    filterOnProcessed
  } = props;
  const {user} = userAuthenticationService.useLoggedUser();
  const filterOnLoggedUser = !!props.filterOnLoggedUser;
  const pageSize = props.pageSize || 20;


  let [sortBy, setSortBy] = useState("createdAt");
  let [isSortDescending, setIsSortDescending] = useState(true);

  let columnsCount = 7 - (hideColumns?.length || 0);

  /* 
   * The query variables are factorized in a function, because they are used in several places (query and cache update) and we
   * need to ensure the that variables are exactly the same in all occurences
   */
  function getQueryVariables() {
    return {
      first: pageSize,
      filterOnLoggedUser,
      filterOnUserAccountId,
      filterOnReviewed,
      filterOnExistingSuppressionRating,
      filterOnProcessed,
      sortings: [{
        sortBy,
        isSortDescending
      }]
    };
  }

  const {data, loadNextPage, hasNextPage, loading, error} = usePaginatedQuery(gqlContributionsQuery, {
    variables: getQueryVariables(),
    fetchPolicy: 'cache-and-network',
    pageSize,
    connectionPath: 'contributedLexicalSenses',
  });

  if (error) {
    console.error('API error received in the component ContributionsListTable');
    console.error(error);
  }

  if (loading && !data) {
    return renderLoadingTable();
  }

  return (
    <div className={classNames([
      style.tableContainer, 
      {
        [style.loading]: loading
      }
    ])}>
    <Table>
      {renderTableHeaders()}
      {renderTableContent()}
    </Table>
    <If condition={hasNextPage}>
      <div className={style.loadMore}>
        <a onClick={loadNextPage} className={style.loadMoreButton}>
          {t("CONTRIBUTIONS_LIST.TABLE.SEE_NEXT")}
        </a>
      </div>
    </If>
  </div>
  );

  function renderTableHeaders() {
    return (
      <Row heading>
        <Col 
          className={style.clickable}
          onClick={() => setSortingFor('createdAt')}
        >
          {t('CONTRIBUTIONS_LIST.TABLE.HEADERS.DATE')}
          <If condition={sortBy === 'createdAt'}> 
            <span className={classNames(style.sortIcon, {[style.descending]: isSortDescending})}/>
          </If>
        </Col>
        <Col>
          {t('CONTRIBUTIONS_LIST.TABLE.HEADERS.FORM')}
        </Col>
        <Col>
          {t('CONTRIBUTIONS_LIST.TABLE.HEADERS.DEFINITION')}
        </Col>
        <If condition={!hideColumns?.includes('person')}>
          <Col>
            {t('CONTRIBUTIONS_LIST.TABLE.HEADERS.PERSON')}
          </Col>
        </If>
        <Col>
          {t('CONTRIBUTIONS_LIST.TABLE.HEADERS.STATUS')}
        </Col>
        <Col 
          title={t('CONTRIBUTIONS_LIST.TABLE.HEADERS.REPORTINGS_TOOLTIP')}
          className={style.clickable}
          onClick={() => setSortingFor('reportingRatingCount')}
        >
          {t('CONTRIBUTIONS_LIST.TABLE.HEADERS.REPORTINGS')}
          <If condition={sortBy === 'reportingRatingCount'}>
            <span className={classNames(style.sortIcon, {[style.descending]: isSortDescending})}/>
          </If>
        </Col>
        <If condition={!hideColumns?.includes('actions')}>
          <Col>
            {t('CONTRIBUTIONS_LIST.TABLE.HEADERS.ACTIONS')}
          </Col>
        </If>
      </Row>
    );
  }

  function renderTableContent() {
    if (!data) {
      return renderEmptyTable(t('CONTRIBUTIONS_LIST.TABLE.ERROR_RESULTS'));
    }
    if (!data.contributedLexicalSenses.edges.length) {
      return renderEmptyTable(t('CONTRIBUTIONS_LIST.TABLE.NO_RESULTS'));
    }

    return data.contributedLexicalSenses.edges
      .map(edge => edge.node)
      .filter(lexicalSense => !!lexicalSense)
      .map((lexicalSense, index) => (
      <Row key={index}>
        <Col>
          {dayjs(lexicalSense.createdAt).format("L")}
        </Col>
        <Col>
          <Link to={formatRoute(ROUTES.FORM_LEXICAL_SENSE, {
            formQuery: lexicalSense.canonicalForm?.writtenRep,
            lexicalSenseId: lexicalSense.id
          })}>
          {lexicalSense.canonicalForm?.writtenRep}
        </Link>
      </Col>
      <Col>
        {lexicalSense.definition}
      </Col>
      <If condition={!hideColumns?.includes('person')}>
        <Col>
          {lexicalSense.creatorPerson?.nickName}
        </Col>
      </If>
      <Col>
        {renderLexicalSenseStatus(lexicalSense)}
      </Col>
      <Col>
        {renderLexicalSenseReportings(lexicalSense)}
      </Col>
      <If condition={!hideColumns?.includes('actions')}>
        <Col className={style.actionsColumn}>
          {renderLexicalSenseActions(lexicalSense)}
        </Col>
      </If>
    </Row>
    ));
  }

  function renderEmptyTable(message) {
    return (
      <Row>
        <Col colSpan={columnsCount} className={style.noResults}>
          {message}
        </Col>
      </Row>
    );
  }

  function renderLexicalSenseActions(lexicalSense) {
    return <Actions
      lexicalSense={lexicalSense}
      onRemoveUpdateCache={(cache) => {
        apolloRemoveNodeCacheUpdate({
          cache,
          query: gqlContributionsQuery,
          variables: getQueryVariables(),
          data,
          connectionPathInData: "contributedLexicalSenses",
          deletedNodeId: lexicalSense.id
        });
      }}
    />;
  }

  function renderLexicalSenseStatus(lexicalSense) {
    /**
     * Determine the status of an entry : 
     *
     * - 'notReviewed' : 
     *    No operator nor admin has rated this entry (be it a validation, asking for suppression, or setting it as processed). 
     *    `lexicalSense.reviewed === false`
     * - 'validated' : 
     *    The entry is not processed, it was rated by at least one operator or admin, and has no resquest for suppression
     *   `lexicalSense.processed === false && lexicalSense.reviewed === true && lexicalSense.suppressionRatingsCount === 0`
     * - 'askedForSuppression' :
     *    The entry is not processed, it was rated by at least one operator or admin, and it has at least one request for suppression
     *    `lexicalSense.processed === false && lexicalSense.reviewed === true && lexicalSense.suppressionRatingsCount === 0`
     * - 'processed' : 
     *    The entry is marked as processed
     */
    let status;
    if (!lexicalSense.reviewed) {
      status = 'notReviewed';
    } else if (lexicalSense.processed) {
      status = 'processed';
    } else if (lexicalSense.suppressionRatingsCount > 0) {
      status = 'askedForSuppression';
    } else {
      status = 'validated'
    }

    let icons = {
      notReviewed: interrogationIcon,
      validated: checkmarkPurpleIcon,
      askedForSuppression: exclamationPinkIcon,
      processed: squaredCheckmarkIcon
    };

    let tooltips = {
      notReviewed: 'Non relu',
      validated: 'Relu',
      askedForSuppression: 'Au moins un opérateur a fait une demande de suppression',
      processed: 'Traité'
    }
    
    return (
      <div className={classNames([style.status, status])} title={tooltips[status]}>
        <img className={style.icon} src={icons[status]}/>
      </div>
    );
  }


  function renderLexicalSenseReportings(lexicalSense) {
    if (lexicalSense.reportingRatingsCount> 0) {
      return (
        <div className={style.reportingsCount} title={`${lexicalSense.reportingRatingsCount} utilisateur(s) ont signalé cette entrée`}>
          <img className={style.icon} src={reportingsIcon}/>
          <span className={style.text}>{lexicalSense.reportingRatingsCount}</span>
        </div>
      );
    }
  }

  function renderLoadingTable() {
    return (
      <Table className={style.loadingPlaceholderTable}>
        <Row heading className={style.heading}>
          <Col>
            <div className={style.textPlaceholder}/>
          </Col>
        </Row>
        <Row>
          <Col> 
            <div className={style.textPlaceholder}/>
          </Col>
        </Row>
      </Table>
    );
  }

  /**
   * On click on a column header, in order to set the sorting for that property. If the property is already selected for sorting,
   * this function toggles the sorting order. If the property isn't already selected, the function sets the property as a sorting filter
   * and sets the sorting order to 'descending'.
   *
   * @param {string} propertyName - 'createdAt' or 'reportingRatingCount'
   */
  function setSortingFor(propertyName) {
    invariant(['createdAt', 'reportingRatingCount'].includes(propertyName), "setSortingFor must take 'createdAt' or 'reportingRatingCount' as parameter");

    if (sortBy === propertyName) {
      setIsSortDescending(descending => !descending);
    } else {
      setSortBy(propertyName);
      setIsSortDescending(true);
    }
  }

}
