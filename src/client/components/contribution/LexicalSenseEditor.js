import React, {useState, useEffect} from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {Formik, yupToFormErrors} from 'formik';
import * as Yup from 'yup';
import gql from 'graphql-tag';
import invariant from 'invariant';
import {useApolloClient} from '@apollo/react-hooks';

import {Input as FormikInput} from '../widgets/forms/formik/Input';
import {Input} from '../widgets/forms/Input';
import {Form} from '../widgets/forms/formik/Form';
import {Button} from '../widgets/forms/Button';
import {FormBottom} from '../widgets/forms/FormBottom';
import {ButtonRow} from '../widgets/forms/ButtonRow';
import {ConceptAutocomplete} from '../widgets/forms/formik/ConceptAutocomplete';
import {notificationService as appNotificationService} from '../../services/NotificationService';
import {GraphQLErrorHandler} from '../../services/GraphQLErrorHandler';


import style from './LexicalSenseEditor.styl';
import editIcon from '../../assets/images/contributions_yellow.svg';


export const gqlGetLexicalEntryInfo = gql`
  query GetLexicalEntryInfo($formWrittenRep: String!) {
    lexicalEntryContribInputsFromFormWrittenRep(formWrittenRep: $formWrittenRep) { 
      lexicalEntryTypeName
      grammaticalCategorySchemeId
    }
  }
`;

/**
 * The LexicalSenseEditor is a component responsible for entering the fields formWrittenRep, grammaticalCategory and definition. It handles
 * the fact that the user can edit the form written representation, and that the grammatical categories suggested can differ depending on
 * the type of the form.
 *
 * This component may be used for creation of a new sense, with the formWrittenRep already initialized or not, or the edition of an existing sense.
 * It is not responsible for the way the initialization data is obtained (it just gets it from its props), and also not responsible for what to do
 * on submission (it just calls the props handler onSubmit).
 *
 * The LexicalSenseEditor works as a state machine, with different steps depending on the form completion process : 
 *
 * - STEP1: Word input. Only the input for "formWrittenRep" is available. When the input is completed and the user clicks "next", 
 *    we fetch some data from the server (needed for STEP2), and then switch to STEP2.
 * - STEP2: POS and definition input. The "formWrittenRep" is already stored, and not editable. POS autocomplete is populated based 
 *    on the lexicalEntryTypeName which is derived from the "formWrittenRep". The user can publish the form, or go back to STEP1 to edit the form.
 *
 * Possible transitions : 
 *
 * - Initial  => STEP1. Default behavior
 * - Initial  => STEP2. If the "formWrittenRep" is given in the props
 * - STEP1 => STEP2. When the user clicks "next" after entering a valid formWrittenRep. On transition, a server query is made to get the lexicalEntryTypeName.
 * - STEP2 => STEP1. When a user clicks "edit" the formWrittenRep. We go back to STEP1, but keep entered data for "grammaticalCategoryId" and "definition" (for better UX).
 *                   If the lexicalEntryTypeName changes when the user transitions to STEP2 again, we need to reset "grammaticalCategoryId". We can keep "definition"
 *
 */
LexicalSenseEditor.propTypes = {
  /* Initial value for the form written rep. This value should not change */
  formWrittenRep: PropTypes.string,
  grammaticalCategoryId: PropTypes.string,
  grammaticalCategoryInputValue: PropTypes.string,
  definition: PropTypes.string,
  /* Default true. If 'false', only STEP2 is available, the user can edit the grammatical category and the definition only */
  canEditFormWrittenRep: PropTypes.bool,
  /* Handler called on click on the "cancel" button" */
  onCancel: PropTypes.func,
  /* 
   * Handler called on click on the submit button. The function signature is `submitHandler(formikValues, formikOptions) => void`.
   * First parameter is the `formikValues` object which contains the following properties : 
   *   - formWrittenRep
   *   - grammaticalCategoryId
   *   - definition
   *   - lexicalEntryTypeName
   * Second parameter is the `formikOptions` object which contains all helpers necessary to interact with the Formik form, for example
   * setFieldError() or setSubmitting()
   */
  onSubmit: PropTypes.func
};
LexicalSenseEditor.defaultProps = {
  canEditFormWrittenRep: true
}
export function LexicalSenseEditor(props) {
  invariant(props.canEditFormWrittenRep || props.formWrittenRep, "If prop 'canEditFormWrittenRep' is false, the prop 'formWrittenRep' must be provided");

  /* Services DI */
  const notificationService = props.notificationService || appNotificationService;

  /* Hooks */
  const {t} = useTranslation();
  const apolloClient = useApolloClient();
  const [lexicalEntryTypeName, updateLexicalEntryTypeName] = useState();
  const [grammaticalCategorySchemeId, updateGrammaticalCategorySchemeId] = useState();

  /** Initializing state from props */
  let initialStep = props.formWrittenRep ? 'STEP2' : 'STEP1';
  /*
   * Only use 'STEP1' or 'STEP2' for this state value
   */
  const [currentStep, updateStep] = useState(initialStep);
  invariant(['STEP1', 'STEP2'].includes(currentStep), 'Invalid value for currentStep. It should be "STEP1" or "STEP2"');

  /* Constants */
  const initialValues = {
    formWrittenRep: props.formWrittenRep || '',
    grammaticalCategoryId: props.grammaticalCategoryId || '',
    definition: props.definition || ''
  };
  const initialStatus = {
    autocompleteInputValues: {
      grammaticalCategoryId: props.grammaticalCategoryInputValue || ''
    }
  };
  const step1ValidationSchema = Yup.object().shape({
    formWrittenRep: Yup.string().required(t('FORM_ERRORS.FIELD_ERRORS.REQUIRED'))
  });
  const step2ValidationSchema = Yup.object().shape({
    formWrittenRep: Yup.string().required(t('FORM_ERRORS.FIELD_ERRORS.REQUIRED')),
    grammaticalCategoryId: Yup.string().nullable().required(t('FORM_ERRORS.FIELD_ERRORS.REQUIRED')),
    definition: Yup.string().required(t('FORM_ERRORS.FIELD_ERRORS.REQUIRED'))
  });


  useEffect(() => {
    /* If prop formWrittenRep is defined, we need to initialize directly at STEP2. This hook should be run only once */
    if (props.formWrittenRep) {
      toStep2({formikValues: {formWrittenRep: props.formWrittenRep}});
    }
  }, [props.formWrittenRep]);


  return (
    <Formik
      initialValues={initialValues}
      initialStatus={initialStatus}
      enableReinitialize
      validate={(values) => {
        let validationSchema = currentStep === 'STEP1' ? step1ValidationSchema : step2ValidationSchema;
        return validationSchema
          .validate(values, {abortEarly: false})
          .catch(err => {
            let errorrToReturn = yupToFormErrors(err);
            throw yupToFormErrors(err);
          });
      }}
      validateOnChange={false}
      validateOnBlur={false}
      onSubmit={handleSubmit}
      render={({values, status, isValid, isSubmitting}) => (
        <Form noValidate>
          <If condition={currentStep === 'STEP1'}>
            <FormikInput type="text" name="formWrittenRep" placeholder={t('CONTRIBUTION.CREATE_LEXICAL_SENSE.FORM_PLACEHOLDER')} isRequired/>
          </If>
          <If condition={currentStep === 'STEP2'}>
            <div className={style.formWrittenRep}>
              {values.formWrittenRep}
              <If condition={props.canEditFormWrittenRep}>
                <img src={editIcon} onClick={() => updateStep('STEP1')} data-testid="edit-form-written-rep-button"/>
              </If>
            </div>
            <FormikInput type="hidden" name="formWrittenRep"/>
          </If>
          <If condition={currentStep==='STEP2'}>
            <If condition={grammaticalCategorySchemeId}>
              <ConceptAutocomplete
                name="grammaticalCategoryId" 
                placeholder={t('CONTRIBUTION.CREATE_LEXICAL_SENSE.POS_PLACEHOLDER')} 
                maxSuggestionsLength={20}
                schemeId={grammaticalCategorySchemeId}
                selectNullOnEmpty
                isRequired
              />
            </If>
            <If condition={!grammaticalCategorySchemeId}>
              <Input data-testid="autocomplete-loading-placeholder" placeholder={t('CONTRIBUTION.CREATE_LEXICAL_SENSE.POS_PLACEHOLDER')} disabled/>
            </If>
            <FormikInput type="text" name="definition" placeholder={t('CONTRIBUTION.CREATE_LEXICAL_SENSE.DEFINITION_PLACEHOLDER')} isRequired/>
          </If>
          <FormBottom>
            <ButtonRow>
              <Button type="button" secondary onClick={props.onCancel}>{t('CONTRIBUTION.CREATE_LEXICAL_SENSE.CANCEL')}</Button>
              <Button type="submit" disabled={isSubmitting} loading={isSubmitting}>
                {t(currentStep === 'STEP1' ? 'CONTRIBUTION.CREATE_LEXICAL_SENSE.NEXT': 'CONTRIBUTION.CREATE_LEXICAL_SENSE.SUBMIT')}
              </Button>
            </ButtonRow>
          </FormBottom>
        </Form>
      )}
    />
  );

  async function handleSubmit(formikValues, formikOptions) {
    const {setSubmitting} = formikOptions;
    if (currentStep === 'STEP1') {
      await toStep2({formikValues, formikOptions, previousLexicalEntryTypeName: lexicalEntryTypeName});
      setSubmitting(false);
    }
    if (currentStep === 'STEP2') {
      props.onSubmit({
        ...formikValues, 
        lexicalEntryTypeName
      }, formikOptions);
    }
  }

  /**
   * steps to go to STEP2 : 
   *
   * - fetch lexicalEntryContribInputsFromFormWrittenRep to get the type of the lexical entry, and the scheme ID for the grammatical categories
   * - If the lexical entry type has changed, reset the grammatical category value in formik state
   * - switch to step 2
   *
   *
   * @param {Object} options
   * @param {Object} options.formikValues
   * @param {string} options.formikValues.formWrittenRep This is the only parameter used from the formikValues object
   * @param {Object} options.formikOptions Pass the formik options object if this is the user triggered transition from STEP1 to STEP2.
   *                                       If this is the initialization to STEP2 from the URL query param, don't pass any object
   * @param {string} options.previousLexicalEntryTypeName Pass the value of lexicalEntryTypeName when this function is called. This is used 
   *                                                      to compare with the new value of lexicalEntryTypeName that will be fetched during
   *                                                      the funtion call to know if the entry type has changed.
   */
  async function toStep2({formikValues, formikOptions, previousLexicalEntryTypeName}) {
    let result = await apolloClient.query({
      query: gqlGetLexicalEntryInfo,
      variables: {formWrittenRep: formikValues.formWrittenRep}
    });

    let success = !!result.data.lexicalEntryContribInputsFromFormWrittenRep;
    let {
      data: {
        lexicalEntryContribInputsFromFormWrittenRep: {
          lexicalEntryTypeName,
          grammaticalCategorySchemeId
        }
      }, 
      error
    } = result;

    if (error) {
      let errorMessage = GraphQLErrorHandler(getLexicalEntryInfoError, {t});
      notificationService.error(errorMessage);
    }

    if (success) {
      if (previousLexicalEntryTypeName !== lexicalEntryTypeName) {
        updateLexicalEntryTypeName(lexicalEntryTypeName);
        if (previousLexicalEntryTypeName && formikOptions) {
          resetGrammaticalCategory(formikOptions);
        }
      }
      updateGrammaticalCategorySchemeId(grammaticalCategorySchemeId);
      updateStep('STEP2');
    }
  }

  /**
   * @param {Object} formikOptions
   */
  function resetGrammaticalCategory({setStatus, setFieldValue, status}) {
    setFieldValue('grammaticalCategoryId', null, false);
    if (!status) status = {};
    if (!status.autocompleteInputValues) status.autocompleteInputValues = {};
    status.autocompleteInputValues.grammaticalCategoryId = '';
    setStatus(status);
  }
}
