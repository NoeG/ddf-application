import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {useTranslation} from 'react-i18next';
import {gql} from 'apollo-boost';
import {useQuery} from '@apollo/react-hooks';

import {apolloErrorHandler} from '../../utilities/apolloErrorHandler';
import {ROUTES} from '../../routes';
import style from './FormNoResults.styl';


const formSuggestionsQuery = gql`
  query Search_Query($first: Int, $qs: String) {
    forms(qs: $qs, first: $first){
      edges{
        node{
          id
          writtenRep
        }
      }
    }
  }
`;


FormNoResults.propTypes = {
  formQuery: PropTypes.string.isRequired
};
export function FormNoResults(props) {

  const {t} = useTranslation();
  const {formQuery} = props;
  const {data, loading, error} = useQuery(formSuggestionsQuery, {
    variables: {
      first: 10,
      qs: formQuery
    },
    fetchPolicy: 'cache-and-network'
  });
  apolloErrorHandler(error, 'log');
  if ((loading && !data) || error) return null;

  const {forms} = data;
  let filteredForms = forms.edges.filter(({node:{writtenRep}}) => writtenRep !== formQuery).map(({node}) => node);

  return (
    <div className={style.container}>
      <div className={style.noResultsText}>
        {t('FORM.NO_RESULTS')}
      </div>

      <If condition={filteredForms.length > 0}>
        <hr/>

        <div className={style.suggestionsText}>
          {t('FORM.CLOSE_FORMS_SUGGESTIONS')}
        </div>

        <div>
          <ul>
            {filteredForms.map(form => (
              <li key={form.id}>
                <Link to={formatRoute(ROUTES.FORM_SEARCH, {formQuery: form.writtenRep})}>{form.writtenRep}</Link>
              </li>
            ))}
          </ul>
        </div>
      </If>
    </div>
  )

}

