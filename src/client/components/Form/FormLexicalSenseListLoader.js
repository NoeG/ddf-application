import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';

import {LexicalSenseExcerptLoader} from './LexicalSenseExcerptLoader';

import formLexicalSenseListStyle from './FormLexicalSenseList.styl';
import desktopSubHeaderStyle from '../../layouts/desktop/DesktopSubHeader.styl';

export function FormLexicalSenseListLoader(props) {
  const {t} = useTranslation();
  
  return (
    <React.Fragment>
      <div className={desktopSubHeaderStyle.secondaryTitle}>{t('FORM.DEFINITION')}</div>
      <div className={formLexicalSenseListStyle.lexicalSenses}>
        <LexicalSenseExcerptLoader className={formLexicalSenseListStyle.lexicalSense} />
        <LexicalSenseExcerptLoader className={formLexicalSenseListStyle.lexicalSense} />
        <LexicalSenseExcerptLoader className={formLexicalSenseListStyle.lexicalSense} />
      </div>
    </React.Fragment>
  );
}
