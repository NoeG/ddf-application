import React from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import classNames from 'classnames';
import {gql} from 'apollo-boost';
import {useQuery} from '@apollo/react-hooks';
import {SanitizedHtml} from '../../utilities/SanitizedHtml';

import {FormTopPostsLoader} from './FormTopPostsLoader';
import {apolloErrorHandler} from '../../utilities/apolloErrorHandler';
import style from './FormTopPosts.styl';

export const FormTopPostsQuery = gql`
  query FormTopPosts_Query($formQs: String) {
    topPostAboutEtymology: topPostAboutEtymologyForAccurateWrittenForm(formQs: $formQs){
      id
      content
    }

    topPostAboutForm: topPostAboutFormForAccurateWrittenForm(formQs: $formQs){
      id
      content
    }
  }
`;

/**
 * Subcomponent that displays the top posts related to a written reprensation of an ontolex:Form
 *
 * @see into ontology :
 *
 * ```
 *   ontolex:Form / ddf:hasPostAboutForm
 *   ontolex:Form > ontolex:LexicalEntry / ddf:hasPostAboutForm
 * ```
 */
FormTopPosts.propTypes = {
  formQuery: PropTypes.string.isRequired
};
export function FormTopPosts(props) {
  const {t} = useTranslation();
  const {formQuery} = props;

  const {data, loading, error} = useQuery(FormTopPostsQuery, {
    variables: {
      formQs: formQuery
    },
    fetchPolicy: 'cache-and-network'
  });
  apolloErrorHandler(error, 'log');
  if (error) return null;
  if (loading && !data) {
    return <FormTopPostsLoader />;
  }

  let {topPostAboutEtymology, topPostAboutForm} = data;

  return (
    <If condition={topPostAboutForm || topPostAboutEtymology}>
      <div className={classNames([style.container, {[style.loading]: loading}])}>
        <If condition={topPostAboutForm}>
          <h5 className={style.title}>{t('FORM.TOP_POSTS.FORM_HEADER')}</h5>
          <div className={style.section}>
            <SanitizedHtml as="span" className={style.content} html={topPostAboutForm.content}/>
          </div>
          <If condition={topPostAboutEtymology}><hr/></If>
        </If>

        <If condition={topPostAboutEtymology}>
          <h5 className={style.title}>{t('FORM.TOP_POSTS.ETYMOLOGY_HEADER')}</h5>
          <div className={style.section}>
            <SanitizedHtml as="span" className={style.content} html={topPostAboutEtymology.content}/>
          </div>
        </If>

      </div>
    </If>
  )
}

