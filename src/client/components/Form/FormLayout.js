import React from 'react';
import classNames from 'classnames';
import get from 'lodash/get';
import {withRouter}  from 'react-router-dom';

import {ResponsiveMainLayout} from '../../layouts/responsive/ResponsiveMainLayout';
import {FormMobileHeader} from '../../layouts/mobile/MobileMainLayout/FormMobileHeader';
import {MobileFooter} from '../../layouts/mobile/MobileMainLayout/MobileFooter';

import style from '../routes/LexicalSense.styl'


/**
 * Usage : 
 *
 * <FormLayout>
 *    <Content />
 * </FormLayout>
 *
 *
 *
 * Properties : 
 * - desktopSideColumn: optional element to render a side column in the desktop layout
 *
 * The form is extracted from the current routes, the component  must used on a route 
 * with `:formQuery` paratemer (see routes.js)
 */
@withRouter
export class FormLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentForm: get(this.props, 'match.params.formQuery')
    }
  }

  componentDidUpdate(prevProps) {
    let prevForm = get(prevProps, 'match.params.formQuery');
    let newForm = get(this.props, 'match.params.formQuery');
    if (prevForm !== newForm) {
      this.setState({
        currentForm: newForm
      });
    }
  }

  render() {
    return (
      <ResponsiveMainLayout
        mobileFooter={<MobileFooter currentForm={this.state.currentForm}/>} 
        mobileHeader={this.renderMobileHeader()}
        currentForm={this.state.currentForm}
      >
        {this.props.children}
      </ResponsiveMainLayout>
    );
  }

  renderMobileHeader() {
    return <FormMobileHeader 
      title={this.state.currentForm} 
      search={true} 
      geolocMarker={true}
      currentForm={this.state.currentForm}
    />;
  }
}

