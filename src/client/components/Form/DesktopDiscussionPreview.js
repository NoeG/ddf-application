import React from 'react';
import PropTypes from 'prop-types';
import {SanitizedHtml} from '../../utilities/SanitizedHtml';

import style from './DesktopDiscussionPreview.styl';

export class DesktopDiscussionPreview extends React.Component {
  
  static propTypes = {
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired
  }


  render() {
    const {title, content} = this.props;

    return (
      <div className={style.discussion}>
        <div className={style.header}>{title}</div>
        <SanitizedHtml className={style.content} html={content}/>
        {/*<div>Salut ça va <a href="/help">help</a></div>*/}
      </div>
    );
  }


}
