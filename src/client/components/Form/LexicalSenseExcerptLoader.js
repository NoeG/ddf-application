import React from 'react';
import classNames from 'classnames';

import style from './LexicalSenseExcerpt.styl';

export class LexicalSenseExcerptLoader extends React.Component {
  render() {
    return (
      <div className={classNames([style.container, style.placeholder, this.props.className])}>
        <div className={style.countriesRow}>
          <div className={style.countries}>
            <div className={style.countryPlaceholder}>&nbsp;</div>
          </div>
        </div>

        <div className={style.pos}>
          <div className={style.posPlaceholder}/>
        </div>

        <div className={style.description}>
          <div className={style.descriptionPlaceholder}/>
        </div>

        <div className={style.sourcesRow}>
          <div className={style.sources}>
            <div className={style.sourcePlaceholder}>&nbsp;</div>
          </div>
        </div>
      </div>
    );
  }
}
