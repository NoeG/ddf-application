import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {gql} from 'apollo-boost';
import {useTranslation} from 'react-i18next';
import classNames from 'classnames';
import {usePaginatedQuery} from '@mnemotix/synaptix-client-toolkit';
import {Helmet} from 'react-helmet-async';

import {LexicalSenseExcerpt} from './LexicalSenseExcerpt';
import {FormLexicalSenseListLoader} from './FormLexicalSenseListLoader';
import {FormNoResults} from './FormNoResults';
import {FormSeeAlso} from './FormSeeAlso';
import {
  getLexicalEntryPartOfSpeechList,
  gqlLexicalEntryPartOfSpeechListFragment
} from '../../utilities/helpers/lexicalEntryPartOfSpeechList';
import {
  getLocalizedEntityCountryData,
  getPlaceCountryId,
  gqlLexicalSenseCountriesFragment
} from '../../utilities/helpers/countries';
import {apolloErrorHandler} from '../../utilities/apolloErrorHandler';

import style from './FormLexicalSenseList.styl';
import desktopSubHeaderStyle from '../../layouts/desktop/DesktopSubHeader.styl';


export const gqlFormQuery = gql`
  query Form_Query($first: Int, $after: String, $formQs: String, $filterByPlaceId: ID) {
    lexicalSenses: lexicalSensesForAccurateWrittenForm(formQs: $formQs, first: $first, after: $after, filterByPlaceId: $filterByPlaceId){
      edges{
        node{
          id
          definition
          ...LexicalSenseCountriesFragment

          lexicalEntry{
            id
            ...LexicalEntryPartOfSpeechListFragment
          }
          
          lexicographicResource{
            id
            altLabel
          }
        }
      }
      pageInfo{
        startCursor
        endCursor
        hasNextPage
      }
    }
  }

  ${gqlLexicalSenseCountriesFragment}
  ${gqlLexicalEntryPartOfSpeechListFragment}
`;



FormLexicalSenseList.propTypes = {
  formQuery: PropTypes.string.isRequired,
  /**
   * The geoname place the user is currently located at, used to customize the results order. See the definition of the Place object
   * at `GeolocService.js`
   */
  currentPlace: PropTypes.object,
};
export function FormLexicalSenseList(props) {

  const {t} = useTranslation();
  const {formQuery, currentPlace} = props;

  let {data, loadNextPage, hasNextPage, loading, error} = usePaginatedQuery(gqlFormQuery, {
    variables: {
      formQs: formQuery,
      filterByPlaceId: getPlaceCountryId(currentPlace)
    },
    pageSize: 10,
    connectionPath: 'lexicalSenses',
    fetchPolicy: 'cache-and-network'
  });

  let lexicalSenses = data?.lexicalSenses;
 
  useEffect(() => {
    /* Load all pages automatically, no infinite scroll or explicit pagination yet */
    if (!loading && hasNextPage) {
      loadNextPage();
    }
  }, [lexicalSenses?.pageInfo?.endCursor, hasNextPage]);

  if (loading && !data) {
    return <FormLexicalSenseListLoader/>
  }

  apolloErrorHandler(error);

  let senses = (lexicalSenses?.edges || []).map(edge => edge.node);

  return (
    <React.Fragment>
      <Helmet>
        <title>{t('DOCUMENT_TITLES.FORM_SEARCH', {formQuery})}</title>
      </Helmet>
      <Choose>
        <When condition={senses.length > 0}>
          <div className={desktopSubHeaderStyle.secondaryTitle}>{t('FORM.DEFINITION')}</div>
          <div className={style.lexicalSenses}>
            {senses.map(renderLexicalSense)}
          </div>
        </When>
        <Otherwise>
          <div>
            {renderNoResults()}
          </div>
        </Otherwise>
      </Choose>

      <FormSeeAlso formQuery={formQuery}/>
    </React.Fragment>
  );

  function renderLexicalSense(lexicalSense) {
    let partOfSpeechList = getLexicalEntryPartOfSpeechList(lexicalSense.lexicalEntry);
    let source = lexicalSense?.lexicographicResource?.altLabel;
    let countryData = getLocalizedEntityCountryData(lexicalSense);

    return <LexicalSenseExcerpt
      className={style.lexicalSense}
      key={lexicalSense.id}
      sense={lexicalSense}
      countryData={countryData}
      source={source}
      partOfSpeechList={partOfSpeechList}
    />
  }

  function renderNoResults() {
    return <FormNoResults formQuery={formQuery}/>
  }
}

