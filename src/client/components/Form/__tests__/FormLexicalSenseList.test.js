import React                          from 'react';
import {render, cleanup}              from '@testing-library/react';
import {act}                          from 'react-dom/test-utils';
import waait                          from 'waait';

import {renderWithMocks} from '../../../../../jest/utilities/renderWithMocks';
import {FormLexicalSenseList, gqlFormQuery} from '../FormLexicalSenseList';
import {ROUTES}  from '../../../routes';

/**
 * Mock all React components used by FormLexicalSenseList with empty components,
 * in order to keep the suite test simple and uniquely about logic of FormLexicalSenseList
 */
jest.mock('../FormNoResults', () => ({FormNoResults: () => null}));
jest.mock('../FormSeeAlso', () => {
  const ActualFormSeeAlso = jest.requireActual('../FormSeeAlso');
  return {
    FormSeeAlso: () => null,
    gqlFormSeeAlsoFragment: ActualFormSeeAlso.gqlFormSeeAlsoFragment
  }
});
jest.mock('../LexicalSenseExcerpt', () => ({LexicalSenseExcerpt: () => null}));

afterEach(() => {
  cleanup();
});


describe('filter by place id', () => {
  const emptyGqlResult = {
    "data": {
      "lexicalSenses" : {
        "edges": [],
        "pageInfo": {
          "startCursor": "YXJyYXljb25uZWN0aW9uOjA=",
          "endCursor": "YXJyYXljb25uZWN0aW9uOjM=",
          "hasNextPage": false,
          "__typename": "PageInfo"
        },
        "__typename": "LexicalSenseConnection"
      },
      "associativeRelationSemanticRelations": null,
      "graphicRelationSemanticRelations": null,
      "morphologicalRelationSemanticRelations": null,
      "__typename": "Query"
    }
  };

  it("makes request with countryId from a place that is a lower geographical division", async () => {
    expect.assertions(1);
    const gqlMock = {
      request: {
        query: gqlFormQuery,
        variables: {
          formQs: "affairé",
          first: 10,
          filterByPlaceId: "geonames:1694008"
        }
      },
      result: () => {
        /* Ensure the callback has been called, means the correct apollo request was made */
        expect(1).toEqual(1);
        return emptyGqlResult;
      }
    };
    const place = {
      "id": "geonames:11832782",
      "__typename": "City",
      "name": "Frances",
      "countryCode": "PH",
      "countryId": "geonames:1694008",
      "countryName": "Philippines",
      "stateName": "Luçon centrale"
    };
  

    const {container} = renderWithMocks({
      locationPath: "/form/affairé",
      gqlMocks: [gqlMock],
      element: <FormLexicalSenseList formQuery="affairé" currentPlace={place}/>,
      routePath: ROUTES.FORM_SEARCH
    });
    await act(waait);
  });

  it("makes request with id from a place that is a country", async () => { 
    expect.assertions(1);
    const gqlMock = {
      request: {
        query: gqlFormQuery,
        variables: {
          formQs: "affairé",
          first: 10,
          filterByPlaceId: "geonames:3017383"
        }
      },
      result: () => {
        /* Ensure the callback has been called, means the correct apollo request was made */
        expect(1).toEqual(1);
        return emptyGqlResult;
      }
    };
    const place = {
      "id": "geonames:3017383",
      "__typename": "Country",
      "name": "France",
      "countryCode": "FR"
    };
  

    const {container} = renderWithMocks({
      locationPath: "/form/affairé",
      gqlMocks: [gqlMock],
      element: <FormLexicalSenseList formQuery="affairé" currentPlace={place}/>,
      routePath: ROUTES.FORM_SEARCH
    });
    await act(waait);
  });
});
