import {gqlFormQuery} from '../../FormLexicalSenseList';
import {FormTopPostsQuery} from '../../../Form/FormTopPosts';

const formQueryMock = {
  request: {
    query: gqlFormQuery,
    variables: {
      formQs: "affairé",
      first: 10
    }
  },
  result: {
    "data": {
      "lexicalSenses": {
        "edges": [{
          "node": {
            "id": "inv/lexical-sense/1234",
            "definition": "Affairiste",
            "places": {
              "edges": [{
                "node": {
                  "id": "place-1",
                  "name": "RDC",
                  "color": "jaune",
                  "__typename": "Country"
                },
                "__typename": "PlaceInterfaceEdge"
              }],
              "__typename": "PlaceInterfaceConnection"
            },
            "lexicalEntry": {
              "id": "inv/word/1234",
              "canonicalForm": {
                "id": "cform-1",
                "places": {
                  "edges": [{
                    "node": {
                      "id": "place-1",
                      "name": "RDC",
                      "color": "jaune",
                      "__typename": "Country"
                    },
                    "__typename": "PlaceInterfaceEdge"
                  },
                    {
                      "node": {
                        "id": "place-2",
                        "name": "Cameroun",
                        "color": "jaune",
                        "__typename": "Country"
                      },
                      "__typename": "PlaceInterfaceEdge"
                    }],
                  "__typename": "PlaceInterfaceConnection"
                },
                "__typename": "Form"
              },
              "__typename": "Word",
              "partOfSpeech": {
                "id": "pos-1",
                "prefLabel": "nom",
                "__typename": "Concept"
              }
            },
            "lexicographicResource": {
              "id": "lexicog-1",
              "altLabel": "Inv",
              "__typename": "Concept"
            },
            "__typename": "LexicalSense"
          },
          "__typename": "LexicalSenseEdge"
        },
          {
            "node": {
              "id": "inv/lexical-sense/45689",
              "definition": "Grand séducteur, coureur de jupons <a href=\"/test\">test</a>",
              "places": {
                "edges": [{
                  "node": {
                    "id": "place-2",
                    "name": "Cameroun",
                    "color": "jaune",
                    "__typename": "Country"
                  },
                  "__typename": "PlaceInterfaceEdge"
                }],
                "__typename": "PlaceInterfaceConnection"
              },
              "lexicalEntry": {
                "id": "inv/word/1234",
                "canonicalForm": {
                  "id": "cform-1",
                  "places": {
                    "edges": [{
                      "node": {
                        "id": "place-1",
                        "name": "RDC",
                        "color": "jaune",
                        "__typename": "Country"
                      },
                      "__typename": "PlaceInterfaceEdge"
                    },
                      {
                        "node": {
                          "id": "place-2",
                          "name": "Cameroun",
                          "color": "jaune",
                          "__typename": "Country"
                        },
                        "__typename": "PlaceInterfaceEdge"
                      }],
                    "__typename": "PlaceInterfaceConnection"
                  },
                  "__typename": "Form"
                },
                "__typename": "Word",
                "partOfSpeech": {
                  "id": "pos-1",
                  "prefLabel": "nom",
                  "__typename": "Concept"
                }
              },
              "lexicographicResource": {
                "id": "lexicog-1",
                "altLabel": "Inv",
                "__typename": "Concept"
              },
              "__typename": "LexicalSense"
            },
            "__typename": "LexicalSenseEdge"
          }, {
            "node": {
              "id": "wikt/entry/1234/en/1/ls/1",
              "definition": "Qui a bien des affaires, qui est occupé.",
              "places": {
                "edges": [],
                "__typename": "PlaceInterfaceConnection"
              },
              "lexicalEntry": {
                "id": "wikt/entry/1234/en/1",
                "canonicalForm": {
                  "id": "cform-2",
                  "places": {
                    "edges": [],
                    "__typename": "PlaceInterfaceConnection"
                  },
                  "__typename": "Form"
                },
                "__typename": "InflectablePOS",
                "partOfSpeech": {
                  "id": "pos-2",
                  "prefLabel": "adjectif",
                  "__typename": "Concept"
                },
                "number": null,
                "gender": null
              },
              "lexicographicResource": {
                "id": "lexicog-2",
                "altLabel": "Wikt",
                "__typename": "Concept"
              },
              "__typename": "LexicalSense"
            },
            "__typename": "LexicalSenseEdge"
          }],
        "pageInfo": {
          "startCursor": "YXJyYXljb25uZWN0aW9uOjA=",
          "endCursor": "YXJyYXljb25uZWN0aW9uOjM=",
          "hasNextPage": false,
          "__typename": "PageInfo"
        },
        "__typename": "LexicalSenseConnection"
      },
      "__typename": "Query"
    }
  }
};

const formTopPostsMock = {
  request: {
    query: FormTopPostsQuery,
    variables: {
      formQs: "affairé",
    }
  },
  result: {
    "data": {
      "topPostAboutEtymology": {
        "content": "Dérivé de affaire",
        "__typename": "Post"
      },
      "topPostAboutForm": {
        "content": "Semble s'être dit d'abord de domestiques, d'intendants, etc.",
        "__typename": "Post"
      }
    }
  }

};

export const formGqlMocks = [formQueryMock, formTopPostsMock];
