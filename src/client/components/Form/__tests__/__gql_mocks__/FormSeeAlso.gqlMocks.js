import {gqlFormSeeAlsoQuery} from '../../FormSeeAlso';

const formSeeAlsoQueryMock = {
  request: {
    query: gqlFormSeeAlsoQuery,
    variables: {
      formQs: "affairé"
    }
  },
  result: {
    "data": {
      "associativeRelationSemanticRelations": {
        "edges": [{
          "node": {
            "id": "inv/73/sr/ass0",
            "canonicalForms": {
              "edges": [{
                "node": {
                  "id": "inv:entry/5500/en/0/fo/0",
                  "writtenRep": "traiteur",
                  "__typename": "Form"
                },
                "__typename": "FormEdge"
              }],
              "__typename": "FormConnection"
            },
            "__typename": "SemanticRelation"
          },
          "__typename": "SemanticRelationEdge"
        }],
        "__typename": "SemanticRelationConnection"
      },
      "graphicRelationSemanticRelations": {
        "edges": [{
          "node": {
            "id": "inv:73/sr/graph0",
            "canonicalForms": {
              "edges": [{
                "node": {
                  "id": "inv:entry/71/en/0/fo/0",
                  "writtenRep": "aff",
                  "__typename": "Form"
                },
                "__typename": "FormEdge"
              }],
              "__typename": "FormConnection"
            },
            "__typename": "SemanticRelation"
          },
          "__typename": "SemanticRelationEdge"
        }],
        "__typename": "SemanticRelationConnection"
      },
      "morphologicalRelationSemanticRelations": {
        "edges": [{
          "node": {
            "id": "inv/73/sr/der0",
            "canonicalForms": {
              "edges": [{
                "node": {
                  "id": "inv:entry/72/en/0/fo/0",
                  "writtenRep": "affaire",
                  "__typename": "Form"
                },
                "__typename": "FormEdge"
              }],
              "__typename": "FormConnection"
            },
            "__typename": "SemanticRelation"
          },
          "__typename": "SemanticRelationEdge"
        }],
        "__typename": "SemanticRelationConnection"
      },
      "__typename": "Query"
    }
  }
};

export const formSeeAlsoGqlMocks = [formSeeAlsoQueryMock];
