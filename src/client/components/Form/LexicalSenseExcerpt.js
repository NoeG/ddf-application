import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {withRouter, Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {withTranslation} from 'react-i18next';
import {SanitizedHtml} from '../../utilities/SanitizedHtml';

import {ROUTES} from '../../routes';
import style from './LexicalSenseExcerpt.styl';

@withRouter
@withTranslation()
export class LexicalSenseExcerpt extends React.Component {
  static propTypes = {
    sense: PropTypes.object,
    countryData: PropTypes.shape({
      names: PropTypes.arrayOf(PropTypes.string),
      color: PropTypes.string
    }),
    partOfSpeechList: PropTypes.array,
    source: PropTypes.string,
    className: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      hasOverlay: false
    }
  }

  descriptionElement = null;
  overlayElement = null;

  componentDidMount() {
    if (this.descriptionTextEl.clientHeight > this.descriptionContainerEl.clientHeight) {
      this.setState({hasOverlay: true});
    }
  }

  render() {
    let {sense, countryData, source, partOfSpeechList, t} = this.props;
    let senseUrl = formatRoute(ROUTES.FORM_LEXICAL_SENSE, {
      formQuery: this.props.match.params.formQuery,
      lexicalSenseId: sense.id
    });

    return (
      <div className={classNames([style.container, style[countryData.color], this.props.className])} data-testid="lexical-sense-excerpt">
        <div className={style.countriesRow}>
          <div className={style.countries}>
            <Choose>
              <When condition={countryData.names.length > 0}>
                {countryData.names.join(", ")}
              </When>
              <Otherwise>
                {t('GEOGRAPHICAL_DETAILS.NO_COUNTRY_LABEL')}
              </Otherwise>
            </Choose>
          </div>
        </div>

        <div className={style.pos}>
          {partOfSpeechList.join(', ')}
        </div>

        <Link
          to={senseUrl}
          className={style.description}
          ref={(el) => this.descriptionContainerEl = el}
        >
          <div ref={(el) => this.descriptionTextEl = el}>
            <SanitizedHtml
              className={style.text}
              removeAllowedTags={['a']}
              html={sense.definition}
            />
          </div>
          <div className={classNames(style.descriptionOverlay, {[style.active]: this.state.hasOverlay})}></div>
        </Link>

        <div className={style.sourcesRow}>
          <div className={style.sources}>Source : {source}</div>
        </div>
      </div>
    );
  }
}
