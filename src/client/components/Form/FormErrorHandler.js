import React from 'react';
import {withRouter} from 'react-router-dom';

import {ErrorHandler} from '../general/ErrorHandler';
import style from './FormErrorHandler.styl';

@withRouter
export class FormErrorHandler extends ErrorHandler {
  render() {
    if (this.state.hasError) {
      return <div className={style.error}>
        Un problème technique nous empêche d'afficher ce contenu.
      </div>;
    }
    return this.props.children;
  }
}
