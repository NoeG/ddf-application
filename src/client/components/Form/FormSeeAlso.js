import React from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {gql} from 'apollo-boost';
import flatten from 'lodash/flatten';
import {useQuery} from '@apollo/react-hooks';

import {ROUTES} from '../../routes';
import {gqlSemanticRelationFormFragment, getSemanticRelationFormList} from '../../utilities/helpers/semanticRelationFormList';

import {getPlaceCountryId} from '../../utilities/helpers/countries';
import {apolloErrorHandler} from '../../utilities/apolloErrorHandler';

import style from './FormSeeAlso.styl';
import desktopSubHeaderStyle from '../../layouts/desktop/DesktopSubHeader.styl';

/**
 * Returns the GQL fragment corresponding to `getFormPostsAboutForm` helper.
 * @type {gql}
 */
export let gqlFormSeeAlsoQuery = gql`
  query FormSeeAlso_Query($formQs: String){
    associativeRelationSemanticRelations: semanticRelationsForAccurateWrittenForm(formQs: $formQs filterOnType: associativeRelation){
      edges{
        node{
          id
          ...SemanticRelationFormFragment
        }
      }
    }

    graphicRelationSemanticRelations: semanticRelationsForAccurateWrittenForm(formQs: $formQs filterOnType: graphicRelation){
      edges{
        node{
          id
          ...SemanticRelationFormFragment
        }
      }
    }

    morphologicalRelationSemanticRelations: semanticRelationsForAccurateWrittenForm(formQs: $formQs filterOnType: morphologicalRelation){
      edges{
        node{
          id
          ...SemanticRelationFormFragment
        }
      }
    }
  }

  ${gqlSemanticRelationFormFragment}
`;

FormSeeAlso.propTypes = {
  formQuery: PropTypes.string.isRequired
};
export function FormSeeAlso(props) {

  const {t} = useTranslation();
  const {formQuery} = props;
  const {data, loading, error} = useQuery(gqlFormSeeAlsoQuery, {
    variables: {
      formQs: formQuery
    },
    fetchPolicy: 'cache-and-network'
  });
  apolloErrorHandler(error, 'log');
  if ((loading && !data) || error) return null;

  const {
    associativeRelationSemanticRelations,
    graphicRelationSemanticRelations,
    morphologicalRelationSemanticRelations
  } = data;

  /* Don't render the FormSeeAlso component if no results for any of the categories */
  if (
    !associativeRelationSemanticRelations.edges.length && 
    !graphicRelationSemanticRelations.edges.length && 
    !morphologicalRelationSemanticRelations.edges.length
  ) {
    return null;
  }


  return (
    <React.Fragment>
      <div className={desktopSubHeaderStyle.secondaryTitle}>{t('FORM.SEE_ALSO.TITLE')}</div>
      <div className={style.container}>
        <h4 className={style.title}>{t('FORM.SEE_ALSO.TITLE')}</h4>

        <If condition={associativeRelationSemanticRelations.edges.length > 0}>
          <p>
            <span className={style.inlineTitle}>{t('FORM.SEE_ALSO.RELATED')}&nbsp;:&nbsp;</span>
            {renderSemanticRelations(associativeRelationSemanticRelations)}
          </p>
        </If>

        <If condition={graphicRelationSemanticRelations.edges.length > 0}>
          <p>
            <span className={style.inlineTitle}>{t('FORM.SEE_ALSO.FORM_VARIATION')}&nbsp;:&nbsp;</span>
            {renderSemanticRelations(graphicRelationSemanticRelations)}
          </p>
        </If>
        <If condition={morphologicalRelationSemanticRelations.edges.length > 0}>
          <p>
            <span className={style.inlineTitle}>{t('FORM.SEE_ALSO.DERIVED')}&nbsp;:&nbsp;</span>
            {renderSemanticRelations(morphologicalRelationSemanticRelations)}
          </p>
        </If>
      </div>
    </React.Fragment>
  );

  function renderSemanticRelations(semanticRelations) {
    let forms = flatten(
      semanticRelations.edges
      .map(({node: semanticRelation}) => getSemanticRelationFormList(semanticRelation))
    );

    return forms.map((form, index) => {
      return (
        <React.Fragment key={index} >
          <Link to={formatRoute(ROUTES.FORM_SEARCH, {formQuery: form})}>{form}</Link>
          <If condition={index < forms.length - 1}>
            <span>, </span>
          </If>
        </React.Fragment>
      );
    })
  }

}
