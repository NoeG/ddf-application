import React from 'react';
import classNames from 'classnames';

import style from './FormTopPosts.styl';

export class FormTopPostsLoader extends React.Component {
  render() {
    return (
      <div className={classNames([style.container, style.placeholder])}>
        <h5 className={style.title}>
          &nbsp;
          <div className={style.titlePlaceholder}/>
        </h5>
        <div className={style.section}>
          <span className={style.content}>&nbsp;</span>
          <div className={style.sectionPlaceholder}/>
        </div>
        <hr/>
        <h5 className={style.title}>
          &nbsp;
          <div className={style.titlePlaceholder}/>
        </h5>
        <div className={style.section}>
          <span className={style.content}>&nbsp;</span>
          <div className={style.sectionPlaceholder}/>
        </div>
      </div>
    )
  }
}

