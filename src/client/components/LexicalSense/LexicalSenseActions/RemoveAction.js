/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import classNames from 'classnames';
import {useTranslation} from 'react-i18next';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks';

import {ActionButton} from './ActionButton';
import {notificationService} from '../../../services/NotificationService';

import removeIcon from '../../../assets/images/remove.svg';

export let gqlRemoveLexicalSenseMutation = gql`
  mutation ($lexicalSenseId: ID!) {
    removeEntity( input: { objectId: $lexicalSenseId }) {
      deletedId
    }
  }
`;

export function RemoveAction({lexicalSense, theme, onActionError, onRemoveSuccess, onRemoveUpdateCache} = {}) {
  let {t} = useTranslation();

  let [removeLexicalSense, {loading}] = useMutation(gqlRemoveLexicalSenseMutation, {
    onError: onActionError,
    onCompleted: () => {
      if (onRemoveSuccess){
        onRemoveSuccess();
      }

      notificationService.success(t("LEXICAL_SENSE_DETAIL.ACTIONS.LEXICAL_SENSE_REMOVE_SUCCESS"));
    }
  });

  return (
    <ActionButton
      text={t('LEXICAL_SENSE_DETAIL.ACTIONS.REMOVE')}
      icon={removeIcon}
      color="black"
      onClick={handleRemove}
      theme={theme}
    />
  );

  async function handleRemove(){
    if (!loading) {
      let {confirmationResult} = await notificationService.info(t('LEXICAL_SENSE_DETAIL.ACTIONS.REMOVE_ARE_YOU_SURE'), {confirm: true});
      if (confirmationResult === 'yes') {
        await removeLexicalSense({
          variables: {lexicalSenseId: lexicalSense.id},
          update: (cache) => {
            if(onRemoveUpdateCache){
              onRemoveUpdateCache(cache);
            }
          }
        });
      }
    }
  }
}
