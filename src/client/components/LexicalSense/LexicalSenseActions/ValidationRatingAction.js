/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks';

import {ActionButton} from './ActionButton';
import {gqlRefreshLexicalSenseReviewedStatus} from '../../../utilities/apollo/queries/RefreshLexicalSenseReviewedStatus';

import validateIcon from '../../../assets/images/circled_checkmark_purple.svg';

export let gqlValidationRatingFragment = gql`
  fragment LexicalSenseValidationRatingFragment on LexicalSense{
    validationRatingsCount
    validationRating{
      id
    }
  }
`;

export let gqlAddValidationRatingMutation = gql`
  mutation ($lexicalSenseId: ID!) {
    createValidationRating( input: { lexicalSenseId: $lexicalSenseId }) {
      createdObject {
        id
      }
    }
  }
`;

export let gqlRemoveValidationRatingMutation = gql`
  mutation ($validationRatingId: ID!) {
    removeEntity( input: { objectId: $validationRatingId }) {
      deletedId
    }
  }
`;


ValidationRatingAction.propTypes = {
  /**
   * If true, the mutation that updates a validationRating will trigger an refetchQuery on the lexicalSense, in order to refresh
   * the value of the property 'reviewed' (which cannot be guessed to update the cache locally). This behavior is not enabled by default because 
   * it causes an additionnal network request on each "validation" action, and the cache update is only needed in the context of the admin dashboard
   * listing the contributions
   */
  enableRefetchQuery: PropTypes.bool
}
export function ValidationRatingAction({lexicalSense, theme, onActionError, enableRefetchQuery} = {}) {
  let {t} = useTranslation();

  let refetchQueries = [];
  if (enableRefetchQuery) {
    refetchQueries.push({
      query: gqlRefreshLexicalSenseReviewedStatus, 
      variables: {
        lexicalSenseId: lexicalSense.id
      }
    });
  }

  let [addValidationRating, {loading: addValidationRatingLoading}] = useMutation(gqlAddValidationRatingMutation, {
    onError: onActionError,
    refetchQueries
  });

  let [removeValidationRating, {loading: removeValidationRatingLoading}] = useMutation(gqlRemoveValidationRatingMutation, {
    onError: onActionError,
    refetchQueries
  });

  return (
    <Choose>
      <When condition={lexicalSense.validationRating?.id}>
        <ActionButton
          text={t('LEXICAL_SENSE_DETAIL.ACTIONS.UNLIKE')}
          icon={validateIcon}
          color="purple"
          onClick={handleRemoveValidationRating}
          unactive={false}
          theme={theme}
        />
      </When>
      <Otherwise>
        <ActionButton
          text={t('LEXICAL_SENSE_DETAIL.ACTIONS.LIKE')}
          icon={validateIcon}
          color="purple"
          onClick={handleAddValidationRating}
          unactive={true}
          theme={theme}
        />
      </Otherwise>
    </Choose>
  );

  async function handleAddValidationRating(){
    if (!addValidationRatingLoading && !removeValidationRatingLoading) {
      await addValidationRating({
        variables: {lexicalSenseId: lexicalSense.id},
        optimisticResponse: {
          __typename: 'Mutation',
          createValidationRating: {
            __typename: 'CreateValidationRatingPayload',
            createdObject: {
              __typename: 'ValidationRating',
              /* 
               * fake ID while we wait for the actual value to return from server. Be sure that no action can be done
               * on this ID before the actual value returns 
               */
              id: '-1'
            }
          }
        },
        update: (cache, {data: {createValidationRating: {createdObject}}}) => {
          cache.writeFragment({
            id: `LexicalSense:${lexicalSense.id}`,
            fragment: gqlValidationRatingFragment,
            data: {
              validationRatingsCount: lexicalSense.validationRatingsCount + 1,
              validationRating: createdObject,
              __typename: "LexicalSense"
            }
          })
        }
      });
    }
  }

  async function handleRemoveValidationRating(){
    if (!addValidationRatingLoading && !removeValidationRatingLoading) {
      await removeValidationRating({
        variables: {validationRatingId: lexicalSense.validationRating.id},
        optimisticResponse: {
          __typename: 'Mutation',
          removeEntity: {
            __typename: 'RemoveEntityPayload',
            deletedId: lexicalSense.validationRating.id
          }
        },
        update: (cache) => {
          cache.writeFragment({
            id: `LexicalSense:${lexicalSense.id}`,
            fragment: gqlValidationRatingFragment,
            data: {
              validationRatingsCount: lexicalSense.validationRatingsCount - 1,
              validationRating: null,
              __typename: "LexicalSense"
            }
          });
        }
      })
    }
  }
}
