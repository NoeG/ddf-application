/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import classNames from "classnames";
import {useTranslation} from "react-i18next";
import {gql} from "apollo-boost";
import {useMutation} from "@apollo/react-hooks";
import PropTypes from 'prop-types';

import {gqlRefreshLexicalSenseReviewedStatus} from '../../../utilities/apollo/queries/RefreshLexicalSenseReviewedStatus';
import {ActionButton} from "./ActionButton";

import toRemoveIcon from "../../../assets/images/circled_minus_pink.svg";

export let gqlSuppressionRatingFragment = gql`
  fragment LexicalSenseSuppressionRatingFragment on LexicalSense{
    suppressionRatingsCount
    suppressionRating{
      id
    }
  }
`;

export let gqlAddSuppressionRatingMutation = gql`
  mutation ($lexicalSenseId: ID!) {
    createSuppressionRating( input: { lexicalSenseId: $lexicalSenseId }) {
      createdObject {
        id
      }
    }
  }
`;

export let gqlRemoveSuppressionRatingMutation = gql`
  mutation ($suppressionRatingId: ID!) {
    removeEntity( input: { objectId: $suppressionRatingId }) {
      deletedId
    }
  }
`;

SuppressionRatingAction.propTypes = {
  /**
   * If true, the mutation that updates a suppressionRating will trigger an refetchQuery on the lexicalSense, in order to refresh
   * the value of the property 'reviewed' (which cannot be guessed to update the cache locally). This behavior is not enabled by default because 
   * it causes an additionnal network request on each "suppressionRating" action, and the cache update is only needed in the context of the admin dashboard
   * listing the contributions
   */
  enableRefetchQuery: PropTypes.bool
}
export function SuppressionRatingAction({lexicalSense, theme, onActionError, enableRefetchQuery} = {}) {
  let {t} = useTranslation();

  let refetchQueries = [];
  if (enableRefetchQuery) {
    refetchQueries.push({
      query: gqlRefreshLexicalSenseReviewedStatus, 
      variables: {
        lexicalSenseId: lexicalSense.id
      }
    });
  }


  let [addSuppressionRating, {loading: addSuppressionRatingLoading}] = useMutation(gqlAddSuppressionRatingMutation, {
    onError: onActionError,
    refetchQueries
  });

  let [removeSuppressionRating, {loading: removeSuppressionRatingLoading}] = useMutation(gqlRemoveSuppressionRatingMutation, {
    onError: onActionError,
    refetchQueries
  });

  return (
    <Choose>
      <When condition={lexicalSense.suppressionRating?.id}>
        <ActionButton
          text={t('LEXICAL_SENSE_DETAIL.ACTIONS.UNASK_REMOVAL')}
          icon={toRemoveIcon}
          color="pink"
          onClick={handleRemoveSuppressionRating}
          unactive={false}
          theme={theme}
        />
      </When>
      <Otherwise>
        <ActionButton
          text={t('LEXICAL_SENSE_DETAIL.ACTIONS.ASK_REMOVAL')}
          icon={toRemoveIcon}
          color="pink"
          onClick={handleAddSuppressionRating}
          unactive={true}
          theme={theme}
        />
      </Otherwise>
    </Choose>
  );

  async function handleAddSuppressionRating(){
    if (!addSuppressionRatingLoading && !removeSuppressionRatingLoading) {
      await addSuppressionRating({
        variables: {lexicalSenseId: lexicalSense.id},
        optimisticResponse: {
          __typename: 'Mutation',
          createSuppressionRating: {
            __typename: 'CreateSuppressionRatingPayload',
            createdObject: {
              __typename: 'SuppressionRating',
              /* 
               * fake ID while we wait for the actual value to return from server. Be sure that no action can be done
               * on this ID before the actual value returns 
               */
              id: '-1'
            }
          }
        },
        update: (cache, {data: {createSuppressionRating: {createdObject}}}) => {
          cache.writeFragment({
            id: `LexicalSense:${lexicalSense.id}`,
            fragment: gqlSuppressionRatingFragment,
            data: {
              suppressionRatingsCount: lexicalSense.suppressionRatingsCount + 1,
              suppressionRating: createdObject,
              __typename: "LexicalSense"
            }
          })
        }
      });
    }
  }

  async function handleRemoveSuppressionRating(){
    if (!addSuppressionRatingLoading && !removeSuppressionRatingLoading) {
      await removeSuppressionRating({
        variables: {suppressionRatingId: lexicalSense.suppressionRating.id},
        optimisticResponse: {
          __typename: 'Mutation',
          removeEntity: {
            __typename: 'RemoveEntityPayload',
            deletedId: lexicalSense.suppressionRating.id
          }
        },
        update: (cache) => {
          cache.writeFragment({
            id: `LexicalSense:${lexicalSense.id}`,
            fragment: gqlSuppressionRatingFragment,
            data: {
              suppressionRatingsCount: lexicalSense.suppressionRatingsCount - 1,
              suppressionRating: null,
              __typename: "LexicalSense"
            }
          })
        }
      });
    }
  }
}
