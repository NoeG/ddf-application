/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import classNames from 'classnames';
import {useTranslation} from 'react-i18next';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks';

import {ActionButton} from './ActionButton';

import reportIcon from '../../../assets/images/circled_exclamation_orange.svg';


export let gqlReportingRatingFragment = gql`
  fragment LexicalSenseReportingRatingFragment on LexicalSense{
    reportingRatingsCount
    reportingRating{
      id
    }
  }
`;

export let gqlAddReportingRatingMutation = gql`
  mutation ($lexicalSenseId: ID!) {
    createReportingRating( input: { lexicalSenseId: $lexicalSenseId }) {
      createdObject {
        id
      }
    }
  }
`;

export let gqlRemoveReportingRatingMutation = gql`
  mutation ($reportingRatingId: ID!) {
    removeEntity( input: { objectId: $reportingRatingId }) {
      deletedId
    }
  }
`;

export function ReportingRatingAction({lexicalSense, theme, onActionError} = {}) {
  let {t} = useTranslation();

  let [addReportingRating, {loading: addMutationLoading}] = useMutation(gqlAddReportingRatingMutation, {onError: onActionError});
  let [removeReportingRating, {loading: removeMutationLoading}] = useMutation(gqlRemoveReportingRatingMutation, {onError: onActionError});

  return (
    <Choose>
      <When condition={lexicalSense.reportingRating?.id}>
        <ActionButton
          text={t('LEXICAL_SENSE_DETAIL.ACTIONS.UNREPORT')}
          icon={reportIcon}
          color="orange"
          onClick={handleRemoveReportingRating}
          loading={removeMutationLoading}
          unactive={false}
          theme={theme}
        />
      </When>
      <Otherwise>
        <ActionButton
          text={t('LEXICAL_SENSE_DETAIL.ACTIONS.REPORT')}
          icon={reportIcon}
          color="orange"
          onClick={handleAddReportingRating}
          loading={addMutationLoading}
          unactive={true}
          theme={theme}
        />
      </Otherwise>
    </Choose>
  );

  async function handleAddReportingRating(){
    if (!addMutationLoading&& !removeMutationLoading) {
      await addReportingRating({
        variables: {lexicalSenseId: lexicalSense.id},
        optimisticResponse: {
          __typename: 'Mutation',
          createReportingRating: {
            __typename: 'CreateReportingRatingPayload',
            createdObject: {
              __typename: 'ReportingRating',
              /* 
               * fake ID while we wait for the actual value to return from server. Be sure that no action can be done
               * on this ID before the actual value returns 
               */
              id: '-1'
            }
          }
        },
        update: (cache, {data: {createReportingRating: {createdObject}}}) => {
          cache.writeFragment({
            id: `LexicalSense:${lexicalSense.id}`,
            fragment: gqlReportingRatingFragment,
            data: {
              reportingRatingsCount: lexicalSense.reportingRatingsCount + 1,
              reportingRating: createdObject,
              __typename: "LexicalSense"
            }
          })
        }
      });
    }
  }

  async function handleRemoveReportingRating(){
    if (!addMutationLoading && !removeMutationLoading) {
      await removeReportingRating({
        variables: {reportingRatingId: lexicalSense.reportingRating.id},
        optimisticResponse: {
          __typename: 'Mutation',
          removeEntity: {
            __typename: 'RemoveEntityPayload',
            deletedId: lexicalSense.reportingRating.id
          }
        },
        update: (cache) => {
          cache.writeFragment({
            id: `LexicalSense:${lexicalSense.id}`,
            fragment: gqlReportingRatingFragment,
            data: {
              reportingRatingsCount: lexicalSense.reportingRatingsCount - 1,
              reportingRating: null,
              __typename: "LexicalSense"
            }
          })
        }
      })
    }
  }
}
