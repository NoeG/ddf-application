import React from 'react';
import classNames from 'classnames';

import style from './ActionButton.styl';

/**
 * Usage :
 *
 * <ActionButton
 *    text="text"
 *    icon={iconSrc}
 *    color="orange"
 * />
 *
 * Properties :
 *
 * - text
 * - icon
 * - color: is one of "orange" or "purple"
 *
 */
export class ActionButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: ["orange", "purple", "pink", "black"].includes(props.color) ? props.color : null
    };
  }

  render() {
    const {theme} = this.props;

    return (
      <div 
        className={classNames([style.actionButton, style[this.state.color], theme?.button], {
          [style.loading]: this.props.loading,
          [style.unactive]: this.props.unactive
        })} 
        title={this.props.text}
        onClick={this.props.onClick}
      >
        <img className={classNames(style.icon, theme?.icon)} src={this.props.icon}/>
      </div>
    );
  }
}
