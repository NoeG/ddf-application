/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks';

import {ActionButton} from './ActionButton';
import checkmarkIcon from '../../../assets/images/circled_checkmark_green.svg';

let gqlUpdateLexicalSenseProcessed = gql`
  mutation UpdateLexicalSenseProcessed($lexicalSenseId: ID!, $processed: Boolean!) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,
      objectInput: {
        processed: $processed
      }
    }) {
      updatedObject {
        id
        processed
        reviewed
      }
    }
  }
`;

export function ProcessAction({lexicalSense, theme, onActionError} = {}) {
  let {t} = useTranslation();

  let [updateProcessedMutation, {loading}] = useMutation(gqlUpdateLexicalSenseProcessed, {onError: onActionError});

  return (
    <Choose>
      <When condition={!lexicalSense.processed}>
        <ActionButton
          text={t('LEXICAL_SENSE_DETAIL.ACTIONS.PROCESS')}
          icon={checkmarkIcon}
          color="green"
          onClick={() => updateProcessed(true)}
          theme={theme}
          unactive
        />
      </When>
      <Otherwise>
        <ActionButton
          text={t('LEXICAL_SENSE_DETAIL.ACTIONS.UNPROCESS')}
          icon={checkmarkIcon}
          color="green"
          onClick={() => updateProcessed(false)}
          theme={theme}
        />
      </Otherwise>
    </Choose>
  );

  async function updateProcessed(processedValue){
    if (loading) return;

    await updateProcessedMutation({
      variables: {
        lexicalSenseId: lexicalSense.id,
        processed: processedValue
      }, 
      optimisticResponse: {
        __typename: 'Mutation',
        updateLexicalSense: {
          __typename: 'UpdateLexicalSensePayload',
          updatedObject: {
            __typename: 'LexicalSense',
            id: lexicalSense.id,
            reviewed: lexicalSense.reviewed,
            processed: processedValue
          }
        }
      }
    });
  }
}
