import React from 'react';
import classNames from 'classnames';
import {useTranslation} from 'react-i18next';
import {useQuery} from '@apollo/react-hooks';
import {Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {ParseNewlines} from '@mnemotix/synaptix-client-toolkit';

import {FormTopPostsQuery} from '../Form/FormTopPosts';
import {getLexicalEntryLexicographicSourceLabel} from '../../utilities/helpers/lexicalEntryLexicographicSource';
import {getLexicalSenseTopPostAboutSense} from '../../utilities/helpers/lexicalSenseTopPostAboutSense';
import {apolloErrorHandler} from '../../utilities/apolloErrorHandler';
import {ROUTES} from '../../routes';
import {DesktopDiscussionPreview} from '../Form/DesktopDiscussionPreview';

import style from './LexicalSenseDesktopSideColumn.styl';
import {LexicalSenseActions} from "./LexicalSenseActions";

/**
 * Usage :
 *
 * <LexicalSenseDesktopSideColumn
 *    lexicalSense={lexicalSense}
 *    formQuery="currentFormQuery"
 * />
 *
 */
export function LexicalSenseDesktopSideColumn(props) {
  const {lexicalSense, formQuery, onRemoveSuccess} = props;
  const {t} = useTranslation();
  const {data, loading, error} = useQuery(FormTopPostsQuery, {
    variables: {
      formQs: formQuery
    },
    fetchPolicy: 'cache-and-network'
  });
  apolloErrorHandler(error, 'log');
  if ((loading && !data) || error) return null;

  const {topPostAboutForm, topPostAboutEtymology} = data;
  const source = lexicalSense?.lexicographicResource?.prefLabel;
  const topPostAboutSense = getLexicalSenseTopPostAboutSense(lexicalSense);
  const likeCount = lexicalSense?.validationRatingsCount;

  return (
    <div className={style.desktopSideColumn}>
      <div className={style.likeCount}>
        <ParseNewlines
          text={ likeCount > 0 ?
              t('LEXICAL_SENSE_DETAIL.DESKTOP.LIKE_COUNT', {count: likeCount}) :
              t('LEXICAL_SENSE_DETAIL.DESKTOP.LIKE_COUNT_zero')
          }
        />
      </div>

      <LexicalSenseActions lexicalSense={lexicalSense} formQuery={formQuery} onRemoveSuccess={onRemoveSuccess}/>

      <If condition={source}>
        <Link
          to={formatRoute(ROUTES.FORM_LEXICAL_SENSE_SOURCES, {formQuery, lexicalSenseId: lexicalSense.id})}
          className={style.source}
        >
          <span className={style.title}>{t('LEXICAL_SENSE_DETAIL.DESKTOP.SOURCE')}</span>
          <span className={style.text}>{source}</span>
        </Link>
      </If>

      <div className={style.discussionsSection}>
        {renderTopPost({title: t('FORM.TOP_POSTS.DESKTOP.FORM_HEADER'), post: topPostAboutForm})}
        {renderTopPost({title: t('FORM.TOP_POSTS.DESKTOP.ETYMOLOGY_HEADER'), post: topPostAboutEtymology})}
        {renderTopPost({title: t('LEXICAL_SENSE_DETAIL.DESKTOP.TOP_POSTS.SENSE_HEADER'), post: topPostAboutSense})}
      </div>
    </div>
  );

  function renderTopPost({title, post}) {
    if (post) {
      return (
        <DesktopDiscussionPreview
          title={title}
          content={post.content}
          commentCount={12}
        />
      );
    }
  }
}
