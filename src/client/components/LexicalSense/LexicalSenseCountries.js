import React from 'react';
import PropTypes from 'prop-types';
import {formatRoute} from 'react-router-named-routes';
import {withTranslation} from 'react-i18next';
import classNames from 'classnames';
import {withRouter} from 'react-router-dom';


import {ROUTES} from '../../routes';
import {SimpleModalPage} from '../../layouts/mobile/SimpleModalPage';
import {colorToCssClassName} from '../../utilities/helpers/countries';
import style from './LexicalSenseCountries.styl';

@withTranslation()
export class LexicalSenseCountries extends React.Component {
  static propTypes = {
    /**
     * An array of Place objects as returned by the GraphQL API. Each place object must have the properties
     * `name` and `color`
     */
    places: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string.isRequired,
      color: PropTypes.string
    }))
  }

  render() {
    const {t} = this.props;
    let places = this.props.places;

    if (!(places?.length)) {
      return <div>{t('GEOGRAPHICAL_DETAILS.NO_RESULTS')}</div>;
    } else {
      return (
        <React.Fragment>
          <div className={style.desktopCaption}>{t('GEOGRAPHICAL_DETAILS.PAGE_CAPTION')}</div>
          <div className={style.countries}>
            {
              places.map((place, idx) => {
                return (
                  <div 
                    key={idx} 
                    className={classNames([
                      style.country, 
                      style[colorToCssClassName(place.color)]
                    ])}
                  >
                    {place.name}
                  </div>
                );
              })
            }
          </div>
        </React.Fragment>
      );
    }
  }

}


@withTranslation()
@withRouter
export class LexicalSenseCountriesMobilePage extends React.Component {
  static propTypes = {
    lexicalSenseId: PropTypes.string.isRequired,
    formQuery: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.closeButtonHandler = this.closeButtonHandler.bind(this);
  }

  render() {
    const {t} = this.props;
    return(
      <SimpleModalPage
        title={t('GEOGRAPHICAL_DETAILS.TITLE')}
        content={<LexicalSenseCountries {...this.props} />}
        controls={['close']}
        onClose={this.closeButtonHandler}
        theme={style}
      />
    );
  }

  closeButtonHandler() {
    const {history, lexicalSenseId, formQuery} = this.props;
    let backPath = formatRoute(ROUTES.FORM_LEXICAL_SENSE, {lexicalSenseId, formQuery});
    history.replace({
      pathname: backPath
    });
  }
}
