import React from 'react';
import waait from 'waait';
import {cleanup, getNodeText} from '@testing-library/react';

import {UsageExamples} from '../UsageExamples';
import {renderWithMocks} from '../../../../../jest/utilities/renderWithMocks';

afterEach(cleanup);

it("works without usageExamples prop", async () => {
  const {container, findByTestId} = renderWithMocks({element: <UsageExamples />});

  await waait();
  expect(container.innerHTML).toEqual("");
})

describe("empty example value", () => {
  test("array consiting of only an empty example", async () => {
    const usageExamples = [{
      value: "",
      bibliographicalCitation: null
    }];

    const {container, findByTestId} = renderWithMocks({element: <UsageExamples usageExamples={usageExamples}/>});

    await waait();
    expect(container.innerHTML).toEqual("");
  });

  test("array with empty and non empty examples", async () => {
    const usageExamples = [{
      value: "un example",
      bibliographicalCitation: "une source"
    }, {
      value: "",
      bibliographicalCitation: null
    }];

    const {findAllByTestId} = renderWithMocks({element: <UsageExamples usageExamples={usageExamples}/>});

    let results = await findAllByTestId("example");
    expect(results.length).toEqual(1);
    expect(results[0].textContent).toEqual("Exemple : un example—une source");
  });

})

describe("no bibliographicalCitation", () => {
  it("doesn't display the bibliographicalCitation nor the dash", async () => {
    const usageExamples = [{
      value: "un example",
      bibliographicalCitation: null
    }];

    const {findAllByTestId} = renderWithMocks({element: <UsageExamples usageExamples={usageExamples}/>});

    let results = await findAllByTestId("example");
    expect(results.length).toEqual(1);
    expect(results[0].textContent).toEqual("Exemple : un example");
  });
});
