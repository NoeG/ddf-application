import React from 'react';
import {render, cleanup, getNodeText} from '@testing-library/react';
import {LexicalSenseSources} from '../LexicalSenseSources';

afterEach(cleanup);

describe('wiktionary source', () => {
  it('links to the list of the contributors', async () => {
    let form = "Être bien affairé (n'est-ce-pas) ?";

    let {container, findByTestId} = render(
      <LexicalSenseSources 
        lexicographicResource={{
          id: 'http://data.dictionnairedesfrancophones.org/resource/wiktionnaire',
          prefLabel: 'Wiktionnaire francophone'
        }}
        formQuery={form}
      />
    );
    let link = await findByTestId('wiktionary-link');
    expect(link.textContent).toEqual("Historique des versions de « Être bien affairé (n'est-ce-pas) ? »");
    expect(link.querySelector("a").getAttribute("href")).toEqual("https://fr.wiktionary.org/w/index.php?title=%C3%8Atre%20bien%20affair%C3%A9%20(n'est-ce-pas)%20%3F&action=history");
  });
});
