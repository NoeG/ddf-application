/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';

import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {useObservable} from 'react-use';
import {gql} from 'apollo-boost';
import classNames from 'classnames';

import {getUserAuthenticationService} from '../../services/UserAuthenticationService';
import {GraphQLErrorHandler} from '../../services/GraphQLErrorHandler';
import {notificationService as appNotificationService} from '../../services/NotificationService';
import {gqlValidationRatingFragment, ValidationRatingAction} from './LexicalSenseActions/ValidationRatingAction';
import {gqlReportingRatingFragment, ReportingRatingAction} from './LexicalSenseActions/ReportingRatingAction';
import {gqlSuppressionRatingFragment, SuppressionRatingAction} from './LexicalSenseActions/SuppressionRatingAction';
import {RemoveAction} from './LexicalSenseActions/RemoveAction';

import style from './LexicalSenseActions.styl';

export let gqlLexicalSenseActionsFragment = gql`
  fragment LexicalSenseActionsFragment on LexicalSense{
    validationRatingsCount
    suppressionRatingsCount
    ...LexicalSenseValidationRatingFragment
    ...LexicalSenseReportingRatingFragment
    ...LexicalSenseSuppressionRatingFragment
  }
  ${gqlValidationRatingFragment}
  ${gqlReportingRatingFragment}
  ${gqlSuppressionRatingFragment}
`;

export function LexicalSenseActions(props) {
  const {lexicalSense, onRemoveSuccess, onRemoveUpdateCache} = props;
  let {t} = useTranslation();
  let notificationService = props.notificationService || appNotificationService;
  let userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();

  const {user, isLogged} = useObservable(userAuthenticationService.currentUser, {});

  return (
    <div className={classNames(style.actions)}>
      <If condition={isLogged}>
        <ValidationRatingAction 
          lexicalSense={lexicalSense} 
          theme={style}
          onActionError={onActionError} 
        />

        <ReportingRatingAction
          lexicalSense={lexicalSense}
          theme={style}
          onActionError={onActionError} 
        />
      </If>

      <If condition={user?.userAccount?.isOperator || user?.userAccount?.isAdmin}>
        <SuppressionRatingAction 
          lexicalSense={lexicalSense} 
          theme={style} 
          onActionError={onActionError} 
        />
      </If>

      <If condition={user?.userAccount?.isAdmin}>
        <RemoveAction 
          lexicalSense={lexicalSense} 
          theme={style} 
          onActionError={onActionError} 
          onRemoveSuccess={onRemoveSuccess} 
          onRemoveUpdateCache={onRemoveUpdateCache}
        />
      </If>
    </div>
  );

  async function onActionError(error) {
    let {globalErrorMessage} = GraphQLErrorHandler(error, {t});
    await notificationService.error(globalErrorMessage);
  }
}
