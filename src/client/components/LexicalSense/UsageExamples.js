import React from 'react';
import PropTypes from 'prop-types';
import {withTranslation} from 'react-i18next';
import {SanitizedHtml} from '../../utilities/SanitizedHtml';

import style from './UsageExamples.styl';

@withTranslation()
export class UsageExamples extends React.Component {


  static propTypes = {
    /**
     * An array of UsageExample objects, as provided by the graphQL API. The array can be empty.
     * It can contain empty examples, they will not be displayed.
     */
    usageExamples: PropTypes.arrayOf(PropTypes.shape({
      bibliographicalCitation: PropTypes.string,
      value: PropTypes.string
    }))
  }


  render() {
    const {t, usageExamples} = this.props;
    if (!usageExamples) return null;
    let filteredExamples = usageExamples.filter(ex => ex.value);

    return (
      <React.Fragment>
        {filteredExamples.map((usageExample, index) => (
          <div key={index} className={style.usageExample} data-testid="example">
            <span className={style.label}>{t('LEXICAL_SENSE_DETAIL.EXAMPLE_LABEL')}&nbsp;:&nbsp;</span>
            <span className={style.text}>
              <SanitizedHtml as="span" html={usageExample.value}/>
              <If condition={usageExample.bibliographicalCitation}>
                &mdash;
                <SanitizedHtml as="span" html={usageExample.bibliographicalCitation}/>
              </If>
            </span>
          </div>
        ))}
      </React.Fragment>
    );
  }
}
