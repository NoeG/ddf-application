import React from 'react';
import classNames from 'classnames';

import style from './LexicalSenseLoader.styl';

export function LexicalSenseLoader(props) {

  return (
    <div className={style.placeholder}>
      <div className={style.countriesRow}>        
        <div className={style.countries}>
          &nbsp;
        </div>
      </div>

      <div className={style.fakeTextLine}>
        &nbsp;
        <div className={style.textPlaceholder}/>
      </div>

      <div className={style.fakeTextLine}>
        &nbsp;
        <div className={style.textPlaceholder}/>
      </div>

    </div>
  );
}
