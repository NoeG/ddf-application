import React from 'react';
import PropTypes from 'prop-types';
import {formatRoute} from 'react-router-named-routes';
import {withTranslation} from 'react-i18next';
import classNames from 'classnames';
import {withRouter} from 'react-router-dom';

import {ROUTES} from '../../routes';
import {SimpleModalPage} from '../../layouts/mobile/SimpleModalPage';
import style from './LexicalSenseSources.styl';

@withTranslation()
export class LexicalSenseSources extends React.Component {
  static propTypes = {
    /**
     * A lexicographic resource object as returned by the graphQL API (property of a LexicalEntry object). Must contain
     * the property `prefLabel`
     */
    lexicographicResource: PropTypes.shape({
      id: PropTypes.string.isRequired,
      prefLabel: PropTypes.string.isRequired
    }).isRequired,
    formQuery: PropTypes.string.isRequired
  }

  render() {
    let {t, lexicographicResource, formQuery} = this.props;
    let {prefLabel: sourceName, id: sourceId} = lexicographicResource;
    let formQueryURI = encodeURIComponent(formQuery);
    let wiktionaryLink = `https://fr.wiktionary.org/w/index.php?title=${formQueryURI}&action=history`;

    return (
      <div>
        <p>{sourceName}</p>
        <If condition={sourceId==='http://data.dictionnairedesfrancophones.org/resource/wiktionnaire'}>
          <br/>
          <p data-testid="wiktionary-link"><a href={wiktionaryLink} target="_blank">
            {t('SOURCES_PAGE.WIKTIONARY_LINK', {formQuery})}  
          </a></p>
        </If>
      </div>
    );
  }


}

@withTranslation()
@withRouter
export class LexicalSenseSourcesMobilePage extends React.Component {
  static propTypes = {
    /**
     * A lexicographic resource object as returned by the graphQL API (property of a LexicalEntry object). Must contain
     * the property `prefLabel`
     */
    lexicographicResource: PropTypes.shape({
      prefLabel: PropTypes.string.isRequired
    }).isRequired,
    lexicalSenseId: PropTypes.string.isRequired,
    formQuery: PropTypes.string.isRequired
  }


  constructor(props) {
    super(props);
    this.closeButtonHandler = this.closeButtonHandler.bind(this);
  }

  render() {
    const {t} = this.props;

    return(
      <SimpleModalPage
        title={t('SOURCES_PAGE.TITLE')}
        content={<LexicalSenseSources 
          lexicographicResource={this.props.lexicographicResource} 
          formQuery={this.props.formQuery}
        />}
        controls={['close']}
        onClose={this.closeButtonHandler}
        theme={style}
      />
    );
  }

  closeButtonHandler() {
    const {history, lexicalSenseId, formQuery} = this.props;
    let backPath = formatRoute(ROUTES.FORM_LEXICAL_SENSE, {lexicalSenseId, formQuery});
    history.replace({
      pathname: backPath
    });
  }
}
