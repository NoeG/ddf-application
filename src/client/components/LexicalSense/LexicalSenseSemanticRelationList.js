/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';
import {useTranslation} from 'react-i18next';
import {Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import flatten from 'lodash/flatten';
import get from 'lodash/get';
import upperFirst from 'lodash/upperFirst';
import {gql} from 'apollo-boost';
import {useQuery} from '@apollo/react-hooks';

import {ROUTES} from '../../routes';
import {
  getGroupedLexicalSenseSemanticRelationList,
  gqlLexicalSenseSemanticRelationFragment
} from '../../utilities/helpers/lexicalSenseSemanticRelationList';

import lexicalSenseLoaderStyle from './LexicalSenseLoader.styl';
import style from './LexicalSenseSemanticRelationList.styl';

export let LexicalSenseSemanticRelationListQuery = gql`
  query LexicalSenseSemanticRelationList_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId){
      id
      ...LexicalSenseSemanticRelationFragment
    }
  }

  ${gqlLexicalSenseSemanticRelationFragment}
`;

/**
 * This component render all semantic relation of a lexicalSense.
 *
 * Need the be given the CSS style for .section through the property `theme`
 *
 * @borrows gqlLexicalSenseSemanticRelationFragment : lexicalSense GraphQL query must add this fragment.
 */
export function LexicalSenseSemanticRelationList({lexicalSenseId, theme}) {
  let {t} = useTranslation();
  const {loading, data} = useQuery(LexicalSenseSemanticRelationListQuery, {
    variables: {
      lexicalSenseId
    },
    fetchPolicy: 'cache-and-network'
  });

  if (data?.lexicalSense){
    let groupedSemanticRelations = getGroupedLexicalSenseSemanticRelationList(data.lexicalSense);
    return Object.entries(groupedSemanticRelations)
      .map(([semanticRelationLabel, semanticRelations], idx) =>
        renderSemanticRelationSection(semanticRelationLabel, semanticRelations, idx)
      );
  } else {
    return renderLoader();
  }

  /**
   * @param semanticRelationLabel {string}    The title of the group of semantic relations, for example "synonymes", to use as a title to the section
   * @param semanticRelations {Array<Object>}  The array of semantic relation objects (see the interface SemanticRelation in the graphQL API)
   * @param idx {number}                      A unique id to use as key in the returned component, because it is intended to be part of a collection
   */
  function renderSemanticRelationSection(semanticRelationLabel, semanticRelations, idx) {
    let forms = flatten(
      semanticRelations.map((semanticRelation) => semanticRelation.canonicalForms.edges)
    ).map(({node: form}) => form);

    return (
      <div className={theme['section']} key={idx}>
        <h3 className={style.semanticRelationTitle}>
          {upperFirst(semanticRelationLabel)}
        </h3>

        {
          forms.map((form, index) => {
            let writtenRep = form?.writtenRep;

            return (
              <div key={index} className={style.semanticRelation}>
                <Link to={formatRoute(ROUTES.FORM_SEARCH, {formQuery: writtenRep})}>{writtenRep}</Link>
              </div>
            )
          })
        }
      </div>
    );
  }

  function renderLoader() {
    return (
      <div className={lexicalSenseLoaderStyle.placeholder}>
        <div className={lexicalSenseLoaderStyle.fakeTextLine}>
          &nbsp;
          <div className={lexicalSenseLoaderStyle.textPlaceholder}/>
        </div>

        <div className={lexicalSenseLoaderStyle.fakeTextLine}>
          &nbsp;
          <div className={lexicalSenseLoaderStyle.textPlaceholder}/>
        </div>
      </div>
    );
  }
}
