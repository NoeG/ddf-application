import React, {useEffect} from 'react';
import classNames from 'classnames';
import {Formik} from 'formik';
import {useTranslation, Trans} from 'react-i18next';
import {useMutation} from '@apollo/react-hooks';
import {Link, useHistory} from 'react-router-dom';
import {Helmet} from 'react-helmet-async';
import {ParseNewlines} from '@mnemotix/synaptix-client-toolkit';

import {ROUTES} from '../../routes';
import {SimpleModalPage} from '../../layouts/responsive/SimpleModalPage';
import {Input as FormikInput} from '../widgets/forms/formik/Input';
import {Checkbox as FormikCheckbox} from '../widgets/forms/formik/Checkbox';
import {Form} from '../widgets/forms/formik/Form';
import {Button} from '../widgets/forms/Button';
import {FormBottom} from '../widgets/forms/FormBottom';
import {PlaceAutocomplete} from '../widgets/forms/formik/PlaceAutocomplete';
import {notificationService as appNotificationService} from '../../services/NotificationService';
import {
  getUserAuthenticationService
} from '../../services/UserAuthenticationService';
import {useBrowsingHistoryHelperService} from '../../services/BrowsingHistoryHelperService';

import style from './Subscribe.styl';
import formsStyle from '../widgets/forms/Forms.styl';
import userPagesStyle from './users/UserPages.styl';




/***
 *
 * List of error messages :
 *
 * - Frontend defined : 
 *   - REQUIRED
 *   - INVALID_EMAIL
 * - Backend defined :
 *   - PASSWORD_TOO_SHORT
 *   - EMAIL_ALREADY_REGISTERED
 *
 */
export function Subscribe(props) {
  const {t} = useTranslation();
  const notificationService = props.notificationService || appNotificationService;
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const {subscribe} = userAuthenticationService.useSubscribe();
  const history = useHistory();
  const {browsingHistoryHelperService} = useBrowsingHistoryHelperService();

  return (
    <React.Fragment>
      <Helmet>
        <title>{t('DOCUMENT_TITLES.SUBSCRIBE')}</title>
      </Helmet>
      <SimpleModalPage 
        title={t('SUBSCRIBE_PAGE.TITLE')}
        content={renderContent()}
        color="pink"
        theme={userPagesStyle}
      />
    </React.Fragment>
  );

  function renderContent() {
    return (
      <React.Fragment>
        <div className={style.topInfoBox}>
          <h2>{t('SUBSCRIBE_PAGE.SECOND_LEVEL_TITLE')}</h2>
          <p><ParseNewlines text={t('SUBSCRIBE_PAGE.TOP_INFOBOX_CONTENT')}/></p>
        </div>

        <Formik
          initialValues={userAuthenticationService.getSubscribeFormInitialValues()}
          validationSchema={userAuthenticationService.getSubscribeValidationSchema()}
          validateOnChange={false}
          validateOnBlur={false}
          onSubmit={handleSubscribe}
          render={({errors, status, touched, isSubmitting, values}) => (
            <Form noValidate>
              <FormikInput type="text" name="nickName" placeholder="Pseudonyme (ou nom)" />
              <FormikInput type="email" name="email" placeholder="Adresse de courriel" />
              <FormikInput type="password" name="password" placeholder="Mot de passe" />
              <PlaceAutocomplete name="placeId" placeholder="Lieu de vie" />
              <FormikCheckbox 
                name="gcu" 
                label={<Trans i18nKey="SUBSCRIBE_PAGE.GCU_CHECKBOX_LABEL">I accept the <Link to={ROUTES.GCU} target="_blank">terms and conditions</Link>.</Trans>}
              />
              <div className={classNames([formsStyle.info, formsStyle.secondary, style.help])}>
                <p><em>{t('SUBSCRIBE_PAGE.MANDATORY_FIELDS')}</em></p>
                <p><ParseNewlines text={t('SUBSCRIBE_PAGE.GEOLOC_DISCLAIMER')}/></p>
              </div>
              <FormBottom>
                <Button type="submit" disabled={isSubmitting} loading={isSubmitting}>{t('SUBSCRIBE_PAGE.SUBMIT')}</Button>
                <Button type="button" secondary onClick={navigateBack}>{t('SUBSCRIBE_PAGE.CANCEL')}</Button>
              </FormBottom>
            </Form>
          )}
        />
      </React.Fragment>
    );
  }

  function navigateBack() {
    let route = browsingHistoryHelperService.getBackFromSecondaryScreenLocation();
    history.push(route ? route.location : '/');
  }

  async function handleSubscribe(values, formikOptions) {
    let res = await subscribe(values, formikOptions);

    if (res.success) {
      let route = browsingHistoryHelperService.getAfterLoginLocation();
      history.push(route ? route.location : '/');
    }
    if (res.globalErrorMessage) {
      notificationService.error(res.globalErrorMessage);
    }
  }

}

