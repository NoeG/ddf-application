import React from 'react';
import classNames from 'classnames';
import {useTranslation} from 'react-i18next';
import {Helmet} from 'react-helmet-async';

import {ProcessedMarkdownWrapper} from '../../utilities/ProcessedMarkdownWrapper';
import {InformationPage} from '../../layouts/responsive/InformationPage';

import style from './Partners.styl';
import content from '../../assets/markdown/partners.md';

export function Partners(props) {
  const {t} = useTranslation();

  return <InformationPage 
    title={t('PARTNERS_PAGE_TITLE')}
    content={<ProcessedMarkdownWrapper content={content}/>}
    theme={style} 
  />;
}
