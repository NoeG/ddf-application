/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import React from 'react';
import {Formik} from 'formik';
import {useTranslation, Trans} from 'react-i18next';
import {useHistory, Link} from 'react-router-dom';
import {Helmet} from 'react-helmet-async';

import {ROUTES} from '../../routes';
import {SimpleModalPage} from '../../layouts/responsive/SimpleModalPage';
import {Input as FormikInput} from '../widgets/forms/formik/Input';
import {Form} from '../widgets/forms/formik/Form';
import {Button} from '../widgets/forms/Button';
import {FormBottom} from '../widgets/forms/FormBottom';
import {notificationService as appNotificationService} from '../../services/NotificationService';
import {
  getUserAuthenticationService
} from '../../services/UserAuthenticationService';
import {useBrowsingHistoryHelperService} from '../../services/BrowsingHistoryHelperService';

import userPagesStyle from './users/UserPages.styl';
import formsStyle from '../widgets/forms/Forms.styl';


export function Login(props) {
  const notificationService = props.notificationService || appNotificationService;
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const {t} = useTranslation();
  const {login} = userAuthenticationService.useLogin();
  const history = useHistory();
  const {browsingHistoryHelperService} = useBrowsingHistoryHelperService();


  return (
    <React.Fragment>
      <Helmet>
        <title>{t('DOCUMENT_TITLES.LOGIN')}</title>
      </Helmet>
      <SimpleModalPage 
        title={t('LOGIN_PAGE.TITLE')}
        content={renderContent()}
        color="pink"
        theme={userPagesStyle}
      />
    </React.Fragment>
  );

  function renderContent() {
    return (
      <React.Fragment>
        <Formik
          initialValues={userAuthenticationService.getLoginFormInitialValues()}
          validationSchema={userAuthenticationService.getLoginValidationSchema()}
          validateOnChange={false}
          validateOnBlur={false}
          onSubmit={handleLogin}
          render={({errors, status, touched, isSubmitting, values}) => (
            <Form noValidate>
              <p className={formsStyle.info}>{t('LOGIN_PAGE.IF_YOU_ALREADY_HAVE_AN_ACCOUNT')}</p>
              <FormikInput type="email" name="email" placeholder={t('LOGIN_PAGE.EMAIL')} />
              <FormikInput type="password" name="password" placeholder={t('LOGIN_PAGE.PASSWORD')}/>
              <p className={formsStyle.info}>
                <Trans i18nKey="LOGIN_PAGE.CREATE_ACCOUNT_INFOTEXT">You can <Link to={ROUTES.SUBSCRIBE}>create an account here</Link>.</Trans>
              </p>
              <p className={formsStyle.info}>
                <span>{t('LOGIN_PAGE.LOST_CREDENTIALS_INFOTEXT')}</span>
                <span> </span>
                <Link to={ROUTES.PASSWORD_FORGOTTEN}>{t('LOGIN_PAGE.LOST_CREDENTIALS_LINK_TEXT')}</Link>
              </p>
              <FormBottom>
                <Button type="submit" disabled={isSubmitting} loading={isSubmitting}>{t('LOGIN_PAGE.SUBMIT')}</Button>
              </FormBottom>
            </Form>
          )}
        />
      </React.Fragment>
    );
  }

  async function handleLogin(values, formikOptions) {
    let res = await login(values, formikOptions);

    if (res.success) {
      let route = browsingHistoryHelperService.getAfterLoginLocation();
      history.push(route ? route.location : '/');
    }
    if (res.globalErrorMessage) {
      notificationService.error(res.globalErrorMessage);
    }
  }

}


