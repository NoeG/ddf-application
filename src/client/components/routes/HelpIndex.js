import React from 'react';
import {withTranslation} from 'react-i18next';
import {Link} from 'react-router-dom';
import classNames from 'classnames';
import {ParseNewlines} from '@mnemotix/synaptix-client-toolkit';
import {Helmet} from 'react-helmet-async';

import {ResponsiveMainLayout} from '../../layouts/responsive/ResponsiveMainLayout';
import {HelpMobileHeader} from '../../layouts/mobile/MobileMainLayout/HelpMobileHeader';
import {ROUTES} from '../../routes';
import style from './HelpIndex.styl';

@withTranslation()
export class HelpIndex extends React.Component {
  render() {
    const {t} = this.props;

    return (
      <React.Fragment>
        <Helmet>
          <title>{t('DOCUMENT_TITLES.HELP')}</title>
        </Helmet>
        <ResponsiveMainLayout mobileHeader={this.renderHeader()} desktopUseDefaultGrid>
          <div className={style.container}>
            {this.renderSection({text: t('HELP.SECTION_LINKS.SEARCH_AND_USER_ACCOUNT'), path: ROUTES.HELP_HOW_TO_SEARCH, color: 'green'})}
            {this.renderSection({text: t('HELP.SECTION_LINKS.UNDERSTAND_ARTICLE'), path: ROUTES.HELP_HOW_TO_READ, color: 'orange'})}
            {this.renderSection({text: t('HELP.SECTION_LINKS.HOW_TO_CONTRIBUTE'), path: ROUTES.HELP_HOW_TO_CONTRIBUTE, color: 'yellow'})}
            {this.renderSection({text: t('HELP.SECTION_LINKS.OFFLINE'), path: ROUTES.HELP_DDF_IRL, color: 'pink'})}
          </div>
        </ResponsiveMainLayout>
      </React.Fragment>
    );
  }

  renderSection({text, path, color}) {
    return (
      <Link 
        to={path} 
        className={classNames([
          style.section, style[color]
        ])}
      >
        <div className={style.button}>
          <div className={style.textContainer}>
            <ParseNewlines text={text}/>
          </div>
        </div>

      </Link>
    );

  }

  renderHeader() {
    const {t} = this.props;
    return <HelpMobileHeader 
      title={t('HELP.HEADER')}
      search={true}
      ddfLogo={true} 
    />;
  }
}
