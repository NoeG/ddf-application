import React, {useState, useEffect} from 'react';
import {useHistory, Link} from 'react-router-dom';
import {formatRoute}      from 'react-router-named-routes';
import {useTranslation}  from 'react-i18next';
import {ParseNewlines} from '@mnemotix/synaptix-client-toolkit';

import {ROUTES} from '../../routes';
import {ResponsiveMainLayout} from '../../layouts/responsive/ResponsiveMainLayout';
import {GeolocPicker} from '../widgets/GeolocPicker';
import {FormSearchInput} from '../widgets/FormSearchInput';
import {BorderedButton} from '../widgets/BorderedButton';
import {useBrowsingHistoryHelperService} from '../../services/BrowsingHistoryHelperService';


import DdfLogo from '../../assets/images/ddf_logo.png';
import style from './Index.styl';

export function Index(props){
  const {t} = useTranslation();
  const history = useHistory();
  const {browsingHistoryHelperService} = useBrowsingHistoryHelperService();
  let [searchQuery, updateSearchQuery] = useState('');
  useEffect(() => {
    /* 
     * Hack, to let the routing process entirely before calling the utility, otherwise the previous route is
     * still considered as the current route
     */
    setTimeout(() => {
      let {formWrittenRep} = browsingHistoryHelperService.getPreviousPageFormWrittenRep();
      if (formWrittenRep) updateSearchQuery(formWrittenRep);
    }, 10);
  }, []);

  return (
    <ResponsiveMainLayout theme={style} desktopHeaderHideSearch={true}>
      <div className={style.logo}>
        <Link to='/'> 
          <img src={DdfLogo}/> 
        </Link>
      </div>

      <div className={style.title}>
        <h1>
          Dictionnaire<br/>des francophones
        </h1>
      </div>

      <div className={style.searchField}>
        <FormSearchInput
          placeholder={t('INDEX.SEARCH_PLACEHOLDER')}
          value={searchQuery}
          onChange={(e) => updateSearchQuery(e.target.value)}
          onSubmit={performSearch}
        />
      </div>

      <div className={style.geolocWidget}>
        <GeolocPicker />
      </div>

      <BorderedButton 
        className={style.searchButton}
        theme={style}
        text={t('INDEX.SEARCH_BUTTON')} 
        onClick={performSearch} 
      />

    <div className={style.footer}>
      <hr/>
      <div className={style.footerText}>
        <ParseNewlines text={t('INDEX.FOOTER')} />
      </div>
      <hr/>
    </div>
  </ResponsiveMainLayout>
  );

  function performSearch() {
    if(!searchQuery) {
      return;
    }
    history.push(formatRoute(ROUTES.FORM_SEARCH, {formQuery: searchQuery}));
  }
}

