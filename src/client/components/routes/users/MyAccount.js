import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {Route, Switch, useRouteMatch} from 'react-router-dom';
import {Helmet} from 'react-helmet-async';

import {SimpleModalPage} from '../../../layouts/mobile/SimpleModalPage';
import {DesktopMainLayout} from '../../../layouts/desktop/DesktopMainLayout';
import {DesktopGrid} from '../../../layouts/desktop/DesktopGrid';
import {DesktopSubHeader} from '../../../layouts/desktop/DesktopSubHeader';
import {useMediaQueries} from '../../../layouts/MediaQueries';
import {BannerMenu} from '../../widgets/BannerMenu';
import {DesktopSideMenu} from '../../widgets/DesktopSideMenu';
import {ROUTES} from '../../../routes';
import {Profile} from './MyAccount/Profile';
import {ProfileEdit} from './MyAccount/ProfileEdit';
import {ProfileSettings} from './MyAccount/ProfileSettings';
import {Contributions} from './MyAccount/Contributions';

import style from './MyAccount.styl';
import userPagesStyle from './UserPages.styl';
Object.assign(style, userPagesStyle);


export function MyAccount(props) {
  const {t} = useTranslation();
  const {isMobile} = useMediaQueries();
  const profileRouteMatch = useRouteMatch(ROUTES.MY_ACCOUNT_PROFILE);
  const contributionsRouteMatch = useRouteMatch(ROUTES.MY_ACCOUNT_CONTRIBUTIONS);

  let menuEntries = [{
    to: ROUTES.MY_ACCOUNT_PROFILE,
    text: t('MY_ACCOUNT.MY_PROFILE_LINK')
  }, {
    to: ROUTES.MY_ACCOUNT_CONTRIBUTIONS,
    text: t('MY_ACCOUNT.MY_CONTRIBUTIONS_LINK')
  }];

  return isMobile ? renderMobile() : renderDesktop();


  function renderMobile() {
    return (
      <SimpleModalPage
        title={t('MY_ACCOUNT.TITLE')}
        extendHeader={renderMobileHeader()}
        content={renderContent()}
        color="pink"
        theme={style}
      />
    );
  }

  function renderMobileHeader() {
    return (
      <React.Fragment>
        <BannerMenu entries={menuEntries} />
        <Route 
          exact 
          path={ROUTES.MY_ACCOUNT_PROFILE_EDIT} 
          render={() => <div className={style.thirdLevelHeader}>{t('MY_ACCOUNT.PROFILE.EDIT.TITLE')}</div>}
        />
      </React.Fragment>
    );
  }

  function renderDesktop() {
    let backLinkOptions = {};
    let subtitle;
    if (profileRouteMatch) {
      subtitle = t('MY_ACCOUNT.MY_PROFILE_LINK');
      if (!profileRouteMatch.isExact) {
        backLinkOptions = {
          backLinkText: 'Retour',
          backLinkTo: ROUTES.MY_ACCOUNT_PROFILE
        };
      }
    }
    if (contributionsRouteMatch) {
      subtitle = t('MY_ACCOUNT.MY_CONTRIBUTIONS_LINK');
    }

    return (
      <DesktopMainLayout>
        <DesktopGrid
          firstSection={<DesktopSubHeader
            primaryTitle={t('MY_ACCOUNT.TITLE')}
            secondaryTitle={subtitle}
            color="pink"
            theme={style}
            {...backLinkOptions}
          />} 
          sideColumn={<DesktopSideMenu entries={menuEntries} title={t('MY_ACCOUNT.DESKTOP_MENU_TITLE')}/>}
        >
          <div className={style.desktopContent}>
            {renderContent()}
          </div>
        </DesktopGrid>
      </DesktopMainLayout>
    );
  }

  function renderContent() {
    return (
      <React.Fragment>
        <Helmet>
          <title>{t('DOCUMENT_TITLES.MY_ACCOUNT')}</title>
        </Helmet>
        <Switch>
          <Route exact path={ROUTES.MY_ACCOUNT_PROFILE_EDIT} component={ProfileEdit} />
          <Route exact path={ROUTES.MY_ACCOUNT_PROFILE_SETTINGS} component={ProfileSettings} />
          <Route exact path={ROUTES.MY_ACCOUNT_PROFILE} component={Profile} />
          <Route exact path={ROUTES.MY_ACCOUNT_CONTRIBUTIONS} component={Contributions} />
        </Switch>
      </React.Fragment>
    );
  }
}
