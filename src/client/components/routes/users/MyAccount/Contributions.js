import React from 'react';
import {useTranslation} from 'react-i18next';

import {ContributionsList} from '../../../contribution/ContributionsList';
import {getUserAuthenticationService} from '../../../../services/UserAuthenticationService';

import style from './Contributions.styl';

export function Contributions({pageSize, userAuthenticationService} = {}){
  let {t} = useTranslation();
  userAuthenticationService = userAuthenticationService || getUserAuthenticationService();

  return (
    <React.Fragment>
      <div className={style.topCaption}>{t('MY_ACCOUNT.CONTRIBUTIONS.TITLE')}</div>

      <ContributionsList 
        pageSize={pageSize} 
        filterOnLoggedUser={true} 
        userAuthenticationService={userAuthenticationService}
        hideColumns={['person', 'actions']}
      />
    </React.Fragment>
  );
}
