import React from 'react';
import {render, cleanup} from '@testing-library/react';
import {MockedProvider} from '@apollo/react-testing';
import {act} from 'react-dom/test-utils';
import waait from 'waait';

import {Contributions} from '../Contributions';
import {renderWithMocks} from '../../../../../../../jest/utilities/renderWithMocks';
import {gqlContributionsQuery} from '../../../../contribution/ContributionsList/ContributionsListTable';

afterEach(cleanup);


describe("MyAccount Contributions", () => {
  it("lists the user contributions", async () => {
    let gqlMocks = [{
      request:{
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "filterOnLoggedUser": true,
          "filterOnReviewed": null,
          "filterOnExistingSuppressionRating": null,
          "filterOnProcessed": null,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ]
        }
      },
      result: {
        "data": {
          "contributedLexicalSenses": {
            "edges": [
              {
                "node": {
                  "id": "lexical-sense/6p7hah8cblhimj",
                  "definition": "new 1",
                  "createdAt": "2020-01-27T16:13:49+01:00",
                  "creatorPerson": {
                    "nickName": "Derek Admin",
                    "__typename": "Person"
                  },
                  "canonicalForm": {
                    "writtenRep": "affairé",
                    "__typename": "Form"
                  },
                  "processed": false,
                  "reviewed": true,
                  "validationRatingsCount": 1,
                  "suppressionRatingsCount": 1,
                  "reportingRatingsCount": 1,
                  "validationRating": {
                    "id": "validation-rating/u95j8glpius7gf",
                    "__typename": "ValidationRating"
                  },
                  "suppressionRating": {
                    "id": "suppression-rating/v33xfa8bhsxynh",
                    "__typename": "ValidationRating"
                  },
                  "reportingRating": {
                    "id": "reporting-rating/a44hxfa8bhxauie",
                    "__typename": "ReportingRating"
                  },
                  "__typename": "LexicalSense"
                },
                "__typename": "LexicalSenseEdge"
              },
              {
                "node": {
                  "id": "lexical-sense/loip527riw7a18",
                  "definition": "Test de definition",
                  "createdAt": "2020-01-15T15:44:49+01:00",
                  "creatorPerson": {
                    "nickName": "Derek Admin",
                    "__typename": "Person"
                  },
                  "canonicalForm": {
                    "writtenRep": "affairé",
                    "__typename": "Form"
                  },
                  "processed": false,
                  "reviewed": false,
                  "validationRatingsCount": 0,
                  "suppressionRatingsCount": 0,
                  "reportingRatingsCount": 0,
                  "validationRating": null,
                  "suppressionRating": null,
                  "reportingRating": null,
                  "__typename": "LexicalSense"
                },
                "__typename": "LexicalSenseEdge"
              }
            ],
            "pageInfo": {
              "endCursor": "offset:1",
              "hasNextPage": false,
              "__typename": "PageInfo"
            },
            "__typename": "LexicalSenseConnection"
          }
        }
      }
    }];

    let userAuthenticationServiceMock = {
      useLoggedUser: () => ({
        user: {}
      })
    };

    const {container} = renderWithMocks({
      gqlMocks: gqlMocks,
      element: <Contributions userAuthenticationService={userAuthenticationServiceMock}/>,
    });

    await act(waait);
    await act(waait);

    expect(container).toMatchSnapshot();
  });
});
