import React from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import {useMutation} from '@apollo/react-hooks';

import {PlaceAutocomplete} from '../../../../widgets/forms/formik/PlaceAutocomplete';
import {GenericSettingField} from './GenericSettingField';
import {getUserAuthenticationService} from '../../../../../services/UserAuthenticationService';

const gqlEditPersonPlace = gql`
  mutation EditPersonPlace($personId: ID!, $objectId: ID){
    updatePerson(input: {
      objectId: $personId, 
      objectInput: {
        placeInput: {id: $objectId}
      }
    }){
      updatedObject{
        id
        place {
          id
        }
      }
    }
  }
`;


PlaceSetting.propTypes = {
  displayValue: PropTypes.string,
  inputValue: PropTypes.string,
  idValue: PropTypes.string,
  meId: PropTypes.string
};
export function PlaceSetting(props) {
  const {displayValue, inputValue, idValue, meId} = props;
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const [editPersonPlaceMutation, mutationResult] = useMutation(gqlEditPersonPlace);


  return (<GenericSettingField
    displayValue={displayValue}
    inputValue={inputValue}
    idValue={idValue}
    meId={meId}
    i18nSectionKey="PLACE"
    mutation={editPersonPlaceMutation}
    AutocompleteComponent={PlaceAutocomplete}
    onSubmit={() => userAuthenticationService.refreshCurrentUser()}
  />);
}
