import React, {useState} from 'react';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import {useMutation} from '@apollo/react-hooks';

import {DomainAutocomplete} from '../../../../widgets/forms/formik/DomainAutocomplete';
import {GenericSettingField} from './GenericSettingField';

const gqlEditPersonPreferredDomainMutation = gql`
  mutation EditPersonPreferredDomain($personId: ID!, $objectId: ID){
    updatePerson(input: {
      objectId: $personId, 
      objectInput: {
        preferredDomainInput: {id: $objectId}
      }
    }){
      updatedObject{
        id
        preferredDomain {
          id
        }
      }
    }
  }
`;


VocabularyDomainSetting.propTypes = {
  displayValue: PropTypes.string,
  idValue: PropTypes.string,
  meId: PropTypes.string
};
export function VocabularyDomainSetting(props) {
  const {displayValue, idValue, meId} = props;
  const [editPersonPreferredDomainMutation, mutationResult] = useMutation(gqlEditPersonPreferredDomainMutation);


  return (<GenericSettingField
    displayValue={displayValue}
    inputValue={displayValue}
    idValue={idValue}
    meId={meId}
    i18nSectionKey="PREFERRED_DOMAIN"
    mutation={editPersonPreferredDomainMutation}
    AutocompleteComponent={DomainAutocomplete}
  />);
}
