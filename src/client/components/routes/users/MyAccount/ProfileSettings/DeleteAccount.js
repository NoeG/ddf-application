import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import {FoldableContainer} from '../../../../widgets/FoldableContainer';
import {Button} from '../../../../widgets/forms/Button';
import {ButtonRow} from '../../../../widgets/forms/ButtonRow';

import style from './DeleteAccount.styl';
import {notificationService as appNotificationService} from "../../../../../services/NotificationService";
import {
  gqlUnregisterUserAccountMutation,
  getUserAuthenticationService
} from "../../../../../services/UserAuthenticationService";
import {useMutation} from "@apollo/react-hooks";
import {withRouter} from 'react-router-dom';

/***
 *
 * List of error messages :
 *
 * - Backend defined :
 *
 */
export function _DeleteAccount(props) {
  const history = props.history;

  const {t} = useTranslation();
  const notificationService = props.notificationService || appNotificationService;
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const {unregisterUserAccount} = userAuthenticationService.useUnregisterUserAccount();
  const [isSubmitting, setSubmitting] = useState(false);

  return (
    <FoldableContainer title={t('MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.TITLE')}>
      {({open, close}) => {
        return (
          <div className={style.container}>
            <p>{t('MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.TOP_CAPTION')}</p>
            <ButtonRow>
              <Button secondary onClick={close}>{t('MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.CANCEL')}</Button>
              <Button
                disabled={isSubmitting}
                loading={isSubmitting}
                onClick={deleteAccountHandler}>{t('MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.SUBMIT')}</Button>
            </ButtonRow>
          </div>
        );
      }}
    </FoldableContainer>
  );

  async function deleteAccountHandler(){
    let {confirmationResult} = await notificationService.info(t('MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.ARE_YOU_SURE'), {confirm: true});
    if (confirmationResult === 'yes') {
      setSubmitting(true);
      let result = await unregisterUserAccount();

      if (result?.success){
        await notificationService.success(t("MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.SUCCESS"));
        history.push('/');
      }

      if (result?.globalErrorMessage) {
        notificationService.error(result.globalErrorMessage);
      }

      setSubmitting(false);
    }
  }
}

export let DeleteAccount = withRouter(_DeleteAccount);
