import React, {useState} from 'react';
import gql from 'graphql-tag';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {Formik} from 'formik';
import {useMutation} from '@apollo/react-hooks';

import {Button} from '../../../../widgets/forms/Button';
import {ButtonRow} from '../../../../widgets/forms/ButtonRow';
import {Form} from '../../../../widgets/forms/formik/Form';
import {notificationService as appNotificationService} from '../../../../../services/NotificationService';
import {GraphQLErrorHandler} from '../../../../../services/GraphQLErrorHandler';

import style from './GenericSettingField.styl';


GenericSettingField.propTypes = {
  /**
   * The value to display in non edit mode (it's in static html tag, e.g "span")
   */
  displayValue: PropTypes.string,
  /**
   * The initial value to display in the html input (before user edit it)
   */
  inputValue: PropTypes.string,
  /**
   * The ID of the setting's object current value (for example if this is a place setting, the current
   * Place ID)
   */
  idValue: PropTypes.string,
  /**
   * The current user ID, needed for the update mutation
   */
  meId: PropTypes.string,
  /**
   * The key for the translations. The i18n prefix used will be "MY_ACCOUNT.PROFILE.SETTINGS.${i18nSectionKey}".
   * The i18n entries used are : 
   *
   * - TITLE
   * - HINT
   * - EMPTY
   */
  i18nSectionKey: PropTypes.string,
  /**
   * The mutation function to use (as provided by useMutation hook) for updating the setting. The mutation must take the variables `meId` (the current user
   * Id), and `objectId` (the setting object ID)
   */
  mutation: PropTypes.any,
  /**
   * The autocomplete component to use to edit the setting. Must work like PlaceAutocomplete component : 
   *      <AutocompleteComponent
   *          name="objectId" 
   *          placeholder="myPlaceholder"
   *          selectNullOnEmpty             
   *      />
   */
  AutocompleteComponent: PropTypes.elementType
};
export function GenericSettingField(props) {
  const {displayValue, meId, idValue, i18nSectionKey, mutation, AutocompleteComponent, onSubmit} = props;
  const {t} = useTranslation();
  const [editing, setEditing] = useState(false);
  const notificationService = props.notificationService || appNotificationService;

  
  return (
    <div className={style.settingField}>
      <div className={style.title}>{t(`MY_ACCOUNT.PROFILE.SETTINGS.${i18nSectionKey}.TITLE`)}</div>
      {!editing && renderReadMode()}
      {editing && renderEditMode()}
    </div>
  );

  function renderReadMode() {
    return (
      <React.Fragment>
        <div className={style.currentSetting}>
          <span className={style.caption}>{t('MY_ACCOUNT.PROFILE.SETTINGS.CURRENT')}</span>
          <span className={style.value}>{displayValue || t(`MY_ACCOUNT.PROFILE.SETTINGS.${i18nSectionKey}.EMPTY`)}</span>
        </div>
        <div className={style.hint}>{t(`MY_ACCOUNT.PROFILE.SETTINGS.${i18nSectionKey}.HINT`)}</div>
        <div className={style.editLink}><a href="#" onClick={openEdit}>{t('MY_ACCOUNT.PROFILE.SETTINGS.MODIFY')}</a></div>
      </React.Fragment>
    );
  }

  function renderEditMode() {
    return (
      <Formik
        onSubmit={(values, formikOptions) => submitHandler({values, formikOptions})}
        initialValues={{objectId: idValue}}
        initialStatus={{autocompleteInputValues: {objectId: props.inputValue}}}
        render={({values, isSubmitting}) => (
          <Form>
            <AutocompleteComponent
              name="objectId" 
              placeholder={t(`MY_ACCOUNT.PROFILE.SETTINGS.${i18nSectionKey}.TITLE`)} 
              selectNullOnEmpty
            />
            <ButtonRow>
              <Button type="button" secondary onClick={closeEdit}>{t('MY_ACCOUNT.PROFILE.SETTINGS.CANCEL')}</Button>
              <Button type="submit" disabled={isSubmitting} loading={isSubmitting}>{t('MY_ACCOUNT.PROFILE.SETTINGS.SUBMIT')}</Button>
            </ButtonRow>
          </Form>
        )}
      />
    );
  }




  async function submitHandler({values, formikOptions: {setSubmitting}}) {
    let variables = {
      personId: meId,
      ...values
    };
    try {
      await mutation({variables});
      closeEdit();
    } catch(error) {
      let errorMessage = GraphQLErrorHandler(error, {t});
      notificationService.error(errorMessage);
    } finally {
      setSubmitting(false);
      onSubmit && onSubmit(values);
    }
  }

  function openEdit() {
    setEditing(true);
  }

  function closeEdit() {
    setEditing(false);
  }
}
