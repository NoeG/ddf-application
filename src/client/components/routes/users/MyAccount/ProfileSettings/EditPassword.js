import React from 'react';
import {useTranslation} from 'react-i18next';
import {useMutation} from '@apollo/react-hooks';
import {Formik} from 'formik';

import {FoldableContainer} from '../../../../widgets/FoldableContainer';
import {Button} from '../../../../widgets/forms/Button';
import {ButtonRow} from '../../../../widgets/forms/ButtonRow';
import {Input} from '../../../../widgets/forms/formik/Input';
import {Form} from '../../../../widgets/forms/formik/Form';
import {getUserAuthenticationService} from '../../../../../services/UserAuthenticationService';

import style from './EditPassword.styl';
import {notificationService as appNotificationService} from "../../../../../services/NotificationService";

/***
 *
 * List of error messages :
 *
 * - Frontend defined :
 *   - REQUIRED
 *   - PASSWORDS_DO_NOT_MATCH
 * - Backend defined :
 *   - IS_REQUIRED
 *   - PASSWORD_TO_SHORT
 *   - PASSWORDS_DO_NOT_MATCH
 *   - WRONG_OLD_PASSWORD
 *
 */
export function EditPassword (props){
  const {t} = useTranslation();
  const notificationService = props.notificationService || appNotificationService;
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const {resetPassword, success, globalErrorMessage} = userAuthenticationService.useResetPassword();

  return (
    <FoldableContainer title={t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.TITLE')}>
      {({close}) => (
        <div className={style.container}>
          <p>{t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.TOP_CAPTION')}</p>
          <Formik
            initialValues={userAuthenticationService.getResetPasswordFormInitialValues()}
            validationSchema={userAuthenticationService.getResetPasswordValidationSchema()}
            onSubmit={(values, formikOptions) => handleSubmit(values, formikOptions, close)}
            validateOnChange={false}
            validateOnBlur={false}
            render={({isSubmitting}) => (
              <Form noValidate>
                <label>{t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.OLD_PASSWORD_CAPTION')}</label>
                <Input type="password" name="oldPassword" placeholder={t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.OLD_PASSWORD_PLACEHOLDER')}/>

                <label>{t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.NEW_PASSWORD_CAPTION')}</label>
                <Input type="password" name="newPassword" placeholder={t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.NEW_PASSWORD_PLACEHOLDER')}/>

                <label>{t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.PASSWORD_CONFIRMATION_CAPTION')}</label>
                <Input type="password" name="newPasswordConfirm" placeholder={t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.PASSWORD_CONFIRMATION_PLACEHOLDER')}/>

                <ButtonRow>
                  <Button type="button" secondary onClick={close}>{t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.CANCEL')}</Button>
                  <Button type="submit" disabled={isSubmitting} loading={isSubmitting}>{t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.SUBMIT')}</Button>
                </ButtonRow>
              </Form>
            )}
          />
        </div>
      )}
    </FoldableContainer>
  );


  async function handleSubmit(values, formikOptions, close) {
    let result = await resetPassword(values, formikOptions);

    if (result?.success){
      await notificationService.success(t("MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.SUCCESS"));
      close();
    }

    if (result?.globalErrorMessage) {
      notificationService.error(result.globalErrorMessage);
    }
  }
}
