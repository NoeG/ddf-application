import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import {useQuery} from '@apollo/react-hooks';
import {useTranslation} from 'react-i18next';

import {EditPassword} from './ProfileSettings/EditPassword';
import {DeleteAccount} from './ProfileSettings/DeleteAccount';
import {PlaceSetting} from './ProfileSettings/PlaceSetting';
import {VocabularyDomainSetting} from './ProfileSettings/VocabularyDomainSetting';
import {formatPlace, gqlPlaceFragment} from '../../../../utilities/helpers/places';

import style from './ProfileSettings.styl';

export const gqlCurrentUserSettings = gql`
  query Me {
    me {
      id
      place {
        id
        ...PlaceFragment
      }
      preferredDomain {
        id
        prefLabel
      }
    }
  }

  ${gqlPlaceFragment}
`;

export function ProfileSettings(props) {
  const {t} = useTranslation();
  const {loading, error, data: {me: currentUser = {}} = {}} = useQuery(gqlCurrentUserSettings, {fetchPolicy: 'cache-and-network'});
  
  return (
    <React.Fragment>
      <PlaceSetting 
        displayValue={formatPlace(currentUser.place, ['name', 'state', 'country'])}
        inputValue={currentUser.place?.name}
        idValue={currentUser.place?.id}
        meId={currentUser.id}
      />
      <hr/>
      <VocabularyDomainSetting 
        displayValue={currentUser.preferredDomain?.prefLabel}
        idValue={currentUser.preferredDomain?.id}
        meId={currentUser.id}
      />
      <hr/>
      <EditPassword/>
      <hr/>
      <DeleteAccount/>
    </React.Fragment>
  );
}
