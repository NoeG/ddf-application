import React, {useState} from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {Link, useHistory} from 'react-router-dom';

import {FormContainer} from '../../../widgets/forms/FormContainer';
import {Button} from '../../../widgets/forms/Button';
import {FormBottom} from '../../../widgets/forms/FormBottom';
import {ButtonRow} from '../../../widgets/forms/ButtonRow';
import {ROUTES} from '../../../../routes';
import {getUserAuthenticationService} from '../../../../services/UserAuthenticationService';
import {useBrowsingHistoryHelperService} from '../../../../services/BrowsingHistoryHelperService';
import {formatPlace} from '../../../../utilities/helpers/places';
import {useMediaQueries, Desktop} from '../../../../layouts/MediaQueries';

import style from './Profile.styl';

export function Profile(props) {
  /* Services DI */
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();

  /* Hooks */
  const {t} = useTranslation();
  const history = useHistory();
  const {user} = userAuthenticationService.useLoggedUser();
  const {logout} = userAuthenticationService.useLogout();
  const {isDesktop} = useMediaQueries();
  const {browsingHistoryHelperService} = useBrowsingHistoryHelperService();
  const [logoutIsPending, setLogoutIsPending] = useState(false);

  const ButtonContainer = isDesktop ? ButtonRow : React.Fragment;

  return (
    <React.Fragment>
      <div className={style.topCaption}>{t('MY_ACCOUNT.PROFILE.INDEX.THIS_IS_YOUR_PROFILE')}</div>
      <div className={style.profileSummary}>
        <p>{user.nickName}</p>
        <p>{user.email}</p>
        <p>{formatPlace(user.place, ['name', 'state', 'country'])}</p>
      </div>
      <FormContainer>
        <FormBottom>
          <ButtonContainer>
            <Button type="button" as={Link} to={ROUTES.MY_ACCOUNT_PROFILE_EDIT}>{t('MY_ACCOUNT.PROFILE.INDEX.EDIT_PROFILE')}</Button>
            <Button type="button" as={Link} to={ROUTES.MY_ACCOUNT_PROFILE_SETTINGS}>{t('MY_ACCOUNT.PROFILE.INDEX.EDIT_SETTINGS')}</Button>
            <Desktop>
              <Button 
                type="button" 
                onClick={handleLogout} 
                disabled={logoutIsPending} 
                loading={logoutIsPending}
              >{t('MY_ACCOUNT.PROFILE.INDEX.LOGOUT')}</Button>
            </Desktop>
          </ButtonContainer>
        </FormBottom>
      </FormContainer>
    </React.Fragment>
  );

  async function handleLogout() {
    setLogoutIsPending(true);
    let res = await logout();
    setLogoutIsPending(false);
    if (res.success) {
      let route = browsingHistoryHelperService.getAfterLogoutLocation({lookAtPreviousPage: true});
      history.push(route ? route.location : '/');
    }
    if (res.globalErrorMessage) {
      notificationService.error(res.globalErrorMessage);
    }
  }
}
