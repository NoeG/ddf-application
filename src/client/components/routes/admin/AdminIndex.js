import React, {useEffect} from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {useHistory, Link} from 'react-router-dom';
import {Helmet} from 'react-helmet-async';

import {ROUTES} from '../../../routes';
import {FormContainer} from '../../widgets/forms/FormContainer';
import {Button} from '../../widgets/forms/Button';
import {SimpleModalPage} from '../../../layouts/responsive/SimpleModalPage';
import {getUserAuthenticationService} from '../../../services/UserAuthenticationService';
import {notificationService as appNotificationService} from '../../../services/NotificationService';


export function AdminIndex(props) {
  const {t} = useTranslation();
  const history = useHistory();
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const notificationService = props.notificationService || appNotificationService;
  const {user, isLogged} = userAuthenticationService.useLoggedUser();

  useEffect(() => {
    (async () => {
      if (isLogged && !(user.userAccount.isOperator || user.userAccount.isAdmin)) {
        await notificationService.error("Seul les administrateurs peuvent accéder à ce contenu");
        history.push('/');
      }
    })();
  }, [user]);
  
  return (
    <React.Fragment>
      <Helmet>
        <title>{t('DOCUMENT_TITLES.ADMIN_INDEX')}</title>
      </Helmet>
      <SimpleModalPage
        title={t('ADMIN.INDEX.TITLE')}
        content={renderContent()}
        color="yellow"
      />
    </React.Fragment>
  );

  function renderContent() {
    return (
      <FormContainer>
        <If condition={user?.userAccount?.isOperator || user?.userAccount?.isAdmin}>
          <Button type="button" as={Link} to={ROUTES.CONTRIBUTIONS_REVIEW}>{t('MOBILE_MENU.OPERATORS.CONTRIBUTIONS')}</Button>
        </If>
        <If condition={user?.userAccount?.isAdmin}>
          <Button type="button" as={Link} to={ROUTES.ADMIN_USERS}>{t('MOBILE_MENU.ADMIN.USERS')}</Button>
        </If>
      </FormContainer>
    );
  }
}
