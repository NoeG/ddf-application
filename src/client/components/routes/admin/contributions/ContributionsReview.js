import React from 'react';
import dayjs from 'dayjs';
import {useTranslation} from 'react-i18next';
import {Helmet} from 'react-helmet-async';

import {SimpleModalPage} from '../../../../layouts/responsive/SimpleModalPage';
import {ContributionsList} from '../../../contribution/ContributionsList';

import style from "./ContributionsReview.styl";

/**
 *
 * @param {int} [pageSize] - Controls how many results are displayed
 * @return {*}
 * @constructor
 */
export function ContributionsReview({pageSize} = {}) {
  const {t} = useTranslation();

  return (
    <React.Fragment>
      <Helmet>
        <title>{t('DOCUMENT_TITLES.ADMIN_CONTRIBUTIONS')}</title>
      </Helmet>
      <SimpleModalPage
        title="Les apports récents"
        hideContributionFooter
        color="pink"
        content={(
          <React.Fragment>
            <div className={style.description}>
              {"Voici les informations ajoutées récemment dans le Dictionnaire des francophones :"}
            </div>
            <ContributionsList 
              pageSize={pageSize} 
              enableFiltering
              filtersInitialValues={{
                processedStatus: 'unprocessed'
              }}
            />
          </React.Fragment>
        )}
      />
    </React.Fragment>
  );
}
