import React from 'react';
import {gql} from 'apollo-boost';
import {useQuery} from '@apollo/react-hooks';
import {Helmet} from 'react-helmet-async';
import {useTranslation} from 'react-i18next';

import {SimpleModalPage} from '../../../../layouts/responsive/SimpleModalPage';
import {Col, Row, Table} from '../../../widgets/Table';
import {UserGroups} from './UserGroups';
import {UserActions} from './UserActions';


const gqlUsersQuery = gql`
  query UsersEdit {
    userAccounts{
      edges{
        node{
          id
          username
          userId
          isDisabled
          isUnregistered
          person{
            nickName
          }
          userGroups{
            edges{
              node{
                id
                label
              }
            }
          }
        }
      }
    }

    userGroups{
      edges{
        node{
          id
          label
        }
      }
    }
  }
`;

export function Users(props) {
  const {data} = useQuery(gqlUsersQuery, {fetchPolicy: 'cache-and-network'});
  const {t} = useTranslation();

  return (
    <React.Fragment>
      <Helmet>
        <title>{t('DOCUMENT_TITLES.ADMIN_USERS')}</title>
      </Helmet>
    <SimpleModalPage
      title="Utilisateurs"
      content={renderContent()}
      color="pink"/>
  </React.Fragment>
  );

  function renderContent() {
    return (
      <React.Fragment>
        <If condition={data}>
          <Table>
            <Row heading>
              <Col>
                Email
              </Col>
              <Col>
                Surnom
              </Col>
              <Col>
                Groupes
              </Col>
              <Col>
                Actions
              </Col>
            </Row>
            {data.userAccounts.edges.map(({node: userAccount}, indexAccount) => (
              <Row key={indexAccount}>
                <Col>
                  {userAccount.username}
                </Col>
                <Col>
                  {userAccount.person?.nickName}
                </Col>
                <Col>
                  <If condition={!userAccount.isUnregistered}>
                    <UserGroups userAccount={userAccount} userGroups={data?.userGroups}/>
                  </If>
                </Col>
                <Col>
                  <UserActions userAccount={userAccount}/>
                </Col>
              </Row>
            ))}
          </Table>
        </If>
      </React.Fragment>
    );
  }
}
