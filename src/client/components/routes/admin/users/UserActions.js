/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from "react";
import style from "./UserActions.styl";
import classnames from "classnames";
import {useTranslation} from "react-i18next";
import {gql} from "apollo-boost";
import {useMutation} from "@apollo/react-hooks";
import {notificationService} from '../../../../services/NotificationService';
import {GraphQLErrorHandler} from "../../../../services/GraphQLErrorHandler";


const gqlUnregisterUserAccountMutation = gql`
  mutation UnregisterUserAccount($userId: String){
    unregisterUserAccount(input: {userId: $userId}){
      success
    }
  }
`;

const gqlDisableUserAccountMutation = gql`
  mutation DisableUserAccount($userId: String){
    disableUserAccount(input: {userId: $userId}){
      success
    }
  }
`;

const gqlEnableUserAccountMutation = gql`
  mutation EnableUserAccount($userId: String){
    enableUserAccount(input: {userId: $userId}){
      success
    }
  }
`;

export function UserActions({userAccount}) {
  const {t} = useTranslation();
  const onMutationError = async (error) => {
    let {globalErrorMessage} = GraphQLErrorHandler(error, {t});
    await notificationService.error(globalErrorMessage);
  };

  const [unregisterUserAccountMutation] = useMutation(gqlUnregisterUserAccountMutation, {
    onCompleted: async (data) => {
      if (data?.unregisterUserAccount?.success) {
        await notificationService.success('Le compte a été supprimé avec succès');
      }
    },
    onError: onMutationError
  });
  const [disableUserAccountMutation] = useMutation(gqlDisableUserAccountMutation, {
    onCompleted: async (data) => {
      if (data?.disableUserAccount?.success) {
        await notificationService.success('Le compte a été bloqué avec succès');
      }
    },
    onError: onMutationError
  });
  const [enableUserAccountMutation] = useMutation(gqlEnableUserAccountMutation, {
    onCompleted: async (data) => {
      if (data?.enableUserAccount?.success) {
        await notificationService.success('Le compte a été débloqué avec succès');
      }
    },
    onError: onMutationError
  });

  return (
    <Choose>
      <When condition={!userAccount.isUnregistered}>
        <Choose>
          <When condition={!userAccount.isDisabled}>
            <button className={style.actionButton} onClick={handleDisableAccount}>
              {t("ADMIN.USERS.ACTIONS.DISABLE_ACCOUNT")}
            </button>
          </When>
          <Otherwise>
            <button className={classnames(style.grant, style.actionButton)} onClick={handleEnableAccount}>
              {t("ADMIN.USERS.ACTIONS.ENABLE_ACCOUNT")}
            </button>
          </Otherwise>
        </Choose>

        <button className={classnames(style.danger, style.actionButton)} onClick={handleUnregisterAccount}>
          {t("ADMIN.USERS.ACTIONS.UNREGISTER_ACCOUNT")}
        </button>
      </When>
      <Otherwise>
        <span className={style.accountUnregistered}>
          {t("ADMIN.USERS.IS_UNREGISTERED")}
        </span>
      </Otherwise>
    </Choose>
  );

  async function handleUnregisterAccount() {
    let {confirmationResult} = await notificationService.info(t('ADMIN.USERS.VALIDATION.UNREGISTER_ACCOUNT'), {confirm: true});
    if (confirmationResult === 'yes') {
      // TODO: put here a validation modal
      await unregisterUserAccountMutation({
        variables: {userId: userAccount.userId},
        update: (cache) => updateUserAccountCache({userAccount, cache, data: {isUnregistered: true}})
      })
    }
  }

  async function handleDisableAccount() {
    await disableUserAccountMutation({
      variables: {userId: userAccount.userId},
      update: (cache) => updateUserAccountCache({userAccount, cache, data: {isDisabled: true}}),
    });
  }

  async function handleEnableAccount() {
    await enableUserAccountMutation({
      variables: {userId: userAccount.userId},
      update: (cache) => updateUserAccountCache({userAccount, cache, data: {isDisabled: false}})
    });
  }

  function updateUserAccountCache({userAccount, cache, data}) {
    cache.writeFragment({
      id: `UserAccount:${userAccount.id}`,
      fragment: gql`
        fragment userAccount on UserAccount {
          isDisabled
          isUnregistered
        }
      `,
      data: {
        __typename: "UserAccount",
        ...data
      }
    });
  }
}