/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from "react";
import style from "./UserGroups.styl";
import {UsersGroupTag} from "./UserGroupTag";
import {useTranslation} from "react-i18next";
import {useMutation} from "@apollo/react-hooks";
import {gql} from "apollo-boost";
import {notificationService} from "../../../../services/NotificationService";
import {GraphQLErrorHandler} from "../../../../services/GraphQLErrorHandler";

const gqlUpdateUserAccountMutation = gql`
  mutation UpdateUserAccount($userAccountId: ID! $userAccountInput: UserAccountInput!){
    updateUserAccount(input: {objectId: $userAccountId, objectInput: $userAccountInput}){
      updatedObject{
        id
        ... on UserAccount{
          userGroups{
            edges{
              node{
                id
                label
              }
            }
          }
        }
      }
    }
  }
`;

const gqlRemoveUserGroupMutation = gql`
  mutation RemoveUserGroup($userAccountId: ID! $userGroupId: ID!){
    removeEntityLink(input: {sourceEntityId: $userAccountId, targetEntityId: $userGroupId, linkName: "hasUserGroup"}){
      sourceEntity{
        id
        ... on UserAccount{
          userGroups{
            edges{
              node{
                id
                label
              }
            }
          }
        }
      }
    }
  }
`;

export function UserGroups({userAccount, userGroups}) {
  const {t} = useTranslation();
  const onMutationError = async (error) => {
    let {globalErrorMessage} = GraphQLErrorHandler(error, {t});
    await notificationService.error(globalErrorMessage);
  };

  const [addToUserGroup] = useMutation(gqlUpdateUserAccountMutation, {
    onCompleted: async (data) => {
      if (data?.updateUserAccount) {
        await notificationService.success('Groupe ajouté avec succès');
      }
    },
    onError: onMutationError
  });

  const [removeFromUserGroup] = useMutation(gqlRemoveUserGroupMutation, {
    onCompleted: async (data) => {
      if (data?.removeEntityLink) {
        await notificationService.success('Groupe retiré avec succès');
      }
    },
    onError: onMutationError
  });

  return (
    <>
      <select
        name="color"
        onChange={async (e) => {
          await addUserAccountToGroup({userAccount, userGroupId: e.target.value});
        }}
        className={style.groupSelect}
      >
        <option value="">Sélectionner un groupe</option>
        {userGroups.edges
          .filter(({node: userGroup}) => {
            return !userAccount.userGroups.edges.find(({node: {id}}) => {
              return id === userGroup.id
            })
          })
          .map(({node: userGroup}) => {
            let label = userGroup.label || userGroup.id;
            return <option key={userGroup.id} value={userGroup.id} label={label}>{label}</option>;
          })}
      </select>
      {userAccount.userGroups.edges.map(({node: userGroup}, indexGroup) => (
        <UsersGroupTag key={indexGroup} name={userGroup.label || userGroup.id} onRemove={() => removeUserAccountFromGroup({
          userAccount,
          userGroup
        })}/>
      ))}
    </>
  );

  async function addUserAccountToGroup({userAccount, userGroupId}) {
    let result = await addToUserGroup({
      variables: {
        userAccountId: userAccount.id,
        userAccountInput: {
          userGroupInputs: [{
            id: userGroupId
          }]
        }
      }
    })
  }

  async function removeUserAccountFromGroup({userAccount, userGroup}) {
    let result = await removeFromUserGroup({
      variables: {
        userAccountId: userAccount.id,
        userGroupId: userGroup.id
      }
    })
  }
}
