/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React                        from 'react';
import {withRouter, Switch, Route}  from 'react-router-dom';
import {formatRoute}                from 'react-router-named-routes';
import get                          from 'lodash/get';
import classNames                   from 'classnames';
import {withTranslation} from 'react-i18next';


import {ROUTES} from '../../routes';
import {FormLayout} from '../Form/FormLayout';
import {FormLexicalSenseList} from '../Form/FormLexicalSenseList';
import {FormTopPosts} from '../Form/FormTopPosts';
import {FormTopPostsDesktop} from '../Form/FormTopPostsDesktop';
import {FormErrorHandler} from '../Form/FormErrorHandler';
import {LexicalSense} from './LexicalSense';
import {ResponsiveGrid} from '../../layouts/responsive/ResponsiveGrid';
import {DesktopSubHeader} from '../../layouts/desktop/DesktopSubHeader';
import {getGeolocService} from '../../services/GeolocService';

import style from './Form.styl';

@withRouter
@withTranslation()
export class Form extends React.Component {
  constructor(props) {
    super(props);
    this.geolocService = props.geolocService || getGeolocService();
    this.state = {
      currentForm: get(this.props, 'match.params.formQuery')
    }
    this.renderFormSearch = this.renderFormSearch.bind(this);
  }

  componentDidMount() {
    this._cpSub = this.geolocService.currentPlace.subscribe(currentPlace => this.setState({currentPlace}));
  }

  componentWillUnmount() {
    this._cpSub.unsubscribe();
  }


  componentDidUpdate(prevProps) {
    let prevForm = get(prevProps, 'match.params.formQuery');
    let newForm = get(this.props, 'match.params.formQuery');
    if (prevForm !== newForm) {
      this.setState({
        currentForm: newForm
      });
    }
  }


  render() {
    const {t, loading, match} = this.props;

    return (
      <FormLayout
        currentForm={this.state.currentForm}
      >
        <FormErrorHandler>
          <div className={style.topPostsSection}>
            <FormTopPosts 
              key={this.state.currentForm /* Forces to mount a new component when the form changes, avoid unwanted side effets */}
              formQuery={this.state.currentForm} 
            /> 
          </div>

          <Switch>
            <Route path={ROUTES.FORM_LEXICAL_SENSE} component={LexicalSense}/>
            <Route render={this.renderFormSearch} />
          </Switch>
        </FormErrorHandler>
      </FormLayout>
    );
  }

  renderFormSearch() {
    return (
      <ResponsiveGrid
        desktopSideColumn={this.renderDesktopSideColumn()}
        desktopFirstSection={this.renderDesktopFirstSection()}
      >
        <FormLexicalSenseList
          key={this.state.currentForm /* Forces to mount a new component when the form changes, avoid unwanted side effets */}
          formQuery={this.state.currentForm}
          currentPlace={this.state.currentPlace}
        />
      </ResponsiveGrid>
    )
  }

  renderDesktopSideColumn() {
    return (
      <div className={style.desktopSideColumn}>
        <FormTopPostsDesktop formQuery={this.state.currentForm} />
      </div>
    );
  }

  renderDesktopFirstSection() {
    const {t} = this.props;
    return (
      <DesktopSubHeader primaryTitle={<>
          <span>{t('FORM.DESKTOP.RESEARCHED_FORM')}</span>
          <span className={style.desktopSubHeaderFormQuery}>{this.state.currentForm}</span>
        </>}
      />
    );
  }
}
