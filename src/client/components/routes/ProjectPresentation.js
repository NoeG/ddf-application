import React from 'react';
import {withTranslation} from 'react-i18next';

import {ProcessedMarkdownWrapper} from '../../utilities/ProcessedMarkdownWrapper';
import {InformationPage} from '../../layouts/responsive/InformationPage';
import content from '../../assets/markdown/project_presentation.md';

@withTranslation()
export class ProjectPresentation extends React.Component {
  render() {
    const {t} = this.props;
    return <InformationPage 
      title={t('PROJECT_PRESENTATION_PAGE_TITLE')}
      content={<ProcessedMarkdownWrapper content={content}/>}
    />;
  }
}

