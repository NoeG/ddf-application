/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React, {useState} from 'react';
import classNames from 'classnames';
import upperFirst from 'lodash/upperFirst';
import {useTranslation} from 'react-i18next';
import {useQuery} from '@apollo/react-hooks';
import {gql} from 'apollo-boost';
import {Link, Route, Switch, useParams, useHistory} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {ROUTES} from '../../routes';


import {Helmet} from 'react-helmet-async';

import {
  getLexicalEntryPartOfSpeechList,
  gqlLexicalEntryPartOfSpeechListFragment
} from '../../utilities/helpers/lexicalEntryPartOfSpeechList';
import {getLexicalSenseTags, gqlLexicalSenseTagsFragment} from '../../utilities/helpers/lexicalSenseTags';
import {
  getLocalizedEntityCountryData,
  gqlFormCountriesFragment,
  gqlLexicalSenseCountriesFragment
} from '../../utilities/helpers/countries';
import {
  getLexicalEntryLexicographicSourceConciseLabel,
  gqlLexicalEntryLexicographicSourceFragment
} from '../../utilities/helpers/lexicalEntryLexicographicSource';
import {gqlLexicalSenseSemanticRelationFragment} from '../../utilities/helpers/lexicalSenseSemanticRelationList';
import {
  getLexicalSenseTopPostAboutSense,
  gqlLexicalSenseTopPostAboutSenseFragment
} from '../../utilities/helpers/lexicalSenseTopPostAboutSense';
import {SanitizedHtml} from '../../utilities/SanitizedHtml';

import {gqlLexicalSenseActionsFragment, LexicalSenseActions} from '../LexicalSense/LexicalSenseActions';
import {LexicalSenseSemanticRelationList} from '../LexicalSense/LexicalSenseSemanticRelationList';
import {LexicalSenseDesktopSideColumn} from '../LexicalSense/LexicalSenseDesktopSideColumn';
import {LexicalSenseLoader} from '../LexicalSense/LexicalSenseLoader';
import {LexicalSenseSources, LexicalSenseSourcesMobilePage} from '../LexicalSense/LexicalSenseSources';
import {LexicalSenseCountries, LexicalSenseCountriesMobilePage} from '../LexicalSense/LexicalSenseCountries';
import {UsageExamples} from '../LexicalSense/UsageExamples';

import {Desktop, Mobile} from '../../layouts/MediaQueries';
import {FullScreenOverlay} from '../../layouts/mobile/FullScreenOverlay';
import {DesktopSubHeader} from '../../layouts/desktop/DesktopSubHeader';
import {ResponsiveGrid} from '../../layouts/responsive/ResponsiveGrid';

import {BackArrowLink} from '../widgets/BackArrowLink';

import checkmarkPurple from '../../assets/images/checkmark_purple.svg';

import style from './LexicalSense.styl'

export let LexicalSenseQuery = gql`
  query LexicalSense_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId){
      id
      definition
      validationRatingsCount
      canUpdate
      
      usageExamples{
        edges{
          node{
            id
            bibliographicalCitation
            value
          }
        }
      }

      lexicalEntry{
        id
        canonicalForm{
          id
          writtenRep

          ...FormCountriesFragment
        }

        ...LexicalEntryPartOfSpeechListFragment
      }

      lexicographicResource{
        id
        prefLabel
        altLabel
      }
      
      ...LexicalSenseCountriesFragment
      ...LexicalSenseTagsFragment
      ...LexicalSenseTopPostAboutSenseFragment
      ...LexicalSenseActionsFragment
    }
  }

  ${gqlLexicalSenseCountriesFragment}
  ${gqlLexicalSenseTagsFragment}
  ${gqlLexicalEntryPartOfSpeechListFragment}
  ${gqlFormCountriesFragment}
  ${gqlLexicalSenseTopPostAboutSenseFragment}
  ${gqlLexicalSenseActionsFragment}
`;

export function LexicalSense(props = {}) {
  let params = useParams();
  let history = useHistory();

  let lexicalSenseId = decodeURIComponent(params.lexicalSenseId);
  let formQuery      = decodeURIComponent(params.formQuery);

  let {t} = useTranslation();
  let {loading, error, data} = useQuery(LexicalSenseQuery, {
    variables: {
      lexicalSenseId
    },
    fetchPolicy: 'cache-and-network'
  });

  let lexicalSense = data?.lexicalSense;

  return (
    <React.Fragment>
      <Helmet>
        <title>{t('DOCUMENT_TITLES.LEXICAL_SENSE', {formQuery})}</title>
      </Helmet>
      <ResponsiveGrid
        desktopFirstSection={renderDesktopTitle()}
        desktopSideColumn={renderDesktopSideColumn()}
      >
        <If condition={loading && !lexicalSense}>
          <LexicalSenseLoader />
        </If>
        <If condition={!loading && !lexicalSense}>
          <div className={classNames([style.section, style.definitionSection])}>
            {t('LEXICAL_SENSE_DETAIL.NO_RESULTS')}
          </div>
        </If>
        <If condition={!loading && lexicalSense}>
          <Mobile>
            {renderLexicalSenseDetails()}
            <Route path={ROUTES.FORM_LEXICAL_SENSE_SOURCES} render={renderMobileSourcesOverlayPage}/>
            <Route path={ROUTES.FORM_LEXICAL_SENSE_COUNTRIES} render={renderMobileCountriesOverlayPage}/>
          </Mobile>
          <Desktop>
            <Switch>
              <Route path={ROUTES.FORM_LEXICAL_SENSE_SOURCES} render={renderDesktopSourcesPage}/>
              <Route path={ROUTES.FORM_LEXICAL_SENSE_COUNTRIES} render={renderDesktopCountriesPage}/>
              <Route path={ROUTES.FORM_LEXICAL_SENSE} render={renderLexicalSenseDetails}/>
            </Switch>
          </Desktop>
        </If>
      </ResponsiveGrid>
    </React.Fragment>
  );


  function renderLexicalSenseDetails() {
    const {lexicalEntry, canUpdate} = lexicalSense;
    let countryData = getLocalizedEntityCountryData(lexicalSense);
    let tags = getLexicalSenseTags(lexicalSense);
    let partOfSpeechList = getLexicalEntryPartOfSpeechList(lexicalEntry);
    let topPostAboutSense = getLexicalSenseTopPostAboutSense(lexicalSense);
    let editSenseRoute = formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT, {
      formQuery, 
      lexicalSenseId
    });

    return (
      <>
        <Mobile>
          <BackArrowLink
            to={formatRoute(ROUTES.FORM_SEARCH, {formQuery})}
            text={t('LEXICAL_SENSE_DETAIL.BACK_TO_FORM_LINK')}
          />
        </Mobile>
        <div className={classNames([style.section, style.definitionSection])}>
          <div className={classNames([style.countriesRow, style[countryData.color]])}>
            <Link
              className={style.countries}
              to={formatRoute(ROUTES.FORM_LEXICAL_SENSE_COUNTRIES, {formQuery, lexicalSenseId})}
            >
              <Choose>
                <When condition={countryData.names.length > 0}>
                  {countryData.names.join(", ")}
                </When>
                <Otherwise>
                  {t('GEOGRAPHICAL_DETAILS.NO_COUNTRY_LABEL')}
                </Otherwise>
              </Choose>
            </Link>
          </div>

          {renderConditionnalCommaList({
            collection: partOfSpeechList,
            className: style.pos
          })}

          {renderConditionnalCommaList({
            collection: tags,
            className: style.tags
          })}


          <SanitizedHtml className={style.description} html={lexicalSense.definition}/>

          <UsageExamples usageExamples={lexicalSense.usageExamples.edges.map(e => e.node)}/>
        </div>

        <If condition={canUpdate}>
          <div className={style.section}>
            <Link className={style.editLink} to={editSenseRoute}>
              {t('LEXICAL_SENSE_DETAIL.EDIT_THIS_DEFINITION')}
            </Link>
          </div>
        </If>

        <LexicalSenseSemanticRelationList lexicalSenseId={lexicalSenseId} theme={style}/>


        <div className={classNames([style.section, style.actionsSection])}>
          <div className={style.actionsRow}>
            <div className={classNames([style.likes, style.action])}>
              {lexicalSense?.validationRatingsCount}
              <img className={style.icon} src={checkmarkPurple}/>
            </div>

            <Link
              to={formatRoute(ROUTES.FORM_LEXICAL_SENSE_SOURCES, {formQuery, lexicalSenseId: lexicalSense.id})}
              className={classNames([style.sources, style.action])}
            >
              {lexicalSense?.lexicographicResource?.altLabel}
            </Link>

            <div className={style.filler}/>

            <LexicalSenseActions lexicalSense={lexicalSense} formQuery={formQuery} onRemoveSuccess={navigateBack}/>
          </div>

        </div>

        <If condition={topPostAboutSense}>
          <div className={style.aboutSense} data-testid="about-sense">
            <h5>{t('LEXICAL_SENSE_DETAIL.ABOUT_SENSE_TITLE')}&nbsp;:&nbsp;</h5>
            <div className={style.text}> {topPostAboutSense.content} </div>
          </div>
        </If>
      </>

    );
  }

  function renderDesktopTitle() {
    let backLinkToSense = {
      backLinkTo: formatRoute(ROUTES.FORM_LEXICAL_SENSE, {lexicalSenseId, formQuery}),
      backLinkText: t('LEXICAL_SENSE_DETAIL.BACK_TO_SENSE_LINK')
    };
    let backLinkToForm = {
      backLinkTo: formatRoute(ROUTES.FORM_SEARCH, {formQuery}),
      backLinkText: t('LEXICAL_SENSE_DETAIL.BACK_TO_FORM_LINK')
    };
    let primaryTitle = (
      <>
        <span>{t('FORM.DESKTOP.RESEARCHED_FORM')}</span>
        <span className={style.desktopSubHeaderFormQuery}>{formQuery}</span>
      </>
    );

    return (
      <Switch>
        <Route path={ROUTES.FORM_LEXICAL_SENSE_SOURCES} render={() =>
          <DesktopSubHeader
            primaryTitle={primaryTitle}
            secondaryTitle={t('LEXICAL_SENSE_DETAIL.DESKTOP.SOURCES')}
            {...backLinkToSense}
          />
        }/>
        <Route path={ROUTES.FORM_LEXICAL_SENSE_COUNTRIES} render={() =>
          <DesktopSubHeader
            primaryTitle={primaryTitle}
            secondaryTitle={t('LEXICAL_SENSE_DETAIL.DESKTOP.GEOGRAPHICAL_DETAILS')}
            {...backLinkToSense}
          />
        }/>
        <Route path={ROUTES.FORM_LEXICAL_SENSE} render={() =>
          <DesktopSubHeader
            primaryTitle={primaryTitle}
            secondaryTitle={t('LEXICAL_SENSE_DETAIL.DESKTOP.DEFINITION')}
            {...backLinkToForm}
          />
        }/>
      </Switch>
    );
  }

  function renderDesktopSideColumn() {
    if (loading || !lexicalSense) {
      return <div/>;
    } else {
      return <LexicalSenseDesktopSideColumn
        lexicalSense={lexicalSense}
        formQuery={formQuery}
        onRemoveSuccess={navigateBack}
      />;
    }
  }

  function renderConditionnalCommaList({collection, className}) {
    if (collection.length === 0) return null;
    
    return (
      <div className={className}>
        {
          collection.map((item, index) => {
            if (index === 0) {
              item = upperFirst(item);
            }
            return (
              <React.Fragment key={index}>
                <span>{item}</span>
                <If condition={index < collection.length - 1}>, </If>
              </React.Fragment>
            )
          })
        }
      </div>
    );
  }

  function renderMobileSourcesOverlayPage() {
    return (
      <FullScreenOverlay>
        <LexicalSenseSourcesMobilePage
          lexicalSenseId={lexicalSenseId}
          formQuery={formQuery}
          lexicographicResource={lexicalSense?.lexicographicResource}
        />
      </FullScreenOverlay>
    );
  }

  function renderMobileCountriesOverlayPage() {
    let places = (lexicalSense?.places?.edges || []).map(({node}) => node);
    return (
      <FullScreenOverlay>
        <LexicalSenseCountriesMobilePage
          lexicalSenseId={lexicalSenseId}
          formQuery={formQuery}
          places={places}
        />
      </FullScreenOverlay>
    );
  }

  function renderDesktopCountriesPage() {
    let places = (lexicalSense?.places?.edges || []).map(({node}) => node);
    return (
      <div className={style.section}>
        <LexicalSenseCountries
          lexicalSenseId={lexicalSenseId}
          formQuery={formQuery}
          places={places}
        />
      </div>
    );
  }

  function renderDesktopSourcesPage() {
    return (
      <div className={style.section}>
        <LexicalSenseSources
          lexicalSenseId={lexicalSenseId}
          formQuery={formQuery}
          lexicographicResource={lexicalSense?.lexicographicResource}
        />
      </div>
    );
  }

  function navigateBack() {
    history.push(formatRoute(ROUTES.FORM_SEARCH, {formQuery}));
  }
}



