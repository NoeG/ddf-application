import React                          from 'react';
import {render, cleanup, getNodeText} from '@testing-library/react';
import waait                          from 'waait';

import {LexicalSense} from '../LexicalSense';
import {renderWithMocks} from '../../../../../jest/utilities/renderWithMocks';
import {setScreenSize} from '../../../../../jest/utilities/setScreenSize';
import {ROUTES}  from '../../../routes';
import {lexicalSenseQuery_withTopPostAboutSense, lexicalSenseQuery_withoutTopPostAboutSense, FormTopPostsQuery_Mock} from './__gql_mocks__/LexicalSense.gqlMocks';
import {lexicalSenseSemanticRelationListQuery_empty} from "./__gql_mocks__/LexicalSenseSemanticRelationList.gqlMocks";

afterEach(() => {
  cleanup();
});

jest.mock('../../../services/UserAuthenticationService', () => {
  return jest.requireActual('../../../../../jest/utilities/servicesMocks/UserAuthenticationServiceMock').generateModuleMock();
});


describe("discussion on usage", () => {
  it("show the top post", async () => {
    setScreenSize('mobile');
    const {container, findByTestId} = renderWithMocks({
      locationPath: "/form/affairé/sense/inv%2Flexical-sense%2F1234",
      gqlMocks: [lexicalSenseQuery_withTopPostAboutSense, FormTopPostsQuery_Mock, lexicalSenseSemanticRelationListQuery_empty],
      element: <LexicalSense />,
      routePath: ROUTES.FORM_LEXICAL_SENSE
    });

    let topPostElement = await findByTestId("about-sense");
    expect(getNodeText(topPostElement.querySelector('.text'))).toEqual(" Affairé signifie aussi, un homme accablé de dettes, dont les affaires sont embarassées... ");
  });

  it("hide the discussion box if no top post to display", async () => {
    setScreenSize('mobile');
    const {container, queryByTestId, findByText} = renderWithMocks({
      locationPath: "/form/affairé/sense/inv%2Flexical-sense%2F1234",
      gqlMocks: [lexicalSenseQuery_withoutTopPostAboutSense, FormTopPostsQuery_Mock, lexicalSenseSemanticRelationListQuery_empty],
      element: <LexicalSense />,
      routePath: ROUTES.FORM_LEXICAL_SENSE
    });

    /* Just wait for the page to be rendered */
    let anElement = await findByText("Affairiste"); 

    /* Then check for absence of top post element */
    expect(queryByTestId('about-sense')).toBeNull();
  });
});

