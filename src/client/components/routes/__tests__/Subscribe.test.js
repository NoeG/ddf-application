import React from 'react';
import {render, cleanup, getNodeText, fireEvent} from '@testing-library/react';
import {MemoryRouter, Route}          from 'react-router-dom'
import {act} from 'react-dom/test-utils';
import waait from 'waait';
import {GraphQLErrorResponses} from '@mnemotix/synaptix.js/testing';

import {Subscribe} from '../Subscribe';
import {gqlRegisterUserAccountMutation} from '../../../services/UserAuthenticationService';
import {renderWithMocks} from '../../../../../jest/utilities/renderWithMocks';
import {setScreenSize} from '../../../../../jest/utilities/setScreenSize';
import {NotificationServiceMock} from '../../../../../jest/utilities/servicesMocks/NotificationServiceMock';
import {ROUTES}  from '../../../routes';


let notificationServiceMock;

/* Disable console.warn for the tests */
let consolewarn;
beforeAll(() => {
  consolewarn = console.warn;
  console.warn = jest.fn(); 
});
afterAll(() => {
  console.warn = consolewarn;
});


/* Setup notificationServiceMock */
beforeEach(() => {
  notificationServiceMock = new NotificationServiceMock();
});

/* Cleanup DOM */
afterEach(() => {
  cleanup();
});

/** 
 *
 * GraphQL mutations scenario to test : 
 *
 * - response with formValidationError
 * - valid response with error property
 * - error when username already exists
 * - unexpected server error
 *
 *
 * - Errors :
 *   formValidationError :
 *     one graphQLError 
 *     graphQLErrors of length 2
 *   Unexpected error (networkError)
 *   Offline error
 */

test('form input error', async () => {
  const mocks = [{
    request: {
      query: gqlRegisterUserAccountMutation,
      variables: { 
        username: 'user@test.com',
        email: 'user@test.com',
        nickName: 'user',
        password: 'pass',
        gcu: true
      },
    },
    result: () => {
      let errorMock = GraphQLErrorResponses.FormValidationErrorResponse;
      /* We tweak a bit the error provided by synaptix.js, because the message IS_REQUIRED isn't handled in our case */
      errorMock.errors[0].formInputErrors[0].field = "input.email";
      errorMock.errors[0].formInputErrors[0].errors[0] = "EMAIL_ALREADY_REGISTERED";
      errorMock.errors[0].formInputErrors[1].errors[0] = "PASSWORD_TOO_SHORT";
      return errorMock;
    }
  }];
  
  const {container, findByTestId, findByText, findByPlaceholderText} = renderWithMocks({
    locationPath: "/subscribe",
    gqlMocks: mocks,
    element: <Subscribe notificationService={notificationServiceMock}/>,
    routePath: ROUTES.SUBSCRIBE
  });

  let submitButton = await findByText("Valider");
  fireEvent.change(await findByPlaceholderText("Pseudonyme (ou nom) *"), {target : {value : 'user'}})
  fireEvent.change(await findByPlaceholderText("Mot de passe *"), {target : {value : 'pass'}})
  fireEvent.change(await findByPlaceholderText("Adresse de courriel *"), {target : {value : 'user@test.com'}})
  fireEvent.click(await findByText(/J'accepte/));
  fireEvent.click(submitButton);
  let emailErrorLabel = await findByTestId('email-error-msg');
  let passwordErrorLabel = await findByTestId('password-error-msg');
  expect(notificationServiceMock.error.mock.calls[0][0]).toEqual("Certains champs sont invalides");
  expect(getNodeText(emailErrorLabel)).toEqual('Cette adresse de courriel est déjà utilisée');
  expect(getNodeText(passwordErrorLabel)).toEqual('Mot de passe trop court');
});

test('network error', async() => {
  const mocks = [{
    request: {
      query: gqlRegisterUserAccountMutation,
      variables: { 
        username: 'user@test.com',
        email: 'user@test.com',
        nickName: 'user',
        password: 'pass',
        gcu: true
      },
    },
    error: {
      "name": "ServerError",
      "response": {},
      "statusCode": 400,
      "result": {
        "errors": [
          {
            "message": "MALFORMED_REQUEST",
            "statusCode": 400,
            "extraInfos": "Cannot query field \"successa\" on type \"RegisterUserAccountPayload\". Did you mean \"success\"?",
            "stack": [
              "GraphQLError: Cannot query field \"successa\" on type \"RegisterUserAccountPayload\". Did you mean \"success\"?",
              "    at Object.Field (/home/quentin/mnemotix/synaptix.js/node_modules/graphql/validation/rules/FieldsOnCorrectType.js:53:31)",
            ]
          }
        ]
      }
    }
  }];
  
  const {container, findByTestId, findByText, findByPlaceholderText} = renderWithMocks({
    locationPath: "/subscribe",
    gqlMocks: mocks,
    element: <Subscribe notificationService={notificationServiceMock}/>,
    routePath: ROUTES.SUBSCRIBE
  });

  let submitButton = await findByText("Valider");
  fireEvent.change(await findByPlaceholderText("Pseudonyme (ou nom) *"), {target : {value : 'user'}})
  fireEvent.change(await findByPlaceholderText("Mot de passe *"), {target : {value : 'pass'}})
  fireEvent.change(await findByPlaceholderText("Adresse de courriel *"), {target : {value : 'user@test.com'}})
  fireEvent.click(await findByText(/J'accepte/));
  fireEvent.click(submitButton);
  await act(async() => await waait(100));
  expect(notificationServiceMock.error.mock.calls[0][0]).toEqual("Un problème non identifié nous empêche d'effectuer cette action. Réessayez plus tard");
});
