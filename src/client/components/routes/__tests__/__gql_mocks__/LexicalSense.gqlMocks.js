import {LexicalSenseQuery, MyQuery} from '../../../routes/LexicalSense';
import {FormTopPostsQuery} from '../../../Form/FormTopPosts';

export const lexicalSenseQuery_withTopPostAboutSense = {
  request: {
    query: LexicalSenseQuery,
    variables: {
      lexicalSenseId: "inv/lexical-sense/1234"
    }
  },
  result: {
    "data": {
      "lexicalSense": {
        "id": "inv/lexical-sense/1234",
        "definition": "Affairiste",
        "usageExamples": {
          "edges": [],
          "__typename": "UsageExampleConnection"
        },
        "lexicalEntry": {
          "id": "inv:word/1234",
          "canonicalForm": {
            "id": "inv:form/1234",
            "writtenRep": "affairé",
            "places": {
              "edges": [],
              "__typename": "PlaceInterfaceConnection"
            },
            "__typename": "Form"
          },
          "partOfSpeech": {
            "id": "lexinfo:noun",
            "prefLabel": "nom",
            "__typename": "Concept"
          },
          "__typename": "Word"
        },
        "places": {
          "edges": [],
          "__typename": "PlaceInterfaceConnection"
        },
        "__typename": "LexicalSense",
        "domains": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "temporalities": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "registers": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "connotations": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "frequencies": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "textualGenres": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "sociolects": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "grammaticalConstraints": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "semanticRelations": {
          "edges": [],
          "__typename": "SemanticRelationConnection"
        },
        "senseToSenseSemanticRelations": {
          "edges": [
            {
              "node": {
                "id": "inv:73/sr/figure",
                "semanticRelationType": {
                  "id": "ddfa:sense-relation/figurativeRelation",
                  "prefLabel": "au figuré",
                  "__typename": "Concept"
                },
                "__typename": "SemanticRelation"
              },
              "__typename": "SemanticRelationEdge"
            }
          ],
          "__typename": "SemanticRelationConnection"
        },
        "topPostAboutSense": {
          "id": "test1234",
          "content": "Affairé signifie aussi, un homme accablé de dettes, dont les affaires sont embarassées...",
          "__typename": "Post"
        },
        "validationRatingsCount": 1,
        "suppressionRatingsCount": 0,
        "reportingRatingsCount": 0,
        "validationRating": null,
        "suppressionRating": null,
        "reportingRating": null,
        "canUpdate": false,
        "lexicographicResource": {
          "id": "lexicog-3",
          "prefLabel": "Dictionnaire des francophones",
          "altLabel": "Ddf",
          "__typename": "Concept"
        },
      }
    }
  }
};

export const lexicalSenseQuery_withoutTopPostAboutSense = {
  request: {
    query: LexicalSenseQuery,
    variables: {
      lexicalSenseId: "inv/lexical-sense/1234"
    }
  },
  result: {
    "data": {
      "lexicalSense": {
        "id": "inv/lexical-sense/1234",
        "definition": "Affairiste",
        "usageExamples": {
          "edges": [],
          "__typename": "UsageExampleConnection"
        },
        "lexicalEntry": {
          "id": "inv:usage-exemple/1234",
          "canonicalForm": {
            "id": "inv:usage-exemple/1234",
            "writtenRep": "affairé",
            "places": {
              "edges": [],
              "__typename": "PlaceInterfaceConnection"
            },
            "__typename": "Form"
          },
          "partOfSpeech": {
            "id": "lexinfo:noun",
            "prefLabel": "nom",
            "__typename": "Concept"
          },
          "__typename": "Word",
        },
        "places": {
          "edges": [],
          "__typename": "PlaceInterfaceConnection"
        },
        "__typename": "LexicalSense",
        "domains": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "temporalities": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "registers": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "connotations": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "frequencies": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "textualGenres": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "sociolects": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "grammaticalConstraints": {
          "edges": [],
          "__typename": "ConceptConnection"
        },
        "semanticRelations": {
          "edges": [],
          "__typename": "SemanticRelationConnection"
        },
        "senseToSenseSemanticRelations": {
          "edges": [
            {
              "node": {
                "id": "inv:73/sr/figure",
                "semanticRelationType": {
                  "id": "ddfa:sense-relation/figurativeRelation",
                  "prefLabel": "au figuré",
                  "__typename": "Concept"
                },
                "__typename": "SemanticRelation"
              },
              "__typename": "SemanticRelationEdge"
            }
          ],
          "__typename": "SemanticRelationConnection"
        },
        "topPostAboutSense": null,
        "validationRatingsCount": 1,
        "suppressionRatingsCount": 0,
        "reportingRatingsCount": 0,
        "validationRating": null,
        "suppressionRating": null,
        "reportingRating": null,
        "canUpdate": false,
        "lexicographicResource": {
          "id": "lexicog-3",
          "prefLabel": "Dictionnaire des francophones",
          "altLabel": "Ddf",
          "__typename": "Concept"
        },
      }
    }
  }
};

export const FormTopPostsQuery_Mock = {
  request: {
    query: FormTopPostsQuery,
    variables: {
      formQs: "affairé",
    }
  },
  result: {
    "data": {
      "topPostAboutEtymology": {
        "content": "Dérivé de affaire",
        "__typename": "Post"
      },
      "topPostAboutForm": {
        "content": "Semble s'être dit d'abord de domestiques, d'intendants, etc.",
        "__typename": "Post"
      }
    }
  }
};

