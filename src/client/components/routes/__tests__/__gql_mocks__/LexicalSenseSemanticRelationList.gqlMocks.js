import {LexicalSenseQuery} from "../../LexicalSense";
import {LexicalSenseSemanticRelationListQuery} from "../../../LexicalSense/LexicalSenseSemanticRelationList";

export const lexicalSenseSemanticRelationListQuery_empty = {
  request: {
    query: LexicalSenseSemanticRelationListQuery,
    variables: {
      lexicalSenseId: "inv/lexical-sense/1234"
    }
  },
  result: {
    "data": {
      "lexicalSense": {
        "id": "inv/lexical-sense/1234",
        "semanticRelations": {
          "edges": [],
          "__typename": "SemanticRelationConnection"
        },
      }
    }
  }
};