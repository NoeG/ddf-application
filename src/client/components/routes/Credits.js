import React from 'react';
import {withTranslation} from 'react-i18next';

import {ProcessedMarkdownWrapper} from '../../utilities/ProcessedMarkdownWrapper';
import {InformationPage} from '../../layouts/responsive/InformationPage';

import content from '../../assets/markdown/credits.md';
import style from './Credits.styl';

@withTranslation()
export class Credits extends React.Component {
  render() {
    const {t} = this.props;
    return <InformationPage 
      title={t('CREDITS_PAGE_TITLE')}
      content={<ProcessedMarkdownWrapper content={content}/>}
      theme={style} 
    />;
  }
}


