import React from 'react';
import {withTranslation} from 'react-i18next';

import {ProcessedMarkdownWrapper} from '../../../utilities/ProcessedMarkdownWrapper';
import {HelpPage} from '../../../layouts/responsive/HelpPage';
import content from '../../../assets/markdown/help_how_to_contribute.md';

@withTranslation()
export class HowToContribute extends React.Component {
  render() {
    const {t} = this.props;

    return (
      <HelpPage 
        title={t('HELP.HEADER')}
        subtitle="Un titre"
        helpSectionTitle={t('HELP.SECTION_LINKS.HOW_TO_CONTRIBUTE')}
        content={<ProcessedMarkdownWrapper content={content}/>}
        color="yellow"
      />
    );
  }
}

