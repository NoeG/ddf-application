import React from 'react';
import {withTranslation} from 'react-i18next';

import {ProcessedMarkdownWrapper} from '../../../utilities/ProcessedMarkdownWrapper';
import {HelpPage} from '../../../layouts/responsive/HelpPage';
import content from '../../../assets/markdown/help_how_to_search.md';

@withTranslation()
export class HowToSearch extends React.Component {
  render() {
    const {t} = this.props;

    return (
      <HelpPage 
        title={t('HELP.HEADER')}
        subtitle={t('HELP.HOW_TO_SEARCH')}
        helpSectionTitle={t('HELP.SECTION_LINKS.SEARCH_AND_USER_ACCOUNT')}
        content={<ProcessedMarkdownWrapper content={content}/>}
        color="green"
      />
    );
  }
}
