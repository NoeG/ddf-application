import React from 'react';
import {withTranslation} from 'react-i18next';

import {ProcessedMarkdownWrapper} from '../../../utilities/ProcessedMarkdownWrapper';
import {HelpPage} from '../../../layouts/responsive/HelpPage';
import content from '../../../assets/markdown/help_ddf_irl.md';

@withTranslation()
export class HelpDdfIrl extends React.Component {
  render() {
    const {t} = this.props;

    return (
      <HelpPage 
        title={t('HELP.HEADER')}
        subtitle="Un titre"
        helpSectionTitle={t('HELP.SECTION_LINKS.OFFLINE')}
        content={<ProcessedMarkdownWrapper content={content}/>}
        color="pink"
      />
    );
  }
}


