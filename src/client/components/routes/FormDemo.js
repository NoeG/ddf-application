import React from 'react';
import {Formik} from 'formik';
import * as Yup from 'yup';

import {SimpleModalPage} from '../../layouts/responsive/SimpleModalPage';
import {Input as FormikInput} from '../widgets/forms/formik/Input';
import {Input} from '../widgets/forms/Input';
import {Textarea} from '../widgets/forms/Textarea';
import {Form} from '../widgets/forms/formik/Form';
import {Button} from '../widgets/forms/Button';
import {FormBottom} from '../widgets/forms/FormBottom';
import {PlaceAutocomplete} from '../widgets/forms/formik/PlaceAutocomplete';
import {Autocomplete} from '../widgets/forms/Autocomplete';
import {AutocompleteController} from '../widgets/forms/AutocompleteController';
import {RadioButton as FormikRadioButton} from '../widgets/forms/formik/RadioButton';
import {Checkbox as FormikCheckbox} from '../widgets/forms/formik/Checkbox';

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .required('Required'),
  email: Yup.string()
    .email('Invalid email')
  //.required('Required'), /* FIXME */
});

const initialValues = {
  name: '',
  email: '',
  gender: 'm'
};


export class FormDemo extends React.Component {
  state = {
    colorTheme: null,
    currentDemo: 'form' /** can be 'form' or 'standaloneInputs' */
  }

  render() {
    return <SimpleModalPage 
      title="S'inscrire" 
      content={this.renderContent()}
    />;
  }

  colorThemeOnChange(evt) {
    this.setState({
      colorTheme: evt.target.value === "yellow" ? "yellow" : null
    });
  }

  renderDemoSwitcher() {
    return (
      <React.Fragment>
        <div>
          Color theme : 
          <input 
            type="radio" 
            name="demoThemeColor" 
            value="default" 
            checked={this.state.colorTheme === null}
            onChange={(e) => this.colorThemeOnChange(e)}
          /> Default
          <input 
            type="radio" 
            name="demoThemeColor" 
            value="yellow" 
            checked={this.state.colorTheme === "yellow"}
            onChange={(e) => this.colorThemeOnChange(e)}
          /> Yellow
        </div>
        <div>
          Demo type : 
          <input 
            type="radio" 
            name="currentDemo" 
            value="form" 
            checked={this.state.currentDemo === "form"}
            onChange={(e) => this.setState({currentDemo: e.target.value})}
          /> Form
          <input 
            type="radio" 
            name="currentDemo" 
            value="standaloneInputs" 
            checked={this.state.currentDemo === "standaloneInputs"}
            onChange={(e) => this.setState({currentDemo: e.target.value})}
          /> Standalone Inputs
        </div>
      </React.Fragment>
    );
  }

  renderForm() {
    return <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={(values, {setSubmitting}) => {
        setTimeout(() => {
          setSubmitting(false);
          console.log('submitted : ', values);
        }, 1000);
      }}
      render={({errors, status, touched, isSubmitting, values}) => (
        <Form colorTheme={this.state.colorTheme}>
          <FormikInput type="text" name="name" placeholder="Pseudonyme (ou nom)" />
          <FormikInput type="email" name="email" placeholder="Adresse de courriel" />
          <FormikInput type="password" name="password" placeholder="Mot de passe" />
          <PlaceAutocomplete name="place" placeholder="Lieu de vie" />
          <Textarea name="biography" placeholder="Champ d'activité / connaissances particulières / champ d'expertise / passion / façon de se présenter aux autres"/>
          <Textarea name="definition" placeholder="Définition" isRequired={true}/> 
          <FormikCheckbox name="gcu" label="Accepter les GCU"/>
          <h2>Genre</h2>
          <FormikRadioButton name="gender" value="f" label="Féminin"/>
          <FormikRadioButton name="gender" value="m" label="Masculin"/>
          <FormikRadioButton name="gender" value="n" label="Ne se prononce pas"/>
          <FormBottom>
            <Button type="submit" disabled={isSubmitting}>Valider</Button>
            <Button type="submit" secondary>Annuler</Button>
          </FormBottom>
          <pre>
            {JSON.stringify({values}, null, 2)}
          </pre>
        </Form>
      )}
    />;
  }

  renderStandaloneInputs() {
    let controller = new AutocompleteController();
    controller.onSuggestionsFetchRequested = () => {
      controller.updateSuggestions(['Paris', 'Londres', 'Milan']);
    }
    const {colorTheme} = this.state;
    return (
      <React.Fragment>
        <Input type="text" name="name" placeholder="Pseudonyme (ou nom)" colorTheme={colorTheme} required/>
        <Input type="password" name="password" placeholder="Mot de passe" colorTheme={colorTheme}/>
        <Textarea name="biography" placeholder="Champ d'activité / connaissances particulières / champ d'expertise / passion / façon de se présenter aux autres" />
        <Textarea name="definition" placeholder="Définition" isRequired={true} colorTheme={colorTheme}/> 
        <Autocomplete controller={controller} name="place" placeholder="Lieu de vie" colorTheme={colorTheme}/>
        <Button type="submit" colorTheme={colorTheme}>Valider</Button>
        <Button type="submit" secondary>Annuler</Button>
      </React.Fragment>
    );
  }

  renderContent() {
    return (
      <React.Fragment>
        {this.renderDemoSwitcher()}
        <If condition={this.state.currentDemo === 'form'}>{this.renderForm()}</If>
        <If condition={this.state.currentDemo === 'standaloneInputs'}>{this.renderStandaloneInputs()}</If>
      </React.Fragment>
    );
  }



}



