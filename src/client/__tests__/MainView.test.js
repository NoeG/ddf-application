import React from 'react';
import {MemoryRouter} from 'react-router-dom'
import {render} from '@testing-library/react';

import {MainView} from '../MainView';
import {renderWithMocks} from '../../../jest/utilities/renderWithMocks';

jest.mock('../components/contribution/EditLexicalSense', () => ({EditLexicalSense: () => <div>EditLexicalSense component rendered</div>}));
jest.mock('../components/routes/Form', () => ({Form: () => <div>Form component rendered</div>}));

describe('routing', () => {
  describe('interaction between FORM_LEXICAL_SENSE_EDIT and FORM_SEARCH', () => {
    test('route FORM_LEXICAL_SENSE_EDIT takes precedence over FORM_SEARCH', async () => {
      let locationPath = "/form/pomme/sense/lexicalSense123/edit";
      const {container, findByText, findByTestId, findByPlaceholderText} = render(
        <MemoryRouter initialEntries={[locationPath]}>
          <MainView/>
        </MemoryRouter>
      );

      let component = await findByText('EditLexicalSense component rendered');
      expect(component).toBeDefined();
    });

    test('route FORM_SEARCH displays correctly', async () => {
      let locationPath = "/form/pomme/sense/lexicalSense123";
      const {container, findByText, findByTestId, findByPlaceholderText} = render(
        <MemoryRouter initialEntries={[locationPath]}>
          <MainView/>
        </MemoryRouter>
      );

      let component = await findByText('Form component rendered');
      expect(component).toBeDefined();
    });
  });
});
