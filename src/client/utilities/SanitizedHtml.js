import React from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';
import sanitizeHtml from 'sanitize-html';

_sanitizedHtml.propTypes = {
  /** By default the container of the sanitized Html is a <div>, but you can make this render as another component */
  as: PropTypes.elementType,
  /** The html to sanitize then set as inner HTML */
  html: PropTypes.string,
  /** class name forwarded to the container element */
  className: PropTypes.string,
  /** A react ref object to attach to the top level element */
  forwardedRef: PropTypes.any,
  /** A list of tag names to remove from the default array  of allowed tags */
  removeAllowedTags: PropTypes.arrayOf(PropTypes.string)
};
function _sanitizedHtml(props = {}) {
  const {as, html, className, forwardedRef} = props;
  const ContainerComponent = as || "div";
  let history = useHistory();
  history = props.history || history;

  return (
    <ContainerComponent 
      data-testid="sanitized-html-container"
      onClick={clickHandlerForNavigation}
      className={className} 
      dangerouslySetInnerHTML={{__html: customSanitizeHtml(html)}} 
      ref={forwardedRef}
    />
  );

  function clickHandlerForNavigation(e) {
    const targetLink = e.target.closest('a');
    if (!targetLink || !isLocalLink(targetLink.href)) return;
    e.preventDefault();
    history.push(targetLink.pathname)
  };

  function customSanitizeHtml(input) {
    const {removeAllowedTags = []} = props;
    let defaultAllowedTags = [
      'b', 'em', 'i', 'small', 'strong', 'sub', 'sup', 'ins', 'del', 'mark', 'h3', 'h4', 'h5', 'h6', 'blockquote', 
      'p', 'a', 'ul', 'ol', 'nl', 'li', 'strike', 'code', 'hr', 'br', 'caption', 'pre'
    ];
    let allowedTags = defaultAllowedTags.filter(tag => !removeAllowedTags.includes(tag));
    return sanitizeHtml(input, {
      allowedTags,
      allowedAttributes: {
        'a': ['href', 'target']
      },
      transformTags: {
        'a': (tagName, attribs) => {
          /* Here, we add target="_blank" to anchors to an external link */
          if (attribs.href && !isLocalLink(attribs.href)) {
            attribs.target = "_blank";
          }
          return {tagName, attribs};
        }
      }
    });
  }

  function isLocalLink(href) {
    let targetHostname = new URL(href, window.location).hostname;
    return window.location.hostname === targetHostname;
  }
}

export const SanitizedHtml = React.forwardRef((props, ref) => {
  const SanitizeHtmlInnerComponent = _sanitizedHtml;
  return <SanitizeHtmlInnerComponent forwardedRef={ref} {...props}/>
});
