import {getIn} from 'formik';

/**
 * Utility to see if a formik field is required based on the Yup validation schema.
 *
 * Takes the formik options object, and the name of the field as input
 */
export function isRequiredField({ validationSchema }, name) {
  if (!validationSchema) return false;
  let tmp = getIn(validationSchema.describe().fields, name)

  return !!(tmp && tmp.tests && tmp.tests.find(({name}) => name === 'required'));
}

