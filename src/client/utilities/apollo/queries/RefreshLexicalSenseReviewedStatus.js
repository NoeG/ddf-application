import {gql} from 'apollo-boost';

export const gqlRefreshLexicalSenseReviewedStatus = gql`
  query lexicalSenseReviewedStatus($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      reviewed
    }
  }
`;
