import get from "lodash/get";
import {gql} from "apollo-boost";

/**
 * Returns the list of "posts" (aka sioc:Item) of a ontolex:Form depending on its specialized type.
 *
 * @see into the ontology : `ontolex:Form/ddf:hasPostAboutForm`
 *
 * @borrows gqlFormPostsAboutFormFragment . GraphQL FormPostsAboutFormFragment  must be added in in Form GraphQL query.
 * @param form
 * @return {string[]}
 */
export function getFormPostsAboutForm(form) {
  return get(form, "postsAboutForm.edges", []);
}

/**
 * Returns the GQL fragment corresponding to `getFormPostsAboutForm` helper.
 * @type {gql}
 */
export let gqlFormPostsAboutFormFragment = gql`
  fragment FormPartOfSpeechListFragment on Form{
    postsAboutForm(first: $postsAboutFormCount, after: $postsAboutFormCursor){
      edges{
        node{
          id
          content
        }
      }
    }
  }
`;
