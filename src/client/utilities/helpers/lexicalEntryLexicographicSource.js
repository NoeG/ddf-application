import get from "lodash/get";
import {gql} from "apollo-boost";

/**
 * Returns the lexicographic source of a ontolex:LexicalEntry
 *
 * @see into the ontology : `ontolex:LexicalEntry > lexicog:Entry > lexicog:LexicographicResource / rdf:value`
 *
 * @borrows gqlLexicalEntryLexicographicSourceFragment . GraphQL LexicalEntryBaseFragment  must be added in in LexicalEntry GraphQL query.
 *
 * @param lexicalEntry
 * @return {string[]}
 */
export function getLexicalEntryLexicographicSourceConciseLabel(lexicalEntry) {
  return get(lexicalEntry, "entry.lexicographicResource.altLabel");
}

export function getLexicalEntryLexicographicSourceLabel(lexicalEntry) {
  return get(lexicalEntry, "entry.lexicographicResource.prefLabel");
}

/**
 * Returns the GQL fragment corresponding to `gqlLexicalEntryLexicographicSourceFragment` helper.
 * @type {gql}
 */
export let gqlLexicalEntryLexicographicSourceFragment = gql`
  fragment LexicalEntryLexicographicSourceFragment on LexicalEntryInterface{
    entry{
      id
      lexicographicResource{
        id
        prefLabel
        altLabel
      }
    }
  }
`;
