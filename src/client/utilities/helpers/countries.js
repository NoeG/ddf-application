import {gql} from 'apollo-boost';
import uniq from 'lodash/uniq';

/**
 * An object that contains the data needed for the UI, relative to a list of countries associated to a localized entity.
 *
 * @typedef {Object} CountryData
 * @property {Array<string>} names  - The list of country names
 * @property {string} color         - The color associated with the localisation. TODO list the possible color values.
 *                                    'green', 'orange', 'yellow', 'pink', 'black', 'purple', 'gray'
 *
 */


/**
 * @param {string} colorName The color name as expected in the database (in french, with specific color name like "vert d'eau").
 * @return {string} The generic CSS class name describing the color ('green', 'orange', 'yellow', 'pink', 'black', 'purple', 'gray')
 */
export function colorToCssClassName(colorName) {
  switch(colorName) {
    case 'jaune':
      return 'yellow';
    case "vert d'eau":
      return 'green';
    case 'rose':
      return 'pink';
    case 'orange':
      return 'orange';
    case 'noir':
      return 'black';
    default:
      return null;
  }
}

/**
 * Returns a list of country names.
 *
 * @borrows gqlFormCountriesFragment . GraphQL FormCountriesFragment must be added in in localizedEntity GraphQL query.
 *
 * @param localizedEntity
 * @return {*}
 */
export function getLocalizedEntityCountryData(localizedEntity){
  let places = localizedEntity.places.edges.map(({node}) => node);
  let placeNames = places.map(place => place.countryName || place.name);
  /* Filter out duplicate country names */
  placeNames = placeNames.filter((value, index, array) => array.indexOf(value) === index);

  return {
    names: placeNames,
    color: colorFromPlaceList(places)
  }
}

/*
 * Extracts the color to use for displaying the group of countries given in input
 *
 * @param {Array<Place>} places - An array of Place object as returned when using the gqlCountryFragment
 * @return {string} A string describing a color (see CountryData.color}
 */
export function colorFromPlaceList(places) {
  if (!places.length) return 'gray';
  let colors = uniq(
    places
    .map(place => colorToCssClassName(place.color))
    .filter(color => color)
  );
  return (colors.length == 1) ? colors[0] : 'purple';
}


/**
 * Extract the id corresponding to the country of the place. In order to handle cases where place is directly the country
 * (in this case we want place.id) or a lower geographical division (in this case we want place.countryId).
 *
 *
 * @param {Place} place - A place object
 */
export function getPlaceCountryId(place) {
  return place && (place.countryId || place.id);
}


/**
 * Returns the GQL fragment corresponding to `getLocalizedEntityCountries` helper.
 * @type {gql}
 */

export let gqlCountryFragment = gql`
  fragment CountryFragment on PlaceInterface{
    id
    name
    color
    ... on City{
      countryName
    }
    ... on State{
      countryName
    }
  }
`;

/**
 * Returns the GQL fragment corresponding to `getFormCountries` helper.
 * @type {gql}
 */
export let gqlFormCountriesFragment = gql`
  fragment FormCountriesFragment on Form{
    places{
      edges{
        node{
          id
          ...CountryFragment
        }
      }
    }
  }
  
  ${gqlCountryFragment}
`;

/**
 * Returns the GQL fragment corresponding to `getLexicalSenseCountries` helper.
 * @type {gql}
 */
export let gqlLexicalSenseCountriesFragment = gql`
  fragment LexicalSenseCountriesFragment on LexicalSense{
    places{
      edges{
        node{
          id
          ...CountryFragment
        }
      }
    }
  }

  ${gqlCountryFragment}
`;

/**
 * Check if a place object is valid to be exploited by the application as the user current location. 
 * It needs to have at least an id and a name.
 */
export function isValidPlace(place) {
  return !!(place && place.id && place.name);
}
