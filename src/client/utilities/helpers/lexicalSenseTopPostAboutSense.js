import get from "lodash/get";
import {gql} from "apollo-boost";

/**
 * Returns the list of "TopPost" (aka sioc:Item) of a ontolex:LexicalSense depending on its specialized type.
 *
 * @see into the ontology : `ontolex:LexicalSense/ddf:hasPostAboutSense`
 *
 * @borrows gqlLexicalSenseTopPostAboutSenseFragment . GraphQL LexicalSenseTopPostAboutSenseFragment  must be added in in LexicalSense GraphQL query.
 * @param lexicalSense
 * @return {string[]}
 */
export function getLexicalSenseTopPostAboutSense(lexicalSense) {
  return get(lexicalSense, "topPostAboutSense");
}

/**
 * Returns the GQL fragment corresponding to `getLexicalSenseTopPostAboutSense` helper.
 * @type {gql}
 */
export let gqlLexicalSenseTopPostAboutSenseFragment = gql`
  fragment LexicalSenseTopPostAboutSenseFragment on LexicalSense{
    topPostAboutSense{
      id
      content
    }
  }
`;
