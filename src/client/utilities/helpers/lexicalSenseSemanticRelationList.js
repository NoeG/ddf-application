import get     from 'lodash/get';
import groupBy     from 'lodash/groupBy';
import {gql} from "apollo-boost";

/**
 * Returns the list of "ddf:SemanticRelation" of a ontolex:LexicalSense grouped by the ddf:semanticRelationType.
 *
 * @see into the ontology `ontolex:LexicalSense > ddf:SemanticRelation/ddf:semanticRelationType`
 *
 * @borrows gqlLexicalSenseSemanticRelationFragment . GraphQL LexicalSenseSemanticRelationFragment  must be added in in LexicalSense GraphQL query.
 *
 * @param lexicalSense
 * @return {object} An object with semanticRelationType as key and semanticRelation[] as value
 */
export function getGroupedLexicalSenseSemanticRelationList(lexicalSense) {
  return groupBy(
    lexicalSense.semanticRelations.edges.map(({node}) => node),
    (semanticRelation) => {
      return get(semanticRelation, 'semanticRelationType.prefLabel');
    });
}

/**
 * Returns the GQL fragment corresponding to `getGroupedLexicalSenseSemanticRelationList` helper.
 * @type {gql}
 */
export let gqlLexicalSenseSemanticRelationFragment = gql`
  fragment LexicalSenseSemanticRelationFragment on LexicalSense{
    semanticRelations(excludeSenseToSenseTypes: true){
      edges{
        node{
          id
          semanticRelationType{
            id
            prefLabel
          }
          canonicalForms{
            edges{
              node{
                id
                writtenRep
              }
            }
          }
        }
      }
    }
  }
`;
