import get from "lodash/get";
import {gql} from "apollo-boost";

/**
 * Returns the list of "posts" (aka sioc:Item) of a ontolex:LexicalSense depending on its specialized type.
 *
 * @see into the ontology : `ontolex:LexicalSense/ddf:hasPostAboutSense`
 *
 * @borrows gqlLexicalSensePostsAboutSenseFragment . GraphQL LexicalSensePostsAboutSenseFragment  must be added in in LexicalSense GraphQL query.
 * @param lexicalSense
 * @return {string[]}
 */
export function getLexicalSensePostsAboutSense(lexicalSense) {
  return get(lexicalSense, "postsAboutSense.edges", []);
}

/**
 * Returns the GQL fragment corresponding to `getLexicalSensePostsAboutSense` helper.
 * @type {gql}
 */
export let gqlLexicalSensePostsAboutSenseFragment = gql`
  fragment LexicalSensePostsAboutSenseFragment on LexicalSense{
    postsAboutSense(first: $postsAboutSenseCount, after: $postsAboutSenseCursor){
      edges{
        node{
          id
          content
        }
      }
    }
  }
`;
