import uniq from 'lodash/uniq';
import get from 'lodash/get';
import {gql} from "apollo-boost";

/**
 * Returns the list of form written representations related to a ddf:SemanticRelation.
 *
 * @see into the ontology `ddf:SemanticRelation > lexicog:Entry > ontolex:LexicalEntry > ontolex:Form
 /ontolex:writtenRep`
 *
 * @borrows gqlSemanticRelationFormFragment . GraphQL SemanticRelationFormFragment  must be added in SemanticRelation GraphQL query.
 *
 * @param semanticRelation
 * @return {object} A list of form written representations
 */
export function getSemanticRelationFormList(semanticRelation) {
  return uniq((semanticRelation.canonicalForms?.edges || []).map(({node: form}) => form.writtenRep));
}

/**
 * Returns the GQL fragment corresponding to `getSemanticRelationFormList` helper.
 * @type {gql}
 */
export let gqlSemanticRelationFormFragment = gql`
  fragment SemanticRelationFormFragment on SemanticRelation{
    id
    canonicalForms{
      edges{
        node{
          id
          writtenRep
        }
      }
    }
  }
`;
