import {getLexicalEntryLexicographicSourceConciseLabel, getLexicalEntryLexicographicSourceLabel} from '../lexicalEntryLexicographicSource';
import lexicalEntryWordMock from './mocks/lexicalEntryWord.json';

describe("getLexicalEntryLexicographicSourceLabel helper", () => {
  test("should return full label", () => {
    expect(getLexicalEntryLexicographicSourceLabel(lexicalEntryWordMock)).toEqual("Inventaire des particularités lexicales du français en Afrique noire");
  });

  test("should return concise label", () => {
    expect(getLexicalEntryLexicographicSourceConciseLabel(lexicalEntryWordMock)).toEqual("Inv");
  });


});