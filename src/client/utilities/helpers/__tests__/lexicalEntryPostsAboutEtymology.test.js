import {getLexicalEntryPostsAboutEtymology} from '../lexicalEntryPostsAboutEtymology';
import lexicalEntryWithPostsAboutEtymologyMock from './mocks/lexicalEntryWithPostsAboutEtymology.json';

describe("getLexicalSenseTags helper", () => {
  test("should return tags in right order", () => {
    let source = getLexicalEntryPostsAboutEtymology(lexicalEntryWithPostsAboutEtymologyMock);

    expect(source).toEqual([{
      "__typename": "PostEdge",
      "node": {
        "__typename": "Post",
        "content": "du yoruba",
      }
    }]);
  });
});