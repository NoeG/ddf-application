import {getLexicalEntryPartOfSpeechList} from '../lexicalEntryPartOfSpeechList';
import lexicalEntryVerbMock from './mocks/lexicalEntryVerb.json';
import lexicalEntryInflectablePOSMock from './mocks/lexicalEntryInflectablePOS.json';
import lexicalEntryMultiwordExpressionMock from './mocks/lexicalEntryMultiwordExpression.json';
import lexicalEntryInvariablePOSMock from './mocks/lexicalEntryInvariablePOS.json';
import lexicalEntryAffixMock from './mocks/lexicalEntryAffix.json';
import lexicalEntryWordMock from './mocks/lexicalEntryWord.json';
import lexicalEntryWithNullsMock from './mocks/lexicalEntryWithNulls.json';

describe("getLexicalEntryPartOfSpeechList helper", () => {
  test("should return ddf:Verb tags in right order", () => {
    let tags = getLexicalEntryPartOfSpeechList(lexicalEntryVerbMock);

    expect(tags).toEqual([
      'verbe',
      'intransitif',
    ])
  });

  test("should return ddf:InflectablePOS tags in right order", () => {
    let tags = getLexicalEntryPartOfSpeechList(lexicalEntryInflectablePOSMock);

    expect(tags).toEqual([
      'nom',
      'masculin',
    ])
  });

  test("should return ddf:InvariablePOS tags in right order", () => {
    let tags = getLexicalEntryPartOfSpeechList(lexicalEntryInvariablePOSMock);

    expect(tags).toEqual([
      'adjectif',
    ])
  });

  test("should return ontolex:Affix tags in right order", () => {
    let tags = getLexicalEntryPartOfSpeechList(lexicalEntryAffixMock);

    expect(tags).toEqual([
      'préfixe',
    ])
  });

  test("should return ontolex:MultiWordExpression tags in right order", () => {
    let tags = getLexicalEntryPartOfSpeechList(lexicalEntryMultiwordExpressionMock);

    expect(tags).toEqual([
      'locution adjectivale',
    ])
  });

  test("should return ontolex:Word tags in right order", () => {
    let tags = getLexicalEntryPartOfSpeechList(lexicalEntryWordMock);

    expect(tags).toEqual([
      'verbe',
    ])
  });

  test("should not return null values in the array if the API gives nulls", () => {
    let tags = getLexicalEntryPartOfSpeechList(lexicalEntryWithNullsMock);

    expect(tags).toEqual([
      'masculin'
    ])
  });
});
