import {getLexicalSenseTags} from '../lexicalSenseTags';
import lexicalSenseCompleteMock from './mocks/lexicalSense.json';
import lexicalSenseWithEmptyTagsMock from './mocks/lexicalSenseWithEmptyTags.json';

describe("getLexicalSenseTags helper", () => {
  test("should return tags in right order", () => {
    let tags = getLexicalSenseTags(lexicalSenseCompleteMock);

    expect(tags).toEqual([
      'lorem',
      'ipsum',
      'alea',
      'familier',
      'jacta',
      'est',
      'et coque',
      'plibili',
      'au figuré'
    ])
  });

  test("should return empty tags", () => {
    let tags = getLexicalSenseTags(lexicalSenseWithEmptyTagsMock);

    expect(tags).toEqual([])
  });
});