import {
  getLocalizedEntityCountryData, 
  colorFromPlaceList,
  getPlaceCountryId,
  isValidPlace
} from '../countries';
import lexicalSenseCompleteMock from './mocks/lexicalSense.json';
import formCompleteMock from './mocks/form.json';

describe("countries helpers", () => {
  describe("getLocalizedEntityCountryData helper", () => {
    it("returns country data for a Form object", () => {
      let countryData = getLocalizedEntityCountryData(lexicalSenseCompleteMock);
      expect(countryData.names).toEqual(['Sénégal', 'Mali']);
    });

    it("returns country data for a LexicalSense object", () => {
      let countryData = getLocalizedEntityCountryData(formCompleteMock);
      expect(countryData.names).toEqual(['Centrafrique']);
    });

    it("doesn't duplicate country names if several places from the same country", () => {
      let localizedEntity = {
        places: {
          edges: [{
            node: {
              name: "Centrafrique",
            },
          }, {
            node: {
              name: "Bamako",
              countryName: "Mali"
            }
          }, {
            node: {
              name: "Gao",
              countryName: "Mali"
            }
          }]
        }
      };
      
      let countryData = getLocalizedEntityCountryData(localizedEntity);
      expect(countryData.names).toEqual(['Centrafrique', 'Mali']);
    });
  });

  describe("colorFromPlaceList", () => {
    it("returns the color of all the places when they have the same", () => {
      let places = [{
        "name": "RDC",
        "color": "orange"
      }, {
        "name": "Cameroun",
        "color": "orange"
      }];
      expect(colorFromPlaceList(places)).toEqual('orange');      
    });

    it("returns the color of the places with a color, when other places have no values", () => {
      let places = [{
        "name": "RDC",
        "color": "orange"
      }, {
        "name": "Cameroun",
      }];
      expect(colorFromPlaceList(places)).toEqual('orange');      

      places = [{
        "name": "RDC",
        "color": "orange"
      }, {
        "name": "Cameroun",
        "color": null
      }];
      expect(colorFromPlaceList(places)).toEqual('orange');      
    });

    it("returns purple when there are conflicting colors", () => {
      let places = [{
        "name": "RDC",
        "color": "green"
      }, {
        "name": "Cameroun",
        "color": "pink"
      }];
      expect(colorFromPlaceList(places)).toEqual('purple');      
    });

    it("returns purple when there is no defined color", () => {
      let places = [{
        "name": "RDC",
        "color": null
      }, {
        "name": "Cameroun",
      }];
      expect(colorFromPlaceList(places)).toEqual('purple');      
    });

    it("returns gray when the place list is empty", () => {
      expect(colorFromPlaceList([])).toEqual('gray');      
    });
  });
});

describe("getPlaceCountryId()", () => {
  it("returns place.id if place is a country", () => {
    let place = {
      "id": "geonames:3017382",
      "__typename": "Country",
      "name": "France",
      "countryCode": "FR"
    };
    expect(getPlaceCountryId(place)).toEqual("geonames:3017382");
  });

  it("returns place.countryId if place has a countryId", () => {
    let place = {
      "id": "geonames:11832782",
      "__typename": "City",
      "name": "Frances",
      "countryCode": "PH",
      "countryId": "geonames:1694008",
      "countryName": "Philippines",
      "stateName": "Luçon centrale"
    };
    expect(getPlaceCountryId(place)).toEqual("geonames:1694008");
  });

  it("returns null if null is provided", () => {
    expect(getPlaceCountryId(null)).toEqual(null);
  });
});

describe("isValidPlace()", () => {
  it("returns true for a country", () => {
    let place = {
      "id": "geonames:3017382",
      "__typename": "Country",
      "name": "France",
      "countryCode": "FR"
    };
    expect(isValidPlace(place)).toEqual(true);
  });

  it("returns true for a city", () => {
    let place = {
      "id": "geonames:11832782",
      "__typename": "City",
      "name": "Frances",
      "countryCode": "PH",
      "countryId": "geonames:1694008",
      "countryName": "Philippines",
      "stateName": "Luçon centrale"
    };
    expect(isValidPlace(place)).toEqual(true);
  });

  it("returns false for an object without id", () => {
    let place = {
      "__typename": "Country",
      "name": "France",
      "countryCode": "FR"
    };
    expect(isValidPlace(place)).toEqual(false);
  });

  it("returns false for a falsy value", () => {
    expect(isValidPlace(null)).toEqual(false);
  });
});
