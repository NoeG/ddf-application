import gql from 'graphql-tag';

/**
 * The following groups are globals and created in dataset.
 * They are listed
 */
export const gqlUserAccountGroupsFragment = gql`
  fragment UserAccountGroupsFragment on UserAccount{
    isAdmin: isInGroup(userGroupId: "user-group/AdministratorGroup")
    isOperator: isInGroup(userGroupId: "user-group/OperatorGroup")
    isContributor: isInGroup(userGroupId: "user-group/ContributorGroup")
  }
`;