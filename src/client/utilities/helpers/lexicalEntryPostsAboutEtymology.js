import  get from "lodash/get";
import {gql} from "apollo-boost";

/**
 * Returns the list of "posts" (aka sioc:Item) of a ontolex:LexicalEntry depending on its specialized type.
 *
 * @see into the ontology : `ontolex:LexicalEntry/ddf:hasPostAboutEtymology`
 *
 * @borrows gqlLexicalEntryPostsAboutEtymologyFragment . GraphQL LexicalEntryPostsAboutEtymologyFragment  must be added in in LexicalEntry GraphQL query.
 * @param lexicalEntry
 * @return {string[]}
 */
export function getLexicalEntryPostsAboutEtymology(lexicalEntry) {
  return get(lexicalEntry, "postsAboutEtymology.edges", []);
}

/**
 * Returns the GQL fragment corresponding to `getLexicalEntryPostsAboutEtymology` helper.
 * @type {gql}
 */
export let gqlLexicalEntryPostsAboutEtymologyFragment = gql`
  fragment LexicalEntryPartOfSpeechListFragment on LexicalEntryInterface{
    postsAboutEtymology(first: $postsAboutEtymologyCount, after: $postsAboutEtymologyCursor){
      edges{
        node{
          id
          content
        }
      }
    }
  }
`;
