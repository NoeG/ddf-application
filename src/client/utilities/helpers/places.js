import {gql} from 'apollo-boost';

export const gqlPlaceFragment = gql`
  fragment PlaceFragment on PlaceInterface {
    id
    __typename
    name
    countryCode
    ... on City {
      countryId
      countryName
      stateName
    }
    ... on State {
      countryId
      countryName
    }
  }
`;

/**
 * Returns a list of country names.
 *
 * @borrows gqlFormCountriesFragment . GraphQL FormCountriesFragment must be added in in localizedEntity GraphQL query.
 *
 * @param {Place} place A Place object as defined in GeolocService.js
 * @return {string} Returns a string representing the place info. That is the state name, and country name (separated) when it applies.
 *                  It can be only the country name if there's no state name to display, or an empty string if there is nothing to display.
 */
export function getPlaceRegionInfo(place){
  return formatPlace(place, ['state', 'country']);
}

/**
 * Joins the elements of a place (place name, place's state name, place's country name) if they exist. You can 
 * specify the elements to include (order is preserved), and the joining string.
 *
 * @param {Object} place The place object
 * @param {Array<string>} elements Array describing which elements names we want. You can pass 'name', 'state', and/or 'country'
 * @param {string} joinStr The string used to join the elements (default is ', ')
 */
export function formatPlace(place, elements, joinStr = ', ') {
  return elements.map((element) => {
    switch(element) {
      case 'name':
        return place?.name;
        break;
      case 'state':
        return place?.stateName;
        break;
      case 'country':
        return place?.countryName;
        break;
    }
  })
    .filter(e => e)
    .join(', ');
}
