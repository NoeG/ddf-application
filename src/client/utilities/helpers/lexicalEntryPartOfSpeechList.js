import reduce from "lodash/reduce";
import get from "lodash/get";
import {gql} from "apollo-boost";

/**
 * Returns the list of "part of speech" of a ontolex:LexicalEntry depending on its specialized type.
 *
 * @see into the ontology :
 *
 * ```
 *  - ontolex:Word/ddf:hasPartOfSpeech
 *  - ddf:Verb/ddf:hasTransitivity
 *  - ddf:InflectablePOS/(ddf:hasNumber|ddf:hasGender)
 *  - ontolex:Affix/lexinfo:termElement
 *  - ontolex:MultiWordExpression/ddf:multiwordType
 * ```
 * @borrows gqlLexicalEntryPartOfSpeechListFragment . GraphQL LexicalEntryPartOfSpeechListFragment  must be added in in LexicalEntry GraphQL query.
 * @param lexicalEntry
 * @return {string[]}
 */
export function getLexicalEntryPartOfSpeechList(lexicalEntry) {
  let typename = lexicalEntry.__typename;
  let typePaths = [];

  // The purpose of the following line is to aggregate all property paths
  // to display following the lexical entry type.
  if(["Verb", "InflectablePOS", "InvariablePOS", "Word"].includes(typename)){
    typePaths.push("partOfSpeech.prefLabel");
    switch (typename) {
      case "Verb":
        typePaths.push("transitivity.prefLabel");
        break;
      case "InflectablePOS":
        typePaths.push("number.prefLabel");
        typePaths.push("gender.prefLabel");
        break;
    }

  } else if (typename === "Affix") {
    typePaths.push("termElement.prefLabel");
  } else if (typename === "MultiWordExpression") {
    typePaths.push("multiWordType.prefLabel");
  }

  // Then reduce them to keep only the existing ones
  return reduce(typePaths, (acc, typePath) => {
    let el = get(lexicalEntry, typePath);
    if (el) {
      acc.push(el);
    }

    return acc;
  }, []);
}

/**
 * Given a lexical entry populated with all the part of speech fragments (as defined by the GQL fragment
 * LexicalEntryPartOfSpeechListFragment difenid below). This function will extract only the object
 * for the grammatical category, depending on the type of the lexical entry. 
 *
 * Returned value based on type : 
 *
 * - WordInterface => lexicalEntry.partOfSpeech
 * - MultiWordExpression => lexicalEntry.multiWordType
 * - Affix => lexicalEntry.termElement
 */
export function getLexicalEntryGrammaticalCategory(lexicalEntry) {
  if (lexicalEntry.multiWordType) {
    return lexicalEntry.multiWordType;
  } else if (lexicalEntry.termElement) {
    return lexicalEntry.termElement;
  } else if (lexicalEntry.partOfSpeech) {
    return lexicalEntry.partOfSpeech;
  }
}

/**
 * Returns the GQL fragment corresponding to `getLexicalEntryPartOfSpeechList` helper.
 * @type {gql}
 */
export let gqlLexicalEntryPartOfSpeechListFragment = gql`
  fragment LexicalEntryPartOfSpeechListFragment on LexicalEntryInterface{
    ...on WordInterface{
      partOfSpeech{
        id
        prefLabel
      }
    }

    ...on InflectablePOS{
      number{
        id
        prefLabel
      }

      gender{
        id
        prefLabel
      }
    }

    ...on Verb{
      transitivity{
        id
        prefLabel
      }
    }

    ...on MultiWordExpression{
      multiWordType{
        id
        prefLabel
      }
    }

    ...on Affix{
      termElement{
        id
        prefLabel
      }
    }
  }
`;
