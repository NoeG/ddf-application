import get from "lodash/get";
import has from "lodash/has";
import {gql} from "apollo-boost";

/**
 * Returns the list of "tags" of a ontolex:LexicalSense depending on its specialized type.
 *
 * @see into the ontology :
 *
 * ```
 *   ontolex:LexicalSense/
 *    - ddf:hasDomain
 *    - ddf:hasTemporality
 *    - ddf:hasRegister
 *    - ddf:hasConnotation
 *    - ddf:hasSociolect
 *    - ddf:hasGrammaticalConstraint
 *  ```
 *
 *  and all  ontolex:LexicalSense >  ddf:SemanticRelation/ddf:semanticRelationType that match the following URIs :
 *
 *  ```
 *    ddf-authority:sense-relation/soundRelation
 *    ddf-authority:sense-relation/demonymicRelation
 *    ddf-authority:sense-relation/oppositeSexRelation
 *    ddf-authority:sense-relation/sameSemanticDivision
 *    ddf-authority:sense-relation/figurativeRelation
 *    ddf-authority:sense-relation/properMeaningRelation
 *    ddf-authority:sense-relation/metonymicRelation
 *    ddf-authority:sense-relation/hyperbolicRelation
 *    ddf-authority:sense-relation/extendedFromRelation
 *    ddf-authority:sense-relation/analogicalRelation
 *    ddf-authority:sense-relation/peculiarRelation
 *    ddf-authority:sense-relation/litoticRelation
 *    ddf-authority:sense-relation/euphemisticRelation
 *    ddf-authority:sense-relation/metaphoricRelation
 *    ddf-authority:sense-relation/rarerThan
 *    ddf-authority:sense-relation/moreCommonThan
 *  ```
 *
 * @borrows gqlLexicalSenseTagsFragment . GraphQL LexicalSenseTagsFragment  must be added in in LexicalSense GraphQL query.
 *
 * @param lexicalSense
 * @return {string[]}
 */
export function getLexicalSenseTags(lexicalSense) {
  let tags = [];

  ["domains", "temporalities", "registers", "connotations",
    "frequencies", "textualGenres", "sociolects", "grammaticalConstraints"].map(tagProp => {
    tags = tags.concat(lexicalSense[tagProp]?.edges.map(({node}) => node?.prefLabel));
  });

  tags = tags.concat(get(lexicalSense, "senseToSenseSemanticRelations.edges", []).map(({node: semanticRelation}) => get(semanticRelation, "semanticRelationType.prefLabel")) || []);

  return tags;
}

/**
 * Returns the GQL fragment corresponding to `getLexicalSenseTags` helper.
 * @type {gql}
 */
export let gqlLexicalSenseTagsFragment = gql`
  fragment LexicalSenseTagsFragment on LexicalSense{
    domains{
      edges{
        node{
          id
          prefLabel  
        }
      }
    }

    temporalities{
      edges{
        node{
          id
          prefLabel
        }
      }
    }

    registers{
      edges{
        node{
          id
          prefLabel
        }
      }
    }

    connotations{
      edges{
        node{
          id
          prefLabel
        }
      }
    }

    frequencies{
      edges{
        node{
          id
          prefLabel
        }
      }
    }

    textualGenres{
      edges{
        node{
          id
          prefLabel
        }
      }
    }

    sociolects{
      edges{
        node{
          id
          prefLabel
        }
      }
    }

    grammaticalConstraints{
      edges{
        node{
          id
          prefLabel
        }
      }
    }

    senseToSenseSemanticRelations: semanticRelations(filterOnSenseToSenseTypes: true){
      edges{
        node{
          id
          semanticRelationType{
            id
            prefLabel
          }
        }
      }
    }
  }
`;
