import React from 'react';
import {withRouter} from 'react-router-dom';

/**
 * This component is a utility to make the view scroll to top on any route change.
 * See https://reacttraining.com/react-router/web/guides/scroll-restoration
 *
 * It is used on the top of the application tree
 *
 */

@withRouter
export class ScrollToTop extends React.Component {
  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    return this.props.children;
  }
}
