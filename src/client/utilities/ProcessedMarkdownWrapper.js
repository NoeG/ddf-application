import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

ProcessedMarkdownWrapper.propTypes = {
  /** 
   * An HTML string. Intended to be the result of a markdown file intend processed by webpack loaders.
   * This component will directly inject the HTML in the page, so be sure to not use it with an unstrusted
   * HTML content
   */
  content: PropTypes.string.isRequired
};
export function ProcessedMarkdownWrapper(props) {
  const {content, ...otherProps} = props;
  return <div 
    dangerouslySetInnerHTML={{__html: content}} 
    {...otherProps}
  />;
}
