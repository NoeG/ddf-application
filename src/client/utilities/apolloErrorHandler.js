

/**
 * @param error {any} - Error object as returned by an apollo hook (useQuery, useMutation)
 * @param strategy {'log'|'throwError'} - 'log': logs the error to the console
 *                                        'throwError' (default) : logs the error to the console, then throw a new Error, in order to be catched
 *                                                       by a react error boundary component higher up in the react tree
 */
export function apolloErrorHandler(error, strategy = 'throwError') {
  if (error) {
    let stack = new Error("Apollo error catched").stack;
    console.error(stack);
    console.error('Apollo query returned the following error', Object.assign({}, error));

    if (strategy === 'throwError') {
      throw new Error('Apollo error occured');
    }
  }
}
