import React from 'react';
import {Route, Switch, Redirect, withRouter} from 'react-router-dom';
import {Helmet} from 'react-helmet-async';

import {ROUTES} from './routes';
import {NotificationsDisplay} from './components/general/NotificationsDisplay';
import {Index} from './components/routes/Index';
import {NotFound} from './components/routes/NotFound';
import {Form} from './components/routes/Form';
import {ProjectPresentation} from './components/routes/ProjectPresentation';
import {Gcu} from './components/routes/Gcu';
import {Partners} from './components/routes/Partners';
import {Credits} from './components/routes/Credits';
import {HelpIndex} from './components/routes/HelpIndex';
import {HowToSearch} from './components/routes/help/HowToSearch';
import {HowToRead} from './components/routes/help/HowToRead';
import {HowToContribute} from './components/routes/help/HowToContribute';
import {HelpDdfIrl} from './components/routes/help/HelpDdfIrl';
import {FormDemo} from './components/routes/FormDemo'; /* FIXME */
import {Subscribe} from './components/routes/Subscribe';
import {Login} from './components/routes/Login';
import {MyAccount} from './components/routes/users/MyAccount';
import {PasswordForgotten} from './components/routes/PasswordForgotten';
import {CreateLexicalSense} from './components/contribution/CreateLexicalSense';
import {EditLexicalSense} from './components/contribution/EditLexicalSense';
import {ContributionsReview} from './components/routes/admin/contributions/ContributionsReview';
import {Users} from './components/routes/admin/users/Users';
import {AdminIndex} from './components/routes/admin/AdminIndex';


@withRouter
export class MainView extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>Dictionnaire des francophones</title>
        </Helmet>
        <NotificationsDisplay/>
        <Switch>
          <Route exact path="/" component={Index}/>
          <Route exact path={ROUTES.FORM_LEXICAL_SENSE_EDIT} component={EditLexicalSense}/>
          <Route path={ROUTES.FORM_SEARCH} component={Form}/>
          <Route path={ROUTES.CREATE_LEXICAL_SENSE} component={CreateLexicalSense}/>
          <Route exact path={ROUTES.PRESENTATION} component={ProjectPresentation} />
          <Route exact path={[ROUTES.GCU, ROUTES.GCU_LEGACY]} component={Gcu} />
          <Route exact path={[ROUTES.PARTNERS, ROUTES.PARTNERS_LEGACY]} component={Partners} />
          <Route exact path={ROUTES.CREDITS} component={Credits} />
          <Route exact path={ROUTES.HELP_INDEX} component={HelpIndex} />
          <Route exact path={ROUTES.HELP_HOW_TO_SEARCH} component={HowToSearch} />
          <Route exact path={ROUTES.HELP_HOW_TO_READ} component={HowToRead} />
          <Route exact path={ROUTES.HELP_HOW_TO_CONTRIBUTE} component={HowToContribute} />
          <Route exact path={ROUTES.HELP_DDF_IRL} component={HelpDdfIrl} />
          <Route exact path={ROUTES.SUBSCRIBE} component={Subscribe} />
          <Route exact path={ROUTES.LOGIN} component={Login} />
          <Route exact path={ROUTES.PASSWORD_FORGOTTEN} component={PasswordForgotten} />
          <Route path={ROUTES.MY_ACCOUNT} component={MyAccount} />
          <Route exact path={ROUTES.FORM_DEMO} component={FormDemo} />
          <Route exact path={ROUTES.ADMIN} component={AdminIndex} />
          <Route exact path={ROUTES.ADMIN_USERS} component={Users} />
          <Route exact path={ROUTES.CONTRIBUTIONS_REVIEW} component={ContributionsReview} />
          <Route component={NotFound}/>
        </Switch>
      </React.Fragment>
    )
  }
}
