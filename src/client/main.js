/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React              from 'react';
import ReactDOM           from 'react-dom';
import {BrowserRouter}    from 'react-router-dom';
import {ApolloProvider}   from 'react-apollo';
import {CookiesProvider}  from 'react-cookie';
import {HelmetProvider} from 'react-helmet-async';

import {DDFApplication} from './DDFApplication';
import {i18nInitialize} from './utilities/i18nInitialize';
import {dayjsInitialize} from './utilities/dayjsInitialize';
import {getDDFApolloClient} from './services/DDFApolloClient';
import {DdfLoadingSplashScreen} from './components/general/DdfLoadingSplashScreen';
import {ScrollToTop} from './utilities/ScrollToTop';
import {DdfGeneralErrorHandler} from './components/general/DdfGeneralErrorHandler';
import {BrowsingHistoryHelperProvider} from './services/BrowsingHistoryHelperService';
import './assets/stylesheets/main.styl';

let reactRootElement = document.getElementById('react-root');

i18nInitialize();
dayjsInitialize();

ReactDOM.render(
  <BrowserRouter>
    <BrowsingHistoryHelperProvider>
      <HelmetProvider>
        <CookiesProvider>
          <ApolloProvider client={getDDFApolloClient()}>
            <React.Suspense fallback={<DdfLoadingSplashScreen/>}>
              <DdfGeneralErrorHandler>
                <ScrollToTop>
                  <DDFApplication/>
                </ScrollToTop>
              </DdfGeneralErrorHandler>
            </React.Suspense>
          </ApolloProvider>
        </CookiesProvider>
      </HelmetProvider>
    </BrowsingHistoryHelperProvider>
  </BrowserRouter>,
  reactRootElement
);
