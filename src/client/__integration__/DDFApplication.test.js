/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {gql} from 'apollo-boost';
import {apolloClient} from '../../../jest/utilities/integrationApolloClient';

require('util').inspect.defaultOptions.depth = null;

jest.setTimeout(60000);



describe('DDF Application', () => {
  test("Apollo test form API", async () => {
    let {data} = await apolloClient.query({
      query: gql`
        {
          form(id: "dinv:1937/en/1/fo/0") {
            writtenRep
            lexicalEntries {
              edges{
                node{
                  id
                  senses {
                    edges {
                      node {
                        definition
                      }
                    }
                  }
                }
              }
            }
          }
        }
      `
    });

    expect(data).toEqual({
      "form": {
        "__typename": "Form",
        "writtenRep": "donner leau de la maison",
        "lexicalEntries": {
          "__typename": "LexicalEntryInterfaceConnection",
          "edges": [
            {
              "__typename": "LexicalEntryInterfaceEdge",
              "node" : {
                "__typename": "InvariablePOS",
                "id": "dinv:1937/en/1",
                "senses": {
                  "__typename": "LexicalSenseConnection",
                  "edges": [
                    {
                      "__typename": "LexicalSenseEdge",
                      "node": {
                        "__typename": "LexicalSense",
                        "definition": "Geste qui consiste à offrir de l'eau à un étranger* ou à un nouveau venu pour lui souhaiter la"
                      }
                    }
                  ]
                }
              }
            }
          ]

        }
      }
    });
  });

  // test("Apollo test place API", async () => {
  //   let {data} = await apolloClient.query({
  //     query: gql`
  //       {
  //         places(qs: "cannes", first:1) {
  //           edges {
  //             node {
  //               id
  //               name
  //               lng
  //               lat
  //               countryCode
  //               countryName
  //               stateName
  //               stateCode
  //             }
  //           }
  //         }
  //       }
  //     `
  //   });
  //
  //   expect(data).toEqual({
  //     "places": {
  //       "__typename": "PlaceConnection",
  //       "edges": [
  //         {
  //           "__typename": "PlaceEdge",
  //           "node": {
  //             "__typename": "Place",
  //             "id": "geonames:6446684",
  //             "name": "Cannes",
  //             "lng": 7.02139,
  //             "lat": 43.5525,
  //             "countryCode": "FR",
  //             "countryName": "France",
  //             "stateName": "Provence-Alpes-Côte d'Azur",
  //             "stateCode": "93"
  //           }
  //         }
  //       ]
  //     }
  //   });
  // });
});
