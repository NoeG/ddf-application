import invariant from 'invariant';

/**
 * This is needed by the BrowsingHistoryHelperService. The routes need to be inserted in matching order. The first route to match will be the one
 * considered by BrowsingHistoryHelperService. I.e if you have routes "/my_account/" and "/my_account/profile", put the most specific route "/my_account/profile" first.
 *
 * This array is used to generate the ROUTES object, which maps the route name with the route path.
 *
 * The different options you can provide to a route. When these options are boolean, you can omit them if the intended value is false.
 *
 * - needsAuthentication {boolean} - This is to determinate to which location we can go back after the user logouts
 * - isMainNavigationSpace {boolean} - This is to determinate to which  location we can go back after closing a secondary screen (i.e any
 *                                     location that isn't flagged as "isMainNavigationSpace")
 * - partOfAuthenticationProcess {boolean} - This is to determine which locations are part of the auhtentication process (login, account creation,
 *                                           password recovery), in order to determine which page to go back after a successfull authentication (i.e the
 *                                           first page in history which doesn't belong to this group)
 *
 *
 */
export const RoutesMetadata = [{
  name: 'FORM_LEXICAL_SENSE_EDIT',
  path: '/form/:formQuery/sense/:lexicalSenseId/edit',
  needsAuthentication: true
}, {
  name: 'FORM_LEXICAL_SENSE_COUNTRIES',
  path: '/form/:formQuery/sense/:lexicalSenseId/countries',
}, {
  name: 'FORM_LEXICAL_SENSE_SOURCES', 
  path: '/form/:formQuery/sense/:lexicalSenseId/sources',
}, {
  name: 'FORM_LEXICAL_SENSE',
  path: '/form/:formQuery/sense/:lexicalSenseId',
  isMainNavigationSpace: true
}, {
  name: 'FORM_SEARCH',
  path: '/form/:formQuery',
  isMainNavigationSpace: true
}, {
  name: 'CREATE_LEXICAL_SENSE',
  path: '/sense/new',
  needsAuthentication: true
}, {
  name: 'PRESENTATION', 
  path: '/presentation',
}, {
  name: 'GCU_LEGACY',
  path: '/terms',
}, { 
  name: 'GCU',
  path: '/cgu',
}, { 
  name: 'CREDITS',
  path: '/credits',
}, {
  name: 'PARTNERS_LEGACY', 
  path: '/partners',
}, {
  name: 'PARTNERS', 
  path: '/partenaires',
}, {
  name: 'HELP_HOW_TO_SEARCH',
  path: '/aide/1',
  isMainNavigationSpace: true
}, {
  name: 'HELP_HOW_TO_READ',
  path: '/aide/2',
  isMainNavigationSpace: true
}, {
  name: 'HELP_HOW_TO_CONTRIBUTE',
  path: '/aide/3',
  isMainNavigationSpace: true
}, {
  name: 'HELP_DDF_IRL',
  path: '/aide/4',
  isMainNavigationSpace: true
}, {
  name: 'HELP_INDEX',
  path: '/aide',
  isMainNavigationSpace: true
}, {
  name: 'SUBSCRIBE', 
  path: '/subscribe',
  partOfAuthenticationProcess: true,
}, {
  name: 'LOGIN', 
  path: '/login',
  partOfAuthenticationProcess: true,
}, {
  name: 'PASSWORD_FORGOTTEN', 
  path: '/password-forgotten',
  partOfAuthenticationProcess: true,
}, {
  name: 'FORM_DEMO',
  path: '/form_demo',
}, {
  name: 'MY_ACCOUNT_PROFILE_EDIT',
  path: '/my_account/profile/edit',
  needsAuthentication: true
}, {
  name: 'MY_ACCOUNT_PROFILE_SETTINGS',
  path: '/my_account/profile/settings',
  needsAuthentication: true
}, {
  name: 'MY_ACCOUNT_PROFILE',
  path: '/my_account/profile',
  needsAuthentication: true
}, {
  name: 'MY_ACCOUNT_CONTRIBUTIONS',
  path: '/my_account/contributions',
  needsAuthentication: true
}, {
  name: 'MY_ACCOUNT',
  path: '/my_account',
  needsAuthentication: true
}, {
  name: 'ADMIN_USERS',
  path: '/admin/users',
  needsAuthentication: true
}, {
  name: 'ADMIN',
  path: '/admin',
  needsAuthentication: true
}, {
  name: 'CONTRIBUTIONS_REVIEW',
  path: '/contributions',
  needsAuthentication: true
}, {
  name: 'INDEX',
  path: '/',
  isMainNavigationSpace: true
}];

/* Generate ROUTES object */
let ROUTES = {};
RoutesMetadata.forEach(routeDef => {
  let {name, path} = routeDef;
  invariant(typeof name === 'string', `Error in the object RoutesMetadata, invalid route name for defintion ${JSON.stringify(routeDef)}`);
  invariant(typeof path === 'string', `Error in the object RoutesMetadata, invalid route path for ${name}`);
  ROUTES[name] = path;
});

export {ROUTES};
