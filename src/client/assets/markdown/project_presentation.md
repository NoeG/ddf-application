Présentation du projet 
======================

Le Dictionnaire des francophones est un [dictionnaire](#dictionnaire) collaboratif numérique [ouvert](#ouvert) qui a pour objectif de rendre compte de la richesse du français parlé au sein de l’[espace francophone](#francophone). C’est un projet [institutionnel](#institutionnel) novateur qui présente à la fois une partie de consultation au sein de laquelle sont compilées plusieurs [ressources lexicographiques](#ressources), et une [partie participative](#participatif) pour développer [les mots](#mots) et faire vivre la [langue française](#langue).


# Dictionnaire <a id="dictionnaire"></a>

Le Dictionnaire des francophones est un ouvrage singulier. En effet, il se rapproche et s’inspire fortement des dictionnaires sans en être un. Pourquoi cela ? 
 
Un dictionnaire est un ouvrage de référence contenant un ensemble de mots d’une langue ou d’un domaine d’activité, généralement rangés par ordre alphabétique et définis, expliqués ou associés à d’autres mots. Un ouvrage de référence est un texte qui a pour but de renseigner ou d’enseigner. On peut tout d’abord dire que le Dictionnaire des francophones n’a pas pour objectif premier d’enseigner la langue, mais plutôt d’inventorier les mots du français du monde entier, pour les décrire. Il ne s’agit pas de prescrire un français correct qui devrait être utilisé plutôt qu’un autre, il s’agit de rendre compte de l’usage.

# Un point sur les données ouvertes <a id="ouvert"></a>

Les données ouvertes ou *open data* sont des données numériques dont la structure est documentée publiquement et qui sont encadrées par une licence garantissant leur réutilisation sans restriction technique, juridique ou financière. L’ouverture des données s'inscrit dans une tendance qui considère l'information comme un bien commun. La majeure partie des données du Dictionnaire des francophones sont publiées comme données ouvertes.

# Perspective francophone  <a id="francophone"></a>  
 
Dans le Dictionnaire des francophones, toutes les variétés du français sont présentées ensemble et placées sur un pied d’égalité. Chacune de ces variétés est une norme au sein de la région de la francophonie dans laquelle elle est parlée. 
 
Le terme francophonie, ou espace francophone, désigne l’ensemble des personnes et institutions qui utilisent le français que ce soit en tant que langue maternelle (première langue apprise par l’enfant), langue d’usage (standard, utilisée dans la vie de tous les jours sans forcément être maternelle), administrative (utilisée par les administrations et dans les documents officiels), d’enseignement (utilisé par les personnels enseignants) ou langue officielle (utilisée dans la vie publique, à chaque moment de la vie et dans chaque domaine). Est donc francophone toute personne ou institution utilisant le français, que ce soit partiellement ou majoritairement. 
Le nombre de ces francophones s’élève à plus de 300 millions de personnes dans le monde, dont environ 240 millions qui en font quotidiennement usage, de la Polynésie française au Canada en passant par le Sénégal. Le français est donc présent un peu partout autour du globe, de manière totale ou partielle. Ces chiffres rendent compte de l’ampleur et de la richesse de la langue française dans le monde, qui présente une quantité importante de variétés. Rendre compte de tout cela n’est pas tâche aisée, c’est pourquoi le Dictionnaire des francophones compte sur l’apport des utilisateurs pour s’enrichir et proposer la plus grande palette d’information possible. 


# Origine du Dictionnaire des francophones <a id="institutionnel"></a>

Ce projet émane de rencontres entre la Délégation générale à la langue française et aux langues de France et l’Institut international pour la Francophonie.  

Crédits 
* Pilotage et Financement : Ministère de la Culture (Délégation générale à la langue française et aux langues de France)
* Direction opérationnelle : Institut international pour la Francophonie, composante de l’Université Jean Moulin Lyon 3
* Conseil scientifique : Bernard Cerquiglini, Claudine Bavoux, Aïcha Bouhjar, Barbara Cassin, Moussa Daff, François Grin, Katia Haddad, Latifa Kadi-Ksouri, Jean-Marie Klinkenberg, Jérémie Kouadio, Mona Laroussi, Lucas Lévêque, Franck Neveu, Jean Tabi Manga, Ginette Galarneau
* Équipe de conception : Noé Gasparini, Kaja Dolar, Marie Steffens, Antoine Bouchez
* Conception de l’ontologie : Jean Delahousse et Noé Gasparini
* Création du logo et habillage du site : Alexandra Simon
* Design UX : Jérémie Cohen
* Explications lexicographiques : Antoine Bouchez
* Équipe de développement : Mnémotix  (Freddy Limpens, Nicolas Delaforge, Mylène Leitzelman, Pierre-René Lherisson, Mathieu Rogelja, Quentin Richaud) 
* Équipe à l’Institut international pour la Francophonie : Marielle Payaud, Sandrine David, Sébastien Gathier, Camelia Danc, Aurore Sudre, Hong Khanh Dang
* Équipe à la Délégation générale à la langue française et aux langues de France : Paul de Sinety, Jean-François Baldi, Thibault Grouas, Paul Petit, Annick Lederlé, Claire-Lyse Chambon, Julie Ménez
* Hébergement : Huma-Num, infrastructure CNRS


# Présentation des ressources <a id="ressources"></a>
 
Le Dictionnaire des francophones a la particularité d’intégrer plusieurs ressources en son sein : le Wiktionnaire francophone et l’Inventaire des particularités lexicales du français en Afrique noire. 

Le Wiktionnaire se définit lui-même comme un “projet lexicographique collaboratif accessible par internet, hébergé par la Wikimedia Foundation, sous licence libre, visant à décrire dans toutes les langues tous les mots”. Le Dictionnaire des francophones n’intègre que la partie francophone du projet, les mots de français décrits en français. C’est une ressource en cours d’enrichissement mais qui présente déjà un grand nombre d’informations précieuses et récentes. Il est d’autant plus intéressant qu’il est entièrement collaboratif, c’est à dire rédigé et organisé par des bénévoles, fonctionnement qui se rapproche de celui du Dictionnaire des francophones. De nombreux principes sont communs entre les deux projets, qui se veulent complémentaires. 
    
L’Inventaire des particularités lexicales du français en Afrique noire est un ouvrage qui fait la synthèse des lexiques et inventaires régionaux de onze pays d’Afrique francophone : Côte d’Ivoire, Togo, Bénin, République démocratique du Congo (Zaïre dans la publication originale), Tchad, Sénégal, Niger, Rwanda, Centrafrique, Cameroun et Burkina Faso. Il est le fruit du travail de plus de vingt linguistes, pendant plus de dix ans (de 1977 à sa publication en 1988). Il est destiné à un public plutôt versé et impliqué dans l’étude des langues, ce qui en fait donc un ouvrage très sérieux, en plus d’être très fourni. Grâce à l’accord de l’Agence Universitaire de la Francophonie pour sa diffusion, il gagne une nouvelle vie au sein du Dictionnaire des francophones.


# Un dictionnaire participatif  <a id="participatif"></a>
 
Par participatif on entend un dictionnaire auquel tout un chacun peut participer à sa manière en ajoutant des entrées, des exemples, en participant aux espaces de discussions, en signalant ou en validant des informations. Cette contribution est encadrée, d’une part, par la structure du Dictionnaire des francophones que ses utilisateurs ne peuvent pas modifier et, d’autre part, par des personnes reconnues par la communauté qui forment deux groupes d’utilisateurs dédiés à la relecture,  les opérateurs et opératrices qui peuvent solliciter la suppression d’un contenu indésirable et les admins qui peuvent apporter des corrections ou supprimer des participations inappropriées.
La contribution ne peut se faire que par l’ajout d’information. C’est à dire qu’une information déjà présente ne peut être modifiée ou supprimée, ceci pour éviter des corrections abusives qui résulteraient de simples divergences d’opinion. Seuls les utilisateurs ayant le statut d’opérateurs sont habilités à supprimer des informations erronées.
    
Les espaces de discussion sont des zones d’échanges qui ont pour objectif d’apporter un supplément d’informations non directement présentes dans les articles du Dictionnaire des francophones. Il en existe trois : un espace de discussion réservé à l’étymologie, un espace réservé à la forme et un espace réservé au sens et à l’usage.
    
Que ce soit dans l’ajout d’information ou dans la discussion, il est possible de signaler des messages jugés abusifs afin de faire savoir aux opérateurs que cet article devrait être relu. Au contraire, il est également possible de valider une entrée afin d’indiquer qu’elle est tout à fait correcte et intéressante, et ainsi la mettre en avant.


# Choix des entrées <a id="mots"></a>
 
Le Dictionnaire des francophones se veut complet et représentatif de la langue française. C’est pourquoi il présente plus de 400 000 mots pour plus de 600 000 définitions. Et ce nombre ne demande qu’à augmenter, grâce à l’aide de son lectorat actif. Tous les mots, tant qu’ils appartiennent à la langue française, peuvent y figurer. On trouvera donc des mots de nombreux pays francophones différents, des mots de la langue courante et des termes techniques, des insultes comme des compliments, des mots nouveaux aussi bien que vieux, et même des abus de langage ! Un même mot peut être écrit de diverses manières et chacune de ces formes pourra bénéficier d’une entrée distincte, dès lors qu’une existence est prouvée par des exemples d’usage.


# Précisions sur la langue  <a id="langue"></a>

Une langue est un bien collectif dont les règles d’utilisation s’imposent aux utilisateurs de cette langue sous peine d’incompréhension entre eux. L’ensemble des individus parlant cette langue est appelé communauté linguistique. Une langue parlée dans un pays ou territoire entier est appelée langue nationale par opposition à la langue régionale qui n’est parlée que dans une seule région. Par exemple, le français est la langue nationale de la France mais le territoire français est parsemé de langues régionales comme le breton ou le basque.

Attention, il ne faut pas confondre les langues régionales et les dialectes d’une langue. On observe une hiérarchie  : une langue régionale n’est comprise que par ses locuteurs. Mais c’est différent pour les dialectes. Un dialecte est une variété de langue parlée sur un territoire restreint, comme la langue régionale. En revanche, un dialecte fait partie d’une langue, on parle de dialecte d’une langue, il est donc plus spécifique que la langue.

De plus, un dialecte est en partie compréhensible par les gens parlant la langue à laquelle ce dialecte appartient. Il est également important de préciser que ces langues régionales et dialectes du français influencent le français puisque certains de leurs termes entrent dans le langage courant. Par exemple, les mots français *bretzel* et *bugne* viennent respectivement de l’alsacien et du parler lyonnais, deux dialectes du français. De la même manière, le mot *abertzale* vient du basque, langue régionale du sud-ouest de la France.

Le nombre et les noms des dialectes du français sont sujets à discussion. Ils sont associées à des pays, à des régions historiques ou actuelles ou bien à des villes. 

Pour conclure quant aux langues, il paraît important d’aborder la question des créoles. Les créoles sont des langues à part entière parlées par un grand nombre de personnes dans le monde, principalement en Guyane, Louisiane et dans l’Océan Indien. Les créoles sont différents en fonction des influences qui ont conduit à leur création (créole à base lexicale anglaise, française, portugaise…). Les créoles à base lexicale française utilisent des mots issus de la langue française. Pour autant, puisque les créoles sont des langues autonomes et pas des variétés de français, ils ne figurent pas dans le Dictionnaire des francophones. Cependant, certains mots de créoles entrés dans la langue française peuvent y figurer comme faisant partie de cette langue. C’est notamment le cas des mots anse (plage de sable), boudin (plat au sang de cochon), cabri(t) (chèvre).

## Pour aller plus loin
* Zevaco, Claudine. *Guide du français pour tous. Le livre de la francophonie*. L’Harmattan. 2000. 263 p. ISBN  : 2-7384-8452-2
* Organisation Internationale de la francophonie. *La langue française dans le monde*. Gallimard. 2019. 365 p. ISBN  : 978-2-07-278683-9
* Masseaut, Jean-Marc. *Creolization in the French Americas*. University of Louisiana. 2016. 286 p. p 139-231. ISBN  : 978-1-935754-68-8 
* Guenin-Lelle, Dianne. *The Story of French New Orleans. History of a Creole City*. University Press of Mississippi. 2016. 226 p. Chapter 3. ISBN : 978-1496804860
* Agora Francophone Internationale. *L’Année Francophone Internationale*. 1991-2019.
* Kilanga Musinde, Julien. *Langue française en Francophonie*. L’Harmattan. Collection “Savoirs”. 196 p. ISBN : 978-2-296-10754-0

# Historique des versions
La conception du Dictionnaire des francophones a débuté en novembre 2018. Le développement a débuté en mars 2019. La première version de consultation a été mise en ligne en octobre 2019. 

Voici l’évolution des versions depuis : 
* 4 octobre 2019 : version interne de consultation mobile et desktop.
* 18 décembre 2019 : diffusion aux partenaires de la première version.
* 20 mars 2020 : diffusion publique de la deuxième version, permettant la contribution.
