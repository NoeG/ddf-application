# Comprendre un article de dictionnaire

* [Présentation générale](#presentation)
* [Définition](#definition)
* [Exemples](#exemple)
* [Indication géographique](#geographie)
* [Classes de mots](#classe)
* [Types de phrases](#phrases)
* [Genre et nombre](#genre)
* [Personnes](#personne)
* [Temps](#temps)
* [Contraintes grammaticales](#grammaire)
* [Genre textuel](#genre-textuel)
* [Transitivité](#transitivite)
* [Flexions](#flexions)
* [Domaines](#domaines)
* [Listes thématiques](#listes-thematiques)
* [Temporalité](#temporalite)
* [Fréquence](#frequence)
* [Registre](#registre)
* [Connotation](#connotation)
* [Argot](#argot)
* [Relations de sens entre les mots](#relations-entre)
* [Relations entre les sens d’un même mot](#relations-en)


# Présentation générale <a id="presentation"></a>
Une entrée de dictionnaire contient beaucoup d’informations, et cette page va détailler chacune de celles-ci. La définition est centrale mais la première information présentée est l’information géographique où est utilisé le sens défini. On trouve ensuite la nature grammaticale du mot, de la locution ou de la partie de mot (préfixe ou suffixe). Sur la ligne au-dessus de la définition, on trouve une liste d’étiquettes de marques qui précisent le sens. 

Sous la définition, on trouve d’abord des exemples d’usage du mot dans le sens présenté. En dessous sont présentés d’autres mots en relation avec le mot.

En plus de ces informations, trois champs de discussions sont proposés, pour des commentaires plus détaillés : forme du mot (sonore et écrite), histoire du mot (étymologie), sens et usage.

# Définition <a id="definition"></a>

Une définition présente le sens d’un mot. Elle est courte et peut intégrer des liens vers d’autres mots qui pourraient être nécessaires pour définir le sens. 

# Exemples <a id="exemples"></a>

L’exemple est une présentation des emplois réels du mot dans un discours. Les exemples peuvent provenir de toutes origines, littéraires comme populaires. 

# Indication géographique <a id="geographie"></a>

L’espace francophone est vaste et divers. Alors que certains pays entiers parlent le français, certaines zones de la francophonie sont limitées à une communauté isolée au sein d’une communauté multiculturelle. C’est pourquoi, afin d’identifier les lieux où certains mots de la francophonie sont utilisés, et pour rendre compte de l’importance de la francophonie dans le monde, il est important de marquer géographiquement les mots, leurs définitions et les exemples. Certains termes ne sont utilisés que dans certaines zones géographiques ou ont une définition propre à cette zone, tel le verbe *caser* qui a le sens de *séduire* au Burkina Faso mais pas en France.  

Ces indications géographiques peuvent être de différentes natures mais le but est de refléter la réalité de la langue de la manière la plus juste et précise possible. C’est pourquoi trois types d’indications principales seront proposées : le pays, la région et la commune. Par région on entend également province, État, cantons et territoires. En réalité, un niveau de précision supplémentaire pourrait exister, la communauté ou la tribu notamment ; cependant, non seulement cette classification est presque impossible à mettre en œuvre, mais elle surchargerait également les informations. Nous nous limiterons donc à la commune. Pour la communauté, l’indication d’un argot lié à un groupe social, ou l’indication d’un usage très familier, associés à un usage régional vont tracer les contours d’usages très localisés. 


# Classes de mots <a id="classe"></a>

Dans le Dictionnaire des francophones, on distingue trois grands types d’entrées : les mots, les groupes de mots et les parties de mots (préfixes et suffixes). Seuls les mots ont une classe de mot. Les classes de mots comprennent :
- Le **verbe** : exprime une action, un procès, un état ou un devenir. Les différents types de verbes sont détaillés dans la section [transitivité](#transitivite). Exemples : Manger, boire. 
- L’**adverbe** : exprime les circonstances ou le degré de déroulement de l’action, permet de faire un commentaire... Exemples : Sérieusement, souvent, juste, franchement.
- Le **nom** : désigne une réalité concrète ou abstraite. Exemples : chat, femme, mur, temps.
- Le **nom propre** : désigne quelque chose d’unique ou une personne nommée. Exemples : Les noms de pays ou les noms de famille.
- L’**adjectif** : modifie ou caractérise le nom auquel il est associé. Exemples : vieux, grand, rapide.
Il existe différents types d’adjectifs :
    - L’**adjectif démonstratif** : montre le nom auquel il est associé. Exemples : ce, ces.
    - L’**adjectif indéfini** : indique le caractère vague, incertain du nom auquel il est associé. Exemples : aucun, quelconque.
    - L’**adjectif exclamatif** : introduit une phrase exclamative. Exemples : quel, quelles.
    - L’**adjectif interrogatif** : interroge sur le nom associé. Exemples : combien, que.
    - L’**adjectif numéral cardinal** : indique le nombre ou la quantité du nom associé. Exemples : deux, treize.
    - L’**adjectif numéral ordinal** : indique l’ordre, le rang du nom associé. Exemples : premier, quinzième.
    - L’**adjectif possessif** : informe sur le possesseur du nom associé. Exemples : ses, mon, ta.
- Le **pronom** : remplace un nom, un groupe nominal, un adjectif ou un groupe prépositionnel. Il existe différents types de pronoms :
    - Le **pronom démonstratif** : exprime le fait de montrer. Exemples : ceci, ce.
    - Le **pronom indéfini** : remplace un élément incertain, vague. Exemples : personne, rien, tout.
    - Le **pronom interrogatif** : sert à poser une question. Exemples : lequel, qui.
    - Le **pronom possessif** : indique le possesseur de l’objet. Exemples : le mien, les vôtres.
    - Le **pronom personnel** : remplace un nom de personne. Exemples : je, tu, nous.
    - Le **pronom relatif** : introduit une proposition subordonnée relative. Exemples : que, qui, dont.
- L’**article** : individualise ou généralise un nom. Il en existe trois types en français :
    - L’**article défini** : individualise un nom. Exemples : le, la, les.
    - L’**article indéfini** : généralise un nom. Exemples : un, une, des.
    - L’**article partitif** : devant indénombrable ou chose abstraite. Exemples : du, de la.
- Les mots-outils, qui ont surtout une fonction grammaticale sont de quatre types :
    - La **préposition** : mot invariable qui permet de relier des mots pour créer des constructions plus grandes. Exemples : devant, hors, pendant.
    - La **conjonction** : lie deux propositions. Il en existe deux types, les conjonctions de coordination (mais, où, et…) et les conjonctions de subordination (puisque, comme, si…).
    - L’**interjection** : mot ou phrase qui exprime une émotion. Exemples : Boum !, Zut !.
    - La **particule** : petit mot-outil. Exemples : ne, y.

Pour aller plus loin : Riegel Martin, Pellat Jean-Christophe, Rioul René. *Grammaire méthodique du français*. 7e édition 2018. 1168 p. ISBN : 978-2130627562
 
# Types de locutions <a id="phrases"></a>

Dans le Dictionnaire des francophones, on distingue trois grands types d’entrées : les mots, les groupes de mots et les parties de mots (préfixes et suffixes). Les groupes de mots s’appellent des locutions. Une **locution** est un groupe de mots figé ou en tout cas très stable qui se comporte comme un seul mot. Cela signifie qu’on ne peut pas enlever ou changer un élément d’une locution (dans les locutions verbales, le verbe peut se conjuguer). On distingue différents types de locutions selon la classe du mot qui est à la tête du groupe de mot. 
- La **locution adjectivale** : groupe de mots remplissant le rôle d’adjectif. Exemples : à l’heure, bleu marine.
- La **locution nominale** : groupe de mots remplissant le rôle de nom. Exemples : accent aigu, canne à pêche.
- La **locution prépositionnelle** : groupe de mots remplissant le rôle de préposition. Exemples : au final, par rapport.
- La **locution verbale** : groupe de mots remplissant le rôle de verbe. Exemples : aller bien, en avoir assez.
- La **locution adverbiale** : groupe de mots remplissant le rôle d’adverbe. Exemples : 24 heures sur 24, mot à mot.
- la **locution pronominale** : groupe de mots remplissant le rôle de pronom. Exemples : tous les deux, Votre Altesse. 
- La **locution conjonctive** : groupe de mots remplissant le rôle de conjonction. Exemples : tant que, même si.
- La **locution interjective** : groupe de mots remplissant le rôle d’interjection. Exemples : au revoir, ça marche.
- La **locution-phrase, proverbe, dicton** : groupe de mots exprimant une vérité, un conseil. Exemples : eau de jouvence, plus facile à dire qu’à faire.
- La **locution interrogative** : groupe de mots introduisant une question. Exemples : est-ce que, qu’est-ce que.

Pour aller plus loin : Riegel Martin, Pellat Jean-Christophe, Rioul René. *Grammaire méthodique du français*. 7e édition 2018. 1168 p. ISBN : 978-2130627562

# Genre et nombre <a id="genre"></a>

Le genre grammatical est une caractéristique de chaque nom en français, qui peut être **masculin**, **féminin** ou **épicène**. Pour les êtres animés, la plupart du temps le genre grammatical reflète le genre réel ou le sexe. Par exemple, actrice est féminin et désigne une personne de sexe féminin et acteur est masculin et désigne un individu de sexe masculin. Il existe quelques rares exceptions comme *une sentinelle*, mot féminin qui désigne aussi bien un homme qu’une femme ou *un mannequin* nom masculin qui peut désigner une femme. Pour les noms d’objets, cette classification en masculin et féminin est bien plus arbitraire puisque les objets ne sont pas sexués.

Ce genre grammatical a une influence sur la forme des autres classes de mots qui s’accordent avec le nom. Par exemple, l’adjectif *grand* s’écrit différemment dans « cet homme est grand » et « cette femme est grande ». De même le nom lui-même varie en fonction du genre du référent, par exemple le nom **masculin** *étudiant* a pour équivalent **féminin** *étudiante*. Dans le Dictionnaire des francophones, ces deux noms forment des entrées distinctes, permettant de mieux les décrire et d’y associer des exemples spécifiques.

Certains mots sont **épicènes**, c’est-à-dire que leur forme ne varie pas selon le genre. Par exemple les mots *archéologue*, *svelte* et *les* (l’article pluriel) s’écrivent toujours de cette manière, qu’ils désignent une femme ou un homme.

Dans certains cas, le genre du nom n’est pas clairement identifié, dans ce cas on trouvera l’indication **l’usage hésite**.  C’est notamment le cas du mot *réglisse* puisqu’on peut dire *le* réglisse ou *la* réglisse. 

Enfin, certains types de mots comme les verbes, les adverbes, les prépositions ou les conjonctions ne possèdent pas de genre et ne s’accordent pas non plus. Aucune indication de genre n’est alors donnée.

La catégorie grammaticale du nombre comprend les valeurs principales de **singulier**, **pluriel** et **ne varie pas en nombre**. Un mot est au **singulier** quand il ne désigne qu’une seule chose, au **pluriel** quand il en désigne plusieurs.  

Certains mots s’emploient **uniquement au singulier**, comme *le vivant* utilisé pour désigner l’ensemble des êtres vivants. De même certains mots comme *lunettes* (pour désigner l’objet qu’on porte pour mieux voir) s’emploient **uniquement au pluriel**.

Finalement, des mots sont également **invariables** quand ils ne changent pas ni en genre ni en nombre (verbes, adverbes, prépositions et conjonctions).

Pour aller plus loin : Riegel Martin, Pellat Jean-Christophe, Rioul René. *Grammaire méthodique du français*. 7e édition 2018. 1168p. ISBN : 978-2130627562


# Personne grammaticale <a id="personne"></a>
 
Il existe six personnes grammaticales en français :
 
* La première personne du singulier, le « je », qui désigne la personne qui parle.
* La deuxième personne du singulier, le « tu » qui désigne la personne à qui l’on s’adresse. 
* La troisième personne du singulier, les « il, elle, on », qui désigne la personne ou la chose dont on parle. 
* La première personne du pluriel, le « nous », qui désigne le groupe de personnes dont la personne qui parle fait partie. 
* La deuxième personne du pluriel, le « vous », qui désigne le groupe de personnes à qui l’on parle ou qui peut être utilisé à la place du « tu » en cas de discussion formelle. 
* La troisième personne du pluriel qui désigne le groupe de personnes ou l’ensemble des choses dont on parle.


# Temps et mode du verbe <a id="temps"></a>

Il existe cinq **temps** en français :

- Le **présent**, qui a pour fonction première de placer l’action/le fait dans le moment de la parole.  Il permet aussi d’énoncer une habitude, une vérité générale, une action passée depuis peu ou qui va bientôt avoir lieu.
Ex : Je mange des céréales tous les matins.

- Le **passé simple**, qui a pour fonction d’indiquer que l’action/le fait est passée et terminée. Il permet également d’énoncer des actions brèves ou soudaines dans le passé. En littérature, le passé simple est le temps du récit, il sert à dérouler l’histoire.
 Ex : En entrant dans la maison, il enleva son manteau.
 
- L’**imparfait**, qui pour fonction première d’indiquer que l’action/le fait a commencé dans le passé, mais contrairement au passé simple, elle/il peut encore être en cours. Il permet également d’énoncer une habitude dans le passé ou une action en cours dans le passé. En littérature, l’imparfait est le temps de la description, il sert à dessiner le contexte de déroulement de l’histoire.
Ex : Il était marin avant de devenir pompier.
 
- Le **futur**, qui a pour fonction première de placer l’action dans l’avenir. Il permet donc de parler de projets lointains ou de donner des ordres de manière plus polie.
Ex : Un jour je serai le meilleur dresseur.
 
- Le **participe passé**, qui est une forme du verbe similaire qui a une fonction d’adjectif.
Ex :  Dans la phrase Il est fatigué, « fatigué » caractérise « Il » comme un adjectif alors que c’est en réalité une forme du verbe fatiguer.
 

Ces temps sont toujours associés à un mode et il y en a cinq aussi :
 
- L’**indicatif**, qui permet d’inscrire l’action dans le monde réel de la personne qui parle.
Ex : Je sortis de table à la fin du repas.
- Le **subjonctif**, qui permet d’exprimer un fait pensé ou imaginé, donc irréel au moment de l’énonciation et qui se construit à l’aide du pronom relatif que ou qu’.
Ex : Il ira à l’hôpital jusqu’à ce qu’il aille mieux.
- Le **conditionnel**, qui permet d’exprimer un souhait, une hypothèse ou une condition.
Ex : J’irais bien à la piscine ce week-end.
- L’**impératif**, qui permet de conseiller ou de donner un ordre.
Ex : N’oublie pas de ranger ta chambre !
- Le **gérondif**, qui exprime la simultanéité d’un fait qui a lieu dans le cadre d’un autre fait et se construit avec la préposition *en*. 
Ex : Prends ton sac en partant !
 
Pour aller plus loin : 
-Riegel Martin, Pellat Jean-Christophe, Rioul René. *Grammaire méthodique du français*. 7e édition 2018. 1168 p. ISBN : 978-2130627562

# Contraintes grammaticales <a id="grammaire"></a>

Les **contraintes grammaticales** indiquent les contraintes de construction qui s’imposent au mot dans une phrase. Le marqueur **en apposition** sert à définir la place du mot après un nom (juste après le nom en l’occurrence). Le marqueur **employé comme épithète** indique que le mot est utilisé comme adjectif dans ce cas. Par exemple, un *bruit monstre* est un *bruit assourdissant*, et le nom « monstre » se comporte bien comme l’adjectif « assourdissant », il ne change cependant pas de forme au pluriel. La différence entre les deux marqueurs concerne la différence de nature des objets apposés. Le marqueur **en apposition** peut porter sur différents types de mots alors que le marqueur **employé comme épithète** ne concerne que les noms.

Le marqueur **absolument** indique que le mot est utilisé sans complément alors qu’il l’est dans d’autres usages. Par exemple, lorsqu’on utilise « boisson » dans la phrase « j’arrête la boisson », on signifie qu’on veut arrêter de consommer trop de boissons alcoolisées alors que boisson avec n’importe quel complément désignera ce qui se boit en général.

Le marqueur **au pluriel **indique que le mot a un sens particulier quand il est au pluriel (et que ce sens n’existe qu’au pluriel). Par exemple, un jour est une durée et on parle de *jours* pour désigner la vie comme dans l’expression « mettre fin à ses jours ». 

Le marqueur **par ellipse** indique que le mot décrit fait partie d’un ensemble mais que les autres mots de l’ensemble ont été omis par souci de raccourci. Par exemple, on parle souvent d’*anémone* pour désigner une *anémone de mer*. C’est donc au lecteur ou à la lectrice de reconstituer l’ensemble. Contrairement à l’absence de complément du marqueur **absolument**, l’ellipse ne change pas le sens du mot.
 

Pour aller plus loin : 
- Riegel Martin, Pellat Jean-Christophe, Rioul René. *Grammaire méthodique du français*. 7e édition. 2018. 1168 p. ISBN : 978-2130627562
- Ricalens-Pourchot, Nicole. *Dictionnaire des figures de style*. 2e édition. Armand Colin, 2016. 224 p. ISBN : 978-2200614775

# Genre textuel <a id="genre-textuel"></a>

Les **genres textuels** correspondent aux types de textes dans lesquels certains mots apparaissent préférentiellement. On en a retenu cinq principaux qui correspondent aux cinq types de textes majeurs et bien distincts : les textes **littéraires** (qui s’inscrivent dans la tradition littéraire), **poétiques** (de la poésie), **didactiques** (qui ont vocation à instruire), **administratifs** (comme les documents officiels) et **scientifiques** (revues ou compte-rendu d’étude). Par exemple, la formule « je soussigné » est propre aux textes administratifs.

# Transitivité <a id="transitivite"></a>

La **transitivité** est une information qui ne porte pas sur le sens d’un mot mais sur la façon dont il s’utilise dans la langue. Elle désigne l’aptitude d’un verbe à s’entourer de différents éléments, ces éléments étant les compléments d’objets et le sujet. Un sujet est la personne ou la chose réalisant l’action, par exemple « le chien » est le sujet de la phrase « le chien aboie ». Le complément d’objet est la personne ou l’objet sur lequel/laquelle porte le verbe. Par exemple dans la phrase « Il voit un lapin », « un lapin » est le complément d’objet.  En grammaire, on distingue deux types de compléments d’objet, les compléments d’objets directs (COD) et les compléments d’objets indirects (COI). La différence est due à la présence d’une préposition obligatoire pour les COI. Pour identifier un COD il suffit de se poser la question « Que/qui verbe le sujet ? ». La réponse à cette question est le COD de la phrase (« Que voit-il? Un lapin »). Pour identifier un COI, il suffit de se poser la question « A quoi/qui verbe le sujet ? ». La réponse à cette question est le COI de la phrase (« Il parle à Henri. - À qui parle-t-il ? - À Henri).   

Un verbe **transitif direct** est un verbe qui est accompagné d’un complément d’objet direct alors qu’un **transitif indirect** est accompagné d’un complément d’objet indirect. Un verbe **intransitif** est un verbe qui ne peut avoir aucun complément d’objet. Par exemple, le verbe vivre est intransitif, *manger* est transitif direct (on mange quelque chose) et *plaire* est transitif indirect (on plaît à quelqu’un). 

Un verbe **ditransitif** est un verbe qui s’accompagne de deux compléments. *Donner* est ditransitif, on donne quelque chose à quelqu’un, on a bien un COD (quelque chose) et un COI (à quelqu’un).

Un verbe **impersonnel** est un verbe qui s’accompagne d’un sujet qui ne renvoie à rien, ce n’est ni une personne ni un objet. En français, le pronom sujet impersonnel est *il*. Dans la phrase « il pleut », le « il » est le sujet grammatical. Il ne renvoie à personne, il est juste là pour faire fonctionner la phrase.

Un verbe **pronominal** est un verbe qui s’accompagne d’un pronom en plus du sujet (le pronom est placé entre le verbe et son sujet).  On en observe trois types :
- Le verbe pronominal **réfléchi** pour lequel le sujet exerce l’action décrite par le verbe sur lui-même. Par exemple, dans « il se lave », « se » est le pronom réfléchi qui indique que la personne qui lave « lave elle-même ».
- Le verbe pronominal **réciproque** qui exprime une action qu’un sujet pluriel ou collectif exerce sur l’autre objet ou personne désignée par le sujet tandis que ce pronom exerce la même action sur le sujet. Par exemple, dans « ils se battent », « ils » désigne tous les partis qui sont en train de se battre.
- Le verbes pronominaux **à la voix passive** dont le sujet subit l’action décrite par le verbe alors qu’il ne l’exécute pas lui-même. Par exemple, dans « le film s’est terminé brusquement », le film n’accomplit pas lui-même l’action de se terminer. Un verbe peut également être à la voix passive sans être pronominal, par exemple, dans « ce livre a été écrit par un grand écrivain ». Dans cette phrase le sujet est bien « ce livre » mais ce n’est pas lui qui exécute l’action, c’est le « grand écrivain ».

Pour aller plus loin : Riegel Martin, Pellat Jean-Christophe, Rioul René. *Grammaire méthodique du français*. 7e édition 2018. 1168p. ISBN : 978-2130627562

# Flexions <a id="flexions"></a>

Dans le Dictionnaire des francophones, les formes fléchies des mots sont indiquées dans des entrées dédiées. Une forme fléchie, ou flexion, est une forme d’un mot modifiée par le contexte, la syntaxe. Par exemple, un adjectif portera la marque du féminin si le nom associé est féminin et un nom portera la marque du pluriel s’il désigne plusieurs choses. En français, il y a cinq facteurs qui peuvent entraîner la flexion d’un mot : le [genre](#genre) (pour les adjectifs), le [nombre](#genre) (pour les noms et adjectifs), la [personne grammaticale](#personne) (pour les verbes), le [temps](#temps) (pour les verbes) et le [mode](#temps) (pour les verbes).

# Domaines <a id="domaines"></a>

Le domaine est le champ de spécialité auquel un mot ou le sens d’un mot appartient. Par exemple, *félin* désigne un mammifère, il appartient donc au domaine de la mammalogie, l’étude des mammifères. L’appartenance à un domaine particulier a une influence sur la définition puisque le sens de *champignon* en *cuisine* n’est pas le même que le sens de *champignon* en *botanique*. Le Dictionnaire des francophones présente beaucoup de domaines, bien plus que dans un dictionnaire classique. Le but est de classer chaque mot dans un domaine afin de pouvoir constituer des dictionnaires spécialisés, et également de créer des listes organisées de termes. Tous ces domaines sont eux-mêmes placés au sein d’autres domaines afin d’obtenir une hiérarchie logique et représentative de la réalité et de faciliter la recherche de mots grâce à l’indication du domaine. Pour l’établissement de la liste des domaines, l’équipe du Dictionnaire des francophones s’est appuyée sur la liste des catégories du *Wiktionnaire* qui était déjà bien détaillée. Finalement, ce qui a été conservé est ce qui nous semblait organiser au mieux le contenu. 

Certains domaines ont donc été supprimés et beaucoup d’autres ont été rajoutés (par exemple, *agriculture* était sous *cuisine*, *alimentaire* a été ajouté pour corriger cette organisation erronée, ou du moins pas assez précise). Les ajouts effectués par notre équipe (notamment *sport*, *jeux-vidéo* et *sciences* où il manquait de nombreux éléments) et la résolution des problèmes d’organisation ont été en partie guidés par l’étude du *Larousse illustré 2014*, du *Dictionnaire universel* de Hachette, du *Guide de la classification décimale* de Dewey et de l’ontologie du *Grand Dictionnaire Terminologique*. Il a ensuite fallu regrouper les domaines dans des catégories plus larges, donc par exemple, *entomologie* dans *zoologie* qui est lui-même dans *biologie* qui est dans sciences afin de construire un inventaire hiérarchisé. Cela a mené à des questionnements intéressants, comme la place de *bowling* dans *jeu* ou dans *sport*, la place de *chasse* dans *sport* ou *loisirs* et à ce niveau des choix ont dû être faits, faute de distinction claire. Dans la mesure du possible, chaque domaine est contenu dans un domaine, deux au maximum lorsqu’on ne pouvait pas faire autrement. 

Au sommet de la hiérarchie des domaines on retrouve neuf méta-catégories, trop générales pour que les catégoriser elles-mêmes ait du sens :
- **alimentaire** qui regroupe tout ce qui a trait à l’alimentation, de la production à la transformation (cuisine, agriculture, saliculture…)
- **art** qui regroupe ce qui concerne la création d’objets d’art grâce à une habileté particulière (artisanat, arts visuels, musique…)
- **technique** qui traite des procédés de fabrication grâce à des machines (robotique, informatique, usinage…)
- **sciences** qui traite des sciences dites naturelles (sciences de la terre, mathématiques, biologie…)
- **sciences humaines et sociales** qui regroupe tout ce qui concerne l’étude des différents aspects de la réalité humaine (économie, histoire, linguistique…) 
- **médias** qui regroupe les moyens de diffusion de l’information (télévision, radio, multimédia…)
- **société** qui regroupe tout ce qui a trait à la vie en communauté (vie domestique, urbanisme, loisirs…)
- **sport** qui regroupe toutes les activités du corps qui sont représentées au niveau professionnel (sports nautiques, sports aériens, sports de raquette…)
- **politique** qui regroupe tout ce qui concerne l’organisation et la régulation de la société (diplomatie, socialisme, monarchie…)

Pour aller plus loin :
Vézina, Robert. *La rédaction de définitions terminologiques*. 2009. Office québécois de la langue française [En ligne](https://www.oqlf.gouv.qc.ca/ressources/bibliotheque/terminologie/redaction_def_terminologiques_2009.pdf)


# Listes thématiques <a id="listes-thematiques"></a>

Les listes thématiques sont des listes de termes qui renvoient à une même notion, à un thème. C’est un niveau de tri qui intervient après le tri en domaines. Il y a donc beaucoup plus de thèmes que de domaines. Ces thèmes sont d’ailleurs placés au sein d’autres thèmes plus généraux puis au sein des domaines afin de faciliter la consultation. Par exemple, le mot-vedette *corbeau* sera contenu dans le thème *oiseaux* lui-même contenu dans le domaine *ornithologie* lui-même contenu dans *zoologie*, lui-même contenu dans *sciences*. Tout ceci permet à l’utilisateur d’effectuer des recherches par listes, que ce soit en thème ou en domaine.    

Pour obtenir ces listes thématiques nous avons procédé de la même manière qu’avec les domaines. Nous avons récupéré les listes thématiques du Wiktionnaire et avons trié l’ensemble. Davantage de listes ont été supprimées qu’ajoutées cette fois. Les thématiques majoritaires concernent les pays ou les sciences mais nous avons conservé des catégories moins « sérieuses », notamment le thème *curiosités linguistiques* qui contient des informations telles que les jeux de mots ou les mots sans consonnes.

La recherche au sein de ces listes n’est pas disponible dans la première version du Dictionnaire des francophones.

# Temporalité <a id="temporalite"></a>

La **temporalité** désigne l’usage du mot-entrée dans l’axe du temps. Le mot peut être **archaïque** s’il est extrêmement vieux (et donc si l’objet qu’il désigne n’existe plus, par exemple la locution *revendeuse à la toilette*) mais également **désuet** si le mot en lui-même est obsolète et donc plus utilisé du tout. Un terme archaïque ou vieux est un terme qui n’est plus clairement compris et donc très rarement produit de manière spontanée, surtout à l’oral ; l’intention d’archaïsme étant plus souvent présente dans les textes littéraires par souci de style. 

Il peut également être **vieilli** s’il est en train de sortir de l’usage petit à petit, son utilisation donnant l’impression d’un langage trop châtié et déconnecté de notre époque. À l’inverse, si un terme est un **néologisme**, cela signifie que son entrée dans le lexique est très récente. Attention, un néologisme est par définition un mot nouveau, mais il peut quand-même désigner une réalité ancienne. Certains nouveaux mots viennent d’autres langues, et ils peuvent être perçus comme n’appartenant pas encore à la langue française. Cette perception pourra être détaillée dans l’espace de discussion sur les usages du mot.

Pour aller plus loin : 
- Riegel Martin, Pellat Jean-Christophe, Rioul René. *Grammaire méthodique du français*. 7e édition 2018. 1168 p. ISBN : 978-2130627562
- Pruvost, Jean et Jean-François Sablayrolles. *Les néologismes*. Que sais-je ?. Presses Universitaires de France - PUF. 2019. 125 p. ISBN : 978-2130815914

# Fréquence <a id="frequence"></a>

La **fréquence** sert à décrire la rareté des occurrences d’un sens dans la langue. Cette catégorie est donc composée d’une gradation de marqueurs qui vont de **courant**, qui signifie que le sens du mot est rencontré fréquemment, à **hapax** qui désigne un mot qui n’apparaît que dans un seul texte ou chez un seul auteur. Le marqueur courant s’utilise uniquement par opposition à **rare**. Cette notion de fréquence n’est pas mesurée rigoureusement, elle est subjective dans le Dictionnaire des francophones. C’est l’expérience de la langue des contributeurs qui établira la fréquence d’usage.

# Registre <a id="registre"></a>

Le **registre** est la manière de s’exprimer, en fonction de la personne à qui l’on s’adresse (l’interlocuteur). Il s’agit d’une distinction sociale, qui peut dépendre du lieu où est parlée la langue, ou de la communauté au sein de laquelle la langue est parlée. Le registre change donc en fonction de la personne à laquelle on s’adresse puisqu’il convient de s’adresser différemment à un supérieur hiérarchique et à un ami par exemple.

Si l’interlocuteur est une personne proche, le registre sera plutôt **familier** alors que si l’interlocuteur est un supérieur hiérarchique, il sera plutôt **soutenu**.

Le registre **familier** correspond au registre informel, c’est celui qui est utilisé dans une conversation entre deux proches ou en tout cas, sans la barrière de l’officialité ou de la hiérarchie. En 1985 avec la refonte du Grand Robert, est apparu le marqueur **très familier** qui vient apporter un niveau de familiarité supérieur (plus familier que familier). Ce marqueur a commencé à être utilisé pour limiter l’usage du marqueur **populaire** qui n’a plus de sens à notre époque puisque plus rien n’est le propre du peuple au sens de « partie de la population non éduquée ». De plus, populaire ayant une connotation trop négative, nous avons suivi le choix des autres dictionnaires de langue de le fusionner tantôt avec familier, tantôt avec très familier en fonction de la situation. Par exemple, le mot *bifton* qui signifie billet de banque sera marqué comme familier et le mot *clamser* qui signifie mourir sera marqué comme très familier alors que les deux auraient autrefois été marqués comme populaires. C’est un familier particulier, ce n’est pas vulgaire mais ce n’en est pas loin. 

Le registre **vulgaire** est un type de registre familier. Il est cependant plus fort que très familier puisqu’il rend compte d’insultes ou d’injures. Plus précisément, est vulgaire un “mot, sens ou emploi choquant, le plus souvent lié à la sexualité et à la violence, qu’on ne peut employer dans un discours soucieux de courtoisie, quelle que soit l’origine sociale.” (Voir le Petit Robert)

Le marqueur **soutenu** est une addition récente puisqu’il apparaît en 1993 chez Larousse pour désigner l’emploi d’une langue recherchée et maîtrisée. Il se rapproche du marqueur de genre textuel [littéraire](#genre-litteraire) mais il est moins spécifique, puisqu’il désigne le langage employé à l’écrit, mais également à l’oral peu importe le genre textuel. Par exemple, la formule de politesse « je vous prie d’agréer mes salutations les plus distinguées » utilisée pour clore les documents officiels est propre au registre soutenu, pas au genre textuel littéraire puisqu’il n’est pas utilisé en littérature.

Le marqueur **langage enfantin** désigne les mots employés par l’enfant ou avec des enfants. Par exemple *dada* pour le cheval. 

Le marqueur **traditionnel** désigne les termes employés dans un contexte très marqué culturellement, comme les cérémonies de mariage ou certains rites.

Pour aller plus loin : 
- Riegel Martin, Pellat Jean-Christophe, Rioul René. *Grammaire méthodique du français*. 7e édition 2018. 1168 p. ISBN : 978-2130627562
- Lehmann Alise et Françoise Martin-Berthet. *Lexicologie : sémantique, morphologie, lexicographie*. Collection cursus, 2013. 4e édition. 317 p. ISBN : 978-2200276751 

# Connotation <a id="connotation"></a>

La **connotation** est une caractéristique des mots qui ont une influence sur le monde. En fait c’est un sens en plus qui s’ajoute aux autres sens d’un mot en fonction du contexte. La connotation est donc complémentaire de la dénotation, qui est le sens littéral du mot. Par exemple, le mot *super* peut avoir un sens **péjoratif** (négatif) dans un contexte d’échec, ou un sens **mélioratif** (positif) dans un contexte heureux. Le contexte régit le sens du mot. La même chose s’applique à l’insulte, qui, si elle a habituellement une connotation **injurieuse** puisqu’elle a pour but de blesser, peut parfois être affectueuse (mais qui appartient toujours au registre familier ou vulgaire puisqu’on n’utilise pas d’insultes, même affectueusement, avec un supérieur hiérarchique). 

Le marqueur **par dérision** indique que le mot est employé pour se moquer, méchamment ou pas. Le marqueur **par plaisanterie** indique simplement que le mot est employé pour faire de l’humour. Encore une fois le contexte est essentiel, un mot ne peut pas être défini comme ayant vocation à faire rire ou à se moquer sans ce contexte. 

Enfin, l’**ironie** est un procédé par lequel on dit le contraire de ce qu’on pense mais pour faire comprendre ce qu’on pense, pour introduire un effet comique ou se moquer. Par exemple, *bénin*, signifie *doux, bienveillant* mais peut être utilisé pour parler d’une personne trop douce, que l’on perçoit en réalité comme naïve, faible.

# Argot <a id="argot"></a>

**Argot** regroupe les usages du langage propres à un groupe social ou à un corps de métier donné. Il désigne donc un vocabulaire propre à un certain groupe de personnes, on a par exemple l’*argot scolaire* ou l’*argot des médecins*. La dimension sociale est ce qui distingue l’argot du domaine ou du thème. L’argot désigne le vocabulaire principalement employé par un groupe social mais ces termes ne sont pas pour autant considérés comme techniques et révélateurs d’une expertise particulière. Des subdivisions existent au sein de ces argots (notamment l’argot scolaire qui n’est pas le même pour les classes préparatoires scientifiques ou les classes préparatoires littéraires) mais un tel niveau de spécificité nous paraissait exagéré, d’autant que ce n’est pas la norme dans les dictionnaires usuels. Dans l’argot scolaire on trouvera notamment *bizut*, *carré*, ou *cube* qui désignent un niveau d’études et/ou de hiérarchie entre les classes.

Pour aller plus loin : Calvet, Louis-Jean. *L’argot en 20 leçons ou Comment ne pas perdre son français*. Payot, 1993. 213 p. ISBN : 978-2228887212

# Relations de sens entre les mots <a id="relations-entre"></a>

Lorsqu’un mot est lié de manière notable à un ou plusieurs autres, ceci est indiqué dans le dictionnaire afin de présenter un maximum d’informations utiles et de faciliter la navigation entre ces différents termes. Il existe de nombreux types de relations de sens entre les mots et les détailler permet de mieux cibler l’information donnée.

Ces relations peuvent être des relations de **synonymie** quand leurs sens sont identiques (vieux et âgé par exemple) ou de **quasi-synonymie** quand ils sont synonymes mais diffèrent par une indication d’usage (registre, connotation, fréquence, temporalité). Par exemple, *voiture* a pour synonyme *auto* et pour quasi-synonyme *bagnole* qui désigne la même chose mais dans un registre familier. À l’inverse, si un mot a un sens contraire de l’autre, il est son **antonyme** (*grand* et *petit* par exemple) et son **quasi-antonyme** s’il est antonyme mais diffère d’une indication d’usage. 

Une abréviation très utilisée d’un mot sera indiquée comme **abréviation**, mais son sens sera le même. 

On rencontre également beaucoup de relations d’hyponymie et d’hyperonymie, qui sont des relations réciproques. L’**hyperonyme** d’un mot est la catégorie à laquelle appartient ce mot (*félin* est l’hyperonyme de *chat*) et l’**hyponyme** est le mot qui appartient à cette catégorie (*chat* est l’hyponyme de *félin*). 

Un **holonyme** est un ensemble dont fait partie le **méronyme**, qui est une partie de cet ensemble. Par exemple, *jambe* est un méronyme de *corps* qui est donc son holonyme. Alors que l’holonyme désigne un ensemble concret, l’hyperonyme désigne une catégorie. 

On indique également **par dérivation** lorsque deux mots partagent la même racine en indiquant cette racine commune et les autres dérivés, c’est-à-dire les mots construits à partir de la même racine. Par exemple, *ensabler* est un dérivé de *sable*.

Lorsque deux termes ont une étymologie commune, ils sont marqués comme **termes apparentés**. Par exemple, Le mot *seoir* a donné *séant* et *asseoir*. Les deux termes *séant* et *asseoir* sont donc apparentés par leur histoire commune puisqu’ils sont issus du même mot.

Lorsque le mot-entrée fait partie d’un **proverbe**, nous l’indiquons également car les proverbes sont des entités culturellement chargées et utilisées dans des contextes particuliers. Par exemple, « pierre qui roule n’amasse pas mousse » pourra être lié à l’entrée *amasser*. 

Lorsque le mot défini est un nom de lieu, les **gentilés**, c’est à dire le nom des habitants de ce lieu, pourront être indiqués.

Enfin, **référent de sexe opposé** est utilisé pour introduire le nom de sexe opposé au mot défini. Par exemple, *facteur* qui est un nom de métier masculin a un équivalent féminin, *factrice* qui sera défini dans une page séparée. En effet, le mot de sexe opposé associé peut recouvrir des réalités différentes qu’il est important de décrire dans le détail, avec des exemples appropriés.

# Relations entre les sens d’un même mot <a id="relations-en"></a>

Lorsqu’un mot présente plusieurs sens, ces sens entretiennent la plupart du temps des relations liées à l’évolution historique du mot. Nous avons choisi de les indiquer afin de rendre la consultation des articles plus logique, de mieux refléter la réalité de la langue. Lorsque la relation n’est pas tout à fait claire, il peut être indiqué simplement **définition proche**. 

Lorsque deux sens sont opposés par la spécificité du mot qu’ils désignent, on utilisera les marqueurs au figuré et sens propre. **Au figuré** signifie que le sens du mot est plus large qu’un autre, tandis que **sens propre** indique que le sens du mot est plus strict, plus restreint que l’autre. Par exemple, au sens strict, *manchot* désigne une espèce d’oiseau, et au sens large il peut désigner une personne maladroite.

Le marqueur **par métonymie** indique que les deux sens sont associés logiquement. Par exemple, le mot *bande dessinée* désigne un type d’œuvre littéraire mais, par métonymie, il désigne également directement l’album de bande dessinée, l’objet matériel.

Le marqueur **par hyperbole** indique que le sens est un sens proche du précédent mais utilisé pour exagérer le propos. En ce sens on augmente sa portée. Par exemple, *gamin* désigne typiquement un enfant. Mais on peut également l’utiliser pour désigner quelqu’un de beaucoup plus jeune que soi.  

Le marqueur **par extension** indique que le premier sens en a donné un second dans un nouveau champ lexical. Les deux sens sont quand-même liés. Par exemple, *fruit* désigne la partie d’une plante qu’on mange. Par extension, ce mot désignait aussi *le dessert*, chose qu’on mange également (d’autant que les fruits se mangent habituellement au dessert).

Le marqueur **par analogie** indique qu’une partie d’un sens (une caractéristique du mot vedette) a donné un autre sens. Par exemple, on emploie le mot *marron* pour désigner la couleur par analogie avec la couleur brune du marron, fruit du marronnier. C’est aussi un mot familier pour un coup de poing, par ressemblance de forme.

Le marqueur **en particulier** indique que le sens qui suit fait référence à quelque chose de plus précis que le précédent. Par exemple, le mot *jour* désigne la période de 24 heures qui constitue une journée. En métrologie, la science des mesures, le mot *jour* désigne plus particulièrement l’unité de mesure du temps.

**Par euphémisme** indique que le mot peut être utilisé afin d’atténuer quelque chose de très négatif. Par exemple, « il s’en est allé » ne veut pas dire « il est parti » mais « il est mort ». On utilise *s’en aller* pour adoucir l’ensemble et ne pas directement faire référence à la mort, qui est un sujet grave et sensible.

**Par litote** indique un phénomène similaire mais qui n’a pas vocation à atténuer une réalité désagréable. Par exemple, *Ce n’est pas faux* signifie *c’est tout à fait vrai*. Dit simplement, la litote consiste à dire moins pour en laisser entendre plus. On ne mentionne pas directement la réalité mais on ne l’atténue pas, on l’amplifie.

Les deux derniers marqueurs sont des relations de fréquence entre les sens. Le marqueur **plus rare** indique qu’un sens est moins utilisé que le précédent tandis que le marqueur **plus courant** indique que le sens est plus utilisé que le précédent. Ils sont équivalents aux marqueurs de fréquence rare et courant, mais précisent une perception contrastive entre deux sens. Par exemple, une *mine* est l’endroit dont on extrait le minerai mais le mot peut également, plus rarement, désigner le minerai en lui-même.

Pour aller plus loin : 
- Bernard Dupriez, *Gradus : Les procédés littéraires (Dictionnaire)*, 10/18 (Union générale d'éditions), (1980) 2019. 
- Groupe µ (J. Dubois, F. Edeline, J.-M. Klinkenberg, P. Minguet), *Rhétorique générale*, Paris, Éditions du Seuil, collection « Points », (1970) 1982.
