Mentions légales – Dictionnaire des francophones 
================================================

1 – Mentions légales
====================

1.1 Editeur
-----------

Le site internet
[*www.dictionnairedesfrancophones.org*](http://www.dictionnairedesfrancophones.org)
ainsi que les applications mobiles « *Dictionnaire des francophones* »
sont édités par le ministère français de la Culture, 182 rue Saint
Honoré, 75033 Paris Cedex 01, France.

**1.2 Directeur de la publication**

Le directeur de la publication est M. Paul de Sinety, Délégué général à
la langue française et aux langues de France au ministère français de la
Culture.

Les contenus proposés sur le *Dictionnaire des francophones* proviennent
des contributions des usagers et de différentes sources externes,
sélectionnées par le conseil scientifique du *Dictionnaire des
francophones*. Nous vous invitons à consulter la page [*Présentation du
projet*](http://www.dictionnairedesfrancophones.org/presentation) pour
davantage d’informations à ce sujet.

**1.3 Hébergement**

Le site internet
[*www.dictionnairedesfrancophones.org*](http://www.dictionnairedesfrancophones.org)
est hébergé par la Très grande infrastructure de recherche
[*Huma-Num*](https://www.huma-num.fr/) – UMS 3598 CNRS, 54 boulevard
Raspail, 75006 Paris.

**1.4 Réalisation du *Dictionnaire des francophones***

La plate-forme technique du *Dictionnaire des francophones* est
développée par la société [*Mnémotix*](https://www.mnemotix.com/), sous
le pilotage de l’Institut international pour la Francophonie (Université
Lyon III Jean Moulin).

Pour plus d’informations sur les partenaires et prestataires impliqués
dans la conception et la réalisation du *Dictionnaire des francophones*,
nous vous invitons à consulter la page
[*Crédits*](http://www.dictionnairedesfrancophones.org/credits).

**1.5 Définitions**

« *Dictionnaire des francophones* » désigne le site internet accessible
à l’adresse :
[*http://www.dictionnairedesfrancophones.org*](http://www.dictionnairedesfrancophones.org)
ainsi que les applications mobiles dénommées « *Dictionnaire des
francophones* » et disponibles sur les plates-formes Google Play et
Apple Store.

« Utilisateur » : désigne toute personne physique accédant au
*Dictionnaire des francophones* et/ou utilisant une des offres
éditoriales ou services qui y sont associés.

« Partenaire(s) » : désigne tout partenaire participant au projet du
*Dictionnaire des francophones*.

« Contenus » : désigne l’ensemble des données et plus généralement des
informations diffusées sur le *Dictionnaire des francophones*.

« Services » : désigne l’une ou l’ensemble des fonctionnalités fournie
sur le site et accessible en ligne à partir du *Dictionnaire des
francophones*.

2 – Conditions Générales d’Utilisation
======================================

**2.1. Objet**

Les présentes Conditions Générales d’Utilisation ont pour objet de
définir les modalités d’accès, d’utilisation et de réutilisation des
contenus et données présents dans le *Dictionnaire des francophones*.

**2.2 Avertissement**

Les Conditions Générales d’Utilisation applicables sont celles dont la
version en vigueur est accessible en ligne sur le site du *Dictionnaire
des francophones* à l’adresse suivante et à la date de la connexion de
l’utilisateur :
[*http://www.dictionnairedesfrancophones.org/cgu*](http://www.dictionnairedesfrancophones.org/cgu).

**2.3. Accès**

Toute personne accède gratuitement au site internet *Dictionnaire des
francophones* ainsi qu’au téléchargement de l’application «
*Dictionnaire des francophones* », à ses mises à jour et à l’intégralité
de ses fonctionnalités depuis la boutique d’applications présente sur
son appareil mobile. Il n’y a pas de contenus ou de fonctionnalités
payantes (achats intégrés) sur le site internet ainsi que dans
l’application « *Dictionnaire des francophones* ».

**2.4. Propriété intellectuelle et réutilisation**

### 2.4.1 Structure générale et code de l’application

L’infrastructure logicielle du site internet « *Dictionnaire des
francophones* » est basée sur du code source mis à disposition sous
licence libre. La licence utilisée est la licence Apache, version 2.0 :
[*http://www.apache.org/licenses/LICENSE-2.0*](http://www.apache.org/licenses/LICENSE-2.0).

Les applications « *Dictionnaire des francophones* » proposées sur
Google Play et Apple Store sont est basée sur du code source mis à
disposition sous licence libre. La licence utilisée est la licence
Apache, version 2.0 :
[*http://www.apache.org/licenses/LICENSE-2.0*](http://www.apache.org/licenses/LICENSE-2.0).

Les logos « *Dictionnaire des francophones* » et « DDF » ainsi que la
charte graphique et les différents pictogrammes utilisés sur le site
internet et les applications mobiles du *Dictionnaire des francophones*
sont mises à disposition du public sous la licence libre Creative
Commons CC-BY-SA 3.0 :
[*https://creativecommons.org/licenses/by-sa/3.0/fr/*](https://creativecommons.org/licenses/by-sa/3.0/fr/).

### 2.4.2 Marques

Les marques de l’Institut international pour la Francophonie, de
l’université Lyon III Jean moulin, du ministère de la Culture et de
leurs Partenaires ainsi que leurs logos sont des marques protégées par
un droit de propriété industrielle.

Toute reproduction totale ou partielle de ces marques et de leur logo
sans l’autorisation expresse de leurs ayants droits est donc strictement
interdite et constitue un délit de contrefaçon au sens du code de la
propriété intellectuelle.

### 2.4.3 Contenus textuels

Tous les contenus de nature textuelle (termes, définitions, nomenclature
et tout autre renseignement associé aux entrées) proposés sur le
*Dictionnaire des francophones*, autre que les catégories de données
énumérées à l’article 2.4.6, sont mis à disposition du public selon les
termes de la licence libre Creative Commons CC-BY-SA 3.0 :
[*https://creativecommons.org/licenses/by-sa/3.0/fr/*](https://creativecommons.org/licenses/by-sa/3.0/fr/).

Cela signifie notamment que tout Utilisateur est libre d’accéder aux
données, de les réutiliser, de les modifier, de les rediffuser, sous
réserve notamment des conditions suivantes :

- Attribution : Vous devez créditer l'Œuvre, intégrer un lien vers la
licence et indiquer si des modifications ont été effectuées à l'Œuvre.
Vous devez indiquer ces informations par tous les moyens raisonnables,
sans toutefois suggérer que l'Offrant vous soutient ou soutient la façon
dont vous avez utilisé son Œuvre.

- Partage dans les Mêmes Conditions : Dans le cas où vous effectuez un
remix, que vous transformez, ou créez à partir du matériel composant
l'Œuvre originale, vous devez diffuser l'Œuvre modifiée dans les mêmes
conditions, c'est à dire avec la même licence avec laquelle l'Œuvre
originale a été diffusée.

### 2.4.4 Contributions des Utilisateurs au Dictionnaire des francophones

Les Utilisateurs qui contribuent au *Dictionnaire des francophones*
acceptent expressément de placer ces contributions sous la licence libre
Creative Commons CC-BY-SA 3.0 :
[*https://creativecommons.org/licenses/by-sa/3.0/fr/*](https://creativecommons.org/licenses/by-sa/3.0/fr/).

L’ensemble des contributions des Utilisateurs sont mises à disposition
du public sous la licence Creative Commons CC-BY-SA 3.0.

### 2.4.5 Contenus sonores et audio-visuels

Tous les contenus de nature sonore ou audio-visuelle (prononciations et
enregistrements sonores des termes proposés notamment) proposés sur le
*Dictionnaire des francophones*, sont mis à disposition du public selon
les termes de la licence libre Creative Commons CC-BY-SA 3.0 :
[*https://creativecommons.org/licenses/by-sa/3.0/fr/*](https://creativecommons.org/licenses/by-sa/3.0/fr/).

### 2.4.6 Contenus culturels et données dictionnairiques produits par les partenaires du ministère de la Culture et présentés dans le cadre du Dictionnaire des francophones

Par exception à l’article 2.4.3 des présentes conditions générales
d’utilisation, les données suivantes proposées par certains Partenaires
du ministère de la Culture, sont soumises à des conditions d’utilisation
particulières :

-   Les données issues de la *Base de données lexicographiques
    > panfrancophone* (BDLP), éditée par l’université Laval à Québec :
    > [*www.bdlp.org*](http://www.bdlp.org) ;

-   Les données issues du *Dictionnaire des régionalismes français*
    (DRF), édité par le laboratoire ATILF-CNRS
    ([*www.atilf.fr*](http://www.atilf.fr)) ;

-   Les données issues du *Grand Dictionnaire terminologique* (GDT),
    > édité par l’Office Québécois de la langue française
    > ([*www.oqlf.gouv.qc.ca/*](http://www.oqlf.gouv.qc.ca/)) ;

-   Les données issues du *Dictionnaire des Belgicismes*, édité en 1994
    par un collectif d’auteurs sous la coordination de Mme.
    Lenoble-Pinson.

Les conditions d’utilisation de ces données sont les suivantes :

- les données sont mises à disposition du public selon les termes de la
licence Creative Commons CC-BY-NC-ND 4.0
([*https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr*](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr))
;

- la reproduction des données des Partenaires respecte leur intégrité et
la présentation qui en est faite sur le *Dictionnaire des francophones*
;

- lorsqu’elles sont réutilisées, les données des Partenaires sont
toujours accompagnées de la mention claire et explicite de leur source
et d’un lien hypertexte renvoyant au site (externe) de l’auteur (lorsque
celui-ci est disponible), afin que l’utilisateur puisse accéder à
davantage d’informations ;

- les données des Partenaires ne peuvent pas être téléchargées dans leur
intégralité (nomenclature complète, extractions automatisées, etc.) à
partir du *Dictionnaire des francophones* ;

- les données des Partenaires ne peuvent pas être versées (cédées,
transférées) dans des projets ou bases de données autres que le
*Dictionnaire des francophones*, sans une autorisation préalable auprès
de l’auteur ;

- les données des Partenaires sont accessibles au sein du *Dictionnaire
des francophones*, sur tous supports actuels ou à venir (site internet,
site pour mobile, applications mobiles pour App Store et Google Play) et
sont réutilisables à des fins pédagogiques ou non commerciales ;

- les utilisations commerciales des données des Partenaires par des
tiers requièrent une autorisation préalable auprès de leurs auteurs.

2.5 Liens hypertexte
--------------------

### 2.5.1 Liens sortants du Dictionnaire des francophones

Le *Dictionnaire des francophones* fournit des liens hypertextes
profonds pointant vers des sites tiers. Ces liens ou références ne
constituent ni une approbation ni une validation de leurs contenus. Le
*Dictionnaire des francophones* ne pourra en aucun cas être tenu
responsable de la teneur des pages du site, et ne sera tenu responsable
d’aucun dommage ou préjudice en découlant. Le fait de proposer des
hyperliens profonds vers d’autres sites fournis aux Utilisateurs
uniquement par pure commodité et n’engendre à ce titre aucune obligation
de quelque nature que ce soit à la charge du *Dictionnaire des
francophones*, qui ne dispose d’aucun moyen de contrôle et de
surveillance des sites ainsi référencés ainsi que de leurs évolutions et
mises à jour. Les Utilisateurs doivent se reporter aux mentions légales
et conditions générales d’utilisation des sites des institutions
partenaires.

### 2.5.2 Liens vers le Dictionnaire des francophones

La création de liens hypertextes en direction d’une page du
*Dictionnaire des francophones* est libre à la condition qu’elle ne
porte pas atteinte aux intérêts matériels ou moraux du ministère de la
Culture et qu’elle ne créé pas de confusion sur la source des Services
et/ou Contenus.

2.6 Accès aux documents administratifs et réutilisation des informations publiques
----------------------------------------------------------------------------------

Aux termes des dispositions des articles L. 330-1 et R. 330-2 du code
des relations entre le public et l’administration (CRPA), la personne
responsable de l'accès aux documents administratifs est le
sous-directeur des affaires juridiques du ministère de la Culture, qu’il
est possible de contacter en envoyant un message à l’adresse suivante :
[*bdl.cada@culture.gouv.fr*](mailto:bdl.cada@culture.gouv.fr).

Vous pouvez également saisir la personne responsable de l’accès aux
documents administratifs par courrier en écrivant à l’adresse suivante :

Ministère de la Culture\
À l’attention du sous-directeur des affaires juridiques, personne
responsable de l’accès aux documents administratifs\
182 rue Saint Honoré\
75033 Paris Cedex 01

2.7. Responsabilité, contributions et modération
------------------------------------------------

Les informations présentées dans le *Dictionnaire des francophones* sont
données à titre purement indicatif. Elles sont modifiables à tout moment
et sans préavis.

Le ministère de la Culture et ses Partenaires ne sauraient en aucun cas
être tenus responsables des opinions éventuellement exprimées dans les
contributions des Utilisateurs, qui n’engagent que leurs auteurs. Il est
précisé que la modération des contributions des utilisateurs est
effectuée a posteriori par la communauté d’Utilisateurs du *Dictionnaire
des francophones*.

Le ministère de la Culture ne peut être tenu responsable des dommages
directs et indirects, prévisibles et imprévisibles tels que des pertes
de gains ou de profits, pertes de données, pertes de matériel ainsi que
des frais de réparation, récupération ou reproduction résultant de
l'utilisation et/ou de l'impossibilité d'utiliser les services et
contenus de l’application.

Malgré les soins et les contrôles de l'équipe en charge du *Dictionnaire
des francophones*, des erreurs, bogues ou omissions involontaires
peuvent subsister. Si vous souhaitez nous faire part de vos remarques ou
réclamations, vous pouvez nous contacter à l'adresse suivante :

[*contact@dictionnairedesfrancophones.org*](mailto:contact@dictionnairedesfrancophones.org).

2.8 Données illicites
---------------------

Conformément à la loi pour la confiance dans l’économie numérique du 21
juin 2004, le site et les applications mobiles du *Dictionnaire des
francophones* permettent à tout individu ou usager de signaler tout
contenu susceptible de revêtir les caractères des infractions visées aux
cinquième et huitième alinéas de l’article 24 de la loi du 29 juillet
1881 sur la liberté de presse et aux articles 227-23 et 227-24 du Code
pénal.

Pour nous signaler un tel contenu, nous vous invitons à nous contacter
en envoyant un message à
[*contact@dictionnairedesfrancophones.org*](mailto:contact@dictionnairedesfrancophones.org)
en précisant la localisation exacte au sein du *Dictionnaire des
francophones* du contenu que vous estimez illicite.

2.9 Loi applicable et juridiction compétente
--------------------------------------------

Les présentes conditions générales d'utilisation sont entièrement
soumises au droit français. L'utilisateur du *Dictionnaire des
francophones* reconnaît la compétence exclusive des tribunaux compétents
de Paris.

3 – Données personnelles
========================

3.1 Informations relatives aux données personnelles collectées sur le site
--------------------------------------------------------------------------

### 3.1.1 Origine et finalité de la collecte des données personnelles

Le *Dictionnaire des francophones* met en œuvre plusieurs traitements de
données personnelles ayant pour finalité d’assurer :

-   la collecte de la géolocalisation de l’Utilisateur rend possible
    l’affichage de résultats de recherche personnalisés dans le
    *Dictionnaire des francophones*, en fonction de sa position
    géographique ; la géolocalisation est soit automatique, réalisée à
    partir des capteurs ou des caractéristiques de connexion à Internet
    de l’Utilisateur, soit saisie manuellement par l’Utilisateur ; pour
    des raisons de confidentialité, la géolocalisation est imprécise, le
    maillage le plus fin retenu s’arrêtant au niveau de la ville ou
    commune, ou de l’arrondissement (uniquement pour les plus grandes
    villes) ; cette fonctionnalité ne permet en aucun cas la collecte
    d’adresses personnelles ;

-   la collecte d’une adresse électronique, d’un pseudonyme et d’un mot
    de passe est obligatoire pour rendre possible la contribution des
    utilisateurs au *Dictionnaire des francophones*, à travers les
    fonctionnalités permettant d’une part l’ajout d’une nouvelle entrée,
    et d’autre part celles permettant de compléter une entrée existante
    avec des informations complémentaires ;

-   la géolocalisation (optionnelle) de l’Utilisateur lors son
    inscription permet de classer et de présenter ses contributions aux
    autres Utilisateurs selon plusieurs zones géographiques non
    identifiantes telles que « Monde », « Europe », ou « Afrique » ;
    cette fonctionnalité ne permet en aucun cas la collecte d’adresses
    personnelles ;

-   les informations complémentaires optionnelles éventuellement
    collectées sur la page « mon profil », lorsque l’utilisateur a
    exprimé son consentement à ce que ces données puissent être
    utilisées à des fins scientifiques et/ou de recherche, à savoir :

    -   son genre (champ fermé),

    -   son année de naissance (champ fermé),

    -   son rapport avec la langue française (champ fermé),

    -   ses autres langues parlées (champ ouvert),

    -   son domaine d’expertise (champ ouvert),

    sont mises à disposition du conseil scientifique du *Dictionnaire des
    francophones* dans l’objectif de mener des études sociolinguistiques,
    analyses ou tout autre projet de recherche à vocation purement
    scientifique à partir des contributions des Utilisateurs ; nous invitons
    les Utilisateurs à ne pas utiliser les champs ouverts proposés sur la
    page « Mon profil » pour renseigner des données à caractère personnel
    autres que celles demandées ;

-   les adresses IP des Utilisateurs et leurs différentes connections
    sur le *Dictionnaire des francophones* sont collectées pour
    permettre le suivi statistique de l’audience du *Dictionnaire des
    francophones* au moyen de l’outil *Matomo* (version à code ouvert
    disponible sur [*https://fr.matomo.org/*](https://fr.matomo.org/)).

Les données recueillies sur le *Dictionnaire des francophones* ne
sauraient provenir que de l’enregistrement volontaire par les
Utilisateurs d’une adresse de courrier électronique et/ou de données
nominatives permettant de bénéficier de certaines fonctionnalités
additionnelles telles que la possibilité de contribuer et d’enrichir le
*Dictionnaire des francophones*, exception faite des adresses IP des
visiteurs, qui sont enregistrées automatiquement par les systèmes de
notre hébergeur Huma-Num et de l’outil d’analyse de trafic *Matomo*.

### 3.1.2 Destination des données personnelles collectées sur le site

Les données personnelles collectées lors de l’inscription sur le
*Dictionnaire des francophones* ne sont accessibles que par l’équipe du
*Dictionnaire des francophones* en charge de son animation et de son
exploitation, parmi laquelle figurent des agents du ministère de la
Culture, de l’Institut international pour la Francophonie, de son
prestataire Mnémotix, et d’éventuels membres du conseil scientifique, à
l’exception des éventuelles données personnelles qui seraient indiquées
par l’Utilisateur dans des champs des entrées du *Dictionnaire des
francophones* qui sont affichés au public, tels que les champs « Mot ou
expression » ou le champ « Définition ».

Les mots de passe des Utilisateurs sont chiffrés par nos systèmes, à
l'aide de l'algorithme PBKDF2 (algorithme décrit par le standard
Internet RFC 2898), lors
de l’inscription sur le *Dictionnaire des francophones* et ne sont
accessibles ni par l’Editeur, ni par ses Partenaires, ni par aucune
autre personne ou tiers ; l’Hébergeur s’engage à garantir la stricte
confidentialité de ces données en mettant en œuvre tous les moyens
techniques nécessaires pour y parvenir.


Les mots de passe des utilisateurs sont chiffrés à l'aide de l'algorithme PBKDF2 algorithm décrit par le standard Internet RFC 2898.

Les données personnelles collectées sur le *Dictionnaire des
francophones* sont à usage purement interne, et ne font l’objet d’aucune
communication, cession ou divulgation à des tiers, à l’exception des
données collectées sur la page « Mon profil » pour lesquelles
l’utilisateur a donné son consentement pour une exploitation
scientifique, qui peuvent être transmises à des Partenaires
scientifiques du ministère de la Culture pour être exploitées à des fins
uniquement scientifiques (analyse des contributions, travaux de
recherche linguistiques...). Il est précisé ici que :

- les Partenaires scientifiques devront au préalable avoir été
accrédités par le conseil scientifique du *Dictionnaire des
francophones* ;

- les Partenaires scientifiques dûment habilités par le conseil
scientifique ne pourront accéder qu’aux contributions publiques des
Usagers et aux informations qu’ils ont renseigné sur la page « Mon
profil » ; plus particulièrement, les données des comptes utilisateurs
associées à ces contributions (adresse électronique et pseudonyme) ne
seront pas divulguées, ce qui permet de garantir l’anonymat des données
ainsi transmises.

### 3.1.3 Responsable des traitements de données personnelles collectées sur le site

Le responsable de l’ensemble des traitements de données à caractère
personnel du *Dictionnaire des francophones* est le Délégué général à la
langue française et aux langues de France du ministère français de la
Culture.

### 3.1.4 Conservation des données personnelles collectées sur le site

Le ministère de la Culture conserve les données personnelles permettant
la contribution pendant une période de trois (3) ans qui court à compter
de la dernière connexion réussie de l’Utilisateur avec ses identifiants
personnels, période à compter de laquelle les données relatives à son
profil seront automatiquement effacées.

L’Utilisateur peut par ailleurs à tout moment demander la suppression
complète de ses données en supprimant son compte personnel depuis la
page « modifier mes préférences » de son profil utilisateur
([*http://www.dictionnairedesfrancophones.org/my\_account/profile/settings*](http://www.dictionnairedesfrancophones.org/my_account/profile/settings))
sur le *Dictionnaire des francophones* ; ce faisant, l’intégralité de
ses données seront supprimées, à l’exception d’un identifiant unique
conservé pour des raisons logicielles et qui ne comporte aucune donnée à
caractère personnel, et de ses contributions au *Dictionnaire des
francophones*.

L’ensemble des données du *Dictionnaire des francophones* est hébergé
par la très grande infrastructure de recherche Huma-Num sur des serveurs
situés exclusivement en France, y compris les données d’analyse
d’audience réalisées au moyen de l’outil Matomo, installé sur la même
infrastructure.

Le ministère de la Culture garantit la confidentialité des informations
enregistrées sur le *Dictionnaire des francophones*. Les adresses
électroniques et les données nominatives des Utilisateurs inscrits n’y
apparaissent à aucun moment, sauf si l’utilisateur a souhaité signer sa
contribution directement dans l’un des champs publics du *Dictionnaire
des francophones*, ce qui est fortement déconseillé.

3.2 Politique de protection des données personnelles 
-----------------------------------------------------

L’adoption par l’Union européenne du règlement 2016/79 relatif à la
protection des personnes physiques à l’égard du traitement des données
personnelles et à la libre circulation de ces données (RGPD) fixe un
nouveau cadre pour la protection des données personnelles, en renforçant
celui issu de la loi n°78-17 relative à l’Informatique, aux fichiers et
aux libertés du 6 janvier 1978 modifiée.

En particulier, le texte renforce les droits pour les personnes
concernées par des traitements de données à caractère personnel : droits
à l’information, d’accès, d’obtenir copie des données et d’opposition,
dès lors qu’une personne a retiré son consentement au traitement de ces
données.

Le RGPD consacre également de nouveaux droits : droit à l’oubli
(effacement général des données collectées), droit à la portabilité des
données.

Enfin, le RGPD impose un principe de transparence, en obligeant tout
responsable de traitement à fournir une information concise, claire,
compréhensible et aisément accessible sur les principales
caractéristiques du traitement des données.

3.3 Exercer vos droits
----------------------

Pour toute information ou exercice de vos droits (droit d’accès, de
rectification ou d’effacement, droit à la limitation du traitement,
droit à la portabilité des données et droit d’opposition) sur les
traitements de données personnelles opérés par le *Dictionnaire des
francophones* et ses différents services, vous pouvez contacter notre
délégué à la protection des données (DPD) via le formulaire disponible à
l’adresse suivante :
[*https://www.culture.gouv.fr/Contact-protection-des-donnees-personnelles*](https://www.culture.gouv.fr/Contact-protection-des-donnees-personnelles).

Vous pouvez également contacter le délégué à la protection des données
(DPD) par courrier en écrivant à l’adresse suivante :

Ministère de la Culture\
À l'attention du délégué à la protection des données (DPD)\
182 rue Saint Honoré\
75033 Paris Cedex 01

Vous disposez également du droit d'introduire une réclamation auprès de
l'autorité de contrôle compétente ([*CNIL*](https://www.cnil.fr/)) si
vous considérez que le *Dictionnaire des francophones* n'a pas respecté
vos droits.

En savoir plus sur vos droits : [*CNIL - Droits de la
personne*](https://www.cnil.fr/fr/reglement-europeen-protection-donnees/chapitre3)

Vous pouvez retrouver sur la page suivante davantage d’information sur
les missions du délégué à la protection des données (DPD) :
[*https://www.culture.gouv.fr/Nous-connaitre/Organisation/Delegue-a-la-protection-des-donnees-DPD*](https://www.culture.gouv.fr/Nous-connaitre/Organisation/Delegue-a-la-protection-des-donnees-DPD)

3.4 Témoins de connexion (COOKIES)
----------------------------------

### 3.4.1 Utilisation des témoins de connexion (cookies) sur le Dictionnaire des francophones

Les options d’enregistrement de vos identifiants, mots de passe et de la
géolocalisation de l’appareil sur le site internet du *Dictionnaire des
francophones* peuvent occasionner l’emploi d’un témoin de connexion ou
cookie persistant (stocké sur le disque dur de votre ordinateur).

La génération de ces témoins de connexion n’est en aucun cas
systématique ; ces fichiers ont pour seule fonction de simplifier votre
accès en supprimant la phase de saisie de votre identifiant et de votre
mot de passe. Nous vous recommandons de ne pas avoir recours à ces
témoins de connexion si vous accédez à notre site internet depuis un
poste en accès public.

Ces témoins de connexion ne sont en aucune façon conçus à des fins de
collecte de données personnelles.

### 3.4.2 Suppression des témoins de connexion présents sur votre ordinateur

L'utilisateur pourra désactiver ces témoins de connexion par
l'intermédiaire des paramètres figurant au sein de son logiciel de
navigation et éventuellement les refuser de la manière décrite à
l’adresse suivante :
[*https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser*](https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser).

Attention : cette manipulation pourra entraîner la suppression de tous
les témoins de connexion utilisés par le navigateur, y compris ceux
employés par d’autres sites internet, ce qui peut conduire à la perte de
certaines informations ou réglages.

4 – Accessibilité
=================

Le site internet « *Dictionnaire des francophones* » est conforme aux
spécifications du W3C relatives au standard HTML5.

Il a été conçu pour fonctionner de manière optimale à partir des
navigateurs internet suivants :

- Chrome, v. 77

- Firefox, v. 70

- Safari, v. 12

- Chrome Android, v.78

- Safari iOS, v12

Un effort particulier a été porté sur l’accessibilité du *Dictionnaire
des francophones* aux personnes en situation de handicap. Les consignes
de conception accessibles présentées dans le Référentiel Général
d'Accessibilité pour les Administrations
([*RGAA*](https://references.modernisation.gouv.fr/rgaa-accessibilite/))
ont été suivies dans la mesure du possible, avec l’objectif d’une
conformité totale aux critères de priorité A.

Cependant, certaines fonctionnalités présentées au sein du *Dictionnaire
des francophones* peuvent fonctionner de manière dégradée sur certains
navigateurs adaptés. N’hésitez pas à nous contacter pour tout problème
d’accessibilité rencontré lors de l’utilisation du *Dictionnaire des
francophones*.
