![MC](../images/partners/MIN_Culture_RVB.png)

Le Dictionnaire des francophones est édité par le ministère français de la Culture (Délégation générale à la langue française et aux langues de France)

<br/>

* Pilotage et Financement : Délégation générale à la langue française et des langues de France
* Direction opérationnelle : Institut international pour la Francophonie, composante de l’Université Jean Moulin Lyon 3
* Conseil scientifique : Bernard Cerquiglini, Claudine Bavoux, Aïcha Bouhjar, Barbara Cassin, Moussa Daff, François Grin, Katia Haddad, Latifa Kadi-Ksouri, Jean-Marie Klinkenberg, Jérémie Kouadio, Mona Laroussi, Lucas Lévêque, Franck Neveu, Jean Tabi Manga, Robert Vezina
* Équipe de conception : Noé Gasparini, Kaja Dolar, Marie Steffens, Antoine Bouchez
* Conception de l’ontologie : Jean Delahousse et Noé Gasparini
* Création du logo : Alexandra Simon
* Design UX : Jérémie Cohen
* Explications lexicographiques : Antoine Bouchez
* Équipe de développement : Mnémotix  (Freddy Limpens, Nicolas Delaforge, Mylène Leitzelman, Pierre-René Lherisson, Mathieu Rogelja, Quentin Richaud) 
* Équipe à l’Institut international pour la Francophonie : Marielle Payaud, Sandrine David, Sébastien Gathier, Camelia Danc
* Équipe à la Délégation générale à la langue française et aux langues de France : Paul de Sinety, Thibault Grouas, Paul Petit, Annick Lederlé, Claire-Lyse Chambon
