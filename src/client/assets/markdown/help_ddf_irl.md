Le Dictionnaire des francophones n’est pas qu’un objet numérique, il vise à rassembler une communauté de personnes parlant la langue française partout autour du monde, à faciliter l’apprentissage de la langue, à permettre un réusage de son contenu pour la production de jeux, à orienter vers d’autres ressources utiles et intéressantes sur la langue. 

Ces divers contenus seront ajoutés progressivement en 2020.
