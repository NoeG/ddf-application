Édité par :

![MC](../images/partners/MIN_Culture_RVB.png)


Maîtrise d’ouvrage et expertise scientifique :

![Lyon 3](../images/partners/lyon3.png)
![2IF](../images/partners/Kx300_Logo_2iF_VECTO.jpg)


Hébergement :

![CNRS](../images/partners/CNRS.png)
![Huma-Num](../images/partners/huma-num.jpg)


Partenaires :

![OIF](../images/partners/OIF.jpg)
![AUF](../images/partners/Logo_AUF.png)
![TV5Monde](../images/partners/tv5monde.png)
![Medias](../images/partners/logo_france_medias_monde.png)


Ils contribuent au contenu :

![Wiktionnaire](../images/partners/WiktionaryFr.svg.png)
![UCLouvain](../images/partners/1024px-UCLouvain_Saint-Louis_-_Bruxelles.png)
![ULaval](../images/partners/Universite_Laval.png)
![OQLF](../images/partners/oqlf.jpg)
![ATILF](../images/partners/atilf.jpg)
![ASOM](../images/partners/asom.jpg)


Développement du socle technique :

![Mnemotix](../images/partners/mnemotix.jpg)
