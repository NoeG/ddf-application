<!-- ## Effectuer une recherche
* [Rechercher un mot](#recherche-niv1)
* [Affiner la recherche](#recherche-niv2)
* [Requête experte dans la base de données](#recherche-niv3)
* Quelques exemples de recherches (amener à la découverte de listes sympas, orientation FLE ?)

## Compte utilisateur

Les comptes personnels seront implémentés pour la première version du Dictionnaire des francophones. Ils permettront de personnaliser son affichage et d’échanger avec les autres utilisateurs et utilisatrices.

Création et personnalisation du compte utilisateur (attendra la mise en place des comptes, encourager les avantages de la personnalisation) -->


# Recherche simple <a id="recherche-niv1"></a>

Pour rechercher un mot ou une expression, il suffit de les saisir dans la barre en haut de l’écran et de valider.

Dans le cas où le terme entré est connu, une liste de définitions qui correspondent exactement au mot recherché s’affiche au centre de l’écran. Les accents sont importants, et le Dictionnaire des francophones différencie les majuscules et les minuscules.

Lorsque la suite de lettres saisie ne dispose pas d’une entrée, une liste de mots proches apparaît afin de guider l’utilisateur vers des termes proches. 

Il n’y a pas d’auto complétion. C’est-à-dire qu’aucun mot n’est proposé sous forme de volet sous la barre de recherche lorsque l’utilisateur est en train de saisir le terme recherché.

L’ordre des définitions dépend ensuite de l’information géographique saisie sur la page d’accueil. Les couleurs correspondent aux grandes zones du monde.

# Recherche avancée <a id="recherche-niv2"></a>

Dans les prochaines versions, il sera possible d’affiner ses recherches en ajoutant un **domaine** de préférence, ainsi que d’autres paramètres.

# Recherche experte <a id="recherche-niv3"></a>

Il est par ailleurs possible de faire des requêtes directes dans la base de données, via une interface dédiée. Celle-ci n’est pour l’instant pas ouverte au grand public. Si vous souhaitez y avoir accès, écrivez à contact@dictionnairedesfrancophones.org 
