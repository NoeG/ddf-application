/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import yargs from 'yargs';
import ora from 'ora';
import dotenv from 'dotenv';
import {GraphQLContext, initEnvironment, NetworkLayerAMQP} from "@mnemotix/synaptix.js";
import env from "env-var";

import {connectors, commonFields, commonEntityFilter} from "./connectors";
import {generateDatastoreAdapater} from "../../middlewares/generateDatastoreAdapter";
import environment from "../../../../config/environment";

dotenv.config();
initEnvironment(environment);

export let indexData = async () => {
  let {repoName, removeIndices} = yargs
    .usage("yarn data:index -r [Repository name]")
    .example("yarn data:index -r ddf -R")
    .option('r', {
      alias: 'repoName',
      default: process.env.RDFSTORE_REPOSITORY_NAME,
      describe: 'RDF repository name',
      nargs: 1
    })
    .option('R', {
      alias: 'removeIndices',
      default: 0,
      describe: 'Remove indices',
      nargs: 1
    })
    .help('h')
    .alias('h', 'help')
    .epilog('Copyright Mnemotix 2019')
    .help()
    .argv;

  const amqpURL = `amqp://${process.env.RABBITMQ_LOGIN}:${process.env.RABBITMQ_PASSWORD}@${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`;

  let networkLayer = new NetworkLayerAMQP(
    amqpURL,
    process.env.RABBITMQ_EXCHANGE_NAME,
    {},
    {
      durable: !!parseInt(process.env.RABBITMQ_EXCHANGE_DURABLE || 1)
    }
  );

  await networkLayer.connect();

  let spinner = ora().start();
  spinner.spinner = "clock";

  const elasticsearchNode = env.get("ES_MASTER_URI").required().asString();

  let inserts = "";

  for(let connector of Object.values(connectors)){
    spinner.info(`Indexing "${connector.name}"`);
    let name = connector.name;
    delete connector.name;

    connector.elasticsearchNode = elasticsearchNode;
    connector.elasticsearchBasicAuthUser = env.get("ES_CLUSTER_USER").asString();
    connector.elasticsearchBasicAuthPassword = env.get("ES_CLUSTER_PWD").asString();
    connector.elasticsearchClusterSniff = false;
    connector.bulkUpdateBatchSize = 500;
    connector.indexCreateSettings = {
      "index.blocks.read_only_allow_delete": null
    };
    if (!name.includes("action")){
      connector.fields = connector.fields.concat(commonFields);
      let actionFilter = commonEntityFilter;
      if (connector.entityFilter){
        connector.entityFilter = `(${connector.entityFilter}) && (${actionFilter})`;
      } else {
        connector.entityFilter = actionFilter;
      }
    }

      inserts += `
    inst:${env.get("INDEX_PREFIX_TYPES_WITH").required().asString()}${name} :dropConnector "" .
    `;
    inserts += `
    inst:${env.get("INDEX_PREFIX_TYPES_WITH").required().asString()}${name} :createConnector '''
  ${JSON.stringify(connector)}
'''.
   `;
  }

  console.log(`PREFIX :<http://www.ontotext.com/connectors/elasticsearch#>
PREFIX inst:<http://www.ontotext.com/connectors/elasticsearch/instance#>
INSERT DATA {
  ${inserts}
}

`);


  // let command = "jobs.graph.index";
  //
  // await networkLayer.request(command, {
  //   headers: {
  //     command,
  //     sender: 'mnx:app:nodejs',
  //     timestamp: Date.now(),
  //   },
  //   body: repoName
  // });

  spinner.succeed(`Copy theses line in GraphDB to index`);
  process.exit(0);
};
