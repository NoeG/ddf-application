import {dataModel} from "../../datamodel/dataModel";
import {GraphQLContext, NetworkLayerAMQP, SynaptixDatastoreRdfAdapter} from '@mnemotix/synaptix.js';

/**
 * RDF model property parsed from a SPARQL query
 *
 */
class RDFProperty {

  constructor(node) {
    const [prefix, name] = node.propName.value.split('#');
    const [prefixValueType, nameValueType] = node.valueType.value.split('#');

    this.prefix = prefix + '#';
    this.name = name;

    // Get property namespace
    const prefixes = JSON.parse(process.env.SCHEMA_NAMESPACE_MAPPING);
    this.namespace = Object.keys(prefixes).find(key => prefixes[key] === this.prefix);

    this.type = {
      prefix: prefixValueType + '#',
      value: nameValueType,
      // Get property value type namespace
      namespace: Object.keys(prefixes).find(key => prefixes[key] === prefixValueType + '#'),
    };
    this.labels = {};
  }

  addLabel(lang, value) {
    this.labels[lang] = value;
  }

  getLabel(lang) {
    return this.labels[lang];
  }

}

/**
 * RDF model parsed from a SPARQL query
 *
 */
class RDFModel {

  /**
   * Create a object model from a SPARQL query response node
   *
   * @param {*} node
   */
  constructor(node) {
    const splitName = node.model.value.split('#');
    this.prefix = splitName[0] + '#';
    this.name = splitName[1];
    this.links = {};
    this.labels = {};
    this.literals = {};

    const prefixes = JSON.parse(process.env.SCHEMA_NAMESPACE_MAPPING);
    this.namespace = Object.keys(prefixes).find(key => prefixes[key] === this.prefix);
  }

  /**
   * Add a RDFProperty to model from a SPARQL query response node
   *
   * @param {*} node
   */
  addProperty(node) {
    const propType = node.propType.value.split('#')[1];
    const valueType = node.valueType.value.split('#')[1];

    let propertyCollection = null;
    if (propType === 'DatatypeProperty' && valueType === 'string') {
      propertyCollection = this.labels;
    } else if (propType === 'DatatypeProperty' && valueType !== 'string') {
      propertyCollection = this.literals;
    } else if (propType === 'ObjectProperty') {
      propertyCollection = this.links;
    }

    if (propertyCollection == null) {
      return;
    }

    const propName = node.propName.value.split('#')[1];
    let currentProperty = (propName in propertyCollection)
      ? propertyCollection[propName]
      : new RDFProperty(node);

    currentProperty.addLabel(node.valueName['xml:lang'], node.valueName.value);
    propertyCollection[propName] = currentProperty;
  }

  setSuperClass(node) {
    let [prefix, name] = node.superClassName.value.split('#');
    prefix = prefix + '#';

    const prefixes = JSON.parse(process.env.SCHEMA_NAMESPACE_MAPPING);

    this.superClass = {
      prefix: prefix,
      namespace: Object.keys(prefixes).find(key => prefixes[key] === prefix),
      name
    };
  }

  hasSuperClass() {
    return (typeof (this.superClass) !== 'undefined');
  }
}


/**
 * Create a Synaptix context and get back RDF fetcher
 *
 */
const getRdfPublisher = async () => {
  // Init network layer.
  const amqpURL = `amqp://${process.env.RABBITMQ_LOGIN}:${process.env.RABBITMQ_PASSWORD}@${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`;
  const amqpNetworkLayer = new NetworkLayerAMQP(amqpURL, process.env.RABBITMQ_EXCHANGE_NAME, {}, {
    durable: !!parseInt(process.env.RABBITMQ_EXCHANGE_DURABLE || 1)
  });
  await amqpNetworkLayer.connect();

  // Init datastore layer
  const datastoreAdapter = new SynaptixDatastoreRdfAdapter({
    networkLayer: amqpNetworkLayer,
    modelDefinitionsRegister: dataModel.generateModelDefinitionsRegister(),
  });
  const session = datastoreAdapter.getSession(new GraphQLContext({
    anonymous: true,
    lang: "fr"
  }));
  return session.getRdfSyncService().getRdfSyncPublisher();
};


/**
 * Fetch all classes and their prperties bound to namespace in graph database
 *
 */
export const fetchOwlClasses = async ({prefix, modelName}) => {

  const uriFilter = (modelName != null)
    ? `FILTER REGEX(str(?model), "${prefix}${modelName}") .`
    : `FILTER (STRSTARTS(str(?model), "${prefix}")) .`;

  // Query returning properties of any object of type owl:Class,
  // filtered by namespace or model name
  const query = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sesame: <http://www.openrdf.org/schema/sesame#>

SELECT ?model ?propName ?propType ?valueType ?valueName ?superClassName
WHERE {
    ${uriFilter}
    {
        ?model a owl:Class .
    } UNION {
		VALUES ?propType { owl:DatatypeProperty owl:ObjectProperty}
    	?propName a ?propType ;
             	 rdfs:domain ?model ;
              	 rdfs:range ?valueType ;
              	 rdfs:label ?valueName
    } UNION {
         ?model sesame:directSubClassOf ?superClassName .
    }
}
ORDER BY ?model ?propType
`;

  const rdfPublisher = await getRdfPublisher();
  const response = await rdfPublisher.select({query});

  const nodesByModel = {};

  // Group nodes by model name
  response.results.bindings.map(node => {
    const nodeName = node.model.value.split('#')[1];
    if (!(nodeName in nodesByModel)) {
      nodesByModel[nodeName] = [];
    }
    nodesByModel[nodeName].push(node);
  });

  let models = {};

  // Convert the set of nodes into list of RDF models,
  // each one containing corresponding properties
  Object.keys(nodesByModel).map(modelName => {
    const modelNodes = nodesByModel[modelName];

    let model = new RDFModel(modelNodes[0]);
    modelNodes.map(node => {
      if (typeof (node.propType) !== 'undefined') {
        model.addProperty(node);
      }
      if (typeof (node.superClassName) !== 'undefined') {
        model.setSuperClass(node)
      }
    });

    models[modelName] = model;
  });

  return models;
};