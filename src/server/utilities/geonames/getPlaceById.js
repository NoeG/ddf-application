/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {gotGet} from "./cache/gotCache";

/**
 * @typedef {object} GeonamePlace
 * @property {number} lat
 * @property {number} lng
 * @property {number} geonameId
 * @property {string} name
 * @property {number} countryId
 * @property {string} countryCode
 * @property {string} countryName
 * @property {string} countryName
 */

/**
 * Fetch Geonames place by an geonamesId. Cache result to save credit.
 *
 * @param {string} id - Geonames ID
 * @param {string} [lang] - Preferred language
 * @param {number} [retry=0] - Retry count
 * @return {GeonamePlace}
 */
export let getPlaceById = async ({id, lang, retry} = {lang: "fr", retry: 0}) => {
  let {body: geonamesPlace} = await gotGet(`http://api.geonames.org/getJSON?geonameId=${id.replace("geonames:", "")}&lang=${lang}&username=${process.env.GEONAMES_USERNAME}`);

  if (geonamesPlace){
    geonamesPlace.id =  `geonames:${geonamesPlace.geonameId}`;
    geonamesPlace.uri = `http://www.geonames.org/${geonamesPlace.geonameId}`;

    if (geonamesPlace.countryId && !geonamesPlace.countryId.includes("geonames:")){
      geonamesPlace.countryId =  `geonames:${geonamesPlace.countryId}`;
    }

    return geonamesPlace;
  }
};