/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {gotGet} from "./cache/gotCache";

/**
 * Search for Geonames places.
 * Use Geonames /searchJSON API and restrict to "ADM1" feature code (http://www.geonames.org/export/codes.html)
 *
 * @param {string} lng - Latitude
 * @param {string} lat - Longitude
 * @param {number} [limit=10] - Limit results size
 * @param {string} lang - Preferred language
 * @param {number} [retry=0] - Retry count
 * @return {GeonamePlace[]}
 */
export let searchNearByPlaces = async ({lat, lng, limit, lang, retry} = {lang: 'fr', limit: 10, retry: 0}) => {
  let {body: results} = await gotGet(`http://api.geonames.org/findNearbyJSON?lat=${lat}&lng=${lng}&lang=${lang}&maxRows=${limit || 10}&featureCode=ADM1&username=${process.env.GEONAMES_USERNAME}`);

  if (results.geonames){
    return results.geonames.map((place) => {
      place.id =  `geonames:${place.geonameId}`;
      place.uri = `http://www.geonames.org/${place.geonameId}`;

      if (place.countryId && !place.countryId.includes("geonames:")){
        place.countryId =  `geonames:${place.countryId}`;
      }

      return place;
    });
  }
};