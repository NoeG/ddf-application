/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import mean from "lodash/mean";

require('util').inspect.defaultOptions.depth = null;
import got from 'got/source';
import yargs from 'yargs';
import fs from 'fs';
import ora from 'ora';
import dotenv from 'dotenv';

dotenv.config();

export let generateGeonamesDistances = async () => {
  let spinner = ora().start();
  spinner.spinner = "clock";

  let {filePath} = yargs
    .usage("yarn data:geonames:distances")

    .option('o', {
      alias: 'filePath',
      default: './src/server/utilities/geonames/distances/geonamesIdsDistances.js',
      describe: 'RDF store REST endpoint',
      nargs: 1
    })
    .help('h')
    .alias('h', 'help')
    .epilog('Copyright Mnemotix 2019')
    .help()
    .argv;

  spinner.info(`Getting all countries ids...`);

  let {body: {records, nhits}} = await got("https://data.opendatasoft.com/api/records/1.0/search/?dataset=geonames-country%40public&rows=10000&facet=geonamesid&facet=languages", {
    json: true
  });

  let distances = fs.existsSync(filePath) ? JSON.parse(fs.readFileSync(filePath)) : {};
  let cacheCount = Object.keys(distances).length;

  records = records.filter(({fields: {languages}}) => (languages || "").includes("fr-"));

  if(cacheCount > 0){
    spinner.info(`Cache already contains ${Object.keys(distances).length} countries.`);
  }

  let remaining = records.length*records.length - cacheCount;

  spinner.info(`Processing ${records.length - cacheCount} french spoken countries`);
  spinner.start("");

  let fetchTimes = [];

  for(let {fields: {geonameid: geonamesid1, country: country1}} of records){
    for(let {fields: {geonameid: geonamesid2, country: country2}} of records){
      let distance;

      if (distances[`${geonamesid1}|${geonamesid2}`]){
        spinner.text = `${country1}|${country2} is in cache, don't refetch it...`;
        continue;
      } else if (geonamesid1 === geonamesid2){
        distance = 0;
      } else {
        let estimatedTime = mean(fetchTimes) / 1000 / 60;

        spinner.text = `Parsing ${country1}|${country2} (remaining ${remaining} combinations) (~${Math.ceil(estimatedTime * remaining)} minutes left)...`;

        let startTime = Date.now();

        let {body: distanceResult} = await got(`https://www.distance24.org/route.json?stops=${country1}|${country2}`, {
          json: true
        });

        fetchTimes.push(Date.now() - startTime);

        if(distanceResult.distance){
          distance = distanceResult.distance;
        }
      }

      distances[`${geonamesid1}|${geonamesid2}`] = distance;
      fs.writeFileSync(filePath, JSON.stringify(distances, null, " "));

      remaining--;
    }
  }

  spinner.succeed(`Distances generated with success.`);
  process.exit(0);
};
