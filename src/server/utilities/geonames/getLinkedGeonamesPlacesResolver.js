/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {connectionFromArray} from "@mnemotix/synaptix.js";
import {getPlaceById} from "./getPlaceById";

/**
 * @param {Model} place
 * @param {SynaptixDatastoreSession} synaptixSession
 * @return {Model}
 */
let enhanceDdfPlaceWithGeonames = async ({place, synaptixSession}) => {
  let id;

  // If place id is an absolute URI, ID is just after the origin URI.
  if (!!place.id.match(/https?:\/\/www.geonames.org\//)){
    id = place.id.replace(/https?:\/\/www.geonames.org\/([^\/]*).*/, "$1");
    // If place id is a prefixed URI, just remove the prefix.
  } else {
    id = place.id.slice(place.id.indexOf(":") + 1);
  }

  let geonamesPlace = await getPlaceById({id, lang: synaptixSession.getContext().getLang()});

  return Object.assign({}, place, geonamesPlace);
};

/**
 * A wrapper to a standard Synaptix.js GraphQL resolver to merge a list of Geonames places.
 *
 * @param {Model} object
 * @param {ResolverArgs|object} args
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {LinkDefinition} linkDefinition
 */
export let getLinkedGeonamesPlacesResolver = async (linkDefinition, object, args, synaptixSession) => {
  let places = await synaptixSession.getLinkedObjectsFor({object, linkDefinition, args});
  let extendedPlaces = [];

  for (let place of places) {
    extendedPlaces.push(await enhanceDdfPlaceWithGeonames({place, synaptixSession}));
  }

  return connectionFromArray(extendedPlaces, args);
};

/**
 * A wrapper to a standard Synaptix.js GraphQL resolver to merge a single Geonames place.
 *
 * @param {Model} object
 * @param {ResolverArgs|object} args
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {LinkDefinition} linkDefinition
 */
export let getLinkedGeonamesPlaceResolver = async (linkDefinition, object, args, synaptixSession) => {
  let place;

  if (object.place?.id){
    place = object.place;
  } else {
    place = await synaptixSession.getLinkedObjectFor({
      object,
      linkDefinition,
      args,
      discardTypeAssertion: true
    });
  }

  if(place){
    return enhanceDdfPlaceWithGeonames({place, synaptixSession});
  }
};