/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {gotGet} from "./cache/gotCache";

/**
 * @typedef {object} GeonamePlace
 * @property {number} lat
 * @property {number} lng
 * @property {number} geonameId
 * @property {string} name
 * @property {number} countryId
 * @property {string} countryCode
 * @property {string} countryName
 * @property {string} countryName
 */

/**
 * Fetch Geonames place by a geocoordinates. Cache result to save credit.
 *
 * @param {string} id - Geonames ID
 * @param {string} lang - Preferred language
 * @param {number} [retry=0] - Retry count
 * @return {GeonamePlace}
 */
export let getPlaceByGeoCoords = async ({lat, lng, lang, retry} = {lang: "fr", retry: 0}) => {
  let {body: results} = await gotGet(`http://api.geonames.org/findNearbyJSON?lat=${lat}&lng=${lng}&lang=${lang}&featureCode=ADM3&featureCode=ADM4&featureCode=ADM5&username=${process.env.GEONAMES_USERNAME}`);

  if (results.geonames && results.geonames.length > 0){
    let place =  results.geonames[0];

    place.id =  `geonames:${place.geonameId}`;
    place.uri = `http://www.geonames.org/${place.geonameId}`;

    if (place.countryId && !place.countryId.includes("geonames:")){
      place.countryId =  `geonames:${place.countryId}`;
    }

    return place;
  }
};