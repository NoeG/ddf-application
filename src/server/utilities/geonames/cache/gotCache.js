/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import got from "got/source";
export const gotCache = new Map();

export const gotGet = async (uri, params = {}) => {
  let hash = `${uri}${JSON.stringify(params)}`;

  if (gotCache.has(hash)){
    return gotCache.get(hash);
  }

  let response = await got(uri, {
    json: true,
    retry: 3,
    ...params
  });

  if (response?.statusCode < 400) {
    gotCache.set(hash, response);
    return response;
  }
};