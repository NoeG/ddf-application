

let gotMock = jest.fn();
jest.doMock('got/source', () => gotMock);
let {searchPlaces} = require('../searchPlaces');

/**
 * Setup process.env.GEONAMES_USERNAME for the test suite, without side effects
 */
let processEnvGeonamesUserSave;
beforeAll(() => {
  processEnvGeonamesUserSave = process.env.GEONAMES_USERNAME;
  process.env.GEONAMES_USERNAME = 'MyGeonamesUser';
});
afterAll(() => {
  process.env.GEONAMES_USERNAME = processEnvGeonamesUserSave;
});

describe('searchPlaces()', () => {
  test('regular call', async () => {
    gotMock.mockImplementation(() => {
      return ({
        statusCode: 200,
        body: {
          geonames: [{
            geonameId: 'place123',
            countryId: 'country456'
          }]
        }
      })
    });
    let results = await searchPlaces({
      lang: 'fr', 
      qs: 'myCity',
      limit: 5
    });

    expect(gotMock.mock.calls[0][0]).toEqual('http://api.geonames.org/searchJSON?name_startsWith=myCity&lang=fr&maxRows=5&featureCode=PCLI&featureCode=PCLD&featureCode=ADM1&featureCode=ADM4&featureCode=ADM4&featureCode=ADM5&username=MyGeonamesUser');
    expect(results).toEqual([{
      geonameId: "place123", 
      countryId: "geonames:country456", 
      id: "geonames:place123", 
      uri: "http://www.geonames.org/place123"
    }]);
  });

  test('default values for parameters', async () => {
    gotMock.mockImplementation(() => ({
      statusCode: 200,
      body: {
        geonames: [{
          geonameId: 'place123',
          countryId: 'country456'
        }]
      }
    }));
    let results = await searchPlaces({
      qs: 'myCity'
    });

    expect(gotMock.mock.calls[0][0]).toEqual('http://api.geonames.org/searchJSON?name_startsWith=myCity&lang=fr&maxRows=10&featureCode=PCLI&featureCode=PCLD&featureCode=ADM1&featureCode=ADM4&featureCode=ADM4&featureCode=ADM5&username=MyGeonamesUser');
    expect(results).toEqual([{
      geonameId: "place123", 
      countryId: "geonames:country456", 
      id: "geonames:place123", 
      uri: "http://www.geonames.org/place123"
    }]);
  });
});
