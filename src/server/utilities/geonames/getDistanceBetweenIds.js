/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {geonamesIdsDistances} from "./distances/geonamesIdsDistances";
import {logDebug} from "@mnemotix/synaptix.js";

/**
 * Get distance between to geonames ids
 *
 * @param {string} id1 - Geonames ID 1
 * @param {string} id2 - Geonames ID 2
 * @return {number}
 */
export let getDistanceBetweenIds = async ({id1, id2} = {retry: 0}) => {
  id1 = id1.replace("geonames:", "");
  id2 = id2.replace("geonames:", "");

  return geonamesIdsDistances[`${id1}|${id2}`] || geonamesIdsDistances[`${id2}|${id1}`] || 0;;
};