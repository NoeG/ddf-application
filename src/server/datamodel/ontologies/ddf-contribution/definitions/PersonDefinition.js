/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  ModelDefinitionAbstract,
  LiteralDefinition,
  MnxOntologies,
  LinkDefinition, LabelDefinition
} from "@mnemotix/synaptix.js";
import PlaceDefinition from "../../geonames/definitions/PlaceDefinition";
import SemanticPropertyDefinition from "../../ontolex/definitions/SemanticPropertyDefinition";

export default class PersonDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [MnxOntologies.mnxAgent.ModelDefinitions.PersonDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ddf:Person";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "agent";
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'preferredDomain',
        rdfObjectProperty: 'ddf:hasPreferredDomain',
        relatedModelDefinition: SemanticPropertyDefinition,
        relatedInputName: "preferredDomainInput"
      }),
      new LinkDefinition({
        linkName: 'place',
        rdfObjectProperty: 'ddf:hasLocalisation',
        relatedModelDefinition: PlaceDefinition,
        relatedInputName: "placeInput"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "linguisticProfile",
        rdfDataProperty: "ddf:linguisticProfile",
        pathInIndex: "linguisticProfile"
      }),
      new LabelDefinition({
        labelName: "otherSpokenLanguages",
        rdfDataProperty: "ddf:otherSpokenLanguages",
        pathInIndex: "otherSpokenLanguages"
      }),
      new LabelDefinition({
        labelName: "otherInformation",
        rdfDataProperty: "ddf:otherInformation",
        pathInIndex: "otherInformation"
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'yearOfBirth',
        rdfDataProperty: 'ddf:yearOfBirth',
        rdfDataType: "http://www.w3.org/2001/XMLSchema#int"
      }),
    ];
  }
};

