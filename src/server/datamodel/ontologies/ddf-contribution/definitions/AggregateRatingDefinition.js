/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  ModelDefinitionAbstract,
  MnxOntologies,
  LinkDefinition
} from "@mnemotix/synaptix.js";

export default class AggregateRatingDefinition extends ModelDefinitionAbstract {
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "schema": "https://schema.org/"
    }
  }
  /**
   * @inheritDoc
   */
  static isInstantiable() {
    return false;
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [MnxOntologies.mnxCommon.ModelDefinitions.EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "schema:AggregateRating";
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return "aggregate-rating";
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasRater',
        rdfObjectProperty: 'ddf:rater',
        relatedModelDefinition: MnxOntologies.mnxAgent.ModelDefinitions.UserAccountDefinition,
        relatedInputName: "userAccountRaterInput"
      })
    ];
  }
};

