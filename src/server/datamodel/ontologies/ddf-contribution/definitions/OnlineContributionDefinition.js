/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {ModelDefinitionAbstract, LinkDefinition, MnxOntologies} from "@mnemotix/synaptix.js";
import {LexicalEntryDefinition, LexicalSenseDefinition, FormDefinition} from "../../ontolex/definitions";

/**
 * Caution, this class is not a extension mnx:OnlineContribution but directly a sioc:Item.
 *
 * @see https://gitlab.com/mnemotix/mnx-models/blob/master/ddf-contrib/ontologie-ddf-contrib.pdf
 */
export default class OnlineContributionDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [MnxOntologies.mnxContribution.ModelDefinitions.OnlineContributionDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "sioc:Item";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "online-contribution";
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'form',
        symmetricLinkName: "contributions",
        rdfObjectProperty: 'ddf:aboutForm',
        relatedModelDefinition: FormDefinition,
        relatedInputName: "aboutFormInput"
      }),
      new LinkDefinition({
        linkName: 'lexicalEntry',
        symmetricLinkName: "contributions",
        rdfObjectProperty: 'ddf:aboutEtymology',
        relatedModelDefinition: LexicalEntryDefinition,
        relatedInputName: "aboutEtymologyInput"
      }),
      new LinkDefinition({
        linkName: 'sense',
        symmetricLinkName: "contributions",
        rdfObjectProperty: 'ddf:aboutSense',
        relatedModelDefinition: LexicalSenseDefinition,
        relatedInputName: "aboutSenseInput"
      })
    ];
  }
};

