/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  LinkDefinition, MnxOntologies,
  ModelDefinitionAbstract,
} from "@mnemotix/synaptix.js";
import AggregateRatingDefinition from "./AggregateRatingDefinition";

export default class ReportingRatingDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [AggregateRatingDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ddf:ReportingRating";
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return AggregateRatingDefinition.getIndexType();
  }

  /**
   * @inheritDoc
   */
  static getIndexFilters(){
    return [{
      "term":  {
        "types": "http://data.dictionnairedesfrancophones.org/ontology/ddf#ReportingRating"
      }
    }]
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasEntity',
        rdfReversedObjectProperty: 'ddf:hasReportingRating',
        relatedModelDefinition: MnxOntologies.mnxCommon.ModelDefinitions.EntityDefinition,
        relatedInputName: "itemReviewedInput"
      })
    ];
  }
};

