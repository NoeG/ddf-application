/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  createObjectInConnectionResolver,
  generateCreateMutationDefinition, I18nError, LinkPath, Model
} from "@mnemotix/synaptix.js";
import {LexicalSenseDefinition} from "../../../ontolex/definitions";
import SuppressionRatingDefinition from "../../definitions/SuppressionRatingDefinition";
import merge from "lodash/merge";

export let CreateSuppressionRating = generateCreateMutationDefinition({
  modelDefinition: SuppressionRatingDefinition,
  extraInputArgs: `
  """ Lexical sense id """
  lexicalSenseId: ID!
  `,
  description: `
   Create a new suppression rating associated to logged user.
  
   This mutation input is protected and following i18nkey errors can be sent :

   - **USER_MUST_BE_AUTHENTICATED** : If inited session is anomymous
   - **USER_NOT_ALLOWED**: If logged user doesn't belong to Operator userGroup
   - **ALREADY_RATED** : If the lexicalSense validation rating is already created for the logged user.
  `
});

export let CreateSuppressionRatingResolvers = {
  Mutation: {
    /**
     * @param {object} _
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    createSuppressionRating: {
      /**
       * @param _
       * @param {string} lexicalSenseId
       * @param {object} objectInput
       * @param {SynaptixDatastoreRdfSession} synaptixSession
       */
      resolve: async (_, {input: {lexicalSenseId, objectInput}}, synaptixSession) => {
        let userAccount = await synaptixSession.getLoggedUserAccount();

        if (userAccount){
          if (await synaptixSession.isObjectExistsForLinkPath({
            object: new Model(synaptixSession.extractIdFromGlobalId(lexicalSenseId)),
            modelDefinition: LexicalSenseDefinition,
            linkPath: new LinkPath()
              .step({
                linkDefinition: LexicalSenseDefinition.getLink("hasSuppressionRating"),
              })
              .step({
                linkDefinition: SuppressionRatingDefinition.getLink("hasRater"),
                targetId: userAccount.id
              })
          })) {
            throw new I18nError("This lexical sense has already been rated by logged user", "ALREADY_RATED");
          }

          objectInput = merge(objectInput, {
            itemReviewedInput: {
              id: synaptixSession.extractIdFromGlobalId(lexicalSenseId)
            },
            userAccountRaterInput: {
              id: userAccount.id
            }
          });

          return createObjectInConnectionResolver(
            SuppressionRatingDefinition,
            [], _, {
              input: {
                objectInput
              }
            }, synaptixSession
          );
        }
      }
    },
  },
};