/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  createObjectInConnectionResolver,
  generateCreateMutationDefinition, I18nError, LinkPath, Model
} from "@mnemotix/synaptix.js";
import ValidationRatingDefinition from "../../definitions/ValidationRatingDefinition";
import {LexicalSenseDefinition} from "../../../ontolex/definitions";
import merge from "lodash/merge";
import ReportingRatingDefinition from "../../definitions/ReportingRatingDefinition";

export let CreateValidationRating = generateCreateMutationDefinition({
  modelDefinition: ValidationRatingDefinition,
  extraInputArgs: `
  """ Lexical sense id """
  lexicalSenseId: ID!
  `,
  description: `
   Create a new validation rating associated to logged user.
  
   This mutation input is protected and following i18nkey errors can be sent :

   - **USER_MUST_BE_AUTHENTICATED** : If inited session is anomymous
   - **ALREADY_RATED** : If the lexicalSense validation rating is already created for the logged user.
  `
});

export let CreateValidationRatingResolvers = {
  Mutation: {
    /**
     * @param {object} _
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    createValidationRating: {
      /**
       * @param _
       * @param {string} lexicalSenseId
       * @param {object} objectInput
       * @param {SynaptixDatastoreRdfSession} synaptixSession
       */
      resolve: async (_, {input: {lexicalSenseId, objectInput}}, synaptixSession) => {
        let userAccount = await synaptixSession.getLoggedUserAccount();

        if (userAccount){
          if (await synaptixSession.isObjectExistsForLinkPath({
            object: new Model(synaptixSession.extractIdFromGlobalId(lexicalSenseId)),
            modelDefinition: LexicalSenseDefinition,
            linkPath: new LinkPath()
              .step({
                linkDefinition: LexicalSenseDefinition.getLink("hasValidationRating"),
              })
              .step({
                linkDefinition: ValidationRatingDefinition.getLink("hasRater"),
                targetId: userAccount.id
              })
          })) {
            throw new I18nError("This lexical sense has already been rated by logged user", "ALREADY_RATED");
          }

          objectInput = merge(objectInput, {
            itemReviewedInput: {
              id: synaptixSession.extractIdFromGlobalId(lexicalSenseId)
            },
            userAccountRaterInput: {
              id: userAccount.id
            }
          });

          return createObjectInConnectionResolver(
            ValidationRatingDefinition,
            [], _, {
              input: {
                objectInput
              }
            }, synaptixSession
          );
        }
      }
    },
  },
};