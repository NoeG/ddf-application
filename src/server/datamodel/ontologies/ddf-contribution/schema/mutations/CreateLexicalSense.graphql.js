/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  createObjectInConnectionResolver,
  FormValidationError,
  generateCreateMutationDefinition,
  LinkPath,
  MnxOntologies,
  Model
} from "@mnemotix/synaptix.js";
import {LexicalSenseDefinition} from "../../../ontolex/definitions";
import {object, string} from "yup";
import merge from "lodash/merge";
import {getLexicalEntryContribInputsFromFormWrittenRep} from "../../../ontolex/helpers/getLexicalEntryContribInputsFromFormWrittenRep";
import env from "env-var";

const {ConceptDefinition} = MnxOntologies.mnxSkos.ModelDefinitions;

export let CreateLexicalSense = generateCreateMutationDefinition({
  modelDefinition: LexicalSenseDefinition,
  extraInputArgs: `
  """ Lexical sense definition """
  definition: String
  """ Related form written representation """
  formWrittenRep: String
  """ Related lexicalEntry inherited type name """
  lexicalEntryTypeName: String
  """ Related grammatical category concept ID """
  grammaticalCategoryId: String
  `,
  description: `
  Create a new lexical sense, precising it's related form written representaiton, lexicalEntry typename and grammatical category Id.

  This mutation input is protected by the Synaptix.js [formValidationMiddleware](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#form-input-validation). Following i18nkey errors can be sent :

  - **USER_MUST_BE_AUTHENTICATED** : If inited session is anomymous.
  - **IS_REQUIRED** : If an input field is missing.
  - **LEXICAL_ENTRY_TYPENAME_NOT_AUTHORIZED** : If "lexicalEntryTypeName" input field is set with not authorized value.
  - **LEXICAL_ENTRY_TYPENAME_NOT_MATCH_FORM** : If "lexicalEntryTypeName" input field not match form written representation
  - **GRAMMATICAL_CATEGORY_NOT_MATCH_FORM** : If "lexicalEntryTypeName" input field not match form written representation

  `
});

export let CreateLexicalSenseResolvers = {
  Mutation: {
    /**
     * @param {object} _
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    createLexicalSense: {
      validationSchema: object().shape({
        input: object().shape({
          definition: string().trim().required("IS_REQUIRED"),
          formWrittenRep: string().trim().required("IS_REQUIRED"),
          lexicalEntryTypeName: string()
            .trim()
            .required("IS_REQUIRED")
            .oneOf(["Word", "Affix", "MultiWordExpression"], "LEXICAL_ENTRY_TYPENAME_NOT_AUTHORIZED")
            .test("match-form", "LEXICAL_ENTRY_TYPENAME_NOT_MATCH_FORM", function (value) {
              if (!!this.parent.formWrittenRep) {
                let {lexicalEntryTypeName} = getLexicalEntryContribInputsFromFormWrittenRep(this.parent.formWrittenRep);
                return lexicalEntryTypeName === value;
              }
            }),
          grammaticalCategoryId: string()
            .trim()
            .required("IS_REQUIRED")
        })
      }),
      /**
       * @param _
       * @param definition
       * @param formWrittenRep
       * @param lexicalEntryTypeName
       * @param grammaticalCategoryId
       * @param objectInput
       * @param {SynaptixDatastoreRdfSession} synaptixSession
       */
      resolve: async (_, {input: {definition, formWrittenRep, lexicalEntryTypeName, grammaticalCategoryId, objectInput}}, synaptixSession) => {

        /**
         * First validate that submitted grammaticalCategoryId is in right scheme
         */
        await assertGrammaticalCategoryIdMatchFormWrittenRep({grammaticalCategoryId, formWrittenRep, synaptixSession});

        /**
         * We tweak here the generic mechanism of creation mutation by merging some extra infos with hypothetical objectInput :
         *
         *  - The related lexicalEntry inherited type to create, precising it's typename because an ontolex:LexicalEntry is not instantiable. We must precise if it's a Word, Affix or MultiWordExpression
         *  - The related entry with lexicographicResource precision.
         *  - The related grammatical category concept, given that the link name differs among different lexicalEntry inherited types.
         */
        let grammaticalCategoryInputName;

        switch (lexicalEntryTypeName) {
          case "MultiWordExpression":
            grammaticalCategoryInputName = "multiWordTypeInput";
            break;
          case "Affix":
            grammaticalCategoryInputName = "termElementInput";
            break;
          default:
            grammaticalCategoryInputName = "partOfSpeechInput";
        }

        objectInput = merge(objectInput, {
          definition,
          processed: false,
          lexicalEntryInput: {
            inheritedTypename: lexicalEntryTypeName,
            canonicalFormInput: {
              writtenRep: formWrittenRep
            },
            entryInput: {
              lexicographicResourceInput: {
                id: env.get("DDF_LEXICOGRAPHIC_RESOURCE_ID").required().asString()
              }
            },
            [grammaticalCategoryInputName]: {
              id: synaptixSession.extractIdFromGlobalId(grammaticalCategoryId)
            }
          }
        });

        return createObjectInConnectionResolver(
          LexicalSenseDefinition,
          [], _, {
            input: {
              objectInput
            }
          }, synaptixSession)
      }
    },
  },
};

/**
 * Check if grammatical category concept id match for form written rep
 *
 * @param grammaticalCategoryId
 * @param formWrittenRep
 * @param synaptixSession
 * @return {boolean}
 */
export async function assertGrammaticalCategoryIdMatchFormWrittenRep({grammaticalCategoryId, formWrittenRep, synaptixSession}) {
  let {grammaticalCategorySchemeId} = getLexicalEntryContribInputsFromFormWrittenRep(formWrittenRep);

  if (!await synaptixSession.isObjectExistsForLinkPath({
    object: new Model(synaptixSession.extractIdFromGlobalId(grammaticalCategoryId)),
    modelDefinition: ConceptDefinition,
    linkPath: new LinkPath()
      .step({
        linkDefinition: ConceptDefinition.getLink("inScheme"),
        targetId: grammaticalCategorySchemeId
      })
  })) {
    throw new FormValidationError(`${grammaticalCategoryId} is not in scheme ${grammaticalCategorySchemeId}`, [{
      field: "input.grammaticalCategoryId",
      errors: [
        "GRAMMATICAL_CATEGORY_NOT_MATCH_FORM"
      ]
    }]);
  }
}