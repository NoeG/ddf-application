/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {getObjectResolver, updateObjectResolver,} from "@mnemotix/synaptix.js";
import LexicalSenseDefinition from "../../../ontolex/definitions/LexicalSenseDefinition";
import merge from "lodash/merge";
import LexicalEntryDefinition from "../../../ontolex/definitions/LexicalEntryDefinition";
import {assertGrammaticalCategoryIdMatchFormWrittenRep} from "./CreateLexicalSense.graphql";
import FormDefinition from "../../../ontolex/definitions/FormDefinition";
import {getLexicalEntryContribInputsFromFormWrittenRep} from "../../../ontolex/helpers/getLexicalEntryContribInputsFromFormWrittenRep";
import MultiWordExpressionDefinition from "../../../ontolex/definitions/MultiWordExpressionDefinition";
import AffixDefinition from "../../../ontolex/definitions/AffixDefinition";
import WordDefinition from "../../../ontolex/definitions/WordDefinition";

export let UpdateLexicalSense = `
input UpdateLexicalSenseInput {
  objectId: ID!
  objectInput: LexicalSenseInput
  """ Lexical sense definition """
  definition: String
  """ Related grammatical category concept ID """
  grammaticalCategoryId: String
}

type UpdateLexicalSensePayload {
  updatedObject: LexicalSense
}

extend type Mutation{
  """
  Update a lexical sense.

  This mutation is possible if one of following graphQL shield rules is passed :

  - isCreator() : If logged user initing the mutation is the lexicalSense object creator.
  - isAdmin()   : If logged user initing the mutation is in the admin group.

  If not passed, the following i18nkey errors can be sent : **USER_NOT_ALLOWED**
  
  This mutation input also is protected by the Synaptix.js [formValidationMiddleware](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#form-input-validation). Following i18nkey errors can be sent :

  - **GRAMMATICAL_CATEGORY_NOT_MATCH_FORM** : If "lexicalEntryTypeName" input field not match form written representation
  """
  updateLexicalSense(input: UpdateLexicalSenseInput!): UpdateLexicalSensePayload
}
`;


export let UpdateLexicalSenseResolvers = {
  Mutation: {
    /**
     * @param {object} _
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    updateLexicalSense: {
      /**
       * @param _
       * @param definition
       * @param grammaticalCategoryId
       * @param objectInput
       * @param objectId
       * @param {SynaptixDatastoreRdfSession} synaptixSession
       */
      resolve: async (_, {input: {definition, grammaticalCategoryId, objectInput, objectId}}, synaptixSession, info) => {
        let lexicalSenseId = synaptixSession.extractIdFromGlobalId(objectId);

        if (grammaticalCategoryId) {
          let lexicalEntry = await synaptixSession.getLinkedObjectFor({
            object: {id: lexicalSenseId},
            linkDefinition: LexicalSenseDefinition.getLink("lexicalEntry")
          });

          let form = await synaptixSession.getLinkedObjectFor({
            object: lexicalEntry,
            linkDefinition: LexicalEntryDefinition.getLink("canonicalForm")
          });

          let formWrittenRep = await synaptixSession.getLocalizedLabelFor({
            object: form,
            labelDefinition: FormDefinition.getLabel("writtenRep")
          });

          /**
           * First validate that submitted grammaticalCategoryId is in right scheme
           */
          await assertGrammaticalCategoryIdMatchFormWrittenRep({
            grammaticalCategoryId,
            formWrittenRep,
            synaptixSession
          });

          /**
           * We tweak here the generic mechanism of creation mutation by merging some extra infos with hypothetical objectInput :
           *
           *  - The related lexicalEntry inherited type to update, precising it's typename because an ontolex:LexicalEntry is not instantiable. We must precise if it's a Word, Affix or MultiWordExpression
           *  - The related entry with lexicographicResource precision.
           *  - The related grammatical category concept, given that the link name differs among different lexicalEntry inherited types.
           */
          let grammaticalCategoryInputName;
          let lexicalEntryDefinition;

          let {lexicalEntryTypeName} = getLexicalEntryContribInputsFromFormWrittenRep(formWrittenRep);

          switch (lexicalEntryTypeName) {
            case "MultiWordExpression":
              grammaticalCategoryInputName = "multiWordTypeInput";
              lexicalEntryDefinition = MultiWordExpressionDefinition;
              break;
            case "Affix":
              grammaticalCategoryInputName = "termElementInput";
              lexicalEntryDefinition = AffixDefinition;
              break;
            default:
              grammaticalCategoryInputName = "partOfSpeechInput";
              lexicalEntryDefinition = WordDefinition;
          }

          await updateObjectResolver(
            lexicalEntryDefinition, _, {
              input: {
                objectId: lexicalEntry.id,
                objectInput: {
                  [grammaticalCategoryInputName]: {
                    id: synaptixSession.extractIdFromGlobalId(grammaticalCategoryId)
                  }
                }
              }
            }, synaptixSession
          );
        }

        if (definition) {
          objectInput = merge(objectInput, {
            definition,
          });
        }

        if (Object.keys(objectInput).length > 0) {
          return updateObjectResolver(
            LexicalSenseDefinition, _, {
              input: {
                objectId,
                objectInput
              }
            }, synaptixSession)
        } else {
          return {
            updatedObject: await getObjectResolver(LexicalSenseDefinition, _, {id: objectId}, synaptixSession, info)
          };
        }
      }
    },
  },
};
