/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *  
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionResolverFor,
  getLocalizedLabelResolver,
  filteringArgs,
  getObjectsResolver,
  getObjectResolver,
  connectionFromArray, generateConnectionForType, MnxOntologies, LinkPath
} from "@mnemotix/synaptix.js";

import OnlineContributionDefinition from "../../definitions/OnlineContributionDefinition";
import FormDefinition from "../../../ontolex/definitions/FormDefinition";
import LexicalEntryDefinition from "../../../ontolex/definitions/LexicalEntryDefinition";
import LexicalSenseDefinition from "../../../ontolex/definitions/LexicalSenseDefinition";

export let OnlineContributionType = `
type OnlineContribution implements EntityInterface & OnlineContributionInterface {
  ${MnxOntologies.mnxContribution.schema.Interface.Properties.OnlineContributionInterfaceProperties}
  
  """ Related entry dealing with etymology """
  aboutEtymology: LexicalEntryInterface
  
  """ Related sense """
  aboutForm: Form
  
  """ Related sense """
  aboutSense: LexicalSense 
}         
  
input OnlineContributionInput {
  ${OnlineContributionDefinition.generateGraphQLInputProperties()}
}

${generateConnectionForType("OnlineContribution")}

extend type Query{
  """ Search for OnlineContribution """
  posts(${connectionArgs}, ${filteringArgs}): OnlineContributionConnection
  
  """ Get OnlineContribution """
  post(id:ID): OnlineContribution
  
  """ Get posts (aka discussion) for a written form etymology """
  postsAboutEtymologyForAccurateWrittenForm(formQs: String, ${connectionArgs}): OnlineContributionConnection
  
  """ Get top-ranked post for a written form style """
  topPostAboutEtymologyForAccurateWrittenForm(formQs: String): OnlineContribution
  
  """ Get posts (aka discussion) for a written form """
  postsAboutFormForAccurateWrittenForm(formQs: String, ${connectionArgs}): OnlineContributionConnection
  
  """ Get top-ranked post for a written form """
  topPostAboutFormForAccurateWrittenForm(formQs: String): OnlineContribution
}
`;

export let OnlineContributionTypeResolvers = {
  OnlineContribution: {
    ...generateTypeResolvers("OnlineContribution"),
  },
  Query: {
    posts: getObjectsResolver.bind(this, OnlineContributionDefinition),
    post: getObjectResolver.bind(this, OnlineContributionDefinition),
    postsAboutEtymologyForAccurateWrittenForm: async (parent, {formQs, ...args}, synaptixSession) => {
      let posts = await getPostsAboutEtymologyForAccurateWrittenForm({synaptixSession, formQs});
      return connectionFromArray(posts, args)
    },
    topPostAboutEtymologyForAccurateWrittenForm: async (parent, {formQs, ...args}, synaptixSession) => {
      let posts = await getPostsAboutEtymologyForAccurateWrittenForm({synaptixSession, formQs});
      return posts[0];
    },
    postsAboutFormForAccurateWrittenForm: async (parent, {formQs, ...args}, synaptixSession) => {
      let posts = await getPostsAboutFormForAccurateWrittenForm({synaptixSession, formQs});
      return connectionFromArray(posts, args)
    },
    topPostAboutFormForAccurateWrittenForm: async (parent, {formQs, ...args}, synaptixSession) => {
      let posts = await getPostsAboutFormForAccurateWrittenForm({synaptixSession, formQs});
      return posts[0];
    }
  },
  ...generateConnectionResolverFor("OnlineContribution")
};

let getPostsAboutEtymologyForAccurateWrittenForm = async ({formQs, synaptixSession}) => synaptixSession.getObjects({
  modelDefinition: OnlineContributionDefinition,
  args: {
    linkPaths: [
      new LinkPath()
        .step({linkDefinition: OnlineContributionDefinition.getLink("lexicalEntry")})
        .step({linkDefinition: LexicalEntryDefinition.getLink("canonicalForm")})
        .filterOnProperty({propertyDefinition: FormDefinition.getProperty("writtenRep"), value: `^${formQs}$`})
    ]
  }
});

let getPostsAboutFormForAccurateWrittenForm = async ({formQs, synaptixSession}) => synaptixSession.getObjects({
  modelDefinition: OnlineContributionDefinition,
  args: {
    linkPaths: [
      new LinkPath()
        .step({linkDefinition: OnlineContributionDefinition.getLink("form")})
        .filterOnProperty({propertyDefinition: FormDefinition.getProperty("writtenRep"), value: `^${formQs}$`})
    ]
  }
});