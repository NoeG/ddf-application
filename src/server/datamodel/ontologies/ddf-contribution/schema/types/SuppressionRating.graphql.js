/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  generateConnectionForType,
  generateConnectionResolverFor,
  generateTypeResolvers,
  getLinkedObjectsCountResolver
} from "@mnemotix/synaptix.js";

import {
  AggregateRatingInterfaceProperties,
  getEntityAggregatingRatingForLoggedUserResolver
} from "./AggregateRatingInterface.graphql";
import LexicalSenseDefinition from "../../../ontolex/definitions/LexicalSenseDefinition";
import SuppressionRatingDefinition from '../../definitions/SuppressionRatingDefinition';

export let SuppressionRatingType = `
type SuppressionRating {
  ${AggregateRatingInterfaceProperties}
}
  
input SuppressionRatingInput{
  ${SuppressionRatingDefinition.generateGraphQLInputProperties()}
}

extend type LexicalSense{
  """ Suppression rating related to logged user """
  suppressionRating: ValidationRating
  
  """ Get the suppression rating count """
  suppressionRatingsCount: Int
}

${generateConnectionForType("SuppressionRating")}
`;


export let SuppressionRatingTypeResolvers = {
  SuppressionRating: {
    ...generateTypeResolvers("SuppressionRating"),
  },
  LexicalSense: {
    suppressionRating: getEntityAggregatingRatingForLoggedUserResolver.bind(this, SuppressionRatingDefinition),
    suppressionRatingsCount: getLinkedObjectsCountResolver.bind(this, LexicalSenseDefinition.getLink("hasSuppressionRating"))
  },
  ...generateConnectionResolverFor("SuppressionRating")
};
