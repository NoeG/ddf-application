/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *  
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  generateTypeResolvers,
  generateConnectionResolverFor,
  getLocalizedLabelResolver,
  getLinkedObjectResolver
} from "@mnemotix/synaptix.js";
import PersonDefinition from "../../definitions/PersonDefinition";
import {getLinkedGeonamesPlaceResolver} from "../../../../../utilities/geonames/getLinkedGeonamesPlacesResolver";

export let PersonType = `
extend type Person {
  """ Year of birth """
  yearOfBirth: Int
  
  """ Linguistic profile """
  linguisticProfile: String
  
  """ Other spoken languages """
  otherSpokenLanguages: String
  
  """ Other information """
  otherInformation: String
  
  """ Preferred domain """
  preferredDomain: Concept
  
  """ Locating place """
  place: PlaceInterface
}         
  
extend input PersonInput {
  ${PersonDefinition.generateGraphQLInputProperties({excludeInheritedInputProperties: true})}
}
`;

export let PersonTypeResolvers = {
  Person: {
    ...generateTypeResolvers("Person"),
    yearOfBirth: (object) => object.yearOfBirth,
    linguisticProfile: getLocalizedLabelResolver.bind(this, PersonDefinition.getLabel("linguisticProfile")),
    otherSpokenLanguages: getLocalizedLabelResolver.bind(this, PersonDefinition.getLabel("otherSpokenLanguages")),
    otherInformation: getLocalizedLabelResolver.bind(this, PersonDefinition.getLabel("otherInformation")),
    preferredDomain: getLinkedObjectResolver.bind(this, PersonDefinition.getLink("preferredDomain")),
    place: getLinkedGeonamesPlaceResolver.bind(this, PersonDefinition.getLink("place"))
  },
  ...generateConnectionResolverFor("Person")
};