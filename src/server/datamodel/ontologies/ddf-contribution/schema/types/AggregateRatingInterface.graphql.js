/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  generateConnectionForType,
  generateConnectionResolverFor,
  generateInterfaceResolvers,
  getLinkedObjectResolver,
  EntityInterfaceProperties, getObjectResolver,
} from "@mnemotix/synaptix.js";
import AggregateRatingDefinition from '../../definitions/AggregateRatingDefinition';
import SuppressionRatingDefinition from "../../definitions/SuppressionRatingDefinition";

export let AggregateRatingInterfaceProperties = `
  ${EntityInterfaceProperties}
   
  """ UserAccount that initiated rating  """
  rater: UserAccount
  
  """ Entity reviewed """
  itemReviewed: EntityInterface
`;

export let AggregateRatingInterface = `
interface AggregateRatingInterface {
  ${AggregateRatingInterfaceProperties}
}

${generateConnectionForType("AggregateRatingInterface")}
`;


export let AggregateRatingInterfaceResolvers = {
  AggregateRatingInterface: {
    ...generateInterfaceResolvers("AggregateRatingInterface"),
    rater: getLinkedObjectResolver.bind(this, AggregateRatingDefinition.getLink('hasRater')),
    itemReviewed: getLinkedObjectResolver.bind(this, AggregateRatingDefinition.getLink('hasEntity'))
  },
  ...generateConnectionResolverFor("AggregateRatingInterface")
};


export let getEntityAggregatingRatingForLoggedUserResolver = async (aggregationRatingDefinition, entity, _, synaptixSession, info) => {
  let userAccount = await synaptixSession.getLoggedUserAccount();

  if (userAccount) {
    return getObjectResolver(aggregationRatingDefinition, null, {
      linkFilters: [
        {
          linkDefinition: aggregationRatingDefinition.getLink("hasRater"),
          id: userAccount.id
        },
        {
          linkDefinition: aggregationRatingDefinition.getLink("hasEntity"),
          id: entity.id
        }
      ]
    }, synaptixSession, info);
  }
};