/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {LinkDefinition, ModelDefinitionAbstract, MnxOntologies} from "@mnemotix/synaptix.js";

import VerbDefinition from "./VerbDefinition"

export default class VerbalInflectionDefinition extends ModelDefinitionAbstract {
  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping()
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ddf:VerbalInflection";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    // return 'nroArea';
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    // return VerbalInflectionIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'mood',
        pathInIndex: 'mood',
        rdfObjectProperty: 'lexinfo:mood',
        relatedModelDefinition: MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      }),
      new LinkDefinition({
        linkName: 'person',
        pathInIndex: 'person',
        rdfObjectProperty: 'lexinfo:person',
        relatedModelDefinition: MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      }),
      new LinkDefinition({
        linkName: 'tense',
        pathInIndex: 'tense',
        rdfObjectProperty: 'lexinfo:tense',
        relatedModelDefinition: MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      }),
      new LinkDefinition({
        linkName: 'isVerbalInflectionOf',
        pathInIndex: 'isVerbalInflectionOf',
        rdfObjectProperty: 'ddf:isVerbalInflectionOf',
        relatedModelDefinition: VerbDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),

    ];
  }
};

