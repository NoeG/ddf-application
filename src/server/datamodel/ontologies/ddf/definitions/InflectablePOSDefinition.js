/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {LinkDefinition, MnxOntologies, ModelDefinitionAbstract} from "@mnemotix/synaptix.js";
import InflectionDefinition from "./InflectionDefinition";
import WordDefinition from "../../ontolex/definitions/WordDefinition";
import GrammaticalPropertyDefinition from "../../ontolex/definitions/GrammaticalPropertyDefinition";

export default class InflectablePOSDefinition extends ModelDefinitionAbstract {
  /**
   * Method to simulate multiple inheritance.
   *
   * @return  {typeof ModelDefinitionAbstract[]}  List of parent definitions to inherit from.
   */
  static getParentDefinitions(){
    return [WordDefinition];
  }

  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping()
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ddf:InflectablePOS";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return super.getIndexType();
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'gender',
        symmetricLinkName: 'lexicalEntries',
        rdfObjectProperty: 'ddf:hasGender',
        relatedModelDefinition: GrammaticalPropertyDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      }),
      new LinkDefinition({
        linkName: 'number',
        symmetricLinkName: 'lexicalEntries',
        rdfObjectProperty: 'ddf:hasNumber',
        relatedModelDefinition: GrammaticalPropertyDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      }),
      new LinkDefinition({
        linkName: 'inflection',
        pathInIndex: 'inflection',
        rdfObjectProperty: 'ddf:hasInflection',
        relatedModelDefinition: InflectionDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      }),
    ]
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),

    ];
  }
};

