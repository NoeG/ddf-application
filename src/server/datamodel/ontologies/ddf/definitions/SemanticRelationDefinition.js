/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {LinkDefinition, ModelDefinitionAbstract, MnxOntologies, FilterDefinition} from "@mnemotix/synaptix.js";

import LexicalSenseDefinition from "../../ontolex/definitions/LexicalSenseDefinition";
import EntryDefinition from "../../lexicog/definitions/EntryDefinition";
import LexicalEntryDefinition from "../../ontolex/definitions/LexicalEntryDefinition";
import SemanticPropertyDefinition from "../../ontolex/definitions/SemanticPropertyDefinition";
import LexicographicResourceDefinition from "../../lexicog/definitions/LexicographicResourceDefinition";
import FormDefinition from "../../ontolex/definitions/FormDefinition";

export default class SemanticRelationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ddf:SemanticRelation";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "semantic-relation";
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'entries',
        symmetricLinkName: 'semanticRelations',
        rdfObjectProperty: 'ddf:semanticRelationOfEntry',
        relatedModelDefinition: EntryDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'lexicalEntries',
        symmetricLinkName: 'semanticRelations',
        rdfObjectProperty: 'ddf:semanticRelationOfLexicalEntry',
        relatedModelDefinition: LexicalEntryDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'lexicalSenses',
        symmetricLinkName: 'semanticRelations',
        rdfObjectProperty: 'ddf:semanticRelationOfLexicalSense',
        relatedModelDefinition: LexicalSenseDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'semanticRelationType',
        rdfObjectProperty: 'ddf:hasSemanticRelationType',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: false
      }),
      //
      // Index only shortcuts
      //
      new LinkDefinition({
        linkName: 'hasCanonicalForm',
        relatedModelDefinition: FormDefinition,
        inIndexOnly: true,
        isPlural: true
      })
    ]
  }

  static getFilters() {
    return [
      ...super.getFilters(),
      new FilterDefinition({
        filterName: "accurateFormWrittenRep",
        indexFilter: ({formQs}) => ({
          "bool": {
            "filter": [{
              "term": {
                "hasCanonicalFormWrittenRep": formQs
              }
            }]
          }
        })
      })
    ];
  }
};

