/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  getLinkedObjectResolver,
  getObjectResolver,
  getObjectsResolver,
  EntityInterfaceProperties,
  filteringArgs
} from "@mnemotix/synaptix.js";
import VerbalInflectionDefinition from '../../definitions/VerbalInflectionDefinition';


export let VerbalInflectionType = `
type VerbalInflection implements EntityInterface {
  """ Has mood"""
  mood: Concept
  
		""" Has person"""
  person: Person
  
		""" Has tense"""
  tense: Concept
  
		""" Is a verbal inflection of"""
  isVerbalInflectionOf: Verb
  
  
  ${EntityInterfaceProperties}
}
  
${generateConnectionForType("VerbalInflection")}
  

input VerbalInflectionInput {
  """ The ID """
  id: ID @formInput(type:"hidden")

  
  
}


extend type Query{
""" Search for VerbalInflection """
verbalInflections(${connectionArgs}, ${filteringArgs}): VerbalInflectionConnection

""" Get VerbalInflection """
verbalInflection(id:ID): VerbalInflection
}
`;


export let VerbalInflectionTypeResolvers = {
  VerbalInflection: {
    ...generateTypeResolvers("VerbalInflection"),
    mood: getLinkedObjectResolver.bind(this, VerbalInflectionDefinition.getLink('mood')),
    person: getLinkedObjectResolver.bind(this, VerbalInflectionDefinition.getLink('person')),
    tense: getLinkedObjectResolver.bind(this, VerbalInflectionDefinition.getLink('tense')),
    isVerbalInflectionOf: getLinkedObjectResolver.bind(this, VerbalInflectionDefinition.getLink('isVerbalInflectionOf')),
  },
  Query: {
    verbalInflections: getObjectsResolver.bind(this, VerbalInflectionDefinition),
    verbalInflection: getObjectResolver.bind(this, VerbalInflectionDefinition)
  },
  ...generateConnectionResolverFor("VerbalInflection")
};

