/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  getObjectResolver,
  getObjectsResolver,
  filteringArgs
} from "@mnemotix/synaptix.js";
import InvariablePOSDefinition from '../../definitions/InvariablePOSDefinition';
import {WordProperties} from "../../../ontolex/schema/types/Word.graphql";


export let InvariablePOSType = `
type InvariablePOS implements EntityInterface &   LexicalEntryInterface & WordInterface   {
  ${WordProperties}
}
  
${generateConnectionForType("InvariablePOS")}
  

input InvariablePOSInput {
  """ The ID """
  id: ID @formInput(type:"hidden")
}


extend type Query{
""" Search for InvariablePOS """
invariablePOSs(${connectionArgs}, ${filteringArgs}): InvariablePOSConnection

""" Get InvariablePOS """
invariablePOS(id:ID): InvariablePOS
}
`;


export let InvariablePOSTypeResolvers = {
  InvariablePOS: {
    ...generateTypeResolvers("InvariablePOS"),
  },
  Query: {
    invariablePOSs: getObjectsResolver.bind(this, InvariablePOSDefinition),
    invariablePOS: getObjectResolver.bind(this, InvariablePOSDefinition)
  },
  ...generateConnectionResolverFor("InvariablePOS")
};

