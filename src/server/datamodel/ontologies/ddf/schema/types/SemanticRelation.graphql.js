/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  connectionFromArray,
  EntityInterfaceProperties,
  filteringArgs, generateConnectionArgs,
  generateConnectionForType,
  generateConnectionResolverFor,
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLinkedObjectsResolver,
  getObjectResolver,
  getObjectsResolver,
  Link,
  LinkPath,
  QueryFilter
} from "@mnemotix/synaptix.js";
import SemanticRelationDefinition from '../../definitions/SemanticRelationDefinition';
import FormDefinition from "../../../ontolex/definitions/FormDefinition";
import LexicalEntryDefinition from "../../../ontolex/definitions/LexicalEntryDefinition";
import LexicalSenseDefinition from "../../../ontolex/definitions/LexicalSenseDefinition";
import EntryDefinition from "../../../lexicog/definitions/EntryDefinition";


export let senseToSenseSemanticRelationTypes = [
  "soundRelation",
  "demonymicRelation",
  "oppositeSexRelation",
  "sameSemanticDivision",
  "figurativeRelation",
  "properMeaningRelation",
  "metonymicRelation",
  "hyperbolicRelation",
  "extendedFromRelation",
  "analogicalRelation",
  "peculiarRelation",
  "litoticRelation",
  "euphemisticRelation",
  "metaphoricRelation",
  "rarerThan",
  "moreCommonThan"
];


export let SemanticRelationType = `
type SemanticRelation implements EntityInterface {
 ${EntityInterfaceProperties}
 
  """ Entries """
  entries: EntryConnection
  
	""" Lexical senses"""
  lexicalSenses: LexicalSenseConnection
  
		""" Has semantic relation type"""
  semanticRelationType: Concept
  
  """ A shortcut to get canonical form (lexicalSense => entry => lexicalEntry => form) """
  canonicalForms(${generateConnectionArgs()}): FormConnection
}
  
${generateConnectionForType("SemanticRelation")}
  

input SemanticRelationInput {
  """ The ID """
  id: ID @formInput(type:"hidden")
}


enum SemanticRelationType{
  associativeRelation
  morphologicalRelation
  derivativeMultiWordExpression
  graphicRelation
}

enum SenseToSenseSemanticRelationType{
  ${senseToSenseSemanticRelationTypes.join("\n")}
}

extend type Query{
  """ Search for semantic relations """
  semanticRelations(${connectionArgs}, ${filteringArgs}): SemanticRelationConnection
  
  """ Search for semantic relations related to an accurate form """
  semanticRelationsForAccurateWrittenForm(formQs: String, filterOnType: SemanticRelationType, ${connectionArgs}): SemanticRelationConnection
  
  """ Get SemanticRelation """
  semanticRelation(id:ID): SemanticRelation
}
`;

let cache = {};

export let SemanticRelationTypeResolvers = {
  SemanticRelation: {
    ...generateTypeResolvers("SemanticRelation"),
    entries: getLinkedObjectsResolver.bind(this, SemanticRelationDefinition.getLink('entries')),
    lexicalSenses: getLinkedObjectsResolver.bind(this, SemanticRelationDefinition.getLink('lexicalSenses')),
    semanticRelationType: getLinkedObjectResolver.bind(this, SemanticRelationDefinition.getLink('semanticRelationType')),
    /**
     * @param {Model} object
     * @param args
     * @param {SynaptixDatastoreRdfSession} synaptixSession
     * @param {GraphQLResolveInfo} info
     */
    canonicalForms: async (object, args, synaptixSession, info) => getObjectsResolver(
      FormDefinition,
      object,
      {
        ...args,
        linkPaths: [
          new LinkPath({
            indexedShortcutLink: new Link({
              linkDefinition: SemanticRelationDefinition.getLink("hasCanonicalForm"),
              targetId: object
            })
          })
            .step({linkDefinition: FormDefinition.getLink("lexicalEntries")})
            .step({linkDefinition: LexicalEntryDefinition.getLink("entry")})
            .step({
              linkDefinition: EntryDefinition.getLink("semanticRelations"),
              targetId: object.id
            })
        ]
      }, synaptixSession, info)
  },
  Query: {
    semanticRelations: getObjectsResolver.bind(this, SemanticRelationDefinition),
    semanticRelation: getObjectResolver.bind(this, SemanticRelationDefinition),
    semanticRelationsForAccurateWrittenForm: async (parent, {formQs, filterOnType, ...args}, synaptixSession, info) => {
      let semanticRelations = [];

      if (cache[formQs]) {
        semanticRelations = cache[formQs];
      } else {
        if (synaptixSession.isIndexDisabled()) {
          // There are two ways for reaching semantic relations.
          // 1. Directly from LexicalEntry => SemanticRelation
          semanticRelations = semanticRelations.concat(
            await synaptixSession.getObjects({
              modelDefinition: SemanticRelationDefinition,
              args: {
                ...args,
                linkPaths: [
                  new LinkPath()
                    .step({
                      linkDefinition: SemanticRelationDefinition.getLink("lexicalEntries")
                    })
                    .step({
                      linkDefinition: LexicalEntryDefinition.getLink("canonicalForm"),
                    })
                    .filterOnProperty({
                      propertyDefinition: FormDefinition.getProperty("writtenRep"),
                      value: `^${formQs}$`
                    })
                ]
              }
            })
          );

          // 2. Undirectly from LexicalEntry => LexicalSense => SemanticRelation
          semanticRelations = semanticRelations.concat(
            await synaptixSession.getObjects({
              modelDefinition: SemanticRelationDefinition,
              args: {
                ...args,
                linkPaths: [
                  new LinkPath()
                    .step({
                      linkDefinition: SemanticRelationDefinition.getLink("lexicalSenses")
                    })
                    .step({
                      linkDefinition: LexicalSenseDefinition.getLink("lexicalEntry")
                    })
                    .step({
                      linkDefinition: LexicalEntryDefinition.getLink("canonicalForm"),
                    })
                    .filterOnProperty({
                      propertyDefinition: FormDefinition.getProperty("writtenRep"),
                      value: `^${formQs}$`
                    })
                ]
              }
            })
          );
        } else {
          semanticRelations = await synaptixSession.getObjects({
            modelDefinition: SemanticRelationDefinition,
            args: {
              queryFilters: [
                new QueryFilter({
                  filterDefinition: SemanticRelationDefinition.getFilter("accurateFormWrittenRep"),
                  filterGenerateParams: {
                    formQs
                  }
                })
              ]
            }
          });
        }

        cache[formQs] = semanticRelations;
      }
      // TODOEND

      // Dedupe and filter on type
      let filteredSemanticRelation = [];

      for (let [index, semanticRelation] of semanticRelations.entries()) {
        if (!filteredSemanticRelation.find(({id}) => id === semanticRelation.id)) {
          if (filterOnType) {
            let semanticRelationType = cache?.[formQs]?.[index]?.semanticRelationType;

            if (!semanticRelationType) {
              semanticRelationType = await synaptixSession.getLinkedObjectFor({
                object: semanticRelation,
                linkDefinition: SemanticRelationDefinition.getLink("semanticRelationType")
              })
            }

            cache[formQs][index].semanticRelationType = semanticRelationType;

            if ((semanticRelationType?.uri || "").includes(filterOnType)) {
              filteredSemanticRelation.push(semanticRelation);
            }
          } else {
            filteredSemanticRelation.push(semanticRelation);
          }
        }
      }

      return connectionFromArray(filteredSemanticRelation, args)
    }
  },
  ...generateConnectionResolverFor("SemanticRelation")
};

