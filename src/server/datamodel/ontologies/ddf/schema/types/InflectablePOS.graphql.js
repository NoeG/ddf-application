/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  getLinkedObjectResolver,
  getObjectResolver,
  getObjectsResolver,
  filteringArgs
} from "@mnemotix/synaptix.js";
import InflectablePOSDefinition from '../../definitions/InflectablePOSDefinition';
import {WordProperties} from "../../../ontolex/schema/types/Word.graphql";

export let InflectablePOSType = `
type InflectablePOS implements EntityInterface &   LexicalEntryInterface & WordInterface   {
  ${WordProperties}
  
  """ Has gender"""
  gender: Concept
  
  """ Has inflection"""
  inflection: Inflection
  
  """ Has number"""
  number: Concept
}
  
${generateConnectionForType("InflectablePOS")}
  
input InflectablePOSInput {
  """ The ID """
  id: ID @formInput(type:"hidden")
}

extend type Query{
  """ Search for InflectablePOS """
  inflectablePOSs(${connectionArgs}, ${filteringArgs}): InflectablePOSConnection
  
  """ Get InflectablePOS """
  inflectablePOS(id:ID): InflectablePOS
}
`;


export let InflectablePOSTypeResolvers = {
  InflectablePOS: {
    ...generateTypeResolvers("InflectablePOS"),

    gender: getLinkedObjectResolver.bind(this, InflectablePOSDefinition.getLink('gender')),
    inflection: getLinkedObjectResolver.bind(this, InflectablePOSDefinition.getLink('inflection')),
    number: getLinkedObjectResolver.bind(this, InflectablePOSDefinition.getLink('number')),
  },
  Query: {
    inflectablePOSs: getObjectsResolver.bind(this, InflectablePOSDefinition),
    inflectablePOS: getObjectResolver.bind(this, InflectablePOSDefinition)
  },
  ...generateConnectionResolverFor("InflectablePOS")
};

