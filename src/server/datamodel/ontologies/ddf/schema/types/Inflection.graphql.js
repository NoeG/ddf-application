/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  getLinkedObjectResolver,
  getObjectResolver,
  getObjectsResolver,
  EntityInterfaceProperties,
  filteringArgs
} from "@mnemotix/synaptix.js";
import InflectionDefinition from '../../definitions/InflectionDefinition';


export let InflectionType = `
type Inflection implements EntityInterface {
  """ Inflection has gender"""
  gender: Concept
  
		""" Inflection has number"""
  number: Concept
  
		""" Is inflection of"""
  isInflectionOf: InflectablePOS
  
  
  ${EntityInterfaceProperties}
}
  
${generateConnectionForType("Inflection")}
  

input InflectionInput {
  """ The ID """
  id: ID @formInput(type:"hidden")

  
  
}


extend type Query{
""" Search for Inflection """
inflections(${connectionArgs}, ${filteringArgs}): InflectionConnection

""" Get Inflection """
inflection(id:ID): Inflection
}
`;


export let InflectionTypeResolvers = {
  Inflection: {
    ...generateTypeResolvers("Inflection"),
    gender: getLinkedObjectResolver.bind(this, InflectionDefinition.getLink('inflectionHasGender')),
    number: getLinkedObjectResolver.bind(this, InflectionDefinition.getLink('inflectionHasNumber')),
    isInflectionOf: getLinkedObjectResolver.bind(this, InflectionDefinition.getLink('isInflectionOf')),
  },
  Query: {
    inflections: getObjectsResolver.bind(this, InflectionDefinition),
    inflection: getObjectResolver.bind(this, InflectionDefinition)
  },
  ...generateConnectionResolverFor("Inflection")
};

