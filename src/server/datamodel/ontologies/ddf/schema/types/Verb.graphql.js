/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  getLinkedObjectResolver,
  getObjectResolver,
  getObjectsResolver,
  filteringArgs
} from "@mnemotix/synaptix.js";
import VerbDefinition from '../../definitions/VerbDefinition';
import {WordInputProperties, WordProperties} from "../../../ontolex/schema/types/Word.graphql";

export let VerbType = `
type Verb implements EntityInterface &  LexicalEntryInterface & WordInterface   {
  ${WordProperties}
  
  """ Has transitivity"""
  transitivity: Concept
  
  """ Has a verbal inflection"""
  verbalInflection: VerbalInflection
}
  
${generateConnectionForType("Verb")}

input VerbInput {
  ${WordInputProperties}
}

extend type Query{
  """ Search for Verb """
  verbs(${connectionArgs}, ${filteringArgs}): VerbConnection
  
  """ Get Verb """
  verb(id:ID): Verb
}
`;


export let VerbTypeResolvers = {
  Verb: {
    ...generateTypeResolvers("Verb"),
    transitivity: getLinkedObjectResolver.bind(this, VerbDefinition.getLink('transitivity')),
    verbalInflection: getLinkedObjectResolver.bind(this, VerbDefinition.getLink('verbalInflection')),
  },
  Query: {
    verbs: getObjectsResolver.bind(this, VerbDefinition),
    verb: getObjectResolver.bind(this, VerbDefinition)
  },
  ...generateConnectionResolverFor("Verb")
};

