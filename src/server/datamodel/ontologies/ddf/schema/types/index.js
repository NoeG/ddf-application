/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {mergeResolvers} from "@mnemotix/synaptix.js";
import {InflectablePOSTypeResolvers, InflectablePOSType} from "./InflectablePOS.graphql";
import {InflectionTypeResolvers, InflectionType} from "./Inflection.graphql";
import {InvariablePOSTypeResolvers, InvariablePOSType} from "./InvariablePOS.graphql";
import {SemanticRelationTypeResolvers, SemanticRelationType} from "./SemanticRelation.graphql";
import {VerbTypeResolvers, VerbType} from "./Verb.graphql";
import {VerbalInflectionTypeResolvers, VerbalInflectionType} from "./VerbalInflection.graphql";


export let DdfResolvers = mergeResolvers(
  InflectablePOSTypeResolvers,
  InflectionTypeResolvers,
  InvariablePOSTypeResolvers,
  SemanticRelationTypeResolvers,
  VerbTypeResolvers,
  VerbalInflectionTypeResolvers
);


export let DdfTypes = [
  InflectablePOSType,
  InflectionType,
  InvariablePOSType,
  SemanticRelationType,
  VerbType,
  VerbalInflectionType
];

