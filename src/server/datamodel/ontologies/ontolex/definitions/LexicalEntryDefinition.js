/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {EntityDefinition, LinkDefinition, MnxOntologies, ModelDefinitionAbstract} from "@mnemotix/synaptix.js";

import LexicalEntry from "../models/LexicalEntry";
import FormDefinition from "./FormDefinition";
import LexicalSenseDefinition from "./LexicalSenseDefinition"
import OnlineContributionDefinition from "../../ddf-contribution/definitions/OnlineContributionDefinition";
import SemanticRelationDefinition from "../../ddf/definitions/SemanticRelationDefinition";
import EntryDefinition from "../../lexicog/definitions/EntryDefinition";
import GrammaticalPropertyDefinition from "./GrammaticalPropertyDefinition";

export default class LexicalEntryDefinition extends ModelDefinitionAbstract {
  /**
   * Set false if the related type is not instantiable.
   * @return {boolean}
   */
  static isInstantiable(){
    return false;
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping()
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ontolex:LexicalEntry";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return LexicalEntry;
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
     return 'lexical-entry';
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    // return LexicalEntryIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'entry',
        symmetricLinkName: 'lexicalEntry',
        rdfReversedObjectProperty: "lexicog:describes",
        relatedModelDefinition: EntryDefinition,
        isPlural: false,
        relatedInputName: "entryInput"
      }),
      new LinkDefinition({
        linkName: 'contributions',
        symmetricLinkName: 'lexicalEntry',
        rdfObjectProperty: 'ddf:hasItemAboutEtymology',
        relatedModelDefinition: OnlineContributionDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'canonicalForm',
        symmetricLinkName: "lexicalEntry",
        pathInIndex: 'canonicalForm',
        rdfObjectProperty: 'ontolex:canonicalForm',
        relatedModelDefinition: FormDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        relatedInputName: "canonicalFormInput"
      }),
      new LinkDefinition({
        linkName: 'lexicalForms',
        pathInIndex: 'lexicalForms',
        rdfObjectProperty: 'ontolex:lexicalForm',
        relatedModelDefinition: FormDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true,
        relatedInputName: "canonicalFormInputs"
      }),
      new LinkDefinition({
        linkName: 'otherForms',
        pathInIndex: 'otherForms',
        rdfObjectProperty: 'ontolex:otherForm',
        relatedModelDefinition: FormDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true,
        relatedInputName: "otherFormInputs"
      }),
      new LinkDefinition({
        linkName: 'senses',
        symmetricLinkName: "lexicalEntry",
        pathInIndex: 'senses',
        rdfObjectProperty: 'ontolex:sense',
        relatedModelDefinition: LexicalSenseDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true,
        relatedInputName: "lexicalSenseInputs"
      }),
      new LinkDefinition({
        linkName: 'semanticRelations',
        symmetricLinkName: 'lexicalEntries',
        rdfObjectProperty: 'ddf:lexicalEntryHasSemanticRelationWith',
        relatedModelDefinition: SemanticRelationDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'grammaticalProperties',
        symmetricLinkName: "lexicalEntries",
        rdfObjectProperty: 'ddf:hasGrammaticalProperty',
        relatedModelDefinition: GrammaticalPropertyDefinition,
        isPlural: true,
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
    ];
  }
};

