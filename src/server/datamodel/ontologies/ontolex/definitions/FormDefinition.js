/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {LinkDefinition, LabelDefinition, ModelDefinitionAbstract, MnxOntologies} from "@mnemotix/synaptix.js";

import Form from "../models/Form";
import PlaceDefinition from "../../geonames/definitions/PlaceDefinition";
import LexicalEntryDefinition from "./LexicalEntryDefinition";
import OnlineContributionDefinition from "../../ddf-contribution/definitions/OnlineContributionDefinition";

export default class FormDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [MnxOntologies.mnxCommon.ModelDefinitions.EntityDefinition];
  }

  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping()
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ontolex:Form";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Form;
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'form';
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    // return FormIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'lexicalEntries',
        rdfReversedObjectProperty: 'ontolex:canonicalForm',
        relatedModelDefinition: LexicalEntryDefinition,
        symmetricLinkName: "canonicalForm",
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'localisation',
        pathInIndex: 'localisation',
        rdfObjectProperty: 'ddf:formHasLocalisation',
        relatedModelDefinition: MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      }),
      new LinkDefinition({
        linkName: 'formType',
        pathInIndex: 'formType',
        rdfObjectProperty: 'ddf:hasFormType',
        relatedModelDefinition: MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      }),
      new LinkDefinition({
        linkName: 'contributions',
        symmetricLinkName: 'form',
        rdfObjectProperty: 'ddf:hasItemAboutForm',
        relatedModelDefinition: OnlineContributionDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'places',
        pathInIndex: 'places',
        rdfObjectProperty: 'ddf:hasLocalisation',
        relatedModelDefinition: PlaceDefinition,
        isPlural: true
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      // new LabelDefinition({
      //   labelName: 'phoneticRep',
      //   rdfDataProperty: 'ontolex:phoneticRep'
      // }),
      new LabelDefinition({
        labelName: 'writtenRep',
        rdfDataProperty: 'ontolex:writtenRep',
        isSearchable: true,
        isRequired: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals()
    ];
  }
};

