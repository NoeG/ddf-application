/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {LinkDefinition, ModelDefinitionAbstract} from "@mnemotix/synaptix.js";

import LexicalConcept from "../models/LexicalConcept";
import LexicalEntryDefinition from "./LexicalEntryDefinition";
import LexicalSenseDefinition from "./LexicalSenseDefinition"

export default class LexicalConceptDefinition extends ModelDefinitionAbstract {
  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping()
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ontolex:LexicalConcept";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return LexicalConcept;
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    // return 'nroArea';
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    // return LexicalConceptIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [

      new LinkDefinition({
        linkName: 'isEvokedBy',
        pathInIndex: 'isEvokedBy',
        rdfObjectProperty: 'ontolex:isEvokedBy',
        relatedModelDefinition: LexicalEntryDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      }),
      new LinkDefinition({
        linkName: 'lexicalizedSense',
        pathInIndex: 'lexicalizedSense',
        rdfObjectProperty: 'ontolex:lexicalizedSense',
        relatedModelDefinition: LexicalSenseDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),

    ];
  }
};

