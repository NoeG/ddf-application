/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  FilterDefinition,
  LabelDefinition,
  LinkDefinition,
  LiteralDefinition,
  MnxOntologies,
  ModelDefinitionAbstract,
  SortingDefinition
} from "@mnemotix/synaptix.js";

import LexicalSense from "../models/LexicalSense";
import LexicalConceptDefinition from "./LexicalConceptDefinition";
import LexicalEntryDefinition from "./LexicalEntryDefinition";
import PlaceDefinition from "../../geonames/definitions/PlaceDefinition";
import UsageExampleDefinition from "../../lexicog/definitions/UsageExampleDefinition";
import SemanticRelationDefinition from "../../ddf/definitions/SemanticRelationDefinition";
import OnlineContributionDefinition from "../../ddf-contribution/definitions/OnlineContributionDefinition";
import ValidationRatingDefinition from "../../ddf-contribution/definitions/ValidationRatingDefinition";
import SuppressionRatingDefinition from "../../ddf-contribution/definitions/SuppressionRatingDefinition";
import ReportingRatingDefinition from "../../ddf-contribution/definitions/ReportingRatingDefinition";
import SemanticPropertyDefinition from "./SemanticPropertyDefinition";
import AggregateRatingDefinition from "../../ddf-contribution/definitions/AggregateRatingDefinition";
import LexicographicResourceDefinition from "../../lexicog/definitions/LexicographicResourceDefinition";
import FormDefinition from "./FormDefinition";

export default class LexicalSenseDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [MnxOntologies.mnxCommon.ModelDefinitions.EntityDefinition];
  }

  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping()
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ontolex:LexicalSense";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return LexicalSense;
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'lexical-sense';
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    // return LexicalSenseIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasConnotation',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasConnotation',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "connotationInput"
      }),
      new LinkDefinition({
        linkName: 'hasDomain',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasDomain',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "domainInput"
      }),
      new LinkDefinition({
        linkName: 'hasFrequency',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasFrequency',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "frequencyInput"
      }),
      new LinkDefinition({
        linkName: 'hasGlossary',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasGlossary',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "glossaryInputs"
      }),
      new LinkDefinition({
        linkName: 'hasGrammaticalConstraint',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasGrammaticalConstraint',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "grammaticalConstraintInput"
      }),
      new LinkDefinition({
        linkName: 'hasRegister',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasRegister',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "registerInput"
      }),
      new LinkDefinition({
        linkName: 'hasSociolect',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasSociolect',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "sociolectInput"
      }),
      new LinkDefinition({
        linkName: 'hasTemporality',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasTemporality',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "temporalityInput"
      }),
      new LinkDefinition({
        linkName: 'hasTextualGenre',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasTextualGenre',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "textualGenreInput"
      }),
      new LinkDefinition({
        linkName: 'semanticRelations',
        symmetricLinkName: 'lexicalSenses',
        rdfObjectProperty: 'ddf:lexicalSenseHasSemanticRelationWith',
        relatedModelDefinition: SemanticRelationDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'hasFormRestriction',
        rdfObjectProperty: 'lexicog:formRestriction',
        relatedModelDefinition: MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false,
        relatedInputName: "formRestrictionInput"
      }),
      new LinkDefinition({
        linkName: 'usageExample',
        symmetricLinkName: 'lexicalSense',
        rdfObjectProperty: 'lexicog:usageExample',
        relatedModelDefinition: UsageExampleDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true,
        relatedInputName: "usageExampleInput"
      }),
      new LinkDefinition({
        linkName: 'isLexicalizedSenseOf',
        pathInIndex: 'isLexicalizedSenseOf',
        rdfObjectProperty: 'ontolex:isLexicalizedSenseOf',
        relatedModelDefinition: LexicalConceptDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      }),
      new LinkDefinition({
        linkName: 'lexicalEntry',
        symmetricLinkName: "senses",
        pathInIndex: 'isSenseOf',
        rdfObjectProperty: 'ontolex:isSenseOf',
        relatedModelDefinition: LexicalEntryDefinition,
        relatedInputName: "lexicalEntryInput"
      }),
      new LinkDefinition({
        linkName: 'places',
        rdfObjectProperty: 'ddf:hasLocalisation',
        relatedModelDefinition: PlaceDefinition,
        isPlural: true,
        relatedInputName: "placeInputs"
      }),
      new LinkDefinition({
        linkName: 'contributions',
        symmetricLinkName: 'sense',
        rdfObjectProperty: 'ddf:hasItemAboutSense',
        relatedModelDefinition: OnlineContributionDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'hasAggregationRating',
        symmetricLinkName: 'hasEntity',
        rdfReversedObjectProperty: 'schema:itemReviewed',
        relatedModelDefinition: AggregateRatingDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'hasValidationRating',
        symmetricLinkName: 'hasEntity',
        rdfObjectProperty: 'ddf:hasValidationRating',
        relatedModelDefinition: ValidationRatingDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'hasSuppressionRating',
        symmetricLinkName: 'hasEntity',
        rdfObjectProperty: 'ddf:hasSuppressionRating',
        relatedModelDefinition: SuppressionRatingDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'hasReportingRating',
        symmetricLinkName: 'hasEntity',
        rdfObjectProperty: 'ddf:hasReportingRating',
        relatedModelDefinition: ReportingRatingDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'semanticProperties',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasSemanticProperty',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
      }),

      //
      // Index only shortcuts
      //
      new LinkDefinition({
        linkName: 'hasLexicographicResource',
        relatedModelDefinition: LexicographicResourceDefinition,
        inIndexOnly: true
      }),
      new LinkDefinition({
        linkName: 'hasCanonicalForm',
        relatedModelDefinition: FormDefinition,
        inIndexOnly: true
      }),
      new LinkDefinition({
        linkName: 'hasCreator',
        relatedModelDefinition: MnxOntologies.mnxAgent.ModelDefinitions.UserAccountDefinition,
        inIndexOnly: true
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "definition",
        pathInIndex: "definition",
        rdfDataProperty: 'skos:definition',
        isRequired: true,
        isSearchable: true
      }),
      new LabelDefinition({
        labelName: 'scientificName',
        pathInIndex: "scientificNames",
        rdfDataProperty: 'ddf:hasScientificName',
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: "processed",
        rdfDataProperty: "ddf:processed",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean",
      })
    ];
  }

  static getFilters() {
    return [
      ...super.getFilters(),
      new FilterDefinition({
        filterName: "hasNearbyPlace",
        indexFilter: ({lat, lon, distance}) => ({
          "geo_distance": {
            "distance": distance || "3000km",
            "geoloc": {lat, lon}
          }
        })
      }),
      new FilterDefinition({
        filterName: "hasSuppressionRatings",
        indexFilter: () => ({
          "exists": {
            "field": LexicalSenseDefinition.getLink("hasSuppressionRating").getPathInIndex()
          }
        })
      }),
      new FilterDefinition({
        filterName: "isReviewed",
        indexFilter: ({userGroupIds}) => ({
          "bool": {
            "should": [{
              "terms": {
                "hasReviewerGroup": userGroupIds
              }
            }, {
              "term": {
                "processed": true
              }
            }]
          }
        })
      }),
      new FilterDefinition({
        filterName: "accurateFormWrittenRep",
        indexFilter: ({formQs}) => ({
          "bool": {
            "filter": [{
              "term": {
                "hasCanonicalFormWrittenRep": formQs
              }
            }]
          }
        })
      }),
      new FilterDefinition({
        filterName: "hasCreator",
        indexFilter: () => ({
          "exists": {
            "field": LexicalSenseDefinition.getLink("hasCreator").getPathInIndex()
          }
        })
      }),
    ];
  }

  /**
   * @return {SortingDefinition[]}
   */
  static getSortings() {
    let generateRatingScript = ({ratingProperty, direction, factor}) =>
      ({
        "_script": {
          "type": "number",
          "script": {
            "lang": "painless",
            "source": `
              if(doc.containsKey('${ratingProperty}')) return doc['${ratingProperty}'].length * params.factor;
              return 0;
            `,
            "params": {
              "factor": factor || 1.1
            }
          },
          "order": direction || "asc"
        }
      });

    return [
      new SortingDefinition({
        sortingName: "nearby",
        indexSorting: ({lat, lon, direction, unit, mode, distanceType}) => ({
          "_geo_distance": {
            "geoloc": {
              lat,
              lon
            },
            "order": direction || "asc",
            "unit": unit || "km",
            "mode": mode || "min",
            "distance_type": distanceType || "arc",
            "ignore_unmapped": true
          }
        })
      }),
      new SortingDefinition({
        sortingName: "reportingRatingCount",
        indexSorting: ({direction}) => generateRatingScript({
          ratingProperty: 'hasReportingRating', direction
        })
      }),
      new SortingDefinition({
        sortingName: "suppressionRatingCount",
        indexSorting: ({direction}) => generateRatingScript({
          ratingProperty: 'hasSuppressionRating', direction
        })
      }),
      new SortingDefinition({
        sortingName: "validationRatingCount",
        indexSorting: ({direction}) => generateRatingScript({
          ratingProperty: 'hasValidationRating', direction
        })
      }),
      new SortingDefinition({
        sortingName: "ratingScore",
        indexSorting: ({direction, factor}) => ({
          "_script": {
            "type": "number",
            "script": {
              "lang": "painless",
              "source": `
              def validationRatings  = doc.containsKey('hasValidationRating')  ? doc['hasValidationRating'].length  : 0;
              def suppressionRatings = doc.containsKey('hasSuppressionRating') ? doc['hasSuppressionRating'].length : 0;
              def reportingRatings   = doc.containsKey('hasReportingRating')   ? doc['hasReportingRating'].length   : 0;
              
              return (validationRatings - suppressionRatings  - reportingRatings) * params.factor ;
            `,
              "params": {
                "factor": factor || 1.1
              }
            },
            "order": direction || "asc"
          }
        })
      })
    ]
  }
};

