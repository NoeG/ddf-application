/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  getLinkedObjectResolver,
  getObjectResolver,
  getObjectsResolver,
  EntityInterfaceProperties,
  filteringArgs
} from "@mnemotix/synaptix.js";
import LexicalConceptDefinition from '../../definitions/LexicalConceptDefinition';


export let LexicalConceptType = `
type LexicalConcept implements EntityInterface {
  """ Is evoked by"""
  isEvokedBy: LexicalEntryInterface
  
		""" Lexicalized sense"""
  lexicalizedSense: LexicalSense
  
  
  ${EntityInterfaceProperties}
}
  
${generateConnectionForType("LexicalConcept")}
  

input LexicalConceptInput {
  """ The ID """
  id: ID @formInput(type:"hidden")

  
  
}


extend type Query{
""" Search for LexicalConcept """
lexicalConcepts(${connectionArgs}, ${filteringArgs}): LexicalConceptConnection

""" Get LexicalConcept """
lexicalConcept(id:ID): LexicalConcept
}
`;


export let LexicalConceptTypeResolvers = {
  LexicalConcept: {
    ...generateTypeResolvers("LexicalConcept"),


    isEvokedBy: getLinkedObjectResolver.bind(this, LexicalConceptDefinition.getLink('isEvokedBy')),
    lexicalizedSense: getLinkedObjectResolver.bind(this, LexicalConceptDefinition.getLink('lexicalizedSense')),
  },
  Query: {
    lexicalConcepts: getObjectsResolver.bind(this, LexicalConceptDefinition),
    lexicalConcept: getObjectResolver.bind(this, LexicalConceptDefinition)
  },
  ...generateConnectionResolverFor("LexicalConcept")
};

