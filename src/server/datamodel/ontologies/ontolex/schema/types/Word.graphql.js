/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  generateInterfaceResolvers,
  getLinkedObjectResolver,
  getObjectResolver,
  getObjectsResolver,
  filteringArgs
} from "@mnemotix/synaptix.js";
import WordDefinition from '../../definitions/WordDefinition';
import {LexicalEntryInputProperties, LexicalEntryInterfaceProperties} from "./LexicalEntryInterface.graphql";

export let WordProperties = `
  ${LexicalEntryInterfaceProperties}
  
  """ Has part of speech"""
  partOfSpeech: Concept
`;


export let WordInputProperties = `
 ${WordDefinition.generateGraphQLInputProperties()}
`;

export let WordType = `
interface WordInterface{
  ${WordProperties}
}

type Word implements EntityInterface & WordInterface & LexicalEntryInterface {
  ${WordProperties}
}

${generateConnectionForType("WordInterface")}  
${generateConnectionForType("Word")}
  
input WordInput{
  ${WordInputProperties}
}

extend type Query{
  """ Search for Word """
  words(${connectionArgs}, ${filteringArgs}): WordInterfaceConnection
  
  """ Get Word """
  word(id:ID): WordInterface
}
`;


export let WordTypeResolvers = {
  WordInterface: {
    ...generateInterfaceResolvers("WordInterface"),
    partOfSpeech: getLinkedObjectResolver.bind(this, WordDefinition.getLink('partOfSpeech')),
  },
  Word: {
    ...generateTypeResolvers("Word"),
  },
  Query: {
    words: getObjectsResolver.bind(this, WordDefinition),
    word: getObjectResolver.bind(this, WordDefinition)
  },
  ...generateConnectionResolverFor("Word")
};

