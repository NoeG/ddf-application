/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  getLinkedObjectResolver,
  getObjectResolver,
  getObjectsResolver,
  filteringArgs
} from "@mnemotix/synaptix.js";
import MultiWordExpressionDefinition from '../../definitions/MultiWordExpressionDefinition';
import {LexicalEntryInterfaceProperties} from "./LexicalEntryInterface.graphql";


export let MultiWordExpressionType = `
type MultiWordExpression implements EntityInterface & LexicalEntryInterface {
  ${LexicalEntryInterfaceProperties}
          
  """ Multiword type"""
  multiWordType: Concept
}
  
${generateConnectionForType("MultiWordExpression")}
  

input MultiWordExpressionInput {
  ${MultiWordExpressionDefinition.generateGraphQLInputProperties()}
}


extend type Query{
  """ Search for MultiWordExpression """
  multiWordExpressions(${connectionArgs}, ${filteringArgs}): MultiWordExpressionConnection
  
  """ Get MultiWordExpression """
  multiWordExpression(id:ID): MultiWordExpression
}
`;


export let MultiWordExpressionTypeResolvers = {
  MultiWordExpression: {
    ...generateTypeResolvers("MultiWordExpression"),
    multiWordType: getLinkedObjectResolver.bind(this, MultiWordExpressionDefinition.getLink('multiWordType')),
  },
  Query: {
    multiWordExpressions: getObjectsResolver.bind(this, MultiWordExpressionDefinition),
    multiWordExpression: getObjectResolver.bind(this, MultiWordExpressionDefinition)
  },
  ...generateConnectionResolverFor("MultiWordExpression")
};

