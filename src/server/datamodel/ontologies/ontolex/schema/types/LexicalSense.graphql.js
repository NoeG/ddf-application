/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import {
  connectionArgs,
  EntityInterfaceProperties,
  filteringArgs,
  generateConnectionArgs,
  generateConnectionForType,
  generateConnectionResolverFor,
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getObjectResolver,
  getObjectsResolver,
  I18nError,
  LinkPath,
  MnxOntologies,
  PropertyFilter,
  QueryFilter,
  Sorting,
  Link, LinkFilter, LinkDefinition
} from "@mnemotix/synaptix.js";
import LexicalSenseDefinition from '../../definitions/LexicalSenseDefinition';
import {getLinkedGeonamesPlacesResolver} from "../../../../../utilities/geonames/getLinkedGeonamesPlacesResolver";
import SemanticRelationDefinition from "../../../ddf/definitions/SemanticRelationDefinition";
import {senseToSenseSemanticRelationTypes} from "../../../ddf/schema/types/SemanticRelation.graphql";
import FormDefinition from "../../definitions/FormDefinition";
import LexicalEntryDefinition from "../../definitions/LexicalEntryDefinition";
import EntryDefinition from "../../../lexicog/definitions/EntryDefinition";
import env from "env-var";
import {LexicographicResourceDefinition} from "../../../lexicog/definitions";
import AggregateRatingDefinition from "../../../ddf-contribution/definitions/AggregateRatingDefinition";
import OnlineContributionDefinition from "../../../ddf-contribution/definitions/OnlineContributionDefinition";
import {getPlaceById} from "../../../../../utilities/geonames/getPlaceById";

const UserGroupDefinition = MnxOntologies.mnxAgent.ModelDefinitions.UserGroupDefinition;
const UserAccountDefinition = MnxOntologies.mnxAgent.ModelDefinitions.UserAccountDefinition;

export let LexicalSenseType = `
type LexicalSense implements EntityInterface {
  ${EntityInterfaceProperties}
  
  """ Is this lexical sense processed by an administrator user ?"""
  processed: Boolean 
  
  """ Is this lexical sense reviewed by an operator or and administrator user ?"""
  reviewed: Boolean
  
  """ Definition """ 
  definition: String

  """ Has connotation"""
  connotations: ConceptConnection
  
  """ Has domain"""
  domains: ConceptConnection
  
  """ Has frequency"""
  frequencies: ConceptConnection
  
  """ Has grammatical constraint"""
  grammaticalConstraints: ConceptConnection
  
  """ Has register"""
  registers: ConceptConnection
  
  """ Has sociolect"""
  sociolects: ConceptConnection
  
  """ Has temporality"""
  temporalities: ConceptConnection
  
  """ Has textual genre"""
  textualGenres: ConceptConnection
  
  """ Has glossary"""
  glossaries: ConceptConnection
  
  """ Restricted to"""
  restrictedTo: FormRestriction
  
  """ Usage examples"""
  usageExamples(${connectionArgs}, ${filteringArgs}): UsageExampleConnection
  
  """ Is lexicalized sense of"""
  isLexicalizedSenseOf: LexicalConcept
  
  """ Is sense of"""
  lexicalEntry: LexicalEntryInterface
  
  """ Semantic relations"""
  semanticRelations(filterOnSenseToSenseTypes: Boolean, excludeSenseToSenseTypes: Boolean, ${connectionArgs}, ${filteringArgs}): SemanticRelationConnection
  
  """ Has discussion about Sense"""
  postsAboutSense(${connectionArgs}, ${filteringArgs}): OnlineContributionConnection
  
  """ Top-ranked post about Sense"""
  topPostAboutSense: OnlineContribution
  
  """ Places of use"""
  places(${connectionArgs}, ${filteringArgs}): PlaceInterfaceConnection
  
  """ A shortcut to get lexicographic resource (lexicalSense => lexicalEntry => entry => lexicographicResource)"""
  lexicographicResource: Concept
  
  """ A shortcut to get canonical form (lexicalSense => lexicalEntry => form) """
  canonicalForm: Form
}
  
${generateConnectionForType("LexicalSense")}
  

input LexicalSenseInput {
  ${LexicalSenseDefinition.generateGraphQLInputProperties()}
}

extend type Query{
  """ Search for LexicalSense """
  lexicalSenses(${connectionArgs}, ${filteringArgs}): LexicalSenseConnection
  
  """ Get LexicalSense """
  lexicalSense(id:ID): LexicalSense
  
  """ Search for lexical senses matching an accurate written form """ 
  lexicalSensesForAccurateWrittenForm(formQs: String, filterByPlaceId: ID, ${connectionArgs}): LexicalSenseConnection
  
   """ 
   Search for contributed lexical senses
   
   Possible sorting properties to use in "sortings: [{sortBy: "...."}]" parameter: 
   
    - createdAt
    - reportingRatingCount
    - suppressionRatingCount
    - validationRatingCount
    
   Possible filtering parameters: 
   
    - filterOnLoggedUser: Only returns contributions for logged querying user.
    - filterOnUserAccountId: Only returns contributions for a specific userAccountId (secured and only available for Admin and Operator).
    - filterOnProcessed: Only returns processed/non processed contributions
    - filterOnReviewed: Only returns reviewed/non reviewed contributions
    - filterOnExistingSuppressionRating: Only returns contributions that have (or not) suppression rating(s).
    
   """
  contributedLexicalSenses(
    ${generateConnectionArgs()} 
    filterOnLoggedUser: Boolean 
    filterOnUserAccountId: ID
    filterOnProcessed: Boolean
    filterOnReviewed: Boolean
    filterOnExistingSuppressionRating: Boolean
  ): LexicalSenseConnection
}
`;


export let LexicalSenseTypeResolvers = {
  LexicalSense: {
    ...generateTypeResolvers("LexicalSense"),
    processed: (object) => !!object.processed,
    definition: getLocalizedLabelResolver.bind(this, LexicalSenseDefinition.getLabel('definition')),
    connotations: getLinkedObjectsResolver.bind(this, LexicalSenseDefinition.getLink('hasConnotation')),
    domains: getLinkedObjectsResolver.bind(this, LexicalSenseDefinition.getLink('hasDomain')),
    frequencies: getLinkedObjectsResolver.bind(this, LexicalSenseDefinition.getLink('hasFrequency')),
    glossaries: getLinkedObjectsResolver.bind(this, LexicalSenseDefinition.getLink('hasGlossary')),
    grammaticalConstraints: getLinkedObjectsResolver.bind(this, LexicalSenseDefinition.getLink('hasGrammaticalConstraint')),
    registers: getLinkedObjectsResolver.bind(this, LexicalSenseDefinition.getLink('hasRegister')),
    sociolects: getLinkedObjectsResolver.bind(this, LexicalSenseDefinition.getLink('hasSociolect')),
    temporalities: getLinkedObjectsResolver.bind(this, LexicalSenseDefinition.getLink('hasTemporality')),
    textualGenres: getLinkedObjectsResolver.bind(this, LexicalSenseDefinition.getLink('hasTextualGenre')),
    restrictedTo: getLinkedObjectResolver.bind(this, LexicalSenseDefinition.getLink('hasFormRestriction')),
    usageExamples: getLinkedObjectsResolver.bind(this, LexicalSenseDefinition.getLink('usageExample')),
    isLexicalizedSenseOf: getLinkedObjectResolver.bind(this, LexicalSenseDefinition.getLink('isLexicalizedSenseOf')),
    lexicalEntry: getLinkedObjectResolver.bind(this, LexicalSenseDefinition.getLink('lexicalEntry')),
    places: getLinkedGeonamesPlacesResolver.bind(this, LexicalSenseDefinition.getLink('places')),
    postsAboutSense: getLinkedObjectsResolver.bind(this, LexicalSenseDefinition.getLink('contributions')),
    /**
     * @param {Model} object
     * @param _
     * @param {SynaptixDatastoreRdfSession} synaptixSession
     */
    reviewed: async (object, _, synaptixSession) => {
      // We assume that a processed entity is reviewed as well.
      if (object.processed === true) {
        return true;
      }

      const adminGroupId = env.get("ADMIN_USER_GROUP_ID").required().asString();
      const operatorGroupId = env.get("OPERATOR_USER_GROUP_ID").required().asString();

      let hasAdminRating = await synaptixSession.isObjectExistsForLinkPath({
        object,
        modelDefinition: LexicalSenseDefinition,
        linkPath: new LinkPath()
          .step({linkDefinition: LexicalSenseDefinition.getLink('hasAggregationRating')})
          .step({linkDefinition: AggregateRatingDefinition.getLink('hasRater')})
          .step({linkDefinition: UserAccountDefinition.getLink('hasUserGroup'), targetId: adminGroupId})
      });

      let hasOperatorRating = await synaptixSession.isObjectExistsForLinkPath({
        object,
        modelDefinition: LexicalSenseDefinition,
        linkPath: new LinkPath()
          .step({linkDefinition: LexicalSenseDefinition.getLink('hasAggregationRating')})
          .step({linkDefinition: AggregateRatingDefinition.getLink('hasRater')})
          .step({linkDefinition: UserAccountDefinition.getLink('hasUserGroup'), targetId: operatorGroupId})
      });

      return hasAdminRating || hasOperatorRating;
    },
    topPostAboutSense: async (lexicalSense, args, synaptixSession) => synaptixSession.getObjects({
      modelDefinition: OnlineContributionDefinition,
      firstOne: true,
      args: {
        linkPaths: [
          new LinkPath()
            .step({linkDefinition: OnlineContributionDefinition.getLink("sense"), targetId: lexicalSense.id})
        ]
      }
    }),
    semanticRelations: async (lexicalSense, {filterOnSenseToSenseTypes, excludeSenseToSenseTypes, ...args}, synaptixSession) => {
      let semanticRelations = [];

      // @See https://mnemotix.atlassian.net/jira/software/projects/DIC/boards/38?selectedIssue=DIC-172
      // A semantic relation can be defined either directly from the lexicalSense or indirectly from the related lexicalEntry.
      // 1. Directly from LexicalSense => SemanticRelation
      semanticRelations = semanticRelations.concat(
        await synaptixSession.getObjects({
          modelDefinition: SemanticRelationDefinition,
          args: {
            ...args,
            linkPaths: [
              new LinkPath()
                .step({
                  linkDefinition: SemanticRelationDefinition.getLink("lexicalSenses"),
                  targetId: lexicalSense.id
                })
            ]
          }
        })
      );

      // 2. Undirectly from LexicalSense => Lexicalentry => SemanticRelation
      semanticRelations = semanticRelations.concat(
        await synaptixSession.getObjects({
          modelDefinition: SemanticRelationDefinition,
          args: {
            ...args,
            linkPaths: [
              new LinkPath()
                .step({
                  linkDefinition: SemanticRelationDefinition.getLink("lexicalEntries"),
                })
                .step({
                  linkDefinition: LexicalEntryDefinition.getLink("senses"),
                  targetId: lexicalSense.id
                })
            ]
          }
        })
      );

      // Now filter them.
      if (filterOnSenseToSenseTypes || excludeSenseToSenseTypes) {
        let filteredSemanticRelations = [];
        let filteredSemanticRelationTypes = [];

        for (let semanticRelation of semanticRelations) {
          let semanticRelationType = await synaptixSession.getLinkedObjectsFor({
            object: semanticRelation,
            linkDefinition: SemanticRelationDefinition.getLink("semanticRelationType"),
            fragmentDefinitions: []
          });

          let isSenseToSenseRelationType = !!senseToSenseSemanticRelationTypes.find(senseToSenseSemanticRelationType => (semanticRelationType?.uri || "").includes(senseToSenseSemanticRelationType));

          if ((filterOnSenseToSenseTypes && isSenseToSenseRelationType) || (excludeSenseToSenseTypes && !isSenseToSenseRelationType)) {
            if(!filteredSemanticRelationTypes.find((filteredSemanticRelationType) => filteredSemanticRelationType?.id === semanticRelationType?.id)){
              filteredSemanticRelations.push(semanticRelation);
              filteredSemanticRelationTypes.push(semanticRelationType);
            }
          }
        }

        semanticRelations = filteredSemanticRelations;
      }

      return synaptixSession.wrapObjectsIntoGraphQLConnection(semanticRelations, args)
    },
    /**
     * @param {Model} object
     * @param __
     * @param {SynaptixDatastoreRdfSession} synaptixSession
     */
    canonicalForm: async (object, __, synaptixSession) => synaptixSession.getObjects({
      firstOne: true,
      modelDefinition: FormDefinition,
      args: {
        linkPaths: [
          new LinkPath({
            indexedShortcutLink: new Link({
              linkDefinition: LexicalSenseDefinition.getLink("hasCanonicalForm"),
              targetId: object
            })
          })
            .step({linkDefinition: FormDefinition.getLink("lexicalEntries")})
            .step({
              linkDefinition: LexicalEntryDefinition.getLink("senses"),
              targetId: object.id
            })
        ]
      }
    }),
    /**
     * @param {Model} object
     * @param __
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    lexicographicResource: async (object, __, synaptixSession) => synaptixSession.getObjects({
      modelDefinition: LexicographicResourceDefinition,
      firstOne: true,
      args: {
        linkPaths: [
          new LinkPath({
            indexedShortcutLink: new Link({
              linkDefinition: LexicalSenseDefinition.getLink("hasLexicographicResource"),
              targetId: object
            })
          })
            .step({linkDefinition: LexicographicResourceDefinition.getLink("entry")})
            .step({linkDefinition: EntryDefinition.getLink("lexicalEntry")})
            .step({
              linkDefinition: LexicalEntryDefinition.getLink("senses"),
              targetId: object.id
            })
        ]
      },
    })
  },
  Query: {
    lexicalSenses: getObjectsResolver.bind(this, LexicalSenseDefinition),
    lexicalSense: getObjectResolver.bind(this, LexicalSenseDefinition),
    /**
     *
     * @param parent
     * @param {string} formQs
     * @param {string} filterByPlaceId
     * @param {ResolverArgs} args
     * @param {SynaptixDatastoreRdfSession} synaptixSession
     * @return {Promise<void>}
     */
    lexicalSensesForAccurateWrittenForm: async (parent, {formQs, filterByPlaceId, ...args}, synaptixSession, info) => {
      if (!synaptixSession.isIndexDisabled()){
        args.sortings = [];

        if (filterByPlaceId) {
          let place = await getPlaceById({id: filterByPlaceId});

          if (place.lat && place.lng) {
            args.sortings.push(new Sorting({
              sortingDefinition: LexicalSenseDefinition.getSorting("nearby"),
              params: {
                lat: parseFloat(place.lat),
                lon: parseFloat(place.lng)
              }
            }));
          }
        }

        args.sortings.push(new Sorting({
          sortingDefinition: LexicalSenseDefinition.getSorting("ratingScore"),
          descending: true
        }));
      }

      if(synaptixSession.isIndexDisabled()){
        args.linkPaths =  [
          new LinkPath()
            .step({linkDefinition: LexicalSenseDefinition.getLink("lexicalEntry")})
            .step({linkDefinition: LexicalEntryDefinition.getLink("canonicalForm")})
            .filterOnProperty({
              propertyDefinition: FormDefinition.getLabel("writtenRep"),
              value: `^${formQs.trim().replace('.', '\.')}$`
            })
        ];
      } else {
        args.queryFilters = [
          new QueryFilter({
            filterDefinition: LexicalSenseDefinition.getFilter("accurateFormWrittenRep"),
            filterGenerateParams: {formQs}
          })
        ]
      }
      return getObjectsResolver(LexicalSenseDefinition, parent, args, synaptixSession, info);
    },
    /**
     * @param parent
     * @param filterOnUserAccountId
     * @param {string} [filterOnLoggedUser]
     * @param {boolean} [filterOnProcessed]
     * @param {boolean} [filterOnReviewed]
     * @param {boolean} [filterOnExistingSuppressionRating]
     * @param {ResolverArgs} args
     * @param {SynaptixDatastoreRdfSession} synaptixSession
     * @param {GraphQLResolveInfo} info
     * @return {Model[]}
     */
    contributedLexicalSenses: async (parent, {
      filterOnLoggedUser,
      filterOnUserAccountId,
      filterOnProcessed,
      filterOnReviewed,
      filterOnExistingSuppressionRating,
      ...args
    }, synaptixSession, info) => {

      let linkPaths = [];
      let propertyFilters = [];
      let queryFilters = [];
      let linkFilters = [];

      if (filterOnLoggedUser || filterOnUserAccountId) {
        let userAccountId;

        if (filterOnUserAccountId) {
          userAccountId = synaptixSession.extractIdFromGlobalId(filterOnUserAccountId);
        } else {
          if (!(await synaptixSession.isLoggedUser())) {
            throw new I18nError(`User must be logged to request it's contribution`, "USER_MUST_BE_AUTHENTICATED", 401);
          }

          userAccountId = (await synaptixSession.getLoggedUserAccount()).id;
        }

        linkPaths.push(
          new LinkPath({
            indexedShortcutLinkFilter: new LinkFilter({
              linkDefinition: LexicalSenseDefinition.getLink("hasCreator"),
              id: userAccountId
            })
          })
            .step({linkDefinition: LexicalSenseDefinition.getLink("hasCreationAction")})
            .step({
              linkDefinition: MnxOntologies.mnxContribution.ModelDefinitions.CreationDefinition.getLink("hasUserAccount"),
              targetId: userAccountId
            })
        );
      } else {
        linkFilters.push(
          new LinkFilter({
            linkDefinition:  LexicalSenseDefinition.getLink("hasCreationAction"),
            any: true
          })
        );
      }

      if (filterOnProcessed != null) {
        propertyFilters.push(new PropertyFilter({
          propertyDefinition: LexicalSenseDefinition.getProperty("processed"),
          value: filterOnProcessed
        }))
      }

      if (filterOnReviewed != null) {
        const adminGroupId = env.get("ADMIN_USER_GROUP_ID").required().asString();
        const operatorGroupId = env.get("OPERATOR_USER_GROUP_ID").required().asString();

        queryFilters.push(new QueryFilter({
          filterDefinition: LexicalSenseDefinition.getFilter("isReviewed"),
          filterGenerateParams: {
            userGroupIds: [adminGroupId, operatorGroupId]
          },
          isStrict: true,
          isNeq: filterOnReviewed === false
        }))
      }

      if (filterOnExistingSuppressionRating != null) {
        queryFilters.push(new QueryFilter({
          filterDefinition: LexicalSenseDefinition.getFilter("hasSuppressionRatings"),
          isStrict: true,
          isNeq: filterOnExistingSuppressionRating === false
        }));
      }

      return getObjectsResolver(
        LexicalSenseDefinition,
        parent,
        {
          ...args,
          linkPaths,
          propertyFilters,
          queryFilters,
          linkFilters
        },
        synaptixSession,
        info
      );
    }
  },
  ...generateConnectionResolverFor("LexicalSense")
};

