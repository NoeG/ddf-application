/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  connectionFromArray,
  EntityInterfaceProperties,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor,
  generateInterfaceResolvers,
  getLinkedObjectResolver,
  getLinkedObjectsResolver,
  getObjectResolver,
  getObjectsResolver
} from "@mnemotix/synaptix.js";

import LexicalEntryDefinition from '../../definitions/LexicalEntryDefinition';
import FormDefinition from "../../definitions/FormDefinition";
import {getLexicalEntryContribInputsFromFormWrittenRep} from "../../helpers/getLexicalEntryContribInputsFromFormWrittenRep";

export let LexicalEntryInterfaceProperties = `
  ${EntityInterfaceProperties}
   
  """ Posts about etymology """
  postsAboutEtymology(${connectionArgs}, ${filteringArgs}): OnlineContributionConnection
  
  """ Canonical form"""
  canonicalForm: Form
  
  """ Lexical form"""
  lexicalForms(${connectionArgs}, ${filteringArgs}): FormConnection
  
  """ Other form"""
  otherForms(${connectionArgs}, ${filteringArgs}): FormConnection
  
  """ Sense"""
  senses(${connectionArgs}, ${filteringArgs}): LexicalSenseConnection
  
  """ Semantic relations"""
  semanticRelations: SemanticRelationConnection
  
  """ Related lexicog entry """
  entry: Entry
`;

export let LexicalEntryInputProperties = `
  ${LexicalEntryDefinition.generateGraphQLInputProperties()}
`;

export let LexicalEntryInterfaceType = `
interface LexicalEntryInterface {
  ${LexicalEntryInterfaceProperties}
}
  
input LexicalEntryInput{
  ${LexicalEntryInputProperties}
}

${generateConnectionForType("LexicalEntryInterface")}

type FormWrittenRepContribInputDefinitions {
  lexicalEntryTypeName: String
  grammaticalCategorySchemeId: ID
}
  
extend type Query{
  """ Search for lexical entries """
  lexicalEntries(${connectionArgs}, ${filteringArgs}): LexicalEntryInterfaceConnection
 
  """ Get LexicalEntry """
  lexicalEntry(id:ID): LexicalEntryInterface
  
  """ 
  Get a (LexicalEntry inherited type name | Grammatical category scheme ID) tuple given a form written representation. The rules are :
  
  If "formWrittenRep" :
   - Contains spaces => it's a MultiWordExpression type
   - Begins/ends with a "-" => it's an Affix type
   - Otherwise => it's a Word
  """
  lexicalEntryContribInputsFromFormWrittenRep(formWrittenRep: String): FormWrittenRepContribInputDefinitions
}
`;


export let LexicalEntryInterfaceResolvers = {
  LexicalEntryInterface: {
    ...generateInterfaceResolvers("LexicalEntryInterface"),
    postsAboutEtymology: getLinkedObjectsResolver.bind(this, LexicalEntryDefinition.getLink('contributions')),
    canonicalForm: getLinkedObjectResolver.bind(this, LexicalEntryDefinition.getLink('canonicalForm')),
    lexicalForms: getLinkedObjectsResolver.bind(this, LexicalEntryDefinition.getLink('lexicalForms')),
    otherForms: getLinkedObjectsResolver.bind(this, LexicalEntryDefinition.getLink('otherForms')),
    senses: getLinkedObjectsResolver.bind(this, LexicalEntryDefinition.getLink('senses')),
    semanticRelations: getLinkedObjectsResolver.bind(this, LexicalEntryDefinition.getLink('semanticRelations')),
    entry: getLinkedObjectResolver.bind(this, LexicalEntryDefinition.getLink('entry'))
  },
  Query: {
    lexicalEntries: getObjectsResolver.bind(this, LexicalEntryDefinition),
    lexicalEntry: getObjectResolver.bind(this, LexicalEntryDefinition),
    lexicalEntryContribInputsFromFormWrittenRep: (_, {formWrittenRep}) => getLexicalEntryContribInputsFromFormWrittenRep(formWrittenRep)
  },
  ...generateConnectionResolverFor("LexicalEntryInterface")
};
