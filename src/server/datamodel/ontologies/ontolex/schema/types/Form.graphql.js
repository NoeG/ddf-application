/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  getLinkedObjectResolver,
  getLinkedObjectsResolver,
  getObjectResolver,
  getObjectsResolver,
  getLocalizedLabelResolver,
  EntityInterfaceProperties,
  filteringArgs
} from "@mnemotix/synaptix.js";
import FormDefinition from '../../definitions/FormDefinition';
import {getLinkedGeonamesPlacesResolver} from "../../../../../utilities/geonames/getLinkedGeonamesPlacesResolver";

export let FormType = `
type Form implements EntityInterface {
  """ Written representation """
  writtenRep: String
  
  #""" Phonetic representation """
  #phoneticRep: String
  
  """ Has form type"""
  formType: Concept
  
  """ posts about form """
  postsAboutForm(${connectionArgs}, ${filteringArgs}): OnlineContributionConnection
  
  """ Related lexical entries """ 
  lexicalEntries: LexicalEntryInterfaceConnection
  
  """ Places of use"""
  places(${connectionArgs}, ${filteringArgs}): PlaceInterfaceConnection
  
  ${EntityInterfaceProperties}
}
  
${generateConnectionForType("Form")}
  

input FormInput {
  """ The ID """
  id: ID @formInput(type:"hidden")
  
  """ Written representation """
  writtenRep: String
  
  """ Phonetic representation """
  phoneticRep: String
}


extend type Query{
""" Search for Form """
forms(${connectionArgs}, ${filteringArgs}): FormConnection

""" Get Form """
form(id:ID): Form
}
`;


export let FormTypeResolvers = {
  Form: {
    ...generateTypeResolvers("Form"),
    lexicalEntries: getLinkedObjectsResolver.bind(this, FormDefinition.getLink('lexicalEntries')),
    writtenRep: getLocalizedLabelResolver.bind(this, FormDefinition.getLabel('writtenRep')),
    //phoneticRep: getLocalizedLabelResolver.bind(this, FormDefinition.getLabel('phoneticRep')),
    formType: getLinkedObjectResolver.bind(this, FormDefinition.getLink('formType')),
    postsAboutForm: getLinkedObjectsResolver.bind(this, FormDefinition.getLink('contributions')),
    places: getLinkedGeonamesPlacesResolver.bind(this, FormDefinition.getLink('places'))
  },
  Query: {
    forms: getObjectsResolver.bind(this, FormDefinition),
    form: getObjectResolver.bind(this, FormDefinition)
  },
  ...generateConnectionResolverFor("Form")
};

