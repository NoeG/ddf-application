/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {I18nError} from "@mnemotix/synaptix.js";

/**
 * Parse a form written representation and get it's related lexicalEntry inherited type name and grammatical category scheme id
 *
 * @param formWrittenRep
 * @return {{grammaticalCategorySchemeId: String, lexicalEntryTypeName: String}}
 */
export function getLexicalEntryContribInputsFromFormWrittenRep(formWrittenRep){
  if (!formWrittenRep) {
    throw new I18nError("formWrittenRep is empty and can not be parsed", "FORM_WRITTEN_REP_EMPTY", 400)
  }

  if(formWrittenRep.includes(" ")){
    // This is a MultiWordExpression
    return {
      lexicalEntryTypeName: "MultiWordExpression",
      grammaticalCategorySchemeId: "http://data.dictionnairedesfrancophones.org/authority/multiword-type"
    }
  } else if (!!formWrittenRep.match(/^-|-$/)){
    // This is an Affix
    return {
      lexicalEntryTypeName: "Affix",
      grammaticalCategorySchemeId: "http://data.dictionnairedesfrancophones.org/authority/term-element"
    }
  } else {
    // This is an Word
    return {
      lexicalEntryTypeName: "Word",
      grammaticalCategorySchemeId: "http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type"
    }
  }
}