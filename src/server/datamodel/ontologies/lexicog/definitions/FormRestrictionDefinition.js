/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {LabelDefinition, ModelDefinitionAbstract} from "@mnemotix/synaptix.js";

import FormRestriction from "../models/FormRestriction";


export default class FormRestrictionDefinition extends ModelDefinitionAbstract {
  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping()
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "lexicog:FormRestriction";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return FormRestriction;
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    // return 'nroArea';
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    // return FormRestrictionIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      // new LinkDefinition({
      //   linkName: 'contract',
      //   pathInIndex: 'contract',
      //   rdfObjectProperty: "hddged:subsequentContract",
      //   relatedModelDefinition: ContractDefinition
      //   isPlural: true
      // })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'description',
        pathInIndex: 'descriptions',
        rdfDataProperty: "ddf:hasDescription"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
    ];
  }
};

