/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {LabelDefinition, LinkDefinition, MnxOntologies, ModelDefinitionAbstract} from "@mnemotix/synaptix.js";

import LexicographicResource from "../models/LexicographicResource";
import {EntryDefinition} from "./index";

export default class LexicographicResourceDefinition extends ModelDefinitionAbstract {

  static getParentDefinitions(){
    return [MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition];
  }

  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping()
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "skos:Concept";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition.getIndexType();
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'entry',
        symmetricLinkName: "lexicographicResource",
        rdfReversedObjectProperty: "ddf:hasLexicographicResource",
        relatedModelDefinition: EntryDefinition,
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'value',
        pathInIndex: 'values',
        rdfDataProperty: "rdf:value"
      })
    ];
  }
};

