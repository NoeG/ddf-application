/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {LabelDefinition, ModelDefinitionAbstract, LinkDefinition, LiteralDefinition} from "@mnemotix/synaptix.js";

import UsageExample from "../models/UsageExample";
import PlaceDefinition from "../../geonames/definitions/PlaceDefinition";
import LexicalSenseDefinition from "../../ontolex/definitions/LexicalSenseDefinition";

export default class UsageExampleDefinition extends ModelDefinitionAbstract {
  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping()
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "lexicog:UsageExample";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return UsageExample;
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'usage-example';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'lexicalSense',
        symmetricLinkName: "usageExample",
        rdfReversedObjectProperty: 'lexicog:usageExample',
        relatedModelDefinition: LexicalSenseDefinition,
      }),
      new LinkDefinition({
        linkName: 'places',
        rdfObjectProperty: 'ddf:hasLocalisation',
        relatedModelDefinition: PlaceDefinition,
        isPlural: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'description',
        rdfDataProperty: "ddf:hasDescription"
      }),
      new LabelDefinition({
        labelName: 'value',
        rdfDataProperty: "rdf:value"
      }),
      new LabelDefinition({
        labelName: 'bibliographicalCitation',
        rdfDataProperty: "http://purl.org/dc/terms/bibliographicalCitation"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals()
    ];
  }
};

