/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  getObjectResolver,
  getObjectsResolver,
  getLocalizedLabelResolver,
  EntityInterfaceInput,
  EntityInterfaceProperties,
  filteringArgs
} from "@mnemotix/synaptix.js";
import LexicographicResourceDefinition from '../../definitions/LexicographicResourceDefinition';


export let LexicographicResourceType = `
type LexicographicResource implements EntityInterface {
  ${EntityInterfaceProperties}
  
  """ Value """
  value: String
}
  
${generateConnectionForType("LexicographicResource")}
  

input LexicographicResourceInput {
  ${LexicographicResourceDefinition.generateGraphQLInputProperties()}
}


extend type Query{
""" Search for LexicographicResource """
lexicographicResources(${connectionArgs}, ${filteringArgs}): LexicographicResourceConnection

""" Get LexicographicResource """
lexicographicResource(id:ID): LexicographicResource
}
`;


export let LexicographicResourceTypeResolvers = {
  LexicographicResource: {
    ...generateTypeResolvers("LexicographicResource"),
    value: getLocalizedLabelResolver.bind(this, LexicographicResourceDefinition.getLabel("value"))
  },
  Query: {
    lexicographicResources: getObjectsResolver.bind(this, LexicographicResourceDefinition),
    lexicographicResource: getObjectResolver.bind(this, LexicographicResourceDefinition)
  },
  ...generateConnectionResolverFor("LexicographicResource")
};

