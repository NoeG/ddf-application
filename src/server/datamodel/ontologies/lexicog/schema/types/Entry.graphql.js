/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  getObjectResolver,
  getObjectsResolver,
  getLinkedObjectResolver,
  EntityInterfaceProperties,
  EntityInterfaceInput,
  filteringArgs
} from "@mnemotix/synaptix.js";
import EntryDefinition from '../../definitions/EntryDefinition';

export let EntryType = `
type Entry implements EntityInterface {
  ${EntityInterfaceProperties}
  
  """ Lexical entry """
  lexicalEntry: LexicalEntryInterface
  
  """ Lexicographic Resource """
  lexicographicResource: Concept
}
  
${generateConnectionForType("Entry")}
  

input EntryInput {
 ${EntryDefinition.generateGraphQLInputProperties()}
}


extend type Query{
""" Search for Entry """
entrys(${connectionArgs}, ${filteringArgs}): EntryConnection

""" Get Entry """
entry(id:ID): Entry
}
`;


export let EntryTypeResolvers = {
  Entry: {
    ...generateTypeResolvers("Entry"),
    lexicalEntry: getLinkedObjectResolver.bind(this, EntryDefinition.getLink('lexicalEntry')),
    lexicographicResource: getLinkedObjectResolver.bind(this, EntryDefinition.getLink('lexicographicResource')),
  },
  Query: {
    entrys: getObjectsResolver.bind(this, EntryDefinition),
    entry: getObjectResolver.bind(this, EntryDefinition)
  },
  ...generateConnectionResolverFor("Entry")
};

