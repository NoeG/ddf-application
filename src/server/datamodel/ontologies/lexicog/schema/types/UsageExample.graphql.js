/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  getObjectResolver,
  getObjectsResolver,
  getLocalizedLabelResolver,
  EntityInterfaceProperties,
  filteringArgs
} from "@mnemotix/synaptix.js";
import UsageExampleDefinition from '../../definitions/UsageExampleDefinition';
import {getLinkedGeonamesPlacesResolver} from "../../../../../utilities/geonames/getLinkedGeonamesPlacesResolver";


export let UsageExampleType = `
type UsageExample implements EntityInterface {
  ${EntityInterfaceProperties}
  
  """ Value """
  value: String
  
  """ Bibliographical citation """
  bibliographicalCitation: String 
  
  """ Places of use"""
  places(${connectionArgs}, ${filteringArgs}): PlaceInterfaceConnection
}
  
${generateConnectionForType("UsageExample")}
  

input UsageExampleInput {
  """ The ID """
  id: ID @formInput(type:"hidden")
}


extend type Query{
""" Search for UsageExample """
usageExamples(${connectionArgs}, ${filteringArgs}): UsageExampleConnection

""" Get UsageExample """
usageExample(id:ID): UsageExample
}
`;


export let UsageExampleTypeResolvers = {
  UsageExample: {
    ...generateTypeResolvers("UsageExample"),
    value: getLocalizedLabelResolver.bind(this, UsageExampleDefinition.getLabel("value")),
    bibliographicalCitation: getLocalizedLabelResolver.bind(this, UsageExampleDefinition.getLabel("bibliographicalCitation")),
    places: getLinkedGeonamesPlacesResolver.bind(this, UsageExampleDefinition.getLink('places'))
  },
  Query: {
    usageExamples: getObjectsResolver.bind(this, UsageExampleDefinition),
    usageExample: getObjectResolver.bind(this, UsageExampleDefinition)
  },
  ...generateConnectionResolverFor("UsageExample")
};

