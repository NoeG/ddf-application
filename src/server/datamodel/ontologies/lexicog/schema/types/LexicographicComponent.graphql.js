/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  getObjectResolver,
  getObjectsResolver,
  EntityInterfaceProperties,
  filteringArgs
} from "@mnemotix/synaptix.js";
import LexicographicComponentDefinition from '../../definitions/LexicographicComponentDefinition';


export let LexicographicComponentType = `
type LexicographicComponent implements EntityInterface {
  ${EntityInterfaceProperties}
}
  
${generateConnectionForType("LexicographicComponent")}
  

input LexicographicComponentInput {
  """ The ID """
  id: ID @formInput(type:"hidden")
}


extend type Query{
""" Search for LexicographicComponent """
lexicographicComponents(${connectionArgs}, ${filteringArgs}): LexicographicComponentConnection

""" Get LexicographicComponent """
lexicographicComponent(id:ID): LexicographicComponent
}
`;


export let LexicographicComponentTypeResolvers = {
  LexicographicComponent: {
    ...generateTypeResolvers("LexicographicComponent"),

    // Define your properties resolvers here...

  },
  Query: {
    lexicographicComponents: getObjectsResolver.bind(this, LexicographicComponentDefinition),
    lexicographicComponent: getObjectResolver.bind(this, LexicographicComponentDefinition)
  },
  ...generateConnectionResolverFor("LexicographicComponent")
};

