/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  LabelDefinition,
  LinkDefinition,
  LiteralDefinition,
  MnxOntologies,
  ModelDefinitionAbstract
} from "@mnemotix/synaptix.js";

import Place from "../models/Place";
import {EntryDefinition} from "../../lexicog/definitions";
import LexicalSenseDefinition from "../../ontolex/definitions/LexicalSenseDefinition";

export default class PlaceDefinition extends ModelDefinitionAbstract {
  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping()
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "skos:Concept";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Place;
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition.getIndexType();
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'senses',
        symmetricLinkName: "places",
        rdfReversedObjectProperty: "ddf:hasLocalisation",
        relatedModelDefinition: LexicalSenseDefinition,
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "names",
        pathInIndex: "alternateNames" // @see http://www.geonames.org/export/web-services.html#get
      }),
      new LabelDefinition({
        labelName: 'color',
        rdfDataProperty: "mnx:color"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'bbox',
        pathInIndex: 'bbox',
      }),
      new LiteralDefinition({
        literalName: 'lng',
        pathInIndex: 'lng',
      }),
      new LiteralDefinition({
        literalName: 'lat',
        pathInIndex: 'lat',
        rdfDataType: "http://www.w3.org/2001/XMLSchema#float",
      }),
      new LiteralDefinition({
        literalName: 'wikipediaURL',
        pathInIndex: 'wikipediaURL'
      })
    ];
  }
};

