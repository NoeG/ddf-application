/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import got from "got";

import {
  connectionArgs,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  filteringArgs,
  EntityInterfaceProperties,
  connectionFromArray,
  getLocalizedLabelResolver
} from "@mnemotix/synaptix.js";

import {getPlaceById} from "../../../../../utilities/geonames/getPlaceById";
import {searchPlaces} from "../../../../../utilities/geonames/searchPlaces";
import {getPlaceByGeoCoords} from "../../../../../utilities/geonames/getPlaceByGeoCoords";
import PlaceDefinition from "../../definitions/PlaceDefinition";


export let PlaceProperties = `
  ${EntityInterfaceProperties}
  
  """ Longitude """
  lng: Float
  
  """ Latitude """
  lat: Float
  
  """ Name """
  name: String
  
  """ Country Code """
  countryCode: String
  
  """ Color """
  color: String
`;

export let PlaceInterface = `

interface PlaceInterface {
  ${PlaceProperties}
}         
  
${generateConnectionForType("PlaceInterface")}
  
input PlaceInput {
  """ The ID """
  id: ID @formInput(type:"hidden")
}


extend type Query{
  """ Search for Place """
  places(${connectionArgs}, ${filteringArgs}, citiesOnly: Boolean): PlaceInterfaceConnection
  
  """ Get Place """
  place(id:ID): PlaceInterface
  
  """ Get place by Geo Coors (lat, lng) """
  placeByGeoCoords(lat:Float, lng:Float): PlaceInterface
}
`;

export let PlaceTypeResolvers = {
  PlaceInterface: {
    ...generateTypeResolvers("PlaceInterface"),
    name: (place) => place.name,
    lng: (place) => place.lng,
    lat: (place) => place.lat,
    color: getLocalizedLabelResolver.bind(this, PlaceDefinition.getLabel('color')),
    countryCode: (place) => place.countryCode,
    __resolveType: (place) => {

      if (place.id === place.countryId){
        return "Country";
      }

      if (place.fcode === "ADM1"){
        return "State";
      }

      return "City";
    }
  },
  Query: {
    places: async (_, args, synaptixSession) => {
      let {qs, first, citiesOnly} = args;

      let places = await searchPlaces({qs, limit: first, citiesOnly, lang: synaptixSession.getContext().getLang()});

      return connectionFromArray(places || [], args);
    },
    place: (_, {id}, synaptixSession) => {
      return getPlaceById({
        id: id.replace("geonames:", ""),
        lang: synaptixSession.getContext().getLang()
      })
    },
    placeByGeoCoords: (_, {lat, lng}, synaptixSession) => {
      return getPlaceByGeoCoords({
        lat,
        lng,
        lang: synaptixSession.getContext().getLang()
      })
    },
  },
  ...generateConnectionResolverFor("PlaceInterface")
};

