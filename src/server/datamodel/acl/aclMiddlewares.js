/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  replaceFieldValueIfRuleFailsMiddleware,
  isAdminRule,
  isCreatorRule,
  isInUserGroupRule,
  isAuthenticatedRule
} from "@mnemotix/synaptix.js";
import {and, or, allow, rule} from "graphql-shield";
import env from "env-var";

/**
 * @return {Rule}
 */
export let isCreatorOrAdminRule = () => and(isAuthenticatedRule(), or(isCreatorRule(), isAdminRule()));


/**
 * @return {Rule}
 */
export let isOperatorRule = () => and(isAuthenticatedRule(), or(isInUserGroupRule({
  userGroupId: () => env.get("OPERATOR_USER_GROUP_ID").required().asString()
}), isAdminRule()));

/**
 * To have a good comprehension of the following definitons, please explore the Synaptix.js documentation.
 *
 * https://mnemotix.gitlab.io/synaptix.js/securizing_data/#graphql-shield-middlewares
 */

/**
 * This middleware is used to set "false" value to a target type field if a shield rule fails.
 */
export let falsifyFieldIfRuleFailsMiddleware = replaceFieldValueIfRuleFailsMiddleware({
  rule: isCreatorOrAdminRule(),
  replaceValue: false
});

/**
 * This middleware definition list apply the field falsification in order to be used in UI.
 */
export let DdfContribAclMiddlewares = {
  Person: {
    canUpdate: falsifyFieldIfRuleFailsMiddleware
  },
  LexicalSense: {
    canUpdate: falsifyFieldIfRuleFailsMiddleware
  }
};

/**
 * This shield middleware definition works alongside DdfContribAclMiddlewares and assert mutations securization.
 */
export let DdfContribAclShieldRules = {
  Query: {
    persons: isOperatorRule(),
    person: isOperatorRule(),
    contributedLexicalSenses: isAuthenticatedRule(),
  },
  Mutation: {
    /**
     * Securizing following mutations by asserting that logged user is EITHER an administrator OR the object creator.
     */
    updateLexicalSense: rule()(
      async (parent, args, ctx, info) => {
        // If "processed" property is mutated, check that user is an Administrator
        if (typeof args?.input?.objectInput?.processed !== "undefined"){
          return isAdminRule().resolve(parent, args, ctx, info);
        } else {
          return isCreatorOrAdminRule().resolve(parent, args, ctx, info)
        }
      },
    ),
    removeObject: isCreatorOrAdminRule(),
    removeEntity: isCreatorOrAdminRule(),
    removeEntityLink: isCreatorOrAdminRule(),
    /**
     * Only operator users can create suppression rating.
     */
    createSuppressionRating: isOperatorRule(),
  },
  // A non admin user can only query person nicknames. Nothing more.
  UserAccount: {
    '*': or(isAdminRule(), rule()(
      /**
       * @param {Model} userAccount
       * @param args
       * @param {SynaptixDatastoreRdfSession} synaptixSession
       * @param info
       */
      async (userAccount, args, synaptixSession, info) => {
        let loggedUserAccount = await synaptixSession.getLoggedUserAccount();

        return loggedUserAccount && loggedUserAccount?.id === userAccount.id;
      }
    ))
  },
};