/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {DataModel, mergeResolvers, mergeRules, MnxDatamodels} from "@mnemotix/synaptix.js";

import {
  DdfModelDefinitions,
  DdfResolvers,
  DdfTypes
} from "./ontologies/ddf";

import {
  LexicogModelDefinitions,
  LexicogResolvers,
  LexicogTypes
} from "./ontologies/lexicog";

import {
  OntolexModelDefinitions,
  OntolexResolvers,
  OntolexTypes
} from "./ontologies/ontolex";


import {
  GeonamesModelDefinitions,
  GeonamesResolvers,
  GeonamesTypes
} from "./ontologies/geonames";


import {
  DdfContributionTypes,
  DdfContributionResolvers,
  DdfContributionMutations,
  DdfContributionMutationsResolvers,
  DdfContributionModelDefinitions
} from "./ontologies/ddf-contribution";

import {DdfContribAclMiddlewares, DdfContribAclShieldRules} from "./acl/aclMiddlewares";

import PersonDefinition from "./ontologies/ddf-contribution/definitions/PersonDefinition";


/**
 * This is an instance of the datamodel.
 * @see https://gitlab.com/mnemotix/synaptix.js#datamodel
 */
export let dataModel = (new DataModel({
  typeDefs: [].concat(
    DdfTypes,
    LexicogTypes,
    OntolexTypes,
    GeonamesTypes,
    DdfContributionTypes,
    DdfContributionMutations,
  ),
  resolvers: mergeResolvers(
    DdfResolvers,
    LexicogResolvers,
    OntolexResolvers,
    GeonamesResolvers,
    DdfContributionResolvers,
    DdfContributionMutationsResolvers,
  ),
  modelDefinitions: Object.assign({},       // List here the model definitions used for organizing and helping data resolution.
    DdfModelDefinitions,
    LexicogModelDefinitions,
    OntolexModelDefinitions,
    GeonamesModelDefinitions,
    DdfContributionModelDefinitions
  ),
  shieldRules: mergeRules(DdfContribAclShieldRules)
}))
  .mergeWithDataModels([MnxDatamodels.mnxCoreDataModel, MnxDatamodels.mnxAgentDataModel])
  .addDefaultSchemaTypeDefs()
  .addSSOMutations()
  .addMiddlewares([DdfContribAclMiddlewares])
  .setModelDefinitionForLoggedUserPerson(PersonDefinition);
