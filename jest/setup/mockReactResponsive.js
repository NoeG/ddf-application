import React from 'react';

// We have to rename window to `mockWindow` so that `jest.mock` doesn't
// complain.
const mockWindow = window;

export function setupMockReactResponsive() {
  jest.mock("react-responsive", () => {
    const MediaQuery = require.requireActual("react-responsive").default;
    const useMediaQuery = require.requireActual("react-responsive").useMediaQuery;

    const MockMediaQuery = (props) => {
      const defaultWidth = mockWindow.innerWidth;
      const defaultHeight = mockWindow.innerHeight;
      const device = Object.assign({}, { width: defaultWidth, height: defaultHeight }, props.device);

      return <MediaQuery {...props} device={device} />;
    }

    function MockUseMediaQuery(options, device = {}) {
      const defaultWidth = mockWindow.innerWidth;
      const defaultHeight = mockWindow.innerHeight;
      return useMediaQuery(options, {
        ...device,
        width: defaultWidth, 
        height: defaultHeight 
      });
    }

    return {
      __esModule: true, 
      default: MockMediaQuery,
      useMediaQuery: MockUseMediaQuery
    };
  })
}
