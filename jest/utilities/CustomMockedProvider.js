import React from 'react';
import {MockedProvider} from '@apollo/react-testing';
import {MockLink} from '@apollo/react-testing';
import {onError} from "apollo-link-error";
import {ApolloLink} from 'apollo-link';

/**
 *  Extend apollo's MockedProvider, in order to display warning for better debuggability of tests, 
 *  in case of missing mocks. See https://github.com/apollographql/apollo-client/issues/5917
 */

export function CustomMockedProvider(props) {
  let {mocks, ...otherProps} = props;

  let mockLink = new MockLink(mocks);
  let errorLoggingLink = onError(({ networkError }) => {
    if (networkError) {
      /No more mocked responses for the query/.test(networkError.message);
      console.warn(`[Network error]: ${networkError}`);
    }
  });
  let link = ApolloLink.from([errorLoggingLink, mockLink]);

  return <MockedProvider {...otherProps} link={link} />;
}
