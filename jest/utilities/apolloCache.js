import {InMemoryCache, IntrospectionFragmentMatcher} from 'apollo-cache-inmemory';
import {FragmentTypes} from '../../src/generated/FragmentTypes';

let fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: FragmentTypes
});

export function generateApolloCache() {
  return new InMemoryCache({fragmentMatcher});
}
