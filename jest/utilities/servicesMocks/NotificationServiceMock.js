import {NEVER} from 'rxjs';

export class NotificationServiceMock {
  constructor() {
    this.alert = jest.fn();
    this.info = jest.fn();
    this.success = jest.fn();
    this.error = jest.fn();

    this.messages$ = NEVER;
  }
}
