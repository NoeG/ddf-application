import {ApolloClient} from 'apollo-client';
import {MockLink} from '@apollo/react-testing';
import {generateApolloCache} from '../apolloCache';

export function generateMockedApolloClient({mocks, defaultOptions, link, resolvers, addTypename}) {
  return new ApolloClient({
    cache: generateApolloCache(),
    defaultOptions,
    link: link || new MockLink(mocks || [], addTypename),
    resolvers
  });
}
