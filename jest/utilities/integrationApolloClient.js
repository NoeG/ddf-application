import {ApolloClient, HttpLink} from 'apollo-boost';
import {CacheWithFragmentMatcher} from './apolloCache';

/**
 * This is an apollo client to use for integration tests. WIP
 */
export const apolloClient = new ApolloClient({
  link: new HttpLink({
    uri: `${process.env.APP_URL}/graphql`,
    fetch: (uri, options) => {
      options.credentials = "omit";
      options.referrer = process.env.APP_URL;
      return global.originalFetch(uri, options);
    }
  }),
  cache: CacheWithFragmentMatcher
});
