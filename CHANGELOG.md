## 2.2.0-rc.1 / 2020-02-17

### Added on API.

- `LexicalSense` now have two new properties `processed` and `reviewed`:

```diff
type LexicalSense {
+  """ Is this lexical sense processed by an administrator user ?"""
+  processed: Boolean

+  """ Is this lexical sense reviewed by an operator or and administrator user ?"""
+  reviewed: Boolean
```

### Breaking change on API.

- `LexicalSense` GraphQL type can now have multivalued semantic properties.

```diff
fragment LexicalSenseSemanticProperties on LexicalSense{
-    domain{
-      id
-      prefLabel
+    domains{
+      edges{
+        node{
+          id
+          prefLabel
+        }
+      }
     }

-    temporality{
-      id
-      prefLabel
+    temporalities{
+      edges{
+        node{
+          id
+          prefLabel
+        }
+      }
     }

-    register{
-      id
-      prefLabel
+    registers{
+      edges{
+        node{
+          id
+          prefLabel
+        }
+      }
     }

-    connotation{
-      id
-      prefLabel
+    connotations{
+      edges{
+        node{
+          id
+          prefLabel
+        }
+      }
     }

-    frequency{
-      id
-      prefLabel
+    frequencies{
+      edges{
+        node{
+          id
+          prefLabel
+        }
+      }
     }

-    textualGenre{
-      id
-      prefLabel
+    textualGenres{
+      edges{
+        node{
+          id
+          prefLabel
+        }
+      }
     }

-    sociolect{
-      id
-      prefLabel
+    sociolects{
+      edges{
+        node{
+          id
+          prefLabel
+        }
+      }
     }

-    grammaticalConstraint{
-      id
-      prefLabel
+    grammaticalConstraints{
+      edges{
+        node{
+          id
+          prefLabel
+        }
+      }
     }
```