FROM node:10.15.3-stretch

ARG CI

RUN mkdir -p /opt/app
WORKDIR /opt/app
ADD . /opt/app
ENV NODE_ENV production
RUN yarn install --prod
RUN yarn build:prod
CMD yarn run start:prod
