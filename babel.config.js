module.exports =  function(api) {
  let isDev = process.env.NODE_ENV !== 'production';

  api.cache(false);

  const presets = [
    "@babel/env", 
    "@babel/react"
  ];

  const plugins = [
    ["@babel/proposal-decorators", { "legacy": true }],
    ["@babel/proposal-class-properties", { "loose" : true }],
    ["@babel/proposal-object-rest-spread"],
    ["@babel/proposal-optional-chaining"],
  ];

  if(isDev) {
    plugins.push(["@babel/plugin-transform-react-jsx-source"]);
  }
  if(!isDev) {
    plugins.push(["babel-plugin-strip-invariant"]);
  }


  return {
    presets,
    plugins
  }
};
